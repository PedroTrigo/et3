<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showcurrent de acción, por lo tanto
 * proporciona la representación visual al formulario de showcurrent de la tabla acción.
 */
class Accion_SHOWCURRENT
{
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera=new head();
        $cabecera->cargar($idi,"detalleAccion",$comprobarUsuarioGrupo);

        ?>


        <!--TABLA SHOWCURRENT-->
        <div class="row">
            <div class="col-md-7">
                <h3>
                    <?=$idi["SHOWCURRENT ACTION"]?>
                </h3>
                <?php
                if($datos!=null){
                foreach($datos as $fila)
                {  ?>

                <table class="table">
                    <tbody>
                    <tr class="active">
                        <td>
                            <?=$idi["IdAccion"]?>
                        </td>
                        <td>
                            <?= $fila['IdAccion']; ?>
                        </td>
                    </tr>
                    <tr class="success">
                        <td>
                            <?=$idi["NombreAccion"]?>
                        </td>
                        <td>
                            <?= $fila['NombreAccion']; ?>
                        </td>
                    </tr>
                    <tr class="warning">
                        <td>
                            <?=$idi["DescripAccion"]?>
                        </td>
                        <td>
                            <?= $fila['DescripAccion']; ?>
                        </td>
                    </tr>

                    <?php
                    }
                    }
                    ?>
                    </tbody>
                </table>
                <a class="btn btn-danger" href="../Controllers/ActionController.php?action=volverAcciones">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>
            </div>
        </div>





        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer=new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>