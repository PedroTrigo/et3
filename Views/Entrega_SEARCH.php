<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista search de entrega, por lo tanto
 * proporciona la representación visual al formulario de search de la tabla entrega.
 */

class Entrega_SEARCH {
    function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"searchEntrega",$comprobarUsuarioGrupo);
		
?>


<!--SEARCH-->
<div id="maincontent" class="col-sm-9">
<div class="row">
	
		<p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrearEntrega"];?> </p> <!--TRADUCIR-->
	
	<h3>
		<?=$idi["searchEntrega"]?>
	</h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormSearch" name="FormSearch" action="../Controllers/EntregaController.php?action=search" method="POST">

		<div class="form-group">
			 
			<label for="IdTrabajo" class="col-sm-2 control-label">
				<?=$idi["NombreTrabajo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdTrabajo' id='IdSearch' value = '' size='20'>
				<p id="IdTextoAdd"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="login" class="col-sm-2 control-label">
				<?=$idi["login"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="login" id="loginSearch"  class="form-control" size='20'>
				<p id="loginTextoSearch"></p>						
			</div>
		</div>
                <div class="form-group">
			 
			<label for="Alias" class="col-sm-2 control-label">
				<?=$idi["Alias"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="Alias" id="AliasSearch"  class="form-control" size='20'>
				<p id="AliasTextoSearch"></p>						
			</div>
		</div>
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormSearch" id="btn-Search" href="#" aria-label="Search">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/EntregaController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>