<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista search de nota, por lo tanto
 * proporciona la representación visual al formulario de search de la tabla nota.
 */

class Nota_SEARCH{

		function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"searchUser",$comprobarUsuarioGrupo);
		
?>


<!--SEARCH-->
<div id="maincontent" class="col-sm-9">
	
		<p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrear"];?> </p>	
	
	<h3>
		<?=$idi["searchUser"]?>
	</h3>

	<form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormSearchNota" name="FormSearchNota" action="../Controllers/ActionController.php?action=SearchNota" method="POST">

<div class="form-group">
			 
			<label for="log" class="col-sm-2 control-label">
				<?=$idi["log"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='logSearch' id='logSearch' size='9'">
				<p id="loginSearch"></p>					
			</div>
		</div>		

		<div class="form-group">
			 
			<label for="idT" class="col-sm-2 control-label">
				<?=$idi["idT"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='idTSearch' id='idTSearch' size='9'">
				<p id="idTSearch"></p>					
			</div>
		</div>

	   <div class="form-group">
			 
			<label for="NotaT" class="col-sm-2 control-label">
				<?=$idi["NotaT"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='NotaTSearch' id='NotaTSearch' size='9'">
				<p id="NotaTSearch"></p>					
			</div>
		</div>

		
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormSearchNota" id="btn-Search" href="#" aria-label="Search">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
					<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarNotas">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>