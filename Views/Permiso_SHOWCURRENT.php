<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showcurrent de permiso, por lo tanto
 * proporciona la representación visual al formulario de showcurrent de la tabla permiso.
 */

class Permiso_SHOWCURRENT {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"detallePermiso",$comprobarUsuarioGrupo);
		
?>


<!--TABLA SHOWCURRENT-->
	<div class="row">
		<div class="col-md-7">
			<h3>
				<?=$idi["detallePermiso"]?>
			</h3>		
	<?php  
	if($datos!=null){ 
		  ?>

			<table class="table">				
				<tbody>
					<tr class="active">
						<td>
							<?=$idi["IdGrupo"]?>
						</td>
						<td>
							<?= $datos[0]; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["IdFuncionalidad"]?>
						</td>
						<td>
							<?= $datos[1]; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["IdAccion"]?>							
						</td>
						<td>
							<?= $datos[2]; ?>
						</td>						
					</tr>
                                                     
					<?php 
                
					}
						?>
				</tbody>
			</table>
                    <a class="btn btn-danger" href="../Controllers/PermisoController.php?action=showAll">					
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
					</a>
		</div>
	</div>





<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>