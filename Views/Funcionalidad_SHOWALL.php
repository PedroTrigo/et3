<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showall de funcionalidad, por lo tanto
 * proporciona la representación visual a la tabla showall de funcionalidad.
 */

class Funcionalidad_SHOWALL {
    function cargar($datos,$texto,$idi,$acciones,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"SHOWALLFun",$comprobarUsuarioGrupo);
		
?>
<!--TABLA SHOWALL-->


	<div class="row">
	<!--Mesajes de feedback-->
	<div class="col-md-3">
		<center>		
		<p class="bg-success"><?php if($texto=="exito")echo"Funcionalidad creada";elseif($texto=="exitoborrar")echo"Funcionalidad borrada";elseif($texto=="exitoModificar")echo $idi["exitoModificarFun"];elseif($texto=="exitoAsignar")echo $idi["exitoAsignarAccion"];elseif($texto=="exitoDesasignar")echo $idi["exitoDesasignarAccion"];?></p>
		<p class="bg-danger"><?php if($texto=="errorborrar")echo"Error al borrar";elseif($texto=="errorcrear")echo"Error al crear";elseif($texto=="errormodificar")echo"Error al modificar";?></p>
		<p class="bg-primary"> <?php  if($datos==null) echo $idi['noFun'];?> </p>
		</center>
	</div>

	<!--Contenido Tabla-->
	<div class="col-md-7">
		<h3>
			<?=$idi["SHOWALLFun"]?>
		</h3>

		<!--Botones añadir y buscar-->
		<div class="row">
			<div class="col-md-7">			
                            <a class="btn btn-success" href="../Controllers/FuncionalidadController.php?action=cargarAlta" aria-label="Add">
				<i class="fa fa-plus" aria-hidden="true"></i>
			</a>

			<a class="btn btn-default" href="../Controllers/FuncionalidadController.php?action=cargarSearch" aria-label="Search">
				<i class="fa fa-search" aria-hidden="true"></i>
			</a>
			</div>
		</div>

		<table class="table">
			<thead>
				<tr>
					<th>
						<?=$idi["IdFuncionalidad"]?>
					</th>
					<th>
						<?=$idi["NombreFuncionalidad"]?>
					</th>
					<th>
						<?=$idi["Acción"]?>
					</th>
				</tr>
			</thead>
			<tbody>
	<?php  
	if($datos!=null){
            $tupla=$datos->fetch_row();
		do
		{  ?>
			
		<tr>
			<td>
				<?= $tupla[0]; ?>
			</td>
			<td>
				<?= $tupla[1]; ?>
			</td>
			<td>
				<!--usuario_EDIT.php-->
                                <a href=../Controllers/FuncionalidadController.php?action=cargarModificar&IdFuncionalidad=<?= $tupla[0];?> class="btn btn-warning"  aria-label="Edit">
				<i class="fa fa-pencil" aria-hidden="true"></i>
				</a>
				<!--usuario_DELETE.php-->
				<a href=../Controllers/FuncionalidadController.php?action=cargarBaja&IdFuncionalidad=<?= $tupla[0];?>
				class="btn btn-danger" aria-label="Delete">
					<i class="fa fa-trash-o" aria-hidden="true"></i>
				</a>
				<!--usuario_SHOWCURRENT.php-->
				<a href=../Controllers/FuncionalidadController.php?action=cargarShowCurrent&IdFuncionalidad=<?= $tupla[0];?> class="btn btn-default" aria-label="Search">
				<i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                
                                <!--Asignar accion-->
                                <a class="btn btn-success" href="../Controllers/FuncionalidadController.php?action=cargarAsignarAccion&IdFuncionalidad=<?= $tupla[0];?>" aria-label="Add">
				<i class="fa fa-plus" aria-hidden="true"></i>
			</a>
			</td>
		</tr>
	<?php 
		$tupla=$datos->fetch_row();
                }
                while(!is_null($tupla));
	}
	?>
				</tbody>
			</table>
                
                <br><br>
                
                <table class="table">
			<thead>
				<tr>
					<th>
						<?=$idi["IdFuncionalidad"]?>
					</th>
					<th>
						<?=$idi["IdAccion"]?>
					</th>
					<th>
						<?=$idi["Acción"]?>
					</th>
				</tr>
			</thead>
			<tbody>
	<?php  
	if($acciones!=null){
            $tupla=$acciones->fetch_row();
		do
		{  ?>
			
		<tr>
			<td>
				<?= $tupla[0]; ?>
			</td>
			<td>
				<?= $tupla[1]; ?>
			</td>
			<td>
				<!--usuario_DELETE.php-->
				<a href=../Controllers/FuncionalidadController.php?action=cargarDesasignarAccion&IdFuncionalidad=<?= $tupla[0];?>&IdAccion=<?= $tupla[1];?>
				class="btn btn-danger" aria-label="Delete">
					<i class="fa fa-trash-o" aria-hidden="true"></i>
				</a>
			</td>
		</tr>
	<?php 
		$tupla=$acciones->fetch_row();
                }
                while(!is_null($tupla));
	}
	?>
				</tbody>
			</table>
		</div>
	</div>



<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>