<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showall de acción, por lo tanto
 * proporciona la representación visual a la tabla showall de acción.
 */
class Accion_SHOWALL
{
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera=new head();
        $cabecera->cargar($idi,"listarAcciones",$comprobarUsuarioGrupo);

        ?>
        <!--TABLA SHOWALL-->


        <div class="row">
            <!--Mesajes de feedback-->
            <div class="col-md-3">
                <center>
                    <p class="bg-success"><?php if($texto=="exitoCrearAccion")echo $idi["exitoCrearAccion"];elseif($texto=="exitoborrarAccion")echo $idi["exitoEliminarAccion"];elseif($texto=="exitoModificarAccion")echo $idi["exitoModificarAccion"];?></p>
                    <p class="bg-danger"><?php if($texto=="errorborrarAccion")echo $idi["errorborrarAccion"];elseif($texto=="errorcrearAccion")echo $idi["errorcrearAccion"];elseif($texto=="errormodificarAccion")echo$idi["errormodificarAccion"];?></p>
                    <p class="bg-primary"> <?php  if($datos==null) echo $idi['noactions'];?> </p>
                </center>
            </div>

            <!--Contenido Tabla-->
            <div class="col-md-7">
                <h3>
                    <?=$idi["SHOWALL ACTIONS"]?>
                </h3>

                <!--Botones añadir y buscar-->
                <div class="row">
                    <div class="col-md-7">
                        <a class="btn btn-success" href="../Controllers/ActionController.php?action=CrearAccionSHOWALL" aria-label="Add">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>

                        <a class="btn btn-default" href="../Controllers/ActionController.php?action=SearchAccionSHOWALL" aria-label="Search">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                <table class="table">
                    <thead>
                    <tr>
                        <th>
                            <?=$idi["IdAccion"]?>
                        </th>
                        <th>
                            <?=$idi["NombreAccion"]?>
                        </th>
                        <th>
                            <?=$idi["DescripAccion"]?>
                        </th>
                        <th>
                            <?=$idi["Acción"]?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($datos!=null){
                        foreach($datos as $fila)
                        {  ?>

                            <tr>
                                <td>
                                    <?= $fila['IdAccion']; ?>
                                </td>
                                <td>
                                    <?= $fila['NombreAccion']; ?>
                                </td>
                                <td>
                                    <?= $fila['DescripAccion']; ?>
                                </td>
                                <td>
                                    <!--usuario_EDIT.php-->
                                    <a href=../Controllers/ActionController.php?modificarSHOWALLAccion=<?= $fila['IdAccion'];?> class="btn btn-warning"  aria-label="Edit">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                    <!--usuario_DELETE.php-->
                                    <a href=../Controllers/ActionController.php?eliminarSHOWALLAccion=<?= $fila['IdAccion'];?>
                                       class="btn btn-danger" aria-label="Delete">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </a>
                                    <!--usuario_SHOWCURRENT.php-->
                                    <a href=../Controllers/ActionController.php?detalleSHOWALLAccion=<?= $fila['IdAccion'];?> class="btn btn-default" aria-label="Search">
                                        <i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>



        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer=new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>