<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista delete de historia, por lo tanto
 * proporciona la representación visual al formulario de delete de la tabla historia.
 */

class Historia_DELETE{

		function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"eliminarHistoria",$comprobarUsuarioGrupo);		
?>

	<!--TABLA ELIMINAR-->
	<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>
	<div class="container-well">
		<div class="row">
			<div class="col-md-7">
				
				<h3>
					<?=$idi["eliminarHistoria"]?>
				</h3>	

			<form id=FormDelete>
			<input type="hidden" name="IdTrabajo" value="<?= $fila['IdTrabajo'];?>" />
			<input type="hidden" name="IdHistoria" value="<?= $fila['IdHistoria'];?>" />
			</form>	

				<table class="table">				
					<tbody>
						<tr class="active">
							<td>
								<?=$idi["IdTrabajo"]?>
							</td>
							<td>
							<?=$idi["IdHistoria"]?>
							</td>	
							<td>
							<?=$idi["TextoHistoria"]?>
							</td>								
						</tr>
						<tr class="success">
							<td>
								<?= $fila['IdTrabajo']; ?>
							</td>
							<td>
								<?= $fila['IdHistoria']; ?>
							</td>			
							<td>
								<?= $fila['TextoHistoria']; ?>
							</td>								
						</tr>
					
					</tbody>
				</table>
				<p><?=$idi["Confirmar"]?></p>
				<!-- <a class="btn btn-success" href="../Controllers/ActionController.php?action=BajaHistoria" aria-label="Add">	 -->
				<!--	<a class="btn btn-success" href=../Controllers/ActionController.php?baja=<?= $fila['IdTrabajo'];?>>	 -->	
				<a class="btn btn-success" href=../Controllers/ActionController.php?bajaH=<?= $fila['IdHistoria'];?>> 
					
					<i class="fa fa-check" aria-hidden="true"></i>
				</a>
				<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListaHistorias">					
					<i class="fa fa-times" aria-hidden="true"></i>
				</a>
			</div>
		</div>			
	</div>
<?php 
	 }
}
?>
	


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>