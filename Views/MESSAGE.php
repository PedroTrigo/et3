<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista de Mmessage, porlo tanto muestra
 los mensajes de control tras realizar operaciones.
 */

class MESSAGE{

		function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"MESSAGE",$comprobarUsuarioGrupo);
		
?>


<div class="container">
<div class="row">
<div class= "col-md-4">
<center>


<p class="bg-danger"> <?php if($texto=="errorDELETE") echo $idi["ErrorBorrar"]; ?> </p>

<p class="bg-danger"> <?php if($texto=="DatosDuplicados") echo $idi["DatosDuplicados"]; ?> </p>

<p class="bg-danger"> <?php if($texto=="errorModificar") echo $idi["DatosDuplicados"];?> </p>

<p class="bg-danger"> <?php if($texto=="errorFormato") echo $idi["errorFormato"]; ?> </p>

<p class="bg-danger"> <?php if($texto=="DatosDuplicados Trabajo") echo $idi["Datos Duplicados Trabajo"]; ?> </p>

<p class="bg-danger"> <?php if($texto=="DatosDuplicados Acción") echo $idi["Datos Duplicados Acción"]; ?> </p>

<p class="bg-danger"> <?php if($texto=="errorAsignar") echo $idi["errorAsignar"]; ?> </p>




</center>

<a class="btn btn-danger" href="../Controllers/ActionController.php?action=volver">					
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
</a>

</div>
</div>
</div> 







<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>