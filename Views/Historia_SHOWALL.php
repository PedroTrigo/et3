<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showall de historia, por lo tanto
 * proporciona la representación visual a la tabla showall de historia.
 */

class Historia_SHOWALL{

		function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"listarHistorias",$comprobarUsuarioGrupo);
		
?>
<!--TABLA SHOWALL-->


	
	<!--Mesajes de feedback-->
	<div class="col-md-3">
		<center>		
		<p class="bg-success"><?php if($texto=="exito")echo"Historia creada";elseif($texto=="exitoborrar")echo"Historia borrada";elseif($texto=="exitoModificar")echo $idi["exitoModificar"];?></p>
		<p class="bg-danger"><?php if($texto=="errorborrar")echo"Error al borrar";elseif($texto=="errorcrear")echo"Error al borrar";elseif($texto=="errormodificar")echo"Error al modificar";?></p>
		<p class="bg-primary"> <?php  if($datos==null) echo $idi['nohistorias'];?> </p>
		</center>
	</div>

	<!--Contenido Tabla-->
	<div class="col-sm-9">
		<h3>
			<?=$idi["listarHistorias"]?>
		</h3>

		<!--Botones añadir y buscar-->
		
					
			<a class="btn btn-success" href="../Controllers/ActionController.php?action=CrearHistoriaSHOWALL" aria-label="Add">
				<i class="fa fa-plus" aria-hidden="true"></i>
			</a>

			<a class="btn btn-default" href="../Controllers/ActionController.php?action=SearchHistoriaSHOWALL" aria-label="Search">
				<i class="fa fa-search" aria-hidden="true"></i>
			</a>
			
		

		<table class="table">
			<thead>
				<tr>
					<th>
						<?=$idi["IdTrabajo"]?>
					</th>
					<th>
						<?=$idi["IdHistoria"]?>
					</th>
					<th>
						<?=$idi["TextoHistoria"]?>
					</th>						
				</tr>
			</thead>
			<tbody>
	<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>
			
		<tr>
			<td>
				<?= $fila['IdTrabajo']; ?>
			</td>
			<td>
				<?= $fila['IdHistoria']; ?>
			</td>
			<td>
				<?= $fila['TextoHistoria']; ?>
			</td>
			<td>
				<!--historia_EDIT.php-->
				<a href=../Controllers/ActionController.php?modificarSHOWALLH=<?= $fila['IdHistoria'];?> class="btn btn-warning"  aria-label="Edit">
				<i class="fa fa-pencil" aria-hidden="true"></i>
				</a>
				<!--historia_DELETE.php-->
				<a href=../Controllers/ActionController.php?eliminarSHOWALLH=<?= $fila['IdHistoria'];?>
				class="btn btn-danger" aria-label="Delete">
					<i class="fa fa-trash-o" aria-hidden="true"></i>
				</a>
				<!--historia_SHOWCURRENT.php-->
				<a href=../Controllers/ActionController.php?detalleSHOWALLH=<?= $fila['IdHistoria'];?> class="btn btn-default" aria-label="Search">
				<i class="fa fa-search-plus" aria-hidden="true"></i></a>
			</td>
		</tr>
	<?php 
		 }
	}
	?>
				</tbody>
			</table>
		</div>
	</div> <!--Cierra el row del contenido principal en head.php-->



<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>