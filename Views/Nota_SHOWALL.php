<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showall de nota, por lo tanto
 * proporciona la representación visual a la tabla showall de nota.
 */

class Nota_SHOWALL{

		function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"listarNotas",$comprobarUsuarioGrupo);
		
?>
<!--TABLA SHOWALL-->


	<div class="row">
	<!--Mesajes de feedback-->
	<div class="col-md-3">
		<center>		
		<p class="bg-success"><?php if($texto=="exito")echo"Nota creada";elseif($texto=="exitoborrar")echo"Nota borrada";elseif($texto=="exitoModificar")echo $idi["exitoModificar"];?></p>
		<p class="bg-danger"><?php if($texto=="errorborrar")echo"Error al borrar";elseif($texto=="errorcrear")echo"Error al borrar";elseif($texto=="errormodificar")echo"Error al modificar";?></p>
		<p class="bg-primary"> <?php  if($datos==null) echo $idi['nonotas'];?> </p>
		</center>
	</div>

	<!--Contenido Tabla-->
	<div class="col-md-7">
		<h3>
			<?=$idi["listarNotas"]?>
		</h3>

		<!--Botones añadir y buscar-->
		<div class="row">
			<div class="col-md-7">			
			<a class="btn btn-success" href="../Controllers/ActionController.php?action=CrearNotaSHOWALL" aria-label="Add">
				<i class="fa fa-plus" aria-hidden="true"></i>
			</a>

			<a class="btn btn-default" href="../Controllers/ActionController.php?action=SearchNotaSHOWALL" aria-label="Search">
				<i class="fa fa-search" aria-hidden="true"></i>
			</a>
			</div>
		</div>

		<table class="table">
			<thead>
				<tr>
					<th>
						<?=$idi["login"]?>
					</th>
					<th>
						<?=$idi["IdTrabajo"]?>
					</th>
					<th>
						<?=$idi["NotaTrabajo"]?>
					</th>						
				</tr>
			</thead>
			<tbody>
	<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>
			
		<tr>
			<td>
				<?= $fila['login']; ?>
			</td>
			<td>
				<?= $fila['IdTrabajo']; ?>
			</td>
			<td>
				<?= $fila['NotaTrabajo']; ?>
			</td>
			<td>
				<!--nota_EDIT.php-->
				<a href=../Controllers/ActionController.php?modificarSHOWALLN=<?= $fila['IdTrabajo'];?> class="btn btn-warning"  aria-label="Edit">
				<i class="fa fa-pencil" aria-hidden="true"></i>
				</a>
				<!--nota_DELETE.php-->
				<a href=../Controllers/ActionController.php?eliminarSHOWALLN=<?= $fila['IdTrabajo'];?>
				class="btn btn-danger" aria-label="Delete">
					<i class="fa fa-trash-o" aria-hidden="true"></i>
				</a>
				<!--nota_SHOWCURRENT.php-->
				<a href=../Controllers/ActionController.php?detalleSHOWALLN=<?= $fila['IdTrabajo'];?> class="btn btn-default" aria-label="Search">
				<i class="fa fa-search-plus" aria-hidden="true"></i></a>
			</td>
		</tr>
	<?php 
		 }
	}
	?>
				</tbody>
			</table>
		</div>
	</div>



<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>