<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista de registrarse, por lo tanto
 muestra el formulario de registro para un usuario.
 */

 class registrar{ 
    function cargar($texto,$idi){ 
?>
<!DOCTYPE html>
<html>  
<head lang="en">  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <link type="text/css" rel="stylesheet" href="../Locales/bootstrap/bootstrap.min.css">  
    <!--Tigra calendar 
    Cambiado el formato fecha por defecto a d/m/Y-->
    <link rel="stylesheet" type="text/css" href="../Locales/simple-calendar/tcal.css" />
    <script type="text/javascript" src="../Locales/simple-calendar/tcal.js"></script> 
    <!--Validaciones-->
    <?php include '../Locales/js/validaciones.js'; ?>
    <!--MD5-->
    <script type="text/javascript" src="../Locales/js/md5-min.js"></script>
    <!--css-->
    <link rel="stylesheet" type="text/css" href="../Locales/css/styles.css">        
    <title><?=$idi['registro']?></title>  
</head>  
<style>  
    .login-panel {  
        margin-top: 150px;  
  
</style>  
<body>  

 
<div class="container">
    <div class="row">
<div class= "col-md-4">
<center>
<p class="bg-danger"> <?php if($texto=="errorRegistrar") echo $idi["errorRegistrar"]; ?> </p>
<p class="bg-danger"> <?php if($texto=="DatosDuplicados") echo $idi["DatosDuplicados"]; ?> </p>
</center>
</div>
</div> 
    <div class="row">
        <div class="col-md-9"> 
            <div class="login-panel panel panel-success">  
                <div class="panel-heading">  
                    <h3 class="panel-title"><?=$idi['registro']?></h3>  
                </div>

    <div class="panel-body">  
        <form class="form-horizontal" role="form" method="post" name="FormAdd" id="FormAdd" enctype="multipart/form-data" onsubmit="return encriptar(this);" action="ActionController.php?action=registrarse">  
            <fieldset>
         
        <div class="form-group row">
             
            <label for="login" class="col-md-3 control-label">
                <?=$idi["loginCampo"]?>
            </label>
            <div class="col-md-5" >
                <input type="text" class="form-control" name='loginAdd' id='loginAdd' value = '' size='9' onblur="comprobarLoginAdd(this,size);">
                <p id="loginTextoAdd"></p>                  
            </div>
        </div>

        <div class="form-group row">
             
            <label for="Password" class="col-md-3 control-label">
                <?=$idi["contraseña"]?>
            </label>
            <div class="col-md-5">
                <input type="password" name="passAdd" id="passAdd"  class="form-control" size='20' onblur="comprobarPassAdd(this,size);">
                <p id="passTextoAdd"></p>                       
            </div>
        </div>
        
        <div class="form-group row">
             
            <label for="dni" class="col-md-3 control-label">
                <?=$idi["DNI"]?>
            </label>
            <div class="col-md-5" >
                <input type="text" class="form-control" name='dniAdd' id='dniAdd' size='9' onblur="comprobarDniAdd(this);">
                <p id="dniTextoAdd"></p>                    
            </div>
        </div>

       <div class="form-group row">

            <label for="nombreUserAddLabel" class="col-md-3 control-label">
                <?=$idi["Nombre"]?> 
            </label>
            <div class="col-md-5">
                <input type="text" class="form-control" name="nombreUserAdd" id="nombreUserAdd" size='30' onblur="comprobarNombreUserAdd(this,size);">
                <p id="nombreUserTextoAdd"></p>
            </div>
        </div>

        <div class="form-group row">

            <label for="apellidosUserAddLabel" class="col-md-3 control-label">
                <?=$idi["Apellidos"]?>
            </label>
            <div class="col-md-5">
                <input type="text" class="form-control" name="apellidosUserAdd" id="apellidosUserAdd" size='50' onblur="comprobarApellUserAdd(this,size);">
                <p id="apellidosUserTextoAdd"></p>
            </div>
        </div>

         <div class="form-group row">

            <label for="emailUserAddLabel" class="col-md-3 control-label">
                <?=$idi["email"]?>
            </label>
            <div class="col-md-5">
                <input type="text" class="form-control" name="emailUserAdd" id="emailUserAdd" size='40' onblur="comprobarEmailAdd(this,size);">
                <p id="emailUserTextoAdd"></p>
            </div>
        </div>

         <div class="form-group row">

            <label for="direccionUserAddLabel" class="col-md-3 control-label">
                <?=$idi["direccion"]?>
            </label>
            <div class="col-md-5">
                <input type="text" class="form-control" name="direccionUserAdd" id="direccionUserAdd" size='60' onblur="comprobarDireccionAdd(this,size);">
                <p id="direccionUserTextoAdd"></p>
            </div>
        </div>

        <div class="form-group row">

            <label for="telefonoAddLabel" class="col-md-3 control-label">
            <?=$idi["Telefono"]?> 
            </label>
            <div class="col-md-5">
                <input type="text" class="form-control" name="telefonoUserAdd" id="telefonoUserAdd" size='11' onblur="comprobarTelefonoAdd(this);">
                <p id="telefonoUserTextoAdd"></p>               
            </div>
        </div>

       

    
        
     <input class="btn btn-lg btn-success btn-block" type="submit" value="<?= $idi["registrarse"]; ?>" name="registrarse" onclick="return validarADD()" >  
  
        </fieldset>  
        </form>
</div>
        <center><b><?=$idi["yaRegistrado"]?>;</b> <br></b><a href="ActionController.php?action=acceder">Login</a></center><!--for centered text--> 
   </div> </div> </div> </div> </div> 
  
</body>  
  
</html>  
  <?php 
  }}
?>