<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista add de evaluación, por lo tanto
 * proporciona la representación visual al formulario de add de la tabla evaluación.
 */

class Evaluacion_ADD {
    function cargar($texto,$idi,$trabajos,$historias,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"crearEvaluacion",$comprobarUsuarioGrupo);
		
?>

<!--ADD-->
<div id="maincontent" class="col-sm-9">
<div class="row">
	
		<p class= "text-danger"><?php if($texto=="EvaluacionRepe")echo $idi["EvaluacionRepe"];?> </p>
	
	<h3>
		<?=$idi["crearEvaluacion"]?>
	</h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormAdd" name="FormAdd" action="../Controllers/EvaluacionController.php?action=alta" method="POST">

                    <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
         
		<div class="form-group">
			 
			<label for="IdTrabajo" class="col-sm-2 control-label">
				<?=$idi["NombreTrabajo"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='IdTrabajo' id='IdAdd'>
                                <?php
                                $tupla=$trabajos->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[1]?></option>
                                <?php 
                                    $tupla=$trabajos->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>
	
	<div class="form-group">
			 
			<label for="LoginEvaluador" class="col-sm-2 control-label">
				<?=$idi["LoginEvaluador"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='LoginEvaluador' id='LoginEvaluadorAdd' size='20'>
				<p id="LoginEvaluadorTextoAdd"></p>					
			</div>
		</div>

		<div class="form-group">
			 
			<label for="AliasEvaluado" class="col-sm-2 control-label">
				<?=$idi["AliasEvaluado"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='AliasEvaluado' id='AliasEvaluadoAdd' size='20'>
				<p id="AliasEvaluadoTextoAdd"></p>					
			</div>
		</div>
		
		<div class="form-group">
			 
			<label for="IdHistoria" class="col-sm-2 control-label">
				<?=$idi["IdHistoria"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='IdHistoria' id='IdHistoriaAdd'>
                                <?php
                                $tupla=$historias->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[0]?></option>
                                <?php 
                                    $tupla=$historias->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>

                	<div class="form-group">
			 
			<label for="CorrectoA" class="col-sm-2 control-label">
				<?=$idi["CorrectoA"]?>
			</label>
			<div class="col-sm-3">
			<input type="checkbox" name="CorrectoA" id="CorrectoAEdit" checked> <span class="label-text"></span>
				<p id="CorrectoATextoEdit"></p>						
			</div>
		</div>

<div class="form-group">
			 
			<label for="ComenIncorrectoA" class="col-sm-2 control-label">
				<?=$idi["ComenIncorrectoA"]?>
			</label>
			<div class="col-sm-3">
                            <input type="text" name="ComenIncorrectoAEvaluado" id="ComenIncorrectoAEdit" autocomplete="off" class="form-control" value="" size='10'>
				<p id="ComenIncorrectoATextoEdit"></p>						
			</div>
		</div>

<div class="form-group">
			 
			<label for="CorrectoP" class="col-sm-2 control-label">
				<?=$idi["CorrectoP"]?>
			</label>
			<div class="col-sm-3">
			<input type="checkbox" name="CorrectoP" id="CorrectoPEdit" checked> <span class="label-text"></span>
				<p id="CorrectoPTextoEdit"></p>						
			</div>
		</div>

<div class="form-group">
			 
			<label for="ComenIncorrectoP" class="col-sm-2 control-label">
				<?=$idi["ComenIncorrectoP"]?>
			</label>
			<div class="col-sm-3">
                            <input type="text" name="ComenIncorrectoP" id="ComenIncorrectoPEdit" autocomplete="off" class="form-control" value="" size='10'>
				<p id="ComenIncorrectoPTextoEdit"></p>						
			</div>
		</div>

<div class="form-group">
			 
			<label for="OK" class="col-sm-2 control-label">
				<?=$idi["OK"]?>
			</label>
			<div class="col-sm-3">
				<input type="checkbox" name="OK" id="OKEdit" checked> <span class="label-text"></span>
				<p id="OKEdit"></p>						
			</div>
		</div>		

	   
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormAdd" id="btn-add" href="#" aria-label="Add">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/EvaluacionController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>