<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista edit de entrega, por lo tanto
 * proporciona la representación visual al formulario de edit de la tabla entrega.
 */

class Entrega_EDIT {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
	//Carga de cabecera
	include("../Locales/Templates/head.php");
	$cabecera=new head();
	$cabecera->cargar($idi,"modificarEntrega",$comprobarUsuarioGrupo);
		
?>




<!--EDIT-->
<div id="maincontent" class="col-sm-9">
<div class="row">

	<h3>
		<?=$idi["modificarEntrega"]?>
	</h3>
	
    <form class="form-horizontal" enctype="multipart/form-data" role="form" name="FormEdit" id ="FormEdit" action="../Controllers/EntregaController.php?action=modificar" method="POST">
		
		<div class="form-group">
			 
			<label for="login" class="col-sm-2 control-label">
				<?=$idi["login"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='login' id='loginEdit' value="<?= $datos[0];?>" size='6' readonly>
				<p id="loginTextoEdit"></p>					
			</div>
		</div>
                <div class="form-group">
			 
			<label for="IdTrabajo" class="col-sm-2 control-label">
				<?=$idi["NombreTrabajo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdTrabajo' id='IdEdit' value="<?= $datos[1];?>" size='6' readonly>
				<p id="IdTextoEdit"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="Alias" class="col-sm-2 control-label">
				<?=$idi["Alias"]?>
			</label>
			<div class="col-sm-3">
                            <input type="text" name="Alias" id="AliasEdit" autocomplete="off" class="form-control" value="" size='10'>
				<p id="AliasTextoEdit"></p>						
			</div>
		</div>
                <div class="form-group">
			 
			<label for="Horas" class="col-sm-2 control-label">
				<?=$idi["Horas"]?>
			</label>
			<div class="col-sm-3">
                            <input type="number" name="Horas" id="HorasEdit" autocomplete="off" class="form-control" value="" size='10'>
				<p id="HorasTextoEdit"></p>						
			</div>
		</div>
                
		
		<!--BOTONES FORMULARIO-->
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
				<!--Boton enviar-->			
					<button class="btn btn-success" form="FormEdit" id="btn-Edit" href="#" aria-label="Edit">
					<i class="fa fa-check" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/EntregaController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
					<!-- <p><?php if($texto=="errormodificar")echo $idi["errorModificar"];?> </p> -->						
				</div>			
			</div>
		</div>
	
	</form>
</div>


</div>




<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>
