<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista delete de usuario, por lo tanto
 * proporciona la representación visual al formulario de delete de la tabla usuario.
 */

class Usuario_DELETE{

		function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"eliminarUser",$comprobarUsuarioGrupo);		
?>

	<!--TABLA ELIMINAR-->
	<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>
	<div class="container-well">
		<div class="row">
			<div class="col-md-7">
				
				<h3>
					<?=$idi["eliminarUser"]?>
				</h3>	

			<form id=FormDelete>
			<input type="hidden" name="login" value="<?= $fila['login'];?>" />
			</form>	

				<table class="table">				
					<tbody>
						<tr class="active">
							<td>
								<?=$idi["Nombre"]?>
							</td>
							<td>
								<?= $fila['Nombre']; ?>
							</td>						
						</tr>
						<tr class="success">
							<td>
								<?=$idi["Apellidos"]?>
							</td>
							<td>
								<?= $fila['Apellidos']; ?>
							</td>						
						</tr>
						<tr class="danger">
							<td>
								<?=$idi["DNI"]?>
							</td>
							<td>
								<?= $fila['DNI']; ?>
							</td>						
						</tr>			
						
								
					</tbody>
				</table>
				<p><?=$idi["Confirmar"]?></p>
				<!-- <a class="btn btn-success" href="../Controllers/ActionController.php?action=BajaUser" aria-label="Add">	 -->
					<a class="btn btn-success" href=../Controllers/ActionController.php?baja=<?= $fila['login'];?>>		

					<i class="fa fa-check" aria-hidden="true"></i>
				</a>
				<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarUsers">					
					<i class="fa fa-times" aria-hidden="true"></i>
				</a>
			</div>
		</div>			
	</div>
<?php 
	 }
}
?>
	


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>