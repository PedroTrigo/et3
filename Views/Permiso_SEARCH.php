<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista search de permiso, por lo tanto
 * proporciona la representación visual al formulario de search de la tabla permiso.
 */

class Permiso_SEARCH {
    function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"searchPermiso",$comprobarUsuarioGrupo);
		
?>


<!--SEARCH-->
<div id="maincontent" class="col-sm-9">
<div class="row">
	
		<p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrearPermiso"];?> </p> <!--TRADUCIR-->
	
	<h3>
		<?=$idi["searchPermiso"]?>
	</h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormSearch" name="FormSearch" action="../Controllers/PermisoController.php?action=search" method="POST">

		<div class="form-group">
			 
			<label for="IdGrupo" class="col-sm-2 control-label">
				<?=$idi["IdGrupo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdGrupo' id='IdGrupoSearch' value = '' size='20'>
				<p id="IdGrupoTextoAdd"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="IdFuncionalidad" class="col-sm-2 control-label">
				<?=$idi["IdFuncionalidad"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="IdFuncionalidad" id="IdFuncionalidadSearch"  class="form-control" size='20'>
				<p id="IdFuncionalidadTextoSearch"></p>						
			</div>
		</div>
                <div class="form-group">
			 
			<label for="IdAccion" class="col-sm-2 control-label">
				<?=$idi["IdAccion"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="IdAccion" id="IdAccionSearch"  class="form-control" size='20'>
				<p id="IdAccionTextoSearch"></p>						
			</div>
		</div>
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormSearch" id="btn-Search" href="#" aria-label="Search">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/PermisoController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>