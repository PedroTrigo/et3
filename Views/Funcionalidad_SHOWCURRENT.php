<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showcurrent de funcionalidad, por lo tanto
 * proporciona la representación visual al formulario de showcurrent de la tabla funcionalidad.
 */

class Funcionalidad_SHOWCURRENT {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"detalleFun",$comprobarUsuarioGrupo);
		
?>


<!--TABLA SHOWCURRENT-->
	<div class="row">
		<div class="col-md-7">
			<h3>
				<?=$idi["SHOWCURRENTFun"]?>
			</h3>		
	<?php  
	if($datos!=null){ 
		  ?>

			<table class="table">				
				<tbody>
					<tr class="active">
						<td>
							<?=$idi["IdFuncionalidad"]?>
						</td>
						<td>
							<?= $datos[0]; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["NombreFuncionalidad"]?>
						</td>
						<td>
							<?= $datos[1]; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["DescripFuncionalidad"]?>							
						</td>
						<td>
							<?= $datos[2]; ?>
						</td>						
					</tr>
					<?php 
                
					}
						?>
				</tbody>
			</table>
			<a class="btn btn-danger" href="../Controllers/FuncionalidadController.php?action=showAll">					
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
					</a>
		</div>
	</div>





<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>