<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista edit de funcionalidad, por lo tanto
 * proporciona la representación visual al formulario de edit de la tabla funcionalidad.
 */

class Funcionalidad_EDIT {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
	//Carga de cabecera
	include("../Locales/Templates/head.php");
	$cabecera=new head();
	$cabecera->cargar($idi,"modificarFun",$comprobarUsuarioGrupo);
		
?>




<!--EDIT-->
<div id="maincontent" class="col-sm-9">

	<h3>
		<?=$idi["EditFun"]?>
	</h3>
	
    <form class="form-horizontal" enctype="multipart/form-data" role="form" name="FormEdit" id ="FormEdit" action="../Controllers/FuncionalidadController.php?action=modificar" method="POST">
		
		<div class="form-group">
			 
			<label for="IdFuncionalidad" class="col-sm-2 control-label">
				<?=$idi["IdFuncionalidad"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdFuncionalidad' id='IdEdit' value="<?= $datos[0];?>" size='6' readonly>
				<p id="IdTextoEdit"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="NombreFuncionalidad" class="col-sm-2 control-label">
				<?=$idi["NombreFuncionalidad"]?>
			</label>
			<div class="col-sm-3">
                            <input type="text" name="NombreFuncionalidad" id="nameEdit" autocomplete="off" class="form-control" value="" size='20'>
				<p id="nameTextoEdit"></p>						
			</div>
		</div>

		<div class="form-group">
			 
			<label for="DescripFuncionalidad" class="col-sm-2 control-label">
				<?=$idi["DescripFuncionalidad"]?>
			</label>
			<div class="col-sm-3">
                            <input type="text" name="DescripFuncionalidad" id="DescripEdit" autocomplete="off" class="form-control" value="" size='20'>
				<p id="nameTextoEdit"></p>						
			</div>
		</div>
		
		<!--BOTONES FORMULARIO-->
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
				<!--Boton enviar-->			
					<button class="btn btn-success" form="FormEdit" id="btn-Edit" href="#" aria-label="Edit">
					<i class="fa fa-check" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
					<a class="btn btn-danger" href="../Controllers/FuncionalidadController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
					<!-- <p><?php if($texto=="errormodificar")echo $idi["errorModificar"];?> </p> -->						
				</div>			
			</div>
		</div>
	
	</form>
</div>


</div>




<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>