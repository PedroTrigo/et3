<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista desasignar de acción, por lo tanto
 * proporciona la representación visual a la desasignación de acción.
 */

class DesasignarAccion {
    function cargar($datos,$texto,$idi,$funcionalidad,$accion,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"desasignarAccion",$comprobarUsuarioGrupo);
?>

	<!--TABLA ELIMINAR-->
	<?php  
	if($datos!=null){ 
		  ?>
	<div class="container-well">
		<div class="row">
			<div class="col-md-7">
				
				<h3>
					<?=$idi["desasignarAccion"]?>
				</h3>	

			<form id=FormDelete>
			<input type="hidden" name="IdFuncionalidad" value="<?= $funcionalidad[0];?>" />
                        <input type="hidden" name="IdAccion" value="<?= $accion[0];?>" />
			</form>	

				<table class="table">				
					<tbody>
						<tr class="active">
						<td>
							<?=$idi["IdFuncionalidad"]?>
						</td>
						<td>
							<?= $funcionalidad[0]; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["NombreFuncionalidad"]?>
						</td>
						<td>
							<?= $funcionalidad[1]; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["IdAccion"]?>							
						</td>
						<td>
							<?= $accion[0]; ?>
						</td>						
					</tr>
                                        <tr class="active">
						<td>
							<?=$idi["NombreAccion"]?>
						</td>
						<td>
							<?= $accion[1]; ?>
						</td>						
					</tr>
								
					</tbody>
				</table>
				<p><?=$idi["Confirmar"]?></p>
				<!-- <a class="btn btn-success" href="../Controllers/ActionController.php?action=BajaUser" aria-label="Add">	 -->
                                <a class="btn btn-success" href=../Controllers/FuncionalidadController.php?action=desasignarAccion&IdFuncionalidad=<?= $funcionalidad[0];?>&IdAccion=<?= $accion[0];?>>		

					<i class="fa fa-check" aria-hidden="true"></i>
				</a>
                                <a class="btn btn-danger" href="../Controllers/FuncionalidadController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
				</a>
			</div>
		</div>			
	</div>
<?php 
	 
}
?>
	


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>
