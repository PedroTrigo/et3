<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showcurrent de usuario, por lo tanto
 * proporciona la representación visual al formulario de showcurrent de la tabla usuario.
 */ 

class Usuario_SHOWCURRENT{

		function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"detalleUser",$comprobarUsuarioGrupo);
		
?>


<!--TABLA SHOWCURRENT-->
	
		<div class="col-sm-9">
			<h3>
				<?=$idi["SHOWCURRENT"]?>				
			</h3>		
	<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>

			<table class="table">				
				<tbody>
					<tr class="active">
						<td>
							<?=$idi["login"]?>
						</td>
						<td>
							<?= $fila['login']; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["Nombre"]?>
						</td>
						<td>
							<?= $fila['Nombre']; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["Apellidos"]?>							
						</td>
						<td>
							<?= $fila['Apellidos']; ?>
						</td>						
					</tr>
					<tr class="danger">
						<td>
							<?=$idi["DNI"]?>
						</td>
						<td>
							<?= $fila['DNI']; ?>
						</td>						
					</tr>
					<tr class="active">
						<td>
							<?=$idi["email"]?>
						</td>
						<td>
							<?= $fila['Correo']; ?>
						</td>											
					</tr>
					<tr class="success">
						<td>
							<?=$idi["direccion"]?>
						</td>
						<td>
							<?= $fila['Direccion']; ?>
						</td>						
						
					</tr>
					<tr class="danger">
						<td>
							<?=$idi["Telefono"]?>
						</td>
						<td>
							<?= $fila['Telefono']; ?>
						</td>					
					</tr>				
					
					<?php 
						 }
					}
						?>
				</tbody>
			</table>
			<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarUsers">					
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
					</a>
		</div>
	</div>





<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>