<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista edit de asignación qa, por lo tanto
 * proporciona la representación visual al formulario de edit de la tabla asignación qa.
 */

class AsignacQA_EDIT {
    function cargar($datos,$texto,$idi,$users,$comprobarUsuarioGrupo){
	//Carga de cabecera
	include("../Locales/Templates/head.php");
	$cabecera=new head();
	$cabecera->cargar($idi,"modificarAsig",$comprobarUsuarioGrupo);
		
?>




<!--EDIT-->
<div id="maincontent" class="col-sm-9">

	<h3>
		<?=$idi["modificarAsig"]?>
	</h3>
	
    <form class="form-horizontal" enctype="multipart/form-data" role="form" name="FormEdit" id ="FormEdit" action="../Controllers/AsignacQAController.php?action=modificar" method="POST">
		
		<div class="form-group">
			 
			<label for="IdTrabajo" class="col-sm-2 control-label">
				<?=$idi["IdTrabajo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdTrabajo' id='IdEdit' value="<?= $datos[0];?>" size='6' readonly>
				<p id="IdTextoEdit"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="LoginEvaluador" class="col-sm-2 control-label">
				<?=$idi["LoginEvaluador"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='LoginEvaluador' id='loginEdit' value="<?= $datos[1];?>" size='6' readonly>
				<p id="loginTextoEdit"></p>					
			</div>
		</div>
        
                <div class="form-group">
			 
			<label for="LoginEvaluado" class="col-sm-2 control-label">
				<?=$idi["LoginEvaluado"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='LoginEvaluado' id='loginEdit'>
                                <?php
                                $tupla=$users->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[0]?></option>
                                <?php 
                                    $tupla=$users->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>
        
                <div class="form-group">
			 
			<label for="AliasEvaluado" class="col-sm-2 control-label">
				<?=$idi["AliasEvaluado"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='AliasEvaluado' id='loginEdit' value="<?= $datos[3];?>" size='6' readonly>
				<p id="aliasTextoEdit"></p>					
			</div>
		</div>
		
		<!--BOTONES FORMULARIO-->
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
				<!--Boton enviar-->			
					<button class="btn btn-success" form="FormEdit" id="btn-Edit" href="#" aria-label="Edit">
					<i class="fa fa-check" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/AsignacQAController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
					<!-- <p><?php if($texto=="errormodificar")echo $idi["errorModificar"];?> </p> -->						
				</div>			
			</div>
		</div>
	
	</form>
</div>


</div>




<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>