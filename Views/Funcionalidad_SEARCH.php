<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista search de funcionalidad, por lo tanto
 * proporciona la representación visual al formulario de search de la tabla funcionalidad.
 */

class Funcionalidad_SEARCH {
    function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"searchFun",$comprobarUsuarioGrupo);
		
?>


<!--SEARCH-->
<div id="maincontent" class="col-md-10">
<div class="row">
	
		<p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrearFun"];?> </p>
	
	<h3>
		<?=$idi["searchFun"]?>
	</h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormSearch" name="FormSearch" action="../Controllers/FuncionalidadController.php?action=search" method="POST">

		<div class="form-group">
			 
			<label for="IdFuncionalidad" class="col-sm-2 control-label">
				<?=$idi["IdFuncionalidad"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdFuncionalidad' id='IdAdd' value = '' size='20'>
				<p id="IdTextoAdd"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="NombreFuncionalidad" class="col-sm-2 control-label">
				<?=$idi["NombreFuncionalidad"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="NombreFuncionalidad" id="nameAdd"  class="form-control" size='20'>
				<p id="nameTextoAdd"></p>						
			</div>
		</div>
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-1 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormSearch" id="btn-Search" href="#" aria-label="Search">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
					<a class="btn btn-danger" href="../Controllers/FuncionalidadController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>

