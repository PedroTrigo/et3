<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showall de asignación qa, por lo tanto
 * proporciona la representación visual al formulario de la tabla showall de asignación qa.
 */

class AsignacQA_SHOWALL {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"SHOWALLAsig",$comprobarUsuarioGrupo);
		
?>
<!--TABLA SHOWALL-->


	<div class="row">
	<!--Mesajes de feedback-->
	<div class="col-md-3">
		<center>		
		<p class="bg-success"><?php if($texto=="exito")echo"Asignacion realizada";elseif($texto=="exitoborrar")echo"Asignacion borrada";elseif($texto=="exitoModificar")echo $idi["exitoModificarAsig"];?></p>
		<p class="bg-danger"><?php if($texto=="errorborrar")echo"Error al borrar";elseif($texto=="errorcrear")echo"Error al crear";elseif($texto=="errormodificar")echo"Error al modificar";?></p>
		<p class="bg-primary"> <?php  if($datos==null) echo $idi['noAsig'];?> </p>
		</center>
	</div>

	<!--Contenido Tabla-->
	<div class="col-md-7">
		<h3>
			<?=$idi["SHOWALLAsig"]?>
		</h3>

		<!--Botones añadir y buscar-->
		<div class="row">
			<div class="col-md-7">			
                            <a class="btn btn-success" href="../Controllers/AsignacQAController.php?action=cargarAlta" aria-label="Add">
				<i class="fa fa-plus" aria-hidden="true"></i>
			</a>

                            <a class="btn btn-default" href="../Controllers/AsignacQAController.php?action=cargarSearch" aria-label="Search">
				<i class="fa fa-search" aria-hidden="true"></i>
			</a>
			</div>
		</div>

		<table class="table">
			<thead>
				<tr>
					<th>
						<?=$idi["IdTrabajo"]?>
					</th>
					<th>
						<?=$idi["LoginEvaluador"]?>
					</th>
					<th>
						<?=$idi["LoginEvaluado"]?>
					</th>
                                        <th>
						<?=$idi["AliasEvaluado"]?>
					</th>
                                        <th>
						<?=$idi["Acción"]?>
					</th>
				</tr>
			</thead>
			<tbody>
	<?php  
	if($datos!=null){
            $tupla=$datos->fetch_row();
		do
		{  ?>
			
		<tr>
			<td>
				<?= $tupla[0]; ?>
			</td>
			<td>
				<?= $tupla[1]; ?>
			</td>
                        <td>
				<?= $tupla[2]; ?>
			</td>
                        <td>
				<?= $tupla[3]; ?>
			</td>
			<td>
				<!--usuario_EDIT.php-->
                                <a href=../Controllers/AsignacQAController.php?action=cargarModificar&IdTrabajo=<?= $tupla[0];?>&LoginEvaluador=<?= $tupla[1];?>&AliasEvaluado=<?= $tupla[3];?> class="btn btn-warning"  aria-label="Edit">
				<i class="fa fa-pencil" aria-hidden="true"></i>
				</a>
				<!--usuario_DELETE.php-->
                                <a href=../Controllers/AsignacQAController.php?action=cargarBaja&IdTrabajo=<?= $tupla[0];?>&LoginEvaluador=<?= $tupla[1];?>&AliasEvaluado=<?= $tupla[3];?>
				class="btn btn-danger" aria-label="Delete">
					<i class="fa fa-trash-o" aria-hidden="true"></i>
				</a>
				<!--usuario_SHOWCURRENT.php-->
                                <a href=../Controllers/AsignacQAController.php?action=cargarShowCurrent&IdTrabajo=<?= $tupla[0];?>&LoginEvaluador=<?= $tupla[1];?>&AliasEvaluado=<?= $tupla[3];?> class="btn btn-default" aria-label="Search">
				<i class="fa fa-search-plus" aria-hidden="true"></i></a>
			</td>
		</tr>
	<?php 
		$tupla=$datos->fetch_row();
                }
                while(!is_null($tupla));
	}
	?>
				</tbody>
			</table>
		</div>
	</div>



<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>