<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showcurrent de grupo, por lo tanto
 * proporciona la representación visual al formulario de showcurrent de la tabla grupo.
 */

class Grupo_SHOWCURRENT{

		function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"detalleGrupo",$comprobarUsuarioGrupo);
		
?>


<!--TABLA SHOWCURRENT-->
	<div class="row">
		<div class="col-md-7">
			<h3>
				<?=$idi["detalleGrupo"]?>				
			</h3>		
	<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>

			<table class="table">				
				<tbody>
					<tr class="active">
						<td>
							<?=$idi["IdGrupo"]?>
						</td>
						<td>
							<?= $fila['IdGrupo']; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["NombreGrupo"]?>
						</td>
						<td>
							<?= $fila['NombreGrupo']; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["DescripGrupo"]?>							
						</td>
						<td>
							<?= $fila['DescripGrupo']; ?>
						</td>						
					</tr>			
					
					<?php 
						 }
					}
						?>
				</tbody>
			</table>
			<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarGrupos">					
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
					</a>
		</div>
	</div>





<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>