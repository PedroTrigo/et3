<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista edit de nota, por lo tanto
 * proporciona la representación visual al formulario de edit de la tabla nota.
 */

class Nota_EDIT{

	function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
	//Carga de cabecera
	include("../Locales/Templates/head.php");
	$cabecera=new head();
	$cabecera->cargar($idi,"modificarNota",$comprobarUsuarioGrupo);
		
?>




<!--EDIT-->
<div id="maincontent" class="col-sm-9">

	<h3>
		<?=$idi["EditarNota"]?>
	</h3>
	
	<form class="form-horizontal" enctype="multipart/form-data" role="form" name="FormEdit" id ="FormEdit" action="../Controllers/ActionController.php?action=modificarNota" method="POST">
		<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>
<div class="form-group">
			 
			<label for="loginEditLabel" class="col-sm-2 control-label">
				<?=$idi["log"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='loginEdit' id='loginEdit' autocomplete="off" value="<?= $fila['login']; ?>" size='9' readonly >
				<p id="loginEditTextoLabel"></p>					
			</div>
		</div>
		
<div class="form-group">
			 
			<label for="IdTrabajoEditLabel" class="col-sm-2 control-label">
				<?=$idi["idT"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='idTEdit' id='idTEdit' autocomplete="off" value="<?= $fila['IdTrabajo']; ?>" size='9' readonly >
				<p id="idTrabajoTextoEdit"></p>					
			</div>
		</div>

		<div class="form-group">
			 
			<label for="NotaTrabajoEditLabel" class="col-sm-2 control-label">
				<?=$idi["NotaT"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='NotaTEdit' id='NotaTEdit' autocomplete="off" value="<?= $fila['NotaTrabajo'];?>" size='9'>
				<p id="NotaTrabajoTextoEdit"></p>					
			</div>
		</div>

		<!--BOTONES FORMULARIO-->
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-1 col-xs-offset-1 col-xs-3">
				<!--Boton enviar-->			
					<button class="btn btn-success" form="FormEdit" id="btn-Edit" href="#" aria-label="Edit" >
					<i class="fa fa-check" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
					<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarNotas">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
					<!-- <p><?php if($texto=="errormodificar")echo $idi["errorModificar"];?> </p> -->						
				</div>			
			</div>
		</div>
	
	</form>
		<?php }} ?>
</div>


</div>




<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>