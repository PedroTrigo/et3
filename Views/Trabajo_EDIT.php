<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista edit de trabajo, por lo tanto
 * proporciona la representación visual al formulario de edit de la tabla trabajo.
 */
class Trabajo_EDIT
{
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
        //Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera=new head();
        $cabecera->cargar($idi,"modificarTrabajo",$comprobarUsuarioGrupo);

        ?>




        <!--EDIT-->
        <div id="maincontent" class="col-sm-9">

                <h3>
                    <?=$idi["Editar trabajo"]?>
                </h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" name="FormEdit" id ="FormEdit" action="../Controllers/ActionController.php?action=modificarSHOWALLTrabajo" onsubmit='return comprobar_TRABAJOS2()' method="POST">

                    <?php foreach($datos as $fila){ ?>
                    <div class="form-group">

                        <label for="IdTrabajo" class="col-sm-2 control-label">
                            <?=$idi["IdTrabajo"]?>
                        </label>
                        <div class="col-sm-3" >
                            <input type="text" class="form-control" name='IdTrabajo' id='IdTrabajo' value="<?= $fila['IdTrabajo'];?>" size='6' readonly>
                            <p id="IdTrabajoTexto"></p>
                        </div>
                    </div>
                    <div class="form-group">

                        <label for="NombreTrabajo" class="col-sm-2 control-label">
                            <?=$idi["NombreTrabajo"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" name="NombreTrabajo" id="NombreTrabajo" autocomplete="off" class="form-control" value="<?= $fila['NombreTrabajo'];?>" size='60' onchange="esVacio(this)  && comprobarText(this,60) && comprobarAlfabetico(this)">
                            <p id="NombreTrabajo"></p>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="FechaIniTrabajo" class="col-sm-2 control-label">
                            <?=$idi["FechaIniTrabajo"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" name="FechaIniTrabajo" id="FechaIniTrabajo" autocomplete="off" class="tcal form-control" placeholder="YYYY/mm/dd" onchange="esVacio(this)" onkeydown="return false" value=<?= $fila['FechaIniTrabajo'];?> >
                        </div>
                        <div class="col-md-offset-5 col-sm3">
                            <p id="FechaIniTrabajo"></p>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="FechaFinTrabajo" class="col-sm-2 control-label">
                            <?=$idi["FechaFinTrabajo"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" name="FechaFinTrabajo" id="FechaFinTrabajo" autocomplete="off" onchange="esVacio(this)" class="tcal form-control" placeholder="YYYY/mm/dd" onkeydown="return false" value=<?= $fila['FechaFinTrabajo'];?> >
                        </div>
                        <div class="col-md-offset-5 col-sm3">
                            <p id="FechaFinTrabajo"></p>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="PorcentajeNota" class="col-sm-2 control-label">
                            <?=$idi["PorcentajeNota"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="PorcentajeNota" id="PorcentajeNota" autocomplete="off" value=<?= $fila['PorcentajeNota'];?> onblur="comprobarEmailEdit(this,size);" onchange="esVacio(this)  && comprobarReal(this,2,0,100)">
                            <p id="PorcentajeNotaTexto"></p>
                        </div>
                    </div>



                    <!--BOTONES FORMULARIO-->
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
                                <!--Boton enviar-->
                                <button class="btn btn-success" form="FormEdit" id="btn-Edit" href="#" aria-label="Edit" onclick="return comprobar_TRABAJOS2()">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </button>
                                <!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/ActionController.php?action=volverTrabajos">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>
                                <!-- <p><?php if($texto=="errormodificar")echo $idi["errorModificar"];?> </p> -->
                            </div>
                        </div>
                    </div>

                </form>
                <?php } ?>
            </div>


        </div>




        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer=new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>