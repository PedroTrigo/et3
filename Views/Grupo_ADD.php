<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista add de grupo, por lo tanto
 * proporciona la representación visual al formulario de add de la tabla grupo.
 */

class Grupo_ADD{

		function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"crearGrupo",$comprobarUsuarioGrupo);
		
?>

<!--ADD-->
<div id="maincontent" class="col-sm-9">
	
		<p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrear"];?> </p>	
	
	<h3>
		<?=$idi["crearGrupo"]?>
	</h3>

	<form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormAdd" name="FormAdd" action="../Controllers/ActionController.php?action=AltaGrupo" method="POST">

		<div class="form-group">
			 
			<label for="idgrupo" class="col-sm-2 control-label">
				<?=$idi["IdGrupo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdGrupo' id='IdGrupo' value = '' size='9' ">
				<p id="IdGrupoAdd"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="nombregrupo" class="col-sm-2 control-label">
				<?=$idi["NombreGrupo"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="NombreGrupo" id="NombreGrupo"  class="form-control" size='20' ">
				<p id="NombreGrupoAdd"></p>						
			</div>
		</div>

		<div class="form-group">
			 
			<label for="descripgrupo" class="col-sm-2 control-label">
				<?=$idi["DescripGrupo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='DescripGrupo' id='DescripGrupo' size='9' ">
				<p id="DescripGrupoAdd"></p>					
			</div>
		</div>
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-1 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormAdd" id="btn-add" href="#" aria-label="Add">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
					<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarGrupos">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>