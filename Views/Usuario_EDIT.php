<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista edit de usuario, por lo tanto
 * proporciona la representación visual al formulario de edit de la tabla usuario.
 */ 

class Usuario_Edit{

	function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
	//Carga de cabecera
	include("../Locales/Templates/head.php");
	$cabecera=new head();
	$cabecera->cargar($idi,"modificarUser",$comprobarUsuarioGrupo);
		
?>




<!--EDIT-->
<div id="maincontent" class="col-sm-9">

	<h3>
		<?=$idi["Edit"]?>
	</h3>
	
	<form class="form-horizontal" enctype="multipart/form-data" role="form" name="FormEdit" id ="FormEdit" action="../Controllers/ActionController.php?action=modificarUser" onsubmit="return encriptar(this);" method="POST">
	
		<?php foreach($datos as $fila){ ?>	
		<div class="form-group">
			 
			<label for="login" class="col-sm-2 control-label">
				<?=$idi["loginCampo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='loginEdit' id='loginEdit' value=<?= $fila['login'];?> size='9' readonly>
				<p id="loginTextoEdit"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="Password" class="col-sm-2 control-label">
				<?=$idi["contraseña"]?>
			</label>
			<div class="col-sm-3">
				<input type="password" name="passEdit" id="passEdit" autocomplete="off" class="form-control" value="" size='20' onblur="comprobarPassEdit(this,size);">
				<p id="passTextoEdit"></p>						
			</div>
		</div>

		<div class="form-group">
			 
			<label for="dni" class="col-sm-2 control-label">
				<?=$idi["DNI"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='dniEdit' id='dniEdit' autocomplete="off" value=<?= $fila['DNI'];?> size='9' onblur="comprobarDniEdit(this);">
				<p id="dniTextoEdit"></p>					
			</div>
		</div>

	   <div class="form-group">

			<label for="nombreUserEditLabel" class="col-sm-2 control-label">
				<?=$idi["Nombre"]?> 
			</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" name="nombreUserEdit" id="nombreUserEdit" autocomplete="off" value=<?= $fila['Nombre'];?> size='30' onblur="comprobarNombreUserEdit(this,size);">
				<p id="nombreUserTextoEdit"></p>
			</div>
		</div>

		<div class="form-group">

			<label for="apellidosUserEditLabel" class="col-sm-2 control-label">
				<?=$idi["Apellidos"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" name="apellidosUserEdit" id="apellidosUserEdit" autocomplete="off" value=<?= $fila['Apellidos'];?> size='50' onblur="comprobarApellUserEdit(this,size);">
				<p id="apellidosUserTextoEdit"></p>
			</div>
		</div>

		<div class="form-group">

			<label for="emailUserEditLabel" class="col-sm-2 control-label">
				<?=$idi["email"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" name="emailUserEdit" id="emailUserEdit" autocomplete="off" value=<?= $fila['Correo'];?> size='40' onblur="comprobarEmailEdit(this,size);">
				<p id="emailUserTextoEdit"></p>
			</div>  
		</div>

		<div class="form-group">

			<label for="direccionEditLabel" class="col-sm-2 control-label">
			<?=$idi["direccion"]?> 
			</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" name="direccionUserEdit" id="direccionUserEdit" autocomplete="off" value=<?= $fila['Direccion'];?> size='60' onblur="comprobarDireccionEdit(this);">
				<p id="direccionUserTextoEdit"></p>				
			</div>
		</div>

		<div class="form-group">

			<label for="telefonoEditLabel" class="col-sm-2 control-label">
			<?=$idi["Telefono"]?> 
			</label>
			<div class="col-sm-3">
				<input type="text" class="form-control" name="telefonoUserEdit" id="telefonoUserEdit" autocomplete="off" value=<?= $fila['Telefono'];?> size='11' onblur="comprobarTelefonoEdit(this);">
				<p id="telefonoUserTextoEdit"></p>				
			</div>
		</div>

		
	

		
		<!--BOTONES FORMULARIO-->
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
				<!--Boton enviar-->			
					<button class="btn btn-success" form="FormEdit" id="btn-Edit" href="#" aria-label="Edit" onclick="return validarEdit()">
					<i class="fa fa-check" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
					<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarUsers">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
					<!-- <p><?php if($texto=="errormodificar")echo $idi["errorModificar"];?> </p> -->						
				</div>			
			</div>
		</div>
	
	</form>
	<?php } ?>
</div>


</div>




<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>