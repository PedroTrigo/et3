<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista add de trabajo, por lo tanto
 * proporciona la representación visual al formulario de add de la tabla trabajo.
 */
class Trabajo_ADD
{
    function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera=new head();
        $cabecera->cargar($idi,"crearTrabajo",$comprobarUsuarioGrupo);

        ?>

        <!--ADD-->
        <div id="maincontent" class="col-sm-9">

                <p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrear"];?> </p>

                <h3>
                    <?=$idi["Añadir trabajo"]?>
                </h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormAdd" name="FormAdd" action="../Controllers/ActionController.php?action=AltaTrabajo" method="POST" onsubmit='return comprobar_TRABAJOS()'>

                    <div class="form-group">

                        <label for="IdTrabajo" class="col-sm-2 control-label">
                            <?=$idi["IdTrabajo"]?>
                        </label>
                        <div class="col-sm-3" >
                            <input type="text" class="form-control" name='IdTrabajo' id='IdTrabajo' size='6' onchange="esVacio(this) && comprobarIdTrabajo(this)">
                            <p id="IdTrabajoTextoAdd"></p>
                        </div>
                    </div>
                    <div class="form-group">

                        <label for="NombreTrabajo" class="col-sm-2 control-label">
                            <?=$idi["NombreTrabajo"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" name="NombreTrabajo" id="NombreTrabajo"  class="form-control" size='60' onchange="esVacio(this)  && comprobarText(this,60) && comprobarAlfabetico(this)">
                            <p id="NombreTrabajoTexto"></p>
                        </div>
                    </div>


                    <div class="form-group">

                        <label for="FechaIniTrabajo" class="col-sm-2 control-label">
                            <?=$idi["FechaIniTrabajo"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" name="FechaIniTrabajo" id="FechaIniTrabajo" class="tcal form-control" autocomplete="off" placeholder="YYYY/mm/dd" onkeydown="return false" onchange="esVacio(this)">
                        </div>
                        <div class="col-md-offset-5 col-sm3">
                            <p id="FechaIniTrabajoTexto"></p>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="FechaFinTrabajo" class="col-sm-2 control-label">
                            <?=$idi["FechaFinTrabajo"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" name="FechaFinTrabajo" id="FechaFinTrabajo" class="tcal form-control" autocomplete="off" placeholder="YYYY/mm/dd" onkeydown="return false" onchange="esVacio(this)">
                        </div>
                        <div class="col-md-offset-5 col-sm3">
                            <p id="FechaFinTrabajoTexto"></p>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="PorcentajeNota" class="col-sm-2 control-label">
                            <?=$idi["PorcentajeNota"]?>
                        </label>
                        <div class="col-sm-3" >
                            <input type="text" class="form-control" name='PorcentajeNota' id='PorcentajeNota' size='6' onchange="esVacio(this) && comprobarReal(this,2,0,100)">
                            <p id="PorcentajeNota"></p>
                        </div>
                    </div>
                    <div class="form-group">
                    </div>


                    <!--BOTONES FORMULARIO-->

                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
                                <!--Boton enviar-->
                                <button class="btn btn-success" form="FormAdd" id="btn-add" href="#" aria-label="Add" onclick="return comprobar_TRABAJOS()">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/ActionController.php?action=volverTrabajos">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>

                            </div>
                        </div>
                    </div>


                </form>
            </div>


        </div>


        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer=new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>