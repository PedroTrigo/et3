<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista edit de permiso, por lo tanto
 * proporciona la representación visual al formulario de edit de la tabla permiso.
 */

class Permiso_EDIT {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
	//Carga de cabecera
	include("../Locales/Templates/head.php");
	$cabecera=new head();
	$cabecera->cargar($idi,"modificarPermiso",$comprobarUsuarioGrupo);
		
?>




<!--EDIT-->
<div id="maincontent" class="col-sm-9">
<div class="row">

	<h3>
		<?=$idi["modificarPermiso"]?>
	</h3>
	
    <form class="form-horizontal" enctype="multipart/form-data" role="form" name="FormEdit" id ="FormEdit" action="../Controllers/PermisoController.php?action=modificar" method="POST">
		
		<div class="form-group">
			 
			<label for="IdGrupo" class="col-sm-2 control-label">
				<?=$idi["IdGrupo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdGrupo' id='IdGrupoEdit' value="<?= $datos[0];?>" size='6' readonly>
				<p id="IdGrupoTextoEdit"></p>					
			</div>
		</div>
                <div class="form-group">
			 
			<label for="IdFuncionalidad" class="col-sm-2 control-label">
				<?=$idi["IdFuncionalidad"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdFuncionalidad' id='IdFuncionalidadEdit' value="<?= $datos[1];?>" size='6' readonly>
				<p id="IdFuncionalidadTextoEdit"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="IdAccion" class="col-sm-2 control-label">
				<?=$idi["IdAccion"]?>
			</label>
			<div class="col-sm-3">
                            <input type="text" name="IdAccion" id="IdAccionEdit" value="<?= $datos[2];?>" size='6' readonly>
				<p id="IdAccionTextoEdit"></p>						
			</div>
		</div>         
		
		<!--BOTONES FORMULARIO-->
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
				<!--Boton enviar-->			
					<button class="btn btn-success" form="FormEdit" id="btn-Edit" href="#" aria-label="Edit">
					<i class="fa fa-check" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/PermisoController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
					<!-- <p><?php if($texto=="errormodificar")echo $idi["errorModificar"];?> </p> -->						
				</div>			
			</div>
		</div>
	
	</form>
</div>


</div>




<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>