<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista delete de acción, por lo tanto
 * proporciona la representación visual al formulario de delete de la tabla acción.
 */
class Accion_DELETE
{
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera=new head();
        $cabecera->cargar($idi,"eliminarAccion",$comprobarUsuarioGrupo);
        ?>

        <!--TABLA ELIMINAR-->
        <?php
        if($datos!=null){
            foreach($datos as $fila)
            {  ?>
                <div class="container-well">
                    <div class="row">
                        <div class="col-md-7">

                            <h3>
                                <?=$idi["eliminarAction"]?>
                            </h3>

                            <form id=FormDelete>
                                <input type="hidden" name="IdAccion" value="<?= $fila['IdAccion'];?>" />
                            </form>

                            <table class="table">
                                <tbody>
                                <tr class="active">
                                    <td>
                                        <?=$idi["IdAccion"]?>
                                    </td>
                                    <td>
                                        <?= $fila['IdAccion']; ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td>
                                        <?=$idi["NombreAccion"]?>
                                    </td>
                                    <td>
                                        <?= $fila['NombreAccion']; ?>
                                    </td>
                                </tr>
                                <tr class="danger">
                                    <td>
                                        <?=$idi["DescripAccion"]?>
                                    </td>
                                    <td>
                                        <?= $fila['DescripAccion']; ?>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                            <p><?=$idi["Confirmar"]?></p>
                            <!-- <a class="btn btn-success" href="../Controllers/ActionController.php?action=BajaUser" aria-label="Add">	 -->
                            <a class="btn btn-success" href=../Controllers/ActionController.php?bajaAccion=<?= $fila['IdAccion'];?>>

                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <a class="btn btn-danger" href="../Controllers/ActionController.php?action=volverAcciones">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>



        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer=new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>