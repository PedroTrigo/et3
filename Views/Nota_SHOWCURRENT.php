<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showcurrent de nota, por lo tanto
 * proporciona la representación visual al formulario de showcurrent de la tabla nota.
 */

class Nota_SHOWCURRENT{

		function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"detalleNota",$comprobarUsuarioGrupo);
		
?>


<!--TABLA SHOWCURRENT-->
	<div class="row">
		<div class="col-md-7">
			<h3>
				<?=$idi["verNota"]?>				
			</h3>		
	<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>

			<table class="table">				
				<tbody>
					<tr class="active">
						<td>
							<?=$idi['login'];?>
						</td>
						<td>
							<?= $fila['login']; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["IdTrabajo"]?>
						</td>
						<td>
							<?= $fila['IdTrabajo']; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi['NotaTrabajo'];?>							
						</td>
						<td>
							<?= $fila['NotaTrabajo']; ?>
						</td>						
					</tr>			
					
					<?php 
						 }
					}
						?>
				</tbody>
			</table>
			<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarNotas">					
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
					</a>
		</div>
	</div>





<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>