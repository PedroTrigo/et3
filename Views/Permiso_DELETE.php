<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista delete de permiso, por lo tanto
 * proporciona la representación visual al formulario de delete de la tabla permiso.
 */

class Permiso_DELETE {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"eliminarPermiso",$comprobarUsuarioGrupo);
?>

	<!--TABLA ELIMINAR-->
	<?php  
	if($datos!=null){ 
		  ?>
	<div class="container-well">
		<div class="row">
			<div class="col-md-7">
				
				<h3>
					<?=$idi["eliminarPermiso"]?>
				</h3>	

			<form id=FormDelete>
                            <input type="hidden" name="IdGrupo" value="<?= $datos[0];?>" />
                            <input type="hidden" name="IdFuncionalidad" value="<?= $datos[1];?>" />
							<input type="hidden" name="IdAccion" value="<?= $datos[1];?>" />
			</form>	

				<table class="table">				
					<tbody>
						<tr class="active">
						<td>
							<?=$idi["IdGrupo"]?>
						</td>
						<td>
							<?= $datos[0]; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["IdFuncionalidad"]?>
						</td>
						<td>
							<?= $datos[1]; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["IdAccion"]?>
						</td>
						<td>
							<?= $datos[2]; ?>
						</td>						
					</tr>
					
					</tbody>
				</table>
				<p><?=$idi["Confirmar"]?></p>
				<!-- <a class="btn btn-success" href="../Controllers/ActionController.php?action=BajaUser" aria-label="Add">	 -->
                                <a class="btn btn-success" href=../Controllers/PermisoController.php?action=baja&IdGrupo=<?= $datos[1];?>&IdFuncionalidad=<?= $datos[0];?>&IdAccion=<?= $datos[0];?>>		

					<i class="fa fa-check" aria-hidden="true"></i>
				</a>
                                <a class="btn btn-danger" href="../Controllers/PermisoController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
				</a>
			</div>
		</div>			
	</div>
<?php 
	 
}
?>
	


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>
