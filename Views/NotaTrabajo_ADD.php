<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista add de nota trabajo, por lo tanto
 * proporciona la representación visual al formulario de add de la tabla nota trabajo.
 */

class NotaTrabajo_ADD {
    function cargar($texto,$idi,$trabajos,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"crearNota",$comprobarUsuarioGrupo);
		
?>

<!--ADD-->
<div id="maincontent" class="col-md-10">
<div class="row">
	
		<p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrearNota"];?> </p>
	
	<h3>
		<?=$idi["ADDNota"]?>
	</h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormAdd" name="FormAdd" action="../Controllers/NotaTrabajoController.php?action=alta" method="POST">

		<div class="form-group">
			 
			<label for="IdTrabajo" class="col-sm-2 control-label">
				<?=$idi["IdTrabajo"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='IdTrabajo' id='IdAdd'>
                                <?php
                                $tupla=$trabajos->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[1]?></option>
                                <?php 
                                    $tupla=$trabajos->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>
		<div class="form-group">
			 
			<label for="NotaTrabajo" class="col-sm-2 control-label">
				<?=$idi["NotaTrabajo"]?>
			</label>
			<div class="col-sm-3">
                                <input type="number" name="NotaTrabajo" id="notaAdd"  class="form-control" size='10'>
				<p id="notaTextoAdd"></p>						
			</div>
		</div>
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-1 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormAdd" id="btn-add" href="#" aria-label="Add">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/NotaTrabajoController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>
