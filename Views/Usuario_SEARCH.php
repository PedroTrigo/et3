<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista search de usuario, por lo tanto
 * proporciona la representación visual al formulario de search de la tabla usuario.
 */

class Usuario_SEARCH{

		function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"searchUser",$comprobarUsuarioGrupo);
		
?>


<!--SEARCH-->
<div id="maincontent" class="col-sm-9">
	
		<p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrear"];?> </p>	
	
	<h3>
		<?=$idi["searchUser"]?>
	</h3>

	<form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormSearch" name="FormSearch" action="../Controllers/ActionController.php?action=SearchUser" method="POST">

		<div class="form-group">
			 
			<label for="login" class="col-sm-2 control-label">
				<?=$idi["loginCampo"]?>
			</label>
			<div class="col-sm-4" >
				<input type="text" class="form-control" name='loginSearch' id='loginSearch' value = '' size='15' onblur="comprobarLoginSearch(this,size);">
				<p id="loginTextoSearch"></p>					
			</div>
		</div>		

		<div class="form-group">
			 
			<label for="dni" class="col-sm-2 control-label">
				<?=$idi["DNI"]?>
			</label>
			<div class="col-sm-4" >
				<input type="text" class="form-control" name='dniSearch' id='dniSearch' size='9' onblur="comprobarDniSearch(this);">
				<p id="dniTextoSearch"></p>					
			</div>
		</div>

	   <div class="form-group">

			<label for="nombreUserSearchLabel" class="col-sm-2 control-label">
				<?=$idi["Nombre"]?> 
			</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="nombreUserSearch" id="nombreUserSearch" size='30' onblur="comprobarNombreUserSearch(this,size);">
				<p id="nombreUserTextoSearch"></p>
			</div>
		</div>

		<div class="form-group">

			<label for="apellidosUserSearchLabel" class="col-sm-2 control-label">
				<?=$idi["Apellidos"]?>
			</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="apellidosUserSearch" id="apellidosUserSearch" size='50' onblur="comprobarApellUserSearch(this,size);">
				<p id="apellidosUserTextoSearch"></p>
			</div>
		</div>

		<div class="form-group">

			<label for="emailUserSearchLabel" class="col-sm-2 control-label">
				<?=$idi["email"]?>
			</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="emailUserSearch" id="emailUserSearch" size='40' onblur="comprobarEmailSearch(this,size);">
				<p id="emailUserTextoSearch"></p>
			</div>
		</div>

		<div class="form-group">

			<label for="direccionUserSearchLabel" class="col-sm-2 control-label">
				<?=$idi["direccion"]?>
			</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="direccionUserSearch" id="direccionUserSearch" size='60' onblur="comprobarDireccionSearch(this,size);">
				<p id="direccionUserTextoSearch"></p>
			</div>
		</div>

		<div class="form-group">

			<label for="telefonoSearchLabel" class="col-sm-2 control-label">
			<?=$idi["Telefono"]?> 
			</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="telefonoUserSearch" id="telefonoUserSearch" size='11' onblur="comprobarTelefonoSearch(this);">
				<p id="telefonoUserTextoSearch"></p>				
			</div>
		</div>	
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormSearch" id="btn-Search" href="#" aria-label="Search" onclick="return validarSearch()">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
					<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarUsers">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>