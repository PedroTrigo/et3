<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista search de asignación qa, por lo tanto
 * proporciona la representación visual al formulario de search de la tabla asignación qa.
 */
class AsignacQA_SEARCH {
    function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"searchAsig",$comprobarUsuarioGrupo);
		
?>


<!--SEARCH-->
<div id="maincontent" class="col-sm-9">
<div class="row">
	
		<p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrearAsig"];?> </p>
	
	<h3>
		<?=$idi["searchAsig"]?>
	</h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormSearch" name="FormSearch" action="../Controllers/AsignacQAController.php?action=search" method="POST">

		<div class="form-group">
			 
			<label for="IdTrabajo" class="col-sm-2 control-label">
				<?=$idi["IdTrabajo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdTrabajo' id='IdSearch' value = '' size='20'>
				<p id="IdTextoAdd"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="LoginEvaluador" class="col-sm-2 control-label">
				<?=$idi["LoginEvaluador"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="LoginEvaluador" id="loginSearch"  class="form-control" size='20'>
				<p id="loginTextoSearch"></p>						
			</div>
		</div>
                <div class="form-group">
			 
			<label for="LoginEvaluado" class="col-sm-2 control-label">
				<?=$idi["LoginEvaluado"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="LoginEvaluado" id="loginSearch"  class="form-control" size='20'>
				<p id="loginTextoSearch"></p>						
			</div>
		</div>
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormSearch" id="btn-Search" href="#" aria-label="Search">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/AsignacQAController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>