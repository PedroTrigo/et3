<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista delete de entrega, por lo tanto
 * proporciona la representación visual al formulario de delete de la tabla entrega.
 */

class Entrega_DELETE {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"eliminarEntrega",$comprobarUsuarioGrupo);
?>

	<!--TABLA ELIMINAR-->
	<?php  
	if($datos!=null){ 
		  ?>
	<div class="container-well">
		<div class="row">
			<div class="col-md-7">
				
				<h3>
					<?=$idi["eliminarEntrega"]?>
				</h3>	

			<form id=FormDelete>
                            <input type="hidden" name="login" value="<?= $datos[0];?>" />
                            <input type="hidden" name="IdTrabajo" value="<?= $datos[1];?>" />
			</form>	

				<table class="table">				
					<tbody>
						<tr class="active">
						<td>
							<?=$idi["login"]?>
						</td>
						<td>
							<?= $datos[0]; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["NombreTrabajo"]?>
						</td>
						<td>
							<?= $datos[1]; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["Alias"]?>
						</td>
						<td>
							<?= $datos[2]; ?>
						</td>						
					</tr>
					<tr class="active">
						<td>
							<?=$idi["Horas"]?>
						</td>
						<td>
							<?= $datos[3]; ?>
						</td>						
					</tr>		
					</tbody>
				</table>
				<p><?=$idi["Confirmar"]?></p>
				<!-- <a class="btn btn-success" href="../Controllers/ActionController.php?action=BajaUser" aria-label="Add">	 -->
                                <a class="btn btn-success" href=../Controllers/EntregaController.php?action=baja&IdTrabajo=<?= $datos[1];?>&login=<?= $datos[0];?>>		

					<i class="fa fa-check" aria-hidden="true"></i>
				</a>
                                <a class="btn btn-danger" href="../Controllers/EntregaController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
				</a>
			</div>
		</div>			
	</div>
<?php 
	 
}
?>
	


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>
