<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showall de entrega usuario, por lo tanto
 * proporciona la representación visual a la tabla showall de entrega usuario.
 */

class EntregaUser_SHOWALL {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"SHOWALLEntrega",$comprobarUsuarioGrupo);
		
?>
<!--TABLA SHOWALL-->


	<div class="row">
	<!--Mesajes de feedback-->
	<div class="col-md-3">
		<center>		
		<p class="bg-success"><?php if($texto=="exitoModificar")echo $idi["exitoModificarEntrega"];?></p>
		<p class="bg-danger"><?php if($texto=="errormodificar")echo"Error al modificar";?></p>
		<p class="bg-primary"> <?php  if($datos==null) echo $idi['noEntrega'];?> </p>
		</center>
	</div>

	<!--Contenido Tabla-->
	<div class="col-md-7">
		<h3>
			<?=$idi["SHOWALLEntrega"]?>
		</h3>

		<table class="table">
			<thead>
				<tr>
					<th>
						<?=$idi["login"]?>
					</th>
					<th>
						<?=$idi["NombreTrabajo"]?>
					</th>
					<th>
						<?=$idi["Alias"]?>
					</th>
                                        <th>
						<?=$idi["Horas"]?>
					</th>
                                        <th>
						<?=$idi["Acción"]?>
					</th>
				</tr>
			</thead>
			<tbody>
	<?php  
	if($datos!=null){
            $tupla=$datos->fetch_row();
		do
		{  ?>
			
		<tr>
			<td>
				<?= $tupla[0]; ?>
			</td>
			<td>
				<?= $tupla[1]; ?>
			</td>
                        <td>
				<?= $tupla[2]; ?>
			</td>
                        <td>
				<?= $tupla[3]; ?>
			</td>
			<td>
				<!--usuario_EDIT.php-->
                                <a href=../Controllers/EntregaController.php?action=cargarUserModificar&IdTrabajo=<?= $tupla[1];?>&login=<?= $tupla[0];?> class="btn btn-warning"  aria-label="Edit">
				<i class="fa fa-pencil" aria-hidden="true"></i>
				</a>
				<!--usuario_SHOWCURRENT.php-->
                                <a href=../Controllers/EntregaController.php?action=cargarShowCurrentUser&IdTrabajo=<?= $tupla[1];?>&login=<?= $tupla[0];?> class="btn btn-default" aria-label="Search">
				<i class="fa fa-search-plus" aria-hidden="true"></i></a>
			</td>
		</tr>
	<?php 
		$tupla=$datos->fetch_row();
                }
                while(!is_null($tupla));
	}
	?>
				</tbody>
			</table>
		</div>
	</div>



<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>