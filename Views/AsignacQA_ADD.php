<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista add de asignación qa, por lo tanto
 * proporciona la representación visual al formulario de add de la tabla asignación qa.
 */

class AsignacQA_ADD {
    function cargar($texto,$idi,$trabajos,$users,$users2,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"crearAsig",$comprobarUsuarioGrupo);
		
?>

<!--ADD-->
<div id="maincontent" class="col-sm-9">
<div class="row">
	
		<p class= "text-danger"><?php if($texto=="AsigRepe")echo $idi["AsigRepe"];?> </p>
	
	<h3>
		<?=$idi["crearAsig"]?>
	</h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormAdd" name="FormAdd" action="../Controllers/AsignacQAController.php?action=alta" method="POST">

                    
		<div class="form-group">
			 
			<label for="IdTrabajo" class="col-sm-2 control-label">
				<?=$idi["IdTrabajo"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='IdTrabajo' id='IdAdd'>
                                <?php
                                $tupla=$trabajos->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[0]?></option>
                                <?php 
                                    $tupla=$trabajos->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>
		<div class="form-group">
			 
			<label for="LoginEvaluador" class="col-sm-2 control-label">
				<?=$idi["LoginEvaluador"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='LoginEvaluador' id='loginAdd'>
                                <?php
                                $tupla=$users->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[0]?></option>
                                <?php 
                                    $tupla=$users->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>
                <div class="form-group">
			 
			<label for="LoginEvaluado" class="col-sm-2 control-label">
				<?=$idi["LoginEvaluado"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='LoginEvaluado' id='loginAdd'>
                                <?php
                                $tupla=$users2->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[0]?></option>
                                <?php 
                                    $tupla=$users2->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>
		

	   
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormAdd" id="btn-add" href="#" aria-label="Add">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/AsignacQAController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>