<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista edit de evaluación, por lo tanto
 * proporciona la representación visual al formulario de edit de la tabla evaluación.
 */

class Evaluacion_EDIT {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
	//Carga de cabecera
	include("../Locales/Templates/head.php");
	$cabecera=new head();
	$cabecera->cargar($idi,"modificarEvaluacion",$comprobarUsuarioGrupo);
		
?>




<!--EDIT-->
<div id="maincontent" class="col-sm-9">
<div class="row">

	<h3>
		<?=$idi["modificarEvaluacion"]?>
	</h3>
	
    <form class="form-horizontal" enctype="multipart/form-data" role="form" name="FormEdit" id ="FormEdit" action="../Controllers/EvaluacionController.php?action=modificar" method="POST">
		
		<div class="form-group">
			 
			<label for="IdTrabajo" class="col-sm-2 control-label">
				<?=$idi["IdTrabajo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdTrabajo' id='IdTrabajoEdit' value="<?= $datos[0];?>" size='6' readonly>
				<p id="IdTrabajoTextoEdit"></p>					
			</div>
		</div>
                <div class="form-group">
			 
			<label for="IdHistoria" class="col-sm-2 control-label">
				<?=$idi["IdHistoria"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdHistoria' id='IdHistoriaEdit' value="<?= $datos[1];?>" size='6' readonly>
				<p id="IdHistoriaTextoEdit"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="LoginEvaluador" class="col-sm-2 control-label">
				<?=$idi["LoginEvaluador"]?>
			</label>
			<div class="col-sm-3">
                            <input type="text" name="LoginEvaluador" id="LoginEvaluadorEdit" autocomplete="off" class="form-control" value="" size='10' readonly>
				<p id="LoginEvaluadorTextoEdit"></p>						
			</div>
		</div>
                <div class="form-group">
			 
			<label for="AliasEvaluado" class="col-sm-2 control-label">
				<?=$idi["AliasEvaluado"]?>
			</label>
			<div class="col-sm-3">
                            <input type="text" name="AliasEvaluado" id="AliasEvaluadoEdit" autocomplete="off" class="form-control" value="" size='10' readonly>
				<p id="AliasEvaluadoTextoEdit"></p>						
			</div>
		</div>
		
	<div class="form-group">
			 
			<label for="CorrectoA" class="col-sm-2 control-label">
				<?=$idi["CorrectoA"]?>
			</label>
			<div class="col-sm-3">
			<input type="checkbox" name="CorrectoA" id="CorrectoAEdit" checked> <span class="label-text"></span>
				<p id="CorrectoATextoEdit"></p>						
			</div>
		</div>

<div class="form-group">
			 
			<label for="ComenIncorrectoA" class="col-sm-2 control-label">
				<?=$idi["ComenIncorrectoA"]?>
			</label>
			<div class="col-sm-3">
                            <input type="text" name="ComenIncorrectoAEvaluado" id="ComenIncorrectoAEdit" autocomplete="off" class="form-control" value="" size='10'>
				<p id="ComenIncorrectoATextoEdit"></p>						
			</div>
		</div>

<div class="form-group">
			 
			<label for="CorrectoP" class="col-sm-2 control-label">
				<?=$idi["CorrectoP"]?>
			</label>
			<div class="col-sm-3">
			<input type="checkbox" name="CorrectoP" id="CorrectoPEdit" checked> <span class="label-text"></span>
				<p id="CorrectoPTextoEdit"></p>						
			</div>
		</div>

<div class="form-group">
			 
			<label for="ComenIncorrectoP" class="col-sm-2 control-label">
				<?=$idi["ComenIncorrectoP"]?>
			</label>
			<div class="col-sm-3">
                            <input type="text" name="ComenIncorrectoP" id="ComenIncorrectoPEdit" autocomplete="off" class="form-control" value="" size='10'>
				<p id="ComenIncorrectoPTextoEdit"></p>						
			</div>
		</div>

<div class="form-group">
			 
			<label for="OK" class="col-sm-2 control-label">
				<?=$idi["OK"]?>
			</label>
			<div class="col-sm-3">
				<input type="checkbox" name="OK" id="OKEdit" checked> <span class="label-text"></span>
				<p id="OKEdit"></p>						
			</div>
		</div>		
		
		
		<!--BOTONES FORMULARIO-->
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
				<!--Boton enviar-->			
					<button class="btn btn-success" form="FormEdit" id="btn-Edit" href="#" aria-label="Edit">
					<i class="fa fa-check" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/EvaluacionController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
					<!-- <p><?php if($texto=="errormodificar")echo $idi["errorModificar"];?> </p> -->						
				</div>			
			</div>
		</div>
	
	</form>
</div>


</div>




<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>