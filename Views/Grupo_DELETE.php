<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista delete de grupo, por lo tanto
 * proporciona la representación visual al formulario de delete de la tabla grupo.
 */

class Grupo_DELETE{

		function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"eliminarGrupo",$comprobarUsuarioGrupo);		
?>

	<!--TABLA ELIMINAR-->
	<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>
	<div class="container-well">
		<div class="row">
			<div class="col-md-7">
				
				<h3>
					<?=$idi["eliminarGrupo"]?>
				</h3>	

			<form id=FormDelete>
			<input type="hidden" name="IdGrupo" value="<?= $fila['IdGrupo'];?>" />
			</form>	

				<table class="table">				
					<tbody>
						<tr class="active">
							<td>
								<?=$idi["IdGrupo"]?>
							</td>
							<td>
							<?=$idi["NombreGrupo"]?>
							</td>	
							<td>
							<?=$idi["DescripGrupo"]?>
							</td>								
						</tr>
						<tr class="success">
							<td>
								<?= $fila['IdGrupo']; ?>
							</td>
							<td>
								<?= $fila['NombreGrupo']; ?>
							</td>			
							<td>
								<?= $fila['DescripGrupo']; ?>
							</td>								
						</tr>
					
					</tbody>
				</table>
				<p><?=$idi["Confirmar"]?></p>
	
				<a class="btn btn-success" href=../Controllers/ActionController.php?bajaG=<?= $fila['IdGrupo'];?>> 
					
					<i class="fa fa-check" aria-hidden="true"></i>
				</a>
				<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarGrupos">					
					<i class="fa fa-times" aria-hidden="true"></i>
				</a>
			</div>
		</div>			
	</div>
<?php 
	 }
}
?>
	


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>