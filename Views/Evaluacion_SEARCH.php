<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista search de evaluación, por lo tanto
 * proporciona la representación visual al formulario de search de la tabla evaluación.
 */

class Evaluacion_SEARCH {
    function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"searchEvaluacion",$comprobarUsuarioGrupo);
		
?>


<!--SEARCH-->
<div id="maincontent" class="col-sm-9">
<div class="row">
	
		<p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrearEvaluacion"];?> </p> <!--TRADUCIR-->
	
	<h3>
		<?=$idi["searchEvaluacion"]?>
	</h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormSearch" name="FormSearch" action="../Controllers/EvaluacionController.php?action=search" method="POST">

	
		<div class="form-group">
			 
			<label for="IdHistoria" class="col-sm-2 control-label">
				<?=$idi["IdHistoria"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="IdHistoria" id="IdHistoriaSearch"  class="form-control" size='20'>
				<p id="IdHistoriaTextoSearch"></p>						
			</div>
		</div>
		
		<div class="form-group">
			 
			<label for="IdTrabajo" class="col-sm-2 control-label">
				<?=$idi["NombreTrabajo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdTrabajo' id='IdSearch' value = '' size='20'>
				<p id="IdTextoAdd"></p>					
			</div>
		</div>
		
                <div class="form-group">
			 
			<label for="LoginEvaluador" class="col-sm-2 control-label">
				<?=$idi["LoginEvaluador"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="LoginEvaluador" id="LoginEvaluadorSearch"  class="form-control" size='20'>
				<p id="LoginEvaluadorTextoSearch"></p>						
			</div>
		</div>
		
		<div class="form-group">
			 
			<label for="AliasEvaluado" class="col-sm-2 control-label">
				<?=$idi["AliasEvaluado"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="AliasEvaluado" id="AliasEvaluadoSearch"  class="form-control" size='20'>
				<p id="AliasEvaluadoTextoSearch"></p>						
			</div>
		</div>
		
		<div class="form-group">
			 
			<label for="CorrectoA" class="col-sm-2 control-label">
				<?=$idi["CorrectoA"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="CorrectoA" id="CorrectoASearch"  class="form-control" size='20'>
				<p id="CorrectoATextoSearch"></p>						
			</div>
		</div>
		
		<div class="form-group">
			 
			<label for="ComenIncorrectoA" class="col-sm-2 control-label">
				<?=$idi["ComenIncorrectoA"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="ComenIncorrectoA" id="ComenIncorrectoASearch"  class="form-control" size='20'>
				<p id="ComenIncorrectoATextoSearch"></p>						
			</div>
		</div>
		
				<div class="form-group">
			 
			<label for="CorrectoP" class="col-sm-2 control-label">
				<?=$idi["CorrectoP"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="CorrectoP" id="CorrectoPSearch"  class="form-control" size='20'>
				<p id="CorrectoPTextoSearch"></p>						
			</div>
		</div>
		
		<div class="form-group">
			 
			<label for="ComenIncorrectoP" class="col-sm-2 control-label">
				<?=$idi["ComenIncorrectoP"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="ComenIncorrectoP" id="ComenIncorrectoPSearch"  class="form-control" size='20'>
				<p id="ComenIncorrectoPTextoSearch"></p>						
			</div>
		</div>
		
		<div class="form-group">
			 
			<label for="OK" class="col-sm-2 control-label">
				<?=$idi["OK"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="OK" id="OKSearch"  class="form-control" size='20'>
				<p id="OKTextoSearch"></p>						
			</div>
		</div>
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormSearch" id="btn-Search" href="#" aria-label="Search">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/EvaluacionController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>