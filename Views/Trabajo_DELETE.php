<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista delete de trabajo, por lo tanto
 * proporciona la representación visual al formulario de delete de la tabla trabajo.
 */
class Trabajo_DELETE
{
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera=new head();
        $cabecera->cargar($idi,"eliminarUser",$comprobarUsuarioGrupo);
        ?>

        <!--TABLA ELIMINAR-->
        <?php
        if($datos!=null){
            foreach($datos as $fila)
            {  ?>
                <div class="container-well">
                    <div class="row">
                        <div class="col-md-7">

                            <h3>
                                <?=$idi["Eliminar trabajo"]?>
                            </h3>

                            <form id=FormDelete>
                                <input type="hidden" name="IdTrabajo" value="<?= $fila['IdTrabajo'];?>" />
                            </form>

                            <table class="table">
                                <tbody>
                                <tr class="active">
                                    <td>
                                        <?=$idi["NombreTrabajo"]?>
                                    </td>
                                    <td>
                                        <?= $fila['NombreTrabajo']; ?>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td>
                                        <?=$idi["FechaIniTrabajo"]?>
                                    </td>
                                    <td>
                                        <?= $fila['FechaIniTrabajo']; ?>
                                    </td>
                                </tr>
                                <tr class="danger">
                                    <td>
                                        <?=$idi["FechaFinTrabajo"]?>
                                    </td>
                                    <td>
                                        <?= $fila['FechaFinTrabajo']; ?>
                                    </td>
                                </tr>
                                <tr class="warning">
                                    <td>
                                        <?=$idi["PorcentajeNota"]?>
                                    </td>
                                    <td>
                                        <?= $fila['PorcentajeNota']; ?>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                            <p><?=$idi["Confirmar"]?></p>
                            <!-- <a class="btn btn-success" href="../Controllers/ActionController.php?action=BajaUser" aria-label="Add">	 -->
                            <a class="btn btn-success" href=../Controllers/ActionController.php?bajaTrabajo=<?= $fila['IdTrabajo'];?>>

                                <i class="fa fa-check" aria-hidden="true"></i>
                            </a>
                            <a class="btn btn-danger" href="../Controllers/ActionController.php?action=volverTrabajos">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>



        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer=new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>