<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista edit de acción, por lo tanto
 * proporciona la representación visual al formulario de edit de la tabla acción.
 */
class Accion_EDIT
{
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
        //Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera=new head();
        $cabecera->cargar($idi,"modificarAction",$comprobarUsuarioGrupo);

        ?>




        <!--EDIT-->
        <div id="maincontent" class="col-sm-9">

                <h3>
                    <?=$idi["Edit Action"]?>
                </h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" name="FormEdit" id ="FormEdit" action="../Controllers/ActionController.php?action=modificarAccion" method="POST" onsubmit='return comprobar_ACCIONES2()'>

                    <?php foreach($datos as $fila){ ?>
                    <div class="form-group">

                        <label for="IdAccion" class="col-sm-2 control-label">
                            <?=$idi["IdAccion"]?>
                        </label>
                        <div class="col-sm-3" >
                            <input type="text" class="form-control" name='IdAccion' id='IdAccion' value=<?= $fila['IdAccion'];?> size='6' readonly>
                            <p id="IdAccionTexto"></p>
                        </div>
                    </div>
                    <div class="form-group">

                        <label for="NombreAccion" class="col-sm-2 control-label">
                            <?=$idi["NombreAccion"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" name="NombreAccion" id="NombreAccion" autocomplete="off" class="form-control" value="<?= $fila['NombreAccion'];?>" size='60' onchange="esVacio(this)  && comprobarText(this,60) && comprobarAlfabetico(this)">
                            <p id="NombreAccionTexto"></p>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="DescripAccion" class="col-sm-2 control-label">
                            <?=$idi["DescripAccion"]?>
                        </label>
                        <div class="col-sm-3" >
                            <input type="text" class="form-control" name='DescripAccion' id='DescripAccion' autocomplete="off" value=<?= $fila['DescripAccion'];?> size='100' onchange="esVacio(this)  && comprobarText(this,100) && comprobarAlfabetico(this)">
                            <p id="DescripAccionTexto"></p>
                        </div>
                    </div>


                    <!--BOTONES FORMULARIO-->
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
                                <!--Boton enviar-->
                                <button class="btn btn-success" form="FormEdit" id="btn-Edit" href="#" aria-label="Edit" onclick="return comprobar_ACCIONES2()">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </button>
                                <!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/ActionController.php?action=volverAcciones">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>
                                <!-- <p><?php if($texto=="errormodificar")echo $idi["errorModificar"];?> </p> -->
                            </div>
                        </div>
                    </div>

                </form>
                <?php } ?>
            </div>


        </div>




        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer=new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>