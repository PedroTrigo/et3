<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista add de usuario, por lo tanto
 * proporciona la representación visual al formulario de add de la tabla usuario.
 */

class Usuario_ADD{

		function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"crearUser",$comprobarUsuarioGrupo);
		
?>

<!--ADD-->
<div id="maincontent" class="col-sm-9">
	
		<p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrear"];?> </p>	
	
	<h3>
		<?=$idi["ADD"]?>
	</h3>

	<form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormAdd" name="FormAdd" action="../Controllers/ActionController.php?action=AltaUser" onsubmit="return encriptar(this);" method="POST">

		<div class="form-group">
			 
			<label for="login" class="col-sm-2 control-label">
				<?=$idi["loginCampo"]?>
			</label>
			<div class="col-sm-4" >
				<input type="text" class="form-control" name='loginAdd' id='loginAdd' value = '' size='9' onblur="comprobarLoginAdd(this,size);">
				<p id="loginTextoAdd"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="Password" class="col-sm-2 control-label">
				<?=$idi["contraseña"]?>
			</label>
			<div class="col-sm-4">
				<input type="password" name="passAdd" id="passAdd"  class="form-control" size='20' onblur="comprobarPassAdd(this,size);">
				<p id="passTextoAdd"></p>						
			</div>
		</div>

		<div class="form-group">
			 
			<label for="dni" class="col-sm-2 control-label">
				<?=$idi["DNI"]?>
			</label>
			<div class="col-sm-4" >
				<input type="text" class="form-control" name='dniAdd' id='dniAdd' size='9' onblur="comprobarDniAdd(this);">
				<p id="dniTextoAdd"></p>					
			</div>
		</div>

	   <div class="form-group">

			<label for="nombreUserAddLabel" class="col-sm-2 control-label">
				<?=$idi["Nombre"]?> 
			</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="nombreUserAdd" id="nombreUserAdd" size='30' onblur="comprobarNombreUserAdd(this,size);">
				<p id="nombreUserTextoAdd"></p>
			</div>
		</div>

		<div class="form-group">

			<label for="apellidosUserAddLabel" class="col-sm-2 control-label">
				<?=$idi["Apellidos"]?>
			</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="apellidosUserAdd" id="apellidosUserAdd" size='50' onblur="comprobarApellUserAdd(this,size);">
				<p id="apellidosUserTextoAdd"></p>
			</div>
		</div>

		<div class="form-group">

			<label for="emailUserAddLabel" class="col-sm-2 control-label">
				<?=$idi["email"]?>
			</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="emailUserAdd" id="emailUserAdd" size='40' onblur="comprobarEmailAdd(this,size);">
				<p id="emailUserTextoAdd"></p>
			</div>
		</div>

		<div class="form-group">

			<label for="direccionUserAddLabel" class="col-sm-2 control-label">
				<?=$idi["direccion"]?>
			</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="direccionUserAdd" id="direccionUserAdd" size='60' onblur="comprobarDireccionAdd(this,size);">
				<p id="direccionUserTextoAdd"></p>
			</div>
		</div>

		<div class="form-group">

			<label for="telefonoAddLabel" class="col-sm-2 control-label">
			<?=$idi["Telefono"]?> 
			</label>
			<div class="col-sm-4">
				<input type="text" class="form-control" name="telefonoUserAdd" id="telefonoUserAdd" size='11' onblur="comprobarTelefonoAdd(this);">
				<p id="telefonoUserTextoAdd"></p>				
			</div>
		</div>	
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormAdd" id="btn-add" href="#" aria-label="Add" onclick="return validarADD()">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
					<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarUsers">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>