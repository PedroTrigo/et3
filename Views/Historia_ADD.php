<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista add de historia, por lo tanto
 * proporciona la representación visual al formulario de add de la tabla historia.
 */

class Historia_ADD{

		function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"crearHistoria",$comprobarUsuarioGrupo);
		
?>

<!--ADD-->
<div id="maincontent" class="col-sm-9">
	
		<p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrear"];?> </p>	
	
	<h3>
		<?=$idi["crearHistoria"]?>
	</h3>

	<form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormAdd" name="FormAdd" action="../Controllers/ActionController.php?action=AltaHistoria" method="POST">

		<div class="form-group">
			 
			<label for="idtrabajo" class="col-sm-2 control-label">
				<?=$idi["idT"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdTrabajo' id='IdTrabajo' value = '' size='9' ">
				<p id="IdTrabajoAdd"></p>					
			</div>
		</div>
		<div class="form-group">
			 
			<label for="idhistoria" class="col-sm-2 control-label">
				<?=$idi["idH"]?>
			</label>
			<div class="col-sm-3">
				<input type="text" name="IdHistoria" id="IdHistoria"  class="form-control" size='20' ">
				<p id="IdHistoriaAdd"></p>						
			</div>
		</div>

		<div class="form-group">
			 
			<label for="textohistoria" class="col-sm-2 control-label">
				<?=$idi["TextoH"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='TextoHistoria' id='TextoHistoria' size='9' ">
				<p id="TextoHistoriaAdd"></p>					
			</div>
		</div>
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormAdd" id="btn-add" href="#" aria-label="Add">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
					<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListaHistorias">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>