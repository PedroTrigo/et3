<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showall de trabajo, por lo tanto
 * proporciona la representación visual a la tabla showall de trabajo.
 */
class Trabajo_SHOWALL
{
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera=new head();
        $cabecera->cargar($idi,"listarTrabajos",$comprobarUsuarioGrupo);

        ?>
        <!--TABLA SHOWALL-->



        <!--Mesajes de feedback-->
        <div class="col-md-3">
            <center>
                <p class="bg-success"><?php if($texto=="exitoCrearTrabajo")echo$idi["exitoCrearTrabajo"];elseif($texto=="exitoborrarTrabajo")echo$idi["exitoborrarTrabajo"];elseif($texto=="exitoModificarTrabajo")echo $idi["exitoModificarTrabajo"];?></p>
                <p class="bg-danger"><?php if($texto=="errorborrarTrabajo")echo $idi["errorborrarTrabajo"];elseif($texto=="errorcrearTrabajo")echo$idi["errorcrearTrabajo"];elseif($texto=="errormodificarTrabajo")echo$idi["errormodificarTrabajo"];?></p>
                <p class="bg-primary"> <?php  if($datos==null) echo $idi['noworks'];?> </p>
            </center>
        </div>

        <!--Contenido Tabla-->
        <div class="col-sm-9">
            <h3>
                <?=$idi["Lista de trabajos"]?>
            </h3>

            <!--Botones añadir y buscar-->
            <div class="row">
                <div class="col-md-7">
                    <a class="btn btn-success" href="../Controllers/ActionController.php?action=CrearTrabajoSHOWALL" aria-label="Add">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </a>

                    <a class="btn btn-default" href="../Controllers/ActionController.php?action=SearchTrabajoSHOWALL" aria-label="Search">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </a>
                </div>
            </div>

            <table class="table">
                <thead>
                <tr>
                    <th>
                        <?=$idi["IdTrabajo"]?>
                    </th>
                    <th>
                        <?=$idi["NombreTrabajo"]?>
                    </th>
                    <th>
                        <?=$idi["FechaIniTrabajo"]?>
                    </th>
                    <th>
                        <?=$idi["FechaFinTrabajo"]?>
                    </th>
                    <th>
                        <?=$idi["PorcentajeNota"]?>
                    </th>
                    <th>
                        <?=$idi["Acción"]?>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php
                if($datos!=null){
                    foreach($datos as $fila)
                    {  ?>

                        <tr>
                            <td>
                                <?= $fila['IdTrabajo']; ?>
                            </td>
                            <td>
                                <?= $fila['NombreTrabajo']; ?>
                            </td>
                            <td>
                                <?= $fila['FechaIniTrabajo']; ?>
                            </td>
                            <td>
                                <?= $fila['FechaFinTrabajo']; ?>
                            </td>
                            <td>
                                <?= $fila['PorcentajeNota']; ?>
                            </td>
                            <td>
                                <!--usuario_EDIT.php-->
                                <a href=../Controllers/ActionController.php?modificarSHOWALLTrabajo=<?= $fila['IdTrabajo'];?> class="btn btn-warning"  aria-label="Edit">
                                    <i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <!--usuario_DELETE.php-->
                                <a href=../Controllers/ActionController.php?eliminarSHOWALLTrabajo=<?= $fila['IdTrabajo'];?>
                                   class="btn btn-danger" aria-label="Delete">
                                    <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                <!--usuario_SHOWCURRENT.php-->
                                <a href=../Controllers/ActionController.php?detalleSHOWALLTrabajo=<?= $fila['IdTrabajo'];?> class="btn btn-default" aria-label="Search">
                                    <i class="fa fa-search-plus" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
        </div>



        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer=new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>