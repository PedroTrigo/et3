<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista search de acción, por lo tanto
 * proporciona la representación visual al formulario de search de la tabla acción.
 */
class Accion_SEARCH
{
    function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera=new head();
        $cabecera->cargar($idi,"searchAccion",$comprobarUsuarioGrupo);

        ?>


        <!--SEARCH-->
        <div id="maincontent" class="col-sm-9">

                <p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrear"];?> </p>

                <h3>
                    <?=$idi["searchAction"]?>
                </h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormSearch" name="FormSearch" action="../Controllers/ActionController.php?action=SearchAccion" method="POST">

                    <div class="form-group">

                        <label for="IdAccion" class="col-sm-2 control-label">
                            <?=$idi["IdAccion"]?>
                        </label>
                        <div class="col-sm-3" >
                            <input type="text" class="form-control" name='IdAccion' id='IdAccion' value = '' size='6' onblur="comprobarLoginSearch(this,size);">
                            <p id="IdAccionTexto"></p>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="NombreAccion" class="col-sm-2 control-label">
                            <?=$idi["NombreAccion"]?>
                        </label>
                        <div class="col-sm-3" >
                            <input type="text" class="form-control" name='NombreAccion' id='NombreAccion' size='60' onblur="comprobarDniSearch(this);">
                            <p id="NombreAccionTexto"></p>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="DescripAccion" class="col-sm-2 control-label">
                            <?=$idi["DescripAccion"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="DescripAccion" id="DescripAccion" size='100' onblur="comprobarNombreUserSearch(this,size);">
                            <p id="DescripAccionTexto"></p>
                        </div>
                    </div>



                    <!--BOTONES FORMULARIO-->

                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
                                <!--Boton enviar-->
                                <button class="btn btn-success" form="FormSearch" id="btn-Search" href="#" aria-label="Search" onclick="return validarSearch()">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/ActionController.php?action=volverAcciones">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>

                            </div>
                        </div>
                    </div>


                </form>
            </div>


        </div>


        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer=new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>