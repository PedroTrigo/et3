<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista asignar de acción, por lo tanto
 * proporciona la representación visual a la asignación de acciones.
 */

class AsignarAccion {
    function cargar($texto,$idi,$IdFuncionalidad,$acciones,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"asignarAccion",$comprobarUsuarioGrupo);
		
?>

<!--ADD-->
<div id="maincontent" class="col-md-10">
<div class="row">
	
	<h3>
		<?=$idi["asignarAccion"]?>
	</h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormAdd" name="FormAdd" action="../Controllers/FuncionalidadController.php?action=asignarAccion" method="POST">

                <div class="form-group">
			 
			<label for="IdFuncionalidad" class="col-sm-2 control-label">
				<?=$idi["IdFuncionalidad"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdFuncionalidad' id='IdEdit' value="<?= $IdFuncionalidad;?>" size='6' readonly>
				<p id="IdTextoAdd"></p>					
			</div>
		</div>
                    
		<div class="form-group">
			 
			<label for="IdAccion" class="col-sm-2 control-label">
				<?=$idi["IdAccion"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='IdAccion' id='IdAdd'>
                                <?php
                                $tupla=$acciones->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[1]?></option>
                                <?php 
                                    $tupla=$acciones->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>

	   
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-1 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormAdd" id="btn-add" href="#" aria-label="Add">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/FuncionalidadController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>