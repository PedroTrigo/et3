<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showcurrent de evaluación, por lo tanto
 * proporciona la representación visual al formulario de showcurrent de la tabla evaluación.
 */

class Evaluacion_SHOWCURRENT {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"detalleEvaluacion",$comprobarUsuarioGrupo);
		
?>


<!--TABLA SHOWCURRENT-->
	<div class="row">
		<div class="col-md-7">
			<h3>
				<?=$idi["detalleEvaluacion"]?>
			</h3>		
	<?php  
	if($datos!=null){ 
		  ?>

			<table class="table">				
				<tbody>
					<tr class="active">
						<td>
							<?=$idi["IdTrabajo"]?>
						</td>
						<td>
							<?= $datos[0]; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["LoginEvaluador"]?>
						</td>
						<td>
							<?= $datos[1]; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["AliasEvaluado"]?>							
						</td>
						<td>
							<?= $datos[2]; ?>
						</td>						
					</tr>
					
					<tr class="active">
						<td>
							<?=$idi["IdHistoria"]?>
						</td>
						<td>
							<?= $datos[3]; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["CorrectoA"]?>
						</td>
						<td>
							<?= $datos[4]; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["ComenIncorrectoA"]?>							
						</td>
						<td>
							<?= $datos[5]; ?>
						</td>						
					</tr>
					
					<tr class="active">
						<td>
							<?=$idi["CorrectoP"]?>
						</td>
						<td>
							<?= $datos[6]; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["ComenIncorrectoP"]?>
						</td>
						<td>
							<?= $datos[7]; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["OK"]?>							
						</td>
						<td>
							<?= $datos[8]; ?>
						</td>						
					</tr>
   
                                       
					<?php 
                
					}
						?>
				</tbody>
			</table>
                    <a class="btn btn-danger" href="../Controllers/EvaluacionController.php?action=showAll">					
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
					</a>
		</div>
	</div>





<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>