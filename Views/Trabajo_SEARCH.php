<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista search de trabajo, por lo tanto
 * proporciona la representación visual al formulario de search de la tabla trabajo.
 */
class Trabajo_SEARCH
{
    function cargar($texto,$idi,$comprobarUsuarioGrupo)
    {
//Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera = new head();
        $cabecera->cargar($idi,"searchTrabajo",$comprobarUsuarioGrupo);

        ?>


        <!--SEARCH-->
        <div id="maincontent" class="col-sm-9">

                <p class="text-danger"><?php if ($texto == "error") echo $idi["errorCrear"]; ?> </p>

                <h3>
                    <?=$idi["Buscar trabajo"]?>
                </h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormSearch"
                      name="FormSearch" action="../Controllers/ActionController.php?action=SearchTrabajo" method="POST">

                    <div class="form-group">

                        <label for="IdTrabajo" class="col-sm-2 control-label">
                            <?=$idi["IdTrabajo"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name='IdTrabajo' id='IdTrabajo' value=''
                                   size='6' onblur="comprobarLoginSearch(this,size);">
                            <p id="IdTrabajo"></p>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="NombreTrabajo" class="col-sm-2 control-label">
                            <?=$idi["NombreTrabajo"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name='NombreTrabajo' id='NombreTrabajo' size='60'
                                   onblur="comprobarDniSearch(this);">
                            <p id="NombreTrabajo"></p>
                        </div>
                    </div>




                    <div class="form-group" id="texto-fecha">

                        <label for="FechaIniTrabajoSearch" class="col-sm-2 control-label">
                            <?=$idi["FechaIniTrabajo"]?>
                        </label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input name="FechaIniTrabajoSearchDia" id="FechaIniTrabajoSearchDia" type="text" size='2'
                                       class="form-control"
                                       placeholder=dia onblur="comprobarDia(this,1,31);">
                                <span class="input-group-addon">-</span>
                                <input name="FechaIniTrabajoSearchMes" id="FechaIniTrabajoSearchMes" type="text" size='2'
                                       class="form-control"
                                       placeholder=mes onblur="comprobarMes(this,1,12);">
                                <span class="input-group-addon">-</span>
                                <input name="FechaIniTrabajoSearchAño" id="FechaIniTrabajoSearchAño" type="text" size='4'
                                       class="form-control"
                                       placeholder=anho onblur="comprobarAño(this,1950,2015);">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-1">
                            <p id="FechaIniTrabajoTextoDia"></p>
                        </div>
                        <div class="col-sm-1">
                            <p id="FechaIniTrabajoTextoMes"></p>
                        </div>
                        <div class="col-sm-1">
                            <p id="FechaIniTrabajoTextoAño"></p>
                        </div>
                    </div>


                    <div class="form-group" id="texto-fecha">

                        <label for="FechaFinTrabajoSearch" class="col-sm-2 control-label">
                            <?=$idi["FechaFinTrabajo"]?>
                        </label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input name="FechaFinTrabajoSearchDia" id="FechaFinTrabajoSearchDia" type="text" size='2'
                                       class="form-control"
                                       placeholder=dia onblur="comprobarDia(this,1,31);">
                                <span class="input-group-addon">-</span>
                                <input name="FechaFinTrabajoSearchMes" id="FechaFinTrabajoSearchMes" type="text" size='2'
                                       class="form-control"
                                       placeholder=mes onblur="comprobarMes(this,1,12);">
                                <span class="input-group-addon">-</span>
                                <input name="FechaFinTrabajoSearchAño" id="FechaFinTrabajoSearchAño" type="text" size='4'
                                       class="form-control"
                                       placeholder=anho onblur="comprobarAño(this,1950,2015);">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-1">
                            <p id="FechaFinTrabajoTextoDia"></p>
                        </div>
                        <div class="col-sm-1">
                            <p id="FechaFinTrabajoTextoMes"></p>
                        </div>
                        <div class="col-sm-1">
                            <p id="FechaFinTrabajoTextoAño"></p>
                        </div>
                    </div>


                    <div class="form-group">

                        <label for="PorcentajeNota" class="col-sm-2 control-label">
                            <?=$idi["PorcentajeNota"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="PorcentajeNota" id="PorcentajeNota"
                                   size='2' onblur="comprobarEmailSearch(this,size);">
                            <p id="PorcentajeNota"></p>
                        </div>
                    </div>



                    <!--BOTONES FORMULARIO-->

                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
                                <!--Boton enviar-->
                                <button class="btn btn-success" form="FormSearch" id="btn-Search" href="#"
                                        aria-label="Search" onclick="return validarSearch()">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/ActionController.php?action=volverTrabajos">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>

                            </div>
                        </div>
                    </div>


                </form>
            </div>


        </div>


        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer = new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>