<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista add de acción, por lo tanto
 * proporciona la representación visual al formulario de add de la tabla acción.
 */
class Accion_ADD
{
    function cargar($texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera=new head();
        $cabecera->cargar($idi,"crearAccion",$comprobarUsuarioGrupo);

        ?>

        <!--ADD-->
        <div id="maincontent" class="col-sm-9">

                <p class= "text-danger"><?php if($texto=="error")echo $idi["errorCrear"];?> </p>

                <h3>
                    <?=$idi["ADD ACTION"]?>
                </h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormAdd" name="FormAdd" action="../Controllers/ActionController.php?action=AltaAccion" method="POST" onsubmit='return comprobar_ACCIONES()'>

                    <div class="form-group">

                        <label for="IdAccion" class="col-sm-2 control-label">
                            <?=$idi["IdAccion"]?>
                        </label>
                        <div class="col-sm-3" >
                            <input type="text" class="form-control" name='IdAccion' id='IdAccion' value = '' size='6' onchange="esVacio(this)  && comprobarText(this,6) && comprobarIdAccion(this)">
                            <p id="IdAccionTexto"></p>
                        </div>
                    </div>
                    <div class="form-group">

                        <label for="NombreAccion" class="col-sm-2 control-label">
                            <?=$idi["NombreAccion"]?>
                        </label>
                        <div class="col-sm-3">
                            <input type="text" name="NombreAccion" id="NombreAccion"  class="form-control" size='60' onchange="esVacio(this)  && comprobarText(this,60) && comprobarAlfabetico(this)">
                            <p id="NombreAccionTexto"></p>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="DescripAccion" class="col-sm-2 control-label">
                            <?=$idi["DescripAccion"]?>
                        </label>
                        <div class="col-sm-3" >
                            <input type="text" class="form-control" name='DescripAccion' id='DescripAccion' size='100' onchange="esVacio(this)  && comprobarText(this,100) && comprobarAlfabetico(this)">
                            <p id="DescripAccionTexto"></p>
                        </div>
                    </div>



                    <!--BOTONES FORMULARIO-->

                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">
                                <!--Boton enviar-->
                                <button class="btn btn-success" form="FormAdd" id="btn-add" href="#" aria-label="Add" onclick="return comprobar_ACCIONES()">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                                <!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/ActionController.php?action=volverAcciones">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>

                            </div>
                        </div>
                    </div>


                </form>
            </div>


        </div>


        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer=new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>