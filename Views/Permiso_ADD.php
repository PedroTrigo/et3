<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista add de permiso, por lo tanto
 * proporciona la representación visual al formulario de add de la tabla permiso.
 */

class Permiso_ADD {
    function cargar($texto,$idi,$grupos,$funcionalidades, $acciones,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"crearPermiso",$comprobarUsuarioGrupo);
		
?>

<!--ADD-->
<div id="maincontent" class="col-sm-9">
<div class="row">
	
		<p class= "text-danger"><?php if($texto=="PermisoRepe")echo $idi["PermisoRepe"];?> </p>
	
	<h3>
		<?=$idi["crearPermiso"]?>
	</h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormAdd" name="FormAdd" action="../Controllers/PermisoController.php?action=alta" method="POST">

                    <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
                    
		<div class="form-group">
			 
			<label for="IdGrupo" class="col-sm-2 control-label">
				<?=$idi["IdGrupo"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='IdGrupo' id='IdGrupoAdd'>
                                <?php
                                $tupla=$grupos->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[0]?></option>
                                <?php 
                                    $tupla=$grupos->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>
		<div class="form-group">
			 
			<label for="IdFuncionalidad" class="col-sm-2 control-label">
				<?=$idi["IdFuncionalidad"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='IdFuncionalidad' id='IdFuncionalidadAdd'>
                                <?php
                                $tupla=$funcionalidades->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[1]?></option>
                                <?php 
                                    $tupla=$funcionalidades->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>
		
			<div class="form-group">
			 
			<label for="IdAccion" class="col-sm-2 control-label">
				<?=$idi["IdAccion"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='IdAccion' id='IdAccionAdd'>
                                <?php
                                $tupla=$acciones->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[1]?></option>
                                <?php 
                                    $tupla=$acciones->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>



	   
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormAdd" id="btn-add" href="#" aria-label="Add">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/PermisoController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>