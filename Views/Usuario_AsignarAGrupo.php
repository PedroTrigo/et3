<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista add de asignar grupo a usuario,
 * por lo tanto muestra el formulario para meter un usuario en un grupo.
 */
class Usuario_AsignarGrupo{

		function cargar($texto,$idi,$gruposDiferentes,$login,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"AsginarGrupo",$comprobarUsuarioGrupo);		
?>

	<!--TABLA ELIMINAR-->
	<div class="container-well">
		<div class="row">
			<div class="col-md-7">
				
				<h3>
					<?=$idi["asignarUserAGrupo"]?>
				</h3>	

			<form method="post" role="form" name="FormAsign" id ="FormAsign" action="../Controllers/ActionController.php?action=asignarGrupo">
			<input type="hidden" id="login" name="login" value=<?= $login;?>>
				

				<table class="table">				
					<tbody>
						<tr class="active">
							<td>
								<?=$idi["Nombre"]?>
							</td>
							<td>
								<?=$login?>
							</td>						
						</tr>
						<tr class="success">
							<td>
								<?=$idi["Grupo"]?>
							</td>
							<td>

							<select class="form-control" required id="IdGrupo" name="IdGrupo"> 
								<option value='0'> <?=$idi['selectGrupo'] ?> </option>;	

								<?php
		         				if($gruposDiferentes!=null){ 

								for ($numar=0;$numar<count($gruposDiferentes);$numar++)
								{	
									
								$id=$gruposDiferentes[$numar]["IdGrupo"];								 

								?>								
								<option value=<?= $id;?>> <?= $id;?> </option>
								 
						
							<?php 
								 }
							}
								?>																						
          					</select>
          	
								
							</td>						
						</tr>						
								
					</tbody>
				</table>
				<p><?=$idi["Confirmar"]?></p>

					<button class="btn btn-success" form="FormAsign" id="btn-Edit" href="#">
					<i class="fa fa-check" aria-hidden="true"></i>
					</button>

				<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarUsers">					
					<i class="fa fa-times" aria-hidden="true"></i>
				</a>
				</form>
			</div>
		</div>			
	</div>	

<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>