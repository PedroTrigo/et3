<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showall de usuario, por lo tanto
 * proporciona la representación visual a la tabla showall de usuario.
 */
class Usuario_SHOWALL{

		function cargar($formfinal,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"listarUsers",$comprobarUsuarioGrupo);
		
?>
<!--TABLA SHOWALL-->


	
	<!--Mesajes de feedback-->
	<div class="col-md-3">
		<center>		
		<p class="bg-success"><?php if($texto=="exito")echo $idi['exitoCrearUser'];elseif($texto=="exitoborrar")echo $idi['exitoBorrarUser'];elseif($texto=="exitoModificar")echo $idi["exitoModificarUser"];elseif($texto=="exitoAsignarGrupo")echo $idi['exitoAsignarGrupo'];?></p>
		<p class="bg-danger"><?php if($texto=="errorborrar")echo $idi['errorBorrarUser'];elseif($texto=="errorcrear")echo $idi['errorCrearUser'];elseif($texto=="errormodificar")echo $idi['errorModificarUser'];?></p>
		<p class="bg-primary"> <?php  if($formfinal==null) echo $idi['nousers'];?> </p>
		</center>
	</div> 

	<!--Contenido Tabla-->
	<div class="col-sm-9">
		<h3>
			<?=$idi["SHOWALL"]?>
		</h3>

		<!--Botones añadir y buscar-->
		<div class="row">
			<div class="col-md-7">			
			<a class="btn btn-success" href="../Controllers/ActionController.php?action=CrearUserSHOWALL" aria-label="Add" title="<?=$idi["crearUser"]?>">
				<i class="fa fa-plus" aria-hidden="true"></i>
			</a>

			<a class="btn btn-default" href="../Controllers/ActionController.php?action=SearchUserSHOWALL" aria-label="Search" title="<?=$idi["searchUser"]?>">
				<i class="fa fa-search" aria-hidden="true"></i>
			</a>
			</div>
		</div>

		<table class="table">
			<thead>
				<tr>
					<th>
						<?=$idi["login"]?>
					</th>					
					<th>
						<?=$idi["DNI"]?>
					</th>
					
					<th>
						<?=$idi["Acción"]?>
					</th>						
				</tr>
			</thead>
			<tbody>
	<?php
	  
	if($formfinal!=null){ 
		for ($numar =0;$numar<count($formfinal);$numar++){
	 ?>
			
		<tr>
			<td>
				<?= $formfinal[$numar]["login"];  ?>
			</td>			
			<td>
				<?= $formfinal[$numar]["DNI"]; ?>
			</td>

			<td>
				<!--usuario_EDIT.php-->
				<a href=../Controllers/ActionController.php?modificarSHOWALL=<?= $formfinal[$numar]["login"];?> class="btn btn-warning"  aria-label="Edit" title="<?=$idi["modificarUser"]?>">
				<i class="fa fa-pencil" aria-hidden="true"></i>
				</a>
				<!--usuario_DELETE.php-->
				<a href=../Controllers/ActionController.php?eliminarSHOWALL=<?= $formfinal[$numar]["login"];?> class="btn btn-danger" aria-label="Delete" title="<?=$idi["eliminarUser"]?>">
					<i class="fa fa-trash-o" aria-hidden="true"></i>
				</a>
				<!--usuario_SHOWCURRENT.php-->
				<a href=../Controllers/ActionController.php?detalleSHOWALL=<?= $formfinal[$numar]["login"];?> class="btn btn-default" aria-label="Search" title="<?=$idi["detalleUser"]?>">
				<i class="fa fa-search-plus" aria-hidden="true"></i></a>
				<!--usuario_AsignarAGrupo.php-->
				<a href=../Controllers/ActionController.php?vistaAsignarAGrupo=<?= $formfinal[$numar]["login"];?> class="btn btn-default" aria-label="Search" title="<?=$idi["AsignarAGrupo"]?>">
				<i class="fa fa-users" aria-hidden="true"></i>
			</td>
		</tr>
	<?php 
		 }
	}
	?>
				</tbody>
			</table>
		</div>
	</div>



<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>