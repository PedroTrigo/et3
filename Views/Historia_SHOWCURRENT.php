<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showcurrent de historia, por lo tanto
 * proporciona la representación visual al formulario de showcurrent de la tabla historia.
 */

class Historia_SHOWCURRENT{

		function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"detalleHistoria",$comprobarUsuarioGrupo);
		
?>


<!--TABLA SHOWCURRENT-->
	<div class="row">
		<div class="col-md-7">
			<h3>
				<?=$idi["verHistoria"]?>				
			</h3>		
	<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>

			<table class="table">				
				<tbody>
					<tr class="active">
						<td>
							<?=$idi["IdTrabajo"]?>
						</td>
						<td>
							<?= $fila['IdTrabajo']; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["IdHistoria"]?>
						</td>
						<td>
							<?= $fila['IdHistoria']; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["TextoHistoria"]?>							
						</td>
						<td>
							<?= $fila['TextoHistoria']; ?>
						</td>						
					</tr>			
					
					<?php 
						 }
					}
						?>
				</tbody>
			</table>
			<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListaHistorias">					
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
					</a>
		</div>
	</div>





<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>