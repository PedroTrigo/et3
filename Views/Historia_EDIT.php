<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista edit de historia, por lo tanto
 * proporciona la representación visual al formulario de edit de la tabla historia.
 */

class Historia_EDIT{

	function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
	//Carga de cabecera
	include("../Locales/Templates/head.php");
	$cabecera=new head();
	$cabecera->cargar($idi,"modificarHistoria",$comprobarUsuarioGrupo);
		
?>




<!--EDIT-->
<div id="maincontent" class="col-sm-9">

	<h3>
		<?=$idi["EditarHistoria"]?>
	</h3>
	
	<form class="form-horizontal" enctype="multipart/form-data" role="form" name="FormEditHistoria" id ="FormEditHistoria" action="../Controllers/ActionController.php?action=modificarHistoria" method="POST">
			<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>
<div class="form-group">
			 
			<label for="IdTrabajoEditLabel" class="col-sm-2 control-label">
				<?=$idi["idT"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='idTEdit' id='idTEdit' autocomplete="off" value="<?= $fila['IdTrabajo'];?>" size='9' readonly >
				<p id="idTrabajoTextoEdit"></p>					
			</div>
		</div>
		
<div class="form-group">
			 
			<label for="IdHistoriaEditLabel" class="col-sm-2 control-label">
				<?=$idi["idH"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='idHEdit' id='idHEdit' autocomplete="off" value="<?= $fila['IdHistoria'];?>" size='9' readonly >
				<p id="idHistoriaTextoEdit"></p>					
			</div>
		</div>

		<div class="form-group">
			 
			<label for="TextoHistoriaEditLabel" class="col-sm-2 control-label">
				<?=$idi["TextoH"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='TextoHEdit' id='TextoHEdit' autocomplete="off" value="<?= $fila['TextoHistoria'];?>" size='9'>
				<p id="TextoHistoriaTextoEdit"></p>					
			</div>
		</div>

		<!--BOTONES FORMULARIO-->
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-1 col-xs-offset-1 col-xs-3">
				<!--Boton enviar-->			
					<button class="btn btn-success" form="FormEditHistoria" id="btn-Edit" href="#" aria-label="Edit" >
					<i class="fa fa-check" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
					<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListaHistorias">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
					<!-- <p><?php if($texto=="errormodificar")echo $idi["errorModificar"];?> </p> -->						
				</div>			
			</div>
		</div>
	
	</form>
	<?php }} ?>
</div>


</div>




<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>