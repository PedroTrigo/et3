<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista de login, por lo tanto
 muestra elformulario para logearse.
 */

    class login{ 
    function cargar($texto,$idi){ 
    
?>
<!DOCTYPE html>
<html>  
<head lang="en">  
    <meta charset="UTF-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link type="text/css" rel="stylesheet" href="../Locales/bootstrap/bootstrap.min.css">
    <!--Validaciones-->
    <?php include '../Locales/js/validaciones.js'; ?>
    <!--MD5-->
    <script type="text/javascript" src="../Locales/js/md5-min.js"></script>
    <!--css-->
    <link rel="stylesheet" type="text/css" href="../Locales/css/styles.css">          
    <title><?=$idi['login']?></title>  
</head>  
<style>  
    .login-panel {  
        margin-top: 150px;  
  
</style>  
<body>  
<div class="container">
    <div class="col-md-7 col-md-offset-3 panel panel-primary">  
        <div class="panel-heading">Intrucciones:</div>
            <div class="panel-body">
              <p >Paso 1: </p>
              <p >Ejecuta la intrucción chmod -R 777 var/www/html</p>
              <p >Paso 2:</p>
              <p >Carga la base de datos que con este script: IP/Locales/DataBase/install_BD.php</p>
        </div>
    </div>
</div>
  
  
<div class="container">  
    <div class="row">  
        <div class="col-md-4 col-md-offset-4">  
            <div class="login-panel panel panel-success">  
                <div class="panel-heading">  
                    <h3 class="panel-title"><?=$idi['login']?></h3>  
                </div>  
                <div class="panel-body">  
                    <form role="form" method="post" id="Loginform" name="Loginform" onsubmit="return encriptar(this);" action="ActionController.php?action=comprobarUsuario">  
                        <fieldset>  
                            <div class="form-group"  >  
                                <input class="form-control" placeholder="<?= $idi["introduceLogin"] ?>" id="usuario" name="usuario" type="text" autofocus maxlength="50">
                                <p id="usuarioparrafo"></p>  
                            </div>  
                            <div class="form-group">  
                                <input type="password" class="form-control" name="password" placeholder="<?= $idi["introducepass"] ?>" id="password" maxlength="25" required>
                            <p id="passwordparrafo"></p>
                            </div>  

        <input type="submit" class="btn btn-lg btn-success btn-block" name="login" value=<?= $idi["entrar"]; ?> />
        <a href="ActionController.php?action=registro"><input type="button" class="btn btn-primary" name="Registrarse"  value="<?= $idi["registrarse"]; ?>" /> </a>  
                                 
  
                             
                        </fieldset>  
                    </form>

                    
<div class="row">
<div class= "col-md-10">
<center>                    
        <p class="bg-success"> <?php if($texto=="UsuarioCreadoconExito") echo $idi["usuariocredoexito"];?> </p>
        <p class="bg-danger"> <?php if($texto=="UsuarioIncorrecto") echo $idi["usuarioerror"]; ?> </p>
        </center>
</div>
</div>   
                </div>  
            </div>  
        </div>  
    </div>  
</div>  
  
  
</body>  
  
</html>  
  
<?php 
   }
  
   }
?>