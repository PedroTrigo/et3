<?php 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista edit de grupo, por lo tanto
 * proporciona la representación visual al formulario de edit de la tabla grupo.
 */

class Grupo_EDIT{

	function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
	//Carga de cabecera
	include("../Locales/Templates/head.php");
	$cabecera=new head();
	$cabecera->cargar($idi,"modificarGrupo",$comprobarUsuarioGrupo);
		
?>




<!--EDIT-->
<div id="maincontent" class="col-sm-9">

	<h3>
		<?=$idi["EditarGrupo"]?>
	</h3>
	
	<form class="form-horizontal" enctype="multipart/form-data" role="form" name="FormEditGrupo" id ="FormEditGrupo" action="../Controllers/ActionController.php?action=modificarGrupo" method="POST">
			<?php  
	if($datos!=null){ 
		foreach($datos as $fila)
		{  ?>
<div class="form-group">
			 
			<label for="IdGrupoEditLabel" class="col-sm-2 control-label">
				<?=$idi["IdGrupo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='IdGrupoEdit' id='IdGrupoEdit' autocomplete="off" value="<?= $fila['IdGrupo'];?>" size='9' readonly >
				<p id="IdGrupoTextoEdit"></p>					
			</div>
		</div>
		
<div class="form-group">
			 
			<label for="NombreGrupoEditLabel" class="col-sm-2 control-label">
				<?=$idi["NombreGrupo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='NombreGrupoEdit' id='NombreGrupoEdit' autocomplete="off" value="<?= $fila['NombreGrupo'];?>" size='9'>
				<p id="NombreGrupoTextoEdit"></p>					
			</div>
		</div>

		<div class="form-group">
			 
			<label for="DescripGrupoEditLabel" class="col-sm-2 control-label">
				<?=$idi["DescripGrupo"]?>
			</label>
			<div class="col-sm-3" >
				<input type="text" class="form-control" name='DescripGrupoEdit' id='DescripGrupoEdit' autocomplete="off" value="<?= $fila['DescripGrupo'];?>" size='9'>
				<p id="DescripGrupoTextoEdit"></p>					
			</div>
		</div>

		<!--BOTONES FORMULARIO-->
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-1 col-xs-offset-1 col-xs-3">
				<!--Boton enviar-->			
					<button class="btn btn-success" form="FormEditGrupo" id="btn-Edit" href="#" aria-label="Edit" >
					<i class="fa fa-check" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
					<a class="btn btn-danger" href="../Controllers/ActionController.php?action=ListarGrupos">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
					<!-- <p><?php if($texto=="errormodificar")echo $idi["errorModificar"];?> </p> -->						
				</div>			
			</div>
		</div>
	
	</form>
	<?php }} ?>
</div>


</div>




<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
?>