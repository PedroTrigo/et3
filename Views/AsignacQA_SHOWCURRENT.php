<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showcurrent de asignación qa, por lo tanto
 * proporciona la representación visual al formulario de showcurrent de la tabla asignación qa.
 */

class AsignacQA_SHOWCURRENT {
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"detalleAsig",$comprobarUsuarioGrupo);
		
?>


<!--TABLA SHOWCURRENT-->
	<div class="row">
		<div class="col-md-7">
			<h3>
				<?=$idi["detalleAsig"]?>
			</h3>		
	<?php  
	if($datos!=null){ 
		  ?>

			<table class="table">				
				<tbody>
					<tr class="active">
						<td>
							<?=$idi["IdTrabajo"]?>
						</td>
						<td>
							<?= $datos[0]; ?>
						</td>						
					</tr>
					<tr class="success">
						<td>
							<?=$idi["LoginEvaluador"]?>
						</td>
						<td>
							<?= $datos[1]; ?>
						</td>						
					</tr>
					<tr class="warning">
						<td>
							<?=$idi["LoginEvaluado"]?>							
						</td>
						<td>
							<?= $datos[2]; ?>
						</td>						
					</tr>
                                        <tr class="active">
						<td>
							<?=$idi["AliasEvaluado"]?>
						</td>
						<td>
							<?= $datos[3]; ?>
						</td>						
					</tr>
					<?php 
                
					}
						?>
				</tbody>
			</table>
                    <a class="btn btn-danger" href="../Controllers/AsignacQAController.php?action=showAll">					
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
					</a>
		</div>
	</div>





<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>