<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista showcurrent de trabajo, por lo tanto
 * proporciona la representación visual al formulario de showcurrent de la tabla trabajo.
 */
class Trabajo_SHOWCURRENT
{
    function cargar($datos,$texto,$idi,$comprobarUsuarioGrupo){
//Carga de cabecera
        include("../Locales/Templates/head.php");
        $cabecera=new head();
        $cabecera->cargar($idi,"detalleUser",$comprobarUsuarioGrupo);

        ?>


        <!--TABLA SHOWCURRENT-->
        <div class="row">
            <div class="col-md-7">
                <h3>
                    <?=$idi["Trabajo en detalle"]?>
                </h3>
                <?php
                if($datos!=null){
                foreach($datos as $fila)
                {  ?>

                <table class="table">
                    <tbody>
                    <tr class="active">
                        <td>
                            <?=$idi["IdTrabajo"]?>
                        </td>
                        <td>
                            <?= $fila['IdTrabajo']; ?>
                        </td>
                    </tr>
                    <tr class="success">
                        <td>
                            <?=$idi["NombreTrabajo"]?>
                        </td>
                        <td>
                            <?= $fila['NombreTrabajo']; ?>
                        </td>
                    </tr>
                    <tr class="danger">
                        <td>
                            <?=$idi["FechaIniTrabajo"]?>
                        </td>
                        <td>
                            <?= $fila['FechaIniTrabajo']; ?>
                        </td>
                    </tr>
                    <tr class="danger">
                        <td>
                            <?=$idi["FechaFinTrabajo"]?>
                        </td>
                        <td>
                            <?= $fila['FechaFinTrabajo']; ?>
                        </td>
                    </tr>
                    <tr class="active">
                        <td>
                            <?=$idi["PorcentajeNota"]?>
                        </td>
                        <td>
                            <?= $fila['PorcentajeNota']; ?>
                        </td>
                    </tr>

                    <?php
                    }
                    }
                    ?>
                    </tbody>
                </table>
                <a class="btn btn-danger" href="../Controllers/ActionController.php?action=dentroTrabajo">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>
            </div>
        </div>





        <!--Carga de pie-->
        <?php
        include('../Locales/Templates/footer.php');
        $footer=new footer();
        $footer->cargar();
        ?>

        </html>

        <?php
    }
}
?>