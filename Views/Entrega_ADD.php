<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la vista add de entrega, por lo tanto
 * proporciona la representación visual al formulario de add de la tabla entrega.
 */

class Entrega_ADD {
    function cargar($texto,$idi,$trabajos,$usuarios,$comprobarUsuarioGrupo){
//Carga de cabecera
		include("../Locales/Templates/head.php");
		$cabecera=new head();
		$cabecera->cargar($idi,"crearEntrega",$comprobarUsuarioGrupo);
		
?>

<!--ADD-->
<div id="maincontent" class="col-sm-9">
<div class="row">
	
		<p class= "text-danger"><?php if($texto=="EntregaRepe")echo $idi["EntregaRepe"];?> </p>
	
	<h3>
		<?=$idi["crearEntrega"]?>
	</h3>

                <form class="form-horizontal" enctype="multipart/form-data" role="form" id="FormAdd" name="FormAdd" action="../Controllers/EntregaController.php?action=alta" method="POST">

                    <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
                    
		<div class="form-group">
			 
			<label for="login" class="col-sm-2 control-label">
				<?=$idi["login"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='login' id='loginAdd'>
                                <?php
                                $tupla=$usuarios->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[0]?></option>
                                <?php 
                                    $tupla=$usuarios->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>
		<div class="form-group">
			 
			<label for="IdTrabajo" class="col-sm-2 control-label">
				<?=$idi["NombreTrabajo"]?>
			</label>
			<div class="col-sm-3" >
                            <select type="text" class="form-control" name='IdTrabajo' id='IdAdd'>
                                <?php
                                $tupla=$trabajos->fetch_row();
                                do
                                {  ?>
                                    <option value="<?=$tupla[0]?>"><?=$tupla[1]?></option>
                                <?php 
                                    $tupla=$trabajos->fetch_row();
                                }
                                while(!is_null($tupla));
                                ?>
                            </select>
			</div>
		</div>

		
                <div class="form-group">
			 
			<label for="Horas" class="col-sm-2 control-label">
				<?=$idi["Horas"]?>
			</label>
			<div class="col-sm-3" >
                            <input type="number" class="form-control" name='Horas' id='HorasAdd' size='20'>
				<p id="HorasTextoAdd"></p>					
			</div>
		</div>
                

	   
		
		<!--BOTONES FORMULARIO-->
		
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-2 col-xs-offset-1 col-xs-3">	
				<!--Boton enviar-->	
					<button class="btn btn-success" form="FormAdd" id="btn-add" href="#" aria-label="Add">
					<i class="fa fa-plus" aria-hidden="true"></i>
					</button>
				<!--Boton volver-->
                                <a class="btn btn-danger" href="../Controllers/EntregaController.php?action=showAll">					
					<i class="fa fa-times" aria-hidden="true"></i>
					</a>
										
				</div>			
			</div>
		</div>
		

	</form>
</div>


</div>


<!--Carga de pie-->
<?php 
include('../Locales/Templates/footer.php');
$footer=new footer();
$footer->cargar();	
?>	
		
</html>

<?php 
	 }
}
	?>
