<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es el modelo de permiso, por lo tanto
 * se encarga de conectar con la base de datos y realizar las
 * sentencias necesarias en SQL para llevar a cabo todas las operaciones posibles.
 */

class Model_PERMISO {
    var $IdGrupo;
    var $IdFuncionalidad;
    var $IdAccion;

    
    function __construct($IdGrupo,$IdFuncionalidad,$IdAccion) {
        $this->IdGrupo=$IdGrupo;
        $this->IdFuncionalidad=$IdFuncionalidad;
        $this->IdAccion=$IdAccion;
    }

    function conexionBD(){
	$mysqli=mysqli_connect("localhost","userET3","passET3","IUET32017");
	if(!$mysqli){
            echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
            exit;
	}
	return $mysqli;
    }
    
    
    function comprobarPermiso(){
	$mysqli=$this->conexionBD();
        
	$query="SELECT * FROM PERMISO WHERE IdGrupo='$this->IdGrupo' AND IdFuncionalidad='$this->IdFuncionalidad' AND IdAccion='$this->IdAccion' ";
	$resultado=$mysqli->query($query);
	if(mysqli_num_rows($resultado)){
            return true;
	}
        else{
            return false;
	}
    }
    
    function insert(){
        $mysqli=$this->conexionBD();
        
        $query="INSERT INTO PERMISO(IdGrupo,IdFuncionalidad,IdAccion) VALUES ('$this->IdGrupo','$this->IdFuncionalidad','$this->IdAccion')";
        $toret=$mysqli->query($query);
        $mysqli->close();
        return $toret;
    }
    
    function delete(){
        $mysqli=$this->conexionBD();
        
        $query="DELETE FROM PERMISO WHERE IdGrupo='$this->IdGrupo' AND IdFuncionalidad='$this->IdFuncionalidad' AND IdAccion='$this->IdAccion'";
        if ($toret=$mysqli->query($query)){
	    return $toret;		
	}
        else{
            return "Error al borrar";
	}
    }
    
    function update(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM PERMISO WHERE IdGrupo='$this->IdGrupo' AND IdFuncionalidad='$this->IdFuncionalidad' AND IdAccion='$this->IdAccion'";
        $resultado=$mysqli->query($query);

        $query="UPDATE PERMISO SET IdGrupo='$IdGrupo',IdFuncionalidad='$IdFuncionalidad',IdAccion='$IdAccion' WHERE IdGrupo='$this->IdGrupo' AND IdFuncionalidad='$this->IdFuncionalidad' AND IdAccion='$this->IdAccion'";
	$resultado=$mysqli->query($query);
	$mysqli->close();
	return $resultado;
    }
    
    function select(){
        $mysqli=$this->conexionBD();
        
        if($this->IdGrupo=="" && $this->IdFuncionalidad=="" && $this->IdAccion==""){
            $query="SELECT * FROM PERMISO";
        }

        if (!($resultado = $mysqli->query($query))){
                return 'Error en la consulta sobre la base de datos';
            }
        else{
            return $resultado;
        }
    }
    
    function showAll(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM PERMISO";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function showCurrent(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM PERMISO WHERE IdGrupo='$this->IdGrupo' AND IdFuncionalidad='$this->IdFuncionalidad' AND IdAccion='$this->IdAccion'";
        if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
            $entrega=$resultado->fetch_row();
	    return $entrega;
	}
    }
    
    function grupos(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT IdGrupo FROM GRUPO";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function funcionalidades(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT IdFuncionalidad FROM FUNCIONALIDAD";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
	
	function acciones(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT IdAccion FROM ACCION";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
}
