<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es el modelo de usuario, por lo tanto
 * se encarga de conectar con la base de datos y realizar las
 * sentencias necesarias en SQL para llevar a cabo todas las operaciones posibles.
 */

class Usuario_Model	{	
	private $login;
	private $password;
	private $dni;
	private $nombre;
	private $apellidos;
	private $email;
	private $direccion;
	private $telefono;



	//constructor
	function constructor($login,$password,$dni,$nombre,$apellidos,$email,$direccion,$telefono)
	{		
		$this->login=$login;
		$this->password=$password;
		$this->dni=$dni;
		$this->nombre=$nombre;
		$this->apellidos=$apellidos;
		$this->email=$email;
		$this->direccion=$direccion;
		$this->telefono=$telefono;					
	}
	
	//funcion para conectar a la base de datos
	function conexionBD(){
		$mysqli=mysqli_connect("127.0.0.1","userET3","passET3","IUET32017");
		if(!$mysqli){
			echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
			echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
			echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
			exit;
		}
		return $mysqli;
	}
		
	function comprobarUsuario($login,$dni,$email)
	{
		$mysqli=$this->conexionBD();
		$query="SELECT * FROM `USUARIO` WHERE  `login`='$login' or  `DNI`='$dni'  or  `Correo`='$email' ";
		$resultado=$mysqli->query($query);
		if(mysqli_num_rows($resultado)){
			return true;
		}else{
			return false;
		}
	}

	function comprobarUsuarioADMINEnUSU_GRUPO($UserSesionActual)
	{
		$mysqli=$this->conexionBD();
		$query="SELECT * FROM `USU_GRUPO` WHERE  `login`='$UserSesionActual' and `IdGrupo`='ADMIN' ";
		$resultado=$mysqli->query($query);
		if(mysqli_num_rows($resultado)){
			return true;
		}else{
			return false;
		}
	}
	
	function comprobarLoginAcceso($user,$pass)
	{
		$mysqli=$this->conexionBD();
		$query="SELECT * FROM `USUARIO` WHERE  login='$user' AND password='$pass'";
		$resultado=$mysqli->query($query);
		if(mysqli_num_rows($resultado)){
			return true;	
		}else{
			return false;
		}
	}
	//Agrega un susario para usar la App
	function crearUsuarioRegistro($user,$pass,$email)
	{
		$mysqli=$this->conexionBD();
		$query="INSERT INTO `USUARIO`(`login`, `password`, `Correo`) VALUES ('$user','$pass','$email')";
		$resultado=$mysqli->query($query);
		$mysqli->close();
		return $resultado;
	}
	//Asigna un grupo por defecto a un usuario alumno
	function asignarGrupoDefecto($login,$grupoDefecto)
	{
		$mysqli=$this->conexionBD();
		$query="INSERT INTO `USU_GRUPO`(`login`, `IdGrupo`) VALUES ('$login','$grupoDefecto')";
		$resultado=$mysqli->query($query);
		$mysqli->close();
		return $resultado;
	}
	function listarUsers(){
		$mysqli=$this->conexionBD();
		$query="SELECT `login`, `DNI`, `Nombre`, `Apellidos`, `Correo`, `Direccion`, `Telefono` FROM `USUARIO` ";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}	    	
			return $filas;
		}
	}

	function showCurrent($login){
		$mysqli=$this->conexionBD();
		$query="SELECT `login`, `DNI`, `Nombre`, `Apellidos`, `Correo`, `Direccion`, `Telefono`  FROM `USUARIO` WHERE `login`='$login' ";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){
			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}	    	
			return $filas;
		}
	}

	//Busca el usuario por su login y devuelve los datos
	function buscarPorLogin($login){
		$mysqli=$this->conexionBD();
		$query="SELECT `login`, `password`, `DNI`, `Nombre`, `Apellidos`, `Correo`, `Direccion`, `Telefono` FROM `USUARIO` WHERE `login`='$login' ";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}    	
			return $filas;
		}
	}


	//Carga el array inicial con con los atributos del usuario (DNI de deportista, id, actividad,fecha de cuando se crea la sesion y asistencia)
	function crearArrayUsuarios()
	{
		$login=null;
		$DNI=null;		
		
		$file = fopen("../Locales/Archivos/ArrayConsultarUsuarios.php", "w");

		fwrite($file,"<?php class consultUsuario { function array_consultarUsuarios(){". PHP_EOL);
		fwrite($file,"\$form=array(" . PHP_EOL);

		$mysqli=$this->conexionBD();
		$resultado=$mysqli->query("SELECT * FROM `USUARIO`");
		if(mysqli_num_rows($resultado)){

			while($fila = $resultado->fetch_array())
			{
				$filas[] = $fila;
			}
			foreach($filas as $fila)
			{
				$login=$fila['login'];
				$DNI=$fila['DNI'];				

				fwrite($file,"array(
					\"login\"=>'$login',
					\"DNI\"=>'$DNI')," . PHP_EOL);
		 	}
		 	
		 	fwrite($file,");return \$form;}}?>". PHP_EOL);
		 	fclose($file);
		 	$resultado->free();
			$mysqli->close();
		}else{
		fwrite($file,")" . PHP_EOL);
		fwrite($file,";return \$form;}}?>". PHP_EOL);	
		fclose($file);
		}
	}

	
	//Crea una array de la tabla USU_GRUPO
	function crearArrayGrupos(){

		$this->conexionBD();
		$form=array();

		$query="SELECT * FROM `USU_GRUPO`";
		$mysqli=$this->conexionBD();
		$resultado=$mysqli->query($query);

		if(mysqli_num_rows($resultado)){

			while($fila = $resultado->fetch_array())
			{
				$filas[] = $fila;
			}
			foreach($filas as $fila)
			{
				$login=$fila['login'];
				$IdGrupo=$fila['IdGrupo'];

				$fila_array=array("login"=>$login,"IdGrupo"=>$IdGrupo);
				array_push($form,$fila_array);
			}
		}			
		$resultado->free();
		$mysqli->close();
		return $form;		
	}

	function RellenarArrayUsuGrupos($grupos){
	include("../Locales/Archivos/ArrayConsultarUsuarios.php");
	$arra=new consultUsuario();
	$form=$arra->array_consultarUsuarios();

	//creo el array con los grupos.
	$file = fopen("../Locales/Archivos/consultUsuGrupos.php", "w");
	fwrite($file,"<?php class consult { function array_consultarUsuGrupos(){". PHP_EOL);
	fwrite($file,"\$form=array(" . PHP_EOL);
	
	//Tala Deportista_reserva_actividad
	for ($numarT=0;$numarT<count($form);$numarT++){
		$login=$form[$numarT]["login"];
		$DNI=$form[$numarT]["DNI"];		
		
		//cargamos el fichero de Usuarios con login y DNI				
		fwrite($file,"array(\"login\"=>'$login',\"DNI\"=>'$DNI'," . PHP_EOL);
		//Datos Gestion Grupos
		//Obtengo el dni del entrenador de esa actividad
		
		for ($numarO=0;$numarO<count($grupos);$numarO++){

			if($login==$grupos[$numarO]["login"]){
			
			$IdGrupo=$grupos[$numarO]["IdGrupo"];
			
			fwrite($file,"
					\"IdGrupo$numarO\"=>'$IdGrupo',
					" . PHP_EOL);
			
			}
			
		}
			
		fwrite($file,")," . PHP_EOL);
	}
		fwrite($file,");return \$form;}}?>". PHP_EOL);
		fclose($file);				 				
	}



	function crearUser($login,$password,$dni,$nombre,$apellidos,$email,$direccion,$telefono){
			$mysqli=$this->conexionBD();
			$query="INSERT INTO `USUARIO`( `login`, `password`, `DNI`, `Nombre`, `Apellidos`, `Correo`, `Direccion`, `Telefono`) VALUES ('$login', '$password', '$dni', '$nombre', '$apellidos', '$email', '$direccion', '$telefono' )";
			$resultado=$mysqli->query($query);
			$mysqli->close();
			return $resultado;
	}


	function eliminarUser($login){
		$mysqli=$this->conexionBD();
		$query="DELETE FROM `USUARIO` WHERE `login`='$login'";
		// si se produce un error en la baja mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){
	    	return "error al eliminar";		
		}else{
			return $resultado;
				}
	}

	function modificarUser($login,$password,$dni,$nombre,$apellidos,$email,$direccion,$telefono){
		$mysqli=$this->conexionBD();		
		$query="UPDATE `USUARIO` SET `password`='$password',`DNI`='$dni', `Nombre`='$nombre', `Apellidos`='$apellidos', `Telefono`='$telefono', `Correo`='$email', `Direccion`='$direccion' WHERE `login`='$login'";
		 if (!($resultado = $mysqli->query($query))){
	    	return "error al modificar";		
		}else{
			return $resultado;
				}
	}

	

	function buscarUsuario($login,$dni,$nombre,$apellidos,$email,$direccion,$telefono){

 		$mysqli=$this->conexionBD();

 		$query="SELECT * FROM `USUARIO` ";
 		$primero=false;

 		if($email!=null ){
 		 $query .="WHERE `Correo` like '%$email%'";
 		 $primero=true;
 		}
 		if ($nombre!=null && $primero==false){
         $query .="WHERE `Nombre` like '%$nombre%'";
         $primero=true;
        }else if ($nombre!=null && $primero==true){
         $query .="or `Nombre` like '%$nombre%'";
         $primero=true;
        }
        if ($apellidos!=null && $primero==false){
         $query .="WHERE `Apellidos` like '%$apellidos%'";
         $primero=true;
        }else if ($apellidos!=null && $primero==true){
         $query .="or `Apellidos` like '%$apellidos%'";
         $primero=true;
        }        
        if ($login!=null && $primero==false){
         $query .="WHERE `login` like '%$login%'";        
         $primero=true;
        }else if ($login!=null && $primero==true){
         $query .="or `login` like '%$login%'";         
         $primero=true;
        }
        if ($dni!=null && $primero==false){
         $query .="WHERE  `DNI` like '%$dni%'";
         $primero=true;        
        }else if ($dni!=null && $primero==true){
         $query .="or `DNI` like '%$dni%'";
         $primero=true;
        }
        if ($direccion!=null && $primero==false){
         $query .="`WHERE Direccion` like '%$direccion%'";
         $primero=true;
        }else if ($direccion!=null && $primero==true){
         $query .=" or `Direccion` like '%$direccion%'";
         $primero=true;
        }       
        if ($telefono!=null && $primero==false){
         $query .="`WHERE Telefono` like '%$telefono%'";
         $primero=true;
        }else if ($telefono!=null && $primero==true){
         $query .=" or `Telefono` like '%$telefono%'";
         $primero=true;
        }

		if (!($resultado = $mysqli->query($query))){

		return 'Error en la consulta sobre la base de datos';
		}
		else{ // si la busqueda es correcta devolvemos el recordset resultado
    	$filas=null;
    		while($fila = $resultado->fetch_array())
				{
					$filas[] = $fila;
				}
    	
		return $filas;
		}

 	}

 	function AsignarUserAGrupo($login,$IdGrupo){
 		$mysqli=$this->conexionBD();
		$query="INSERT INTO `USU_GRUPO`( `login`, `IdGrupo`) VALUES ('$login', '$IdGrupo')";
		$resultado=$mysqli->query($query);
		$mysqli->close();
		return $resultado;
 	}

	function GruposDiferentes(){
	$mysqli=$this->conexionBD();
	$query="SELECT DISTINCT `IdGrupo` FROM `GRUPO`";
	 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
    if (!($resultado = $mysqli->query($query))){

		return 'Error en la consulta sobre la base de datos';
	}
    else{ // si la busqueda es correcta devolvemos el recordset resultado
    	$filas=null;
    		while($fila = $resultado->fetch_array())
				{
					$filas[] = $fila;
				}    	
		return $filas;
		}
	}


}
?>


