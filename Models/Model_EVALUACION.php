<?php

/**
 * Description of Model_EVALUACION
 *
 * @author pablo
 */

class Model_EVALUACION {
    var $IdTrabajo;
    var $LoginEvaluador;
    var $AliasEvaluado;
    var $IdHistoria;
    var $CorrectoA;
	var $ComenIncorrectoA;
	var $CorrectoP;
	var $ComenIncorrectoP;
	var $OK;
    
    function __construct($IdTrabajo,$LoginEvaluador,$AliasEvaluado,$IdHistoria,$CorrectoA,$ComenIncorrectoA,$CorrectoP,$ComenIncorrectoP,$OK) {
        $this->IdTrabajo=$IdTrabajo;
        $this->LoginEvaluador=$LoginEvaluador;
        $this->AliasEvaluado=$AliasEvaluado;
        $this->IdHistoria=$IdHistoria;
        $this->CorrectoA=$CorrectoA;
		$this->ComenIncorrectoA=$ComenIncorrectoA;
        $this->CorrectoP=$CorrectoP;
        $this->ComenIncorrectoP=$ComenIncorrectoP;
        $this->OK=$OK;
    }

    function conexionBD(){
	$mysqli=mysqli_connect("localhost","userET3","passET3","IUET32017");
	if(!$mysqli){
            echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
            exit;
	}
	return $mysqli;
    }
    
    function comprobarEvaluacion(){
	$mysqli=$this->conexionBD();
        
	$query="SELECT * FROM EVALUACION WHERE IdTrabajo='$this->IdTrabajo' AND LoginEvaluador='$this->LoginEvaluador' AND AliasEvaluado='$this->AliasEvaluado' AND IdHistoria='$this->IdHistoria' ";
	$resultado=$mysqli->query($query);
	if(mysqli_num_rows($resultado)){
            return true;
	}
        else{
            return false;
	}
	}

		
    function insert(){
        $mysqli=$this->conexionBD();
        
        $query="INSERT INTO EVALUACION(IdTrabajo,LoginEvaluador,AliasEvaluado,IdHistoria,CorrectoA,ComenIncorrectoA,CorrectoP,ComenIncorrectoP,OK) VALUES 
		('$this->IdTrabajo','$this->LoginEvaluador','$this->AliasEvaluado','$this->IdHistoria','$this->CorrectoA','$this->ComenIncorrectoA',
		'$this->CorrectoP','$this->ComenIncorrectoP','$this->OK')";
        $toret=$mysqli->query($query);
        $mysqli->close();
        return $toret;
    }
    
    function delete(){
        $mysqli=$this->conexionBD();
        
        $query="DELETE FROM EVALUACION WHERE IdTrabajo='$this->IdTrabajo' AND LoginEvaluador='$this->LoginEvaluador' AND AliasEvaluado='$this->AliasEvaluado' AND IdHistoria='$this->IdHistoria'";
        if ($toret=$mysqli->query($query)){
	    return $toret;		
	}
        else{
            return "Error al borrar";
	}
    }
    
    function update(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM EVALUACION WHERE IdTrabajo='$this->IdTrabajo' AND LoginEvaluador='$this->LoginEvaluador' AND AliasEvaluado='$this->AliasEvaluado' AND IdHistoria='$this->IdHistoria'";
        $resultado=$mysqli->query($query);
        $tupla=$resultado->fetch_row();
        if($this->CorrectoA==""){
            $CorrectoA=$tupla[2];
        }
        else{
            $CorrectoA=$this->CorrectoA;
        }
        if($this->ComenIncorrectoA==""){
            $ComenIncorrectoA=$tupla[3];
        }
        else{
            $ComenIncorrectoA=$this->ComenIncorrectoA;
        }
		if($this->CorrectoP==""){
            $CorrectoP=$tupla[2];
        }
        else{
            $CorrectoP=$this->CorrectoP;
        }
        if($this->ComenIncorrectoP==""){
            $ComenIncorrectoP=$tupla[3];
        }
        else{
            $ComenIncorrectoP=$this->ComenIncorrectoP;
        }
        if($this->OK==""){
            $OK=$tupla[4];
        }
        else{
            $OK=$this->OK;
        }
        
        $query="UPDATE EVALUACION SET CorrectoA='$CorrectoA',ComenIncorrectoA='$ComenIncorrectoA',CorrectoP='$CorrectoP',
		ComenIncorrectoP='$ComenIncorrectoP',OK='$OK' WHERE IdTrabajo='$this->IdTrabajo' AND LoginEvaluador='$this->LoginEvaluador' AND AliasEvaluado='$this->AliasEvaluado' AND IdHistoria='$this->IdHistoria'";
	$resultado=$mysqli->query($query);
	$mysqli->close();
	return $resultado;
    }
    
    function select(){
        $mysqli=$this->conexionBD();
        
        if($this->IdTrabajo=="" && $this->LoginEvaluador=="" && $this->AliasEvaluado=="" && $this->IdHistoria==""){
            $query="SELECT * FROM ENTREGA";
        }
        else{
            $query="SELECT * FROM ENTREGA WHERE ";
            $unico=TRUE;
            if($this->IdTrabajo<>""){
                $query.="IdTrabajo LIKE '%$this->IdTrabajo%'";
                $unico=FALSE;
            }
            if($this->LoginEvaluador<>""){
                if(!$unico){
                    $query.=" AND ";
                }
                $query.="LoginEvaluador LIKE '%$this->LoginEvaluador%'";
                $segundo=FALSE;
            }
            if($this->AliasEvaluado<>""){
                if(!$unico){
                    $query.=" AND ";
                }
                $query.="AliasEvaluado LIKE '%$this->AliasEvaluado%'";
            }
			
			if($this->IdHistoria<>""){
                if(!$unico){
                    $query.=" AND ";
                }
                $query.="IdHistoria LIKE '%$this->IdHistoria%'";
            }
        }
        if (!($resultado = $mysqli->query($query))){
                return 'Error en la consulta sobre la base de datos';
            }
        else{
            return $resultado;
        }
    }
    
    function showAll(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM EVALUACION";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
	
    function showCurrent(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM EVALUACION WHERE IdTrabajo='$this->IdTrabajo' AND LoginEvaluador='$this->LoginEvaluador' AND AliasEvaluado='$this->AliasEvaluado' AND IdHistoria='$this->IdHistoria'";
        if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
            $evaluacion=$resultado->fetch_row();
	    return $evaluacion;
	}
    }
    
    function trabajos(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT IdTrabajo,NombreTrabajo FROM TRABAJO";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function historias(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT IdHistoria FROM HISTORIA";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
	
	//LOGIN EVALUADOR Y ALIAS EVALUADO (?)
}
