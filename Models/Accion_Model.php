<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es el modelo de acción, por lo tanto
 * se encarga de conectar con la base de datos y realizar las
 * sentencias necesarias en SQL para llevar a cabo todas las operaciones posibles.
 */
class Accion_Model
{
    private $IdAccion;
    private $NombreAccion;
    private $DescripAccion;

    //constructor
    function constructor($IdAccion,$NombreAccion,$DescripAccion)
    {
        $this->IdAccion=$IdAccion;
        $this->NombreAccion=$NombreAccion;
        $this->DescripAccion=$DescripAccion;
    }
    //funcion para conectar a la base de datos
    function conexionBD(){
        $mysqli=mysqli_connect("localhost","userET3","passET3","IUET32017");
        if(!$mysqli){
            echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
        return $mysqli;
    }

    function comprobarAccion($IdAccion,$NombreAccion)
    {
        $mysqli=$this->conexionBD();
        $query="SELECT * FROM `ACCION` WHERE  `IdAccion`='$IdAccion' or  `NombreAccion`='$NombreAccion'";
        $resultado=$mysqli->query($query);
        if(mysqli_num_rows($resultado)){
            return true;
        }else{
            return false;
        }
    }

    /*function comprobarLoginAcceso($user,$pass)
    {
        $mysqli=$this->conexionBD();
        $query="SELECT * FROM `USUARIO` WHERE  login='$user' AND password='$pass'";
        $resultado=$mysqli->query($query);
        if(mysqli_num_rows($resultado)){
            return true;
        }else{
            return false;
        }
    }*/
    //Agrega un susario para usar la App
    /*function crearUsuarioRegistro($user,$pass,$email)
    {
        $mysqli=$this->conexionBD();
        $query="INSERT INTO `USUARIO`(`login`, `password`, `email`) VALUES ('$user','$pass','$email')";
        $resultado=$mysqli->query($query);
        $mysqli->close();
        return $resultado;
    }*/

    function listarAcciones(){
        $mysqli=$this->conexionBD();
        $query="SELECT `IdAccion`, `NombreAccion`, `DescripAccion` FROM `ACCION` ";
        // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if (!($resultado = $mysqli->query($query))){

            return 'Error en la consulta sobre la base de datos';
        }
        else{ // si la busqueda es correcta devolvemos el recordset resultado
            $filas=null;
            while($fila = $resultado->fetch_array())
            {
                $filas[] = $fila;
            }
            return $filas;
        }
    }

    function showCurrent($IdAccion){
        $mysqli=$this->conexionBD();
        $query="SELECT `IdAccion`, `NombreAccion`, `DescripAccion`FROM `ACCION` WHERE `IdAccion`='$IdAccion' ";
        // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
        }
        else{ // si la busqueda es correcta devolvemos el recordset resultado
            $filas=null;
            while($fila = $resultado->fetch_array())
            {
                $filas[] = $fila;
            }
            return $filas;
        }
    }

    //Busca el usuario por su login y devuelve los datos
    function buscarPorId($IdAccion){
        $mysqli=$this->conexionBD();
        $query="SELECT `IdAccion`, `NombreAccion`, `DescripAccion`FROM `ACCION` WHERE `IdAccion`='$IdAccion' ";
        // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if (!($resultado = $mysqli->query($query))){

            return 'Error en la consulta sobre la base de datos';
        }
        else{ // si la busqueda es correcta devolvemos el recordset resultado
            $filas=null;
            while($fila = $resultado->fetch_array())
            {
                $filas[] = $fila;
            }
            return $filas;
        }
    }


    function crearAccion($IdAccion,$NombreAccion,$DescripAccion){
        $mysqli=$this->conexionBD();
        $query="INSERT INTO `ACCION`( `IdAccion`, `NombreAccion`, `DescripAccion`) VALUES ('$IdAccion','$NombreAccion', '$DescripAccion')";
        $resultado=$mysqli->query($query);
        $mysqli->close();
        return $resultado;
    }


    function eliminarAccion($IdAccion){
        $mysqli=$this->conexionBD();
        $query="DELETE FROM `ACCION` WHERE `IdAccion`='$IdAccion'";
        // si se produce un error en la baja mandamos el mensaje de error en la consulta
        if (!($resultado = $mysqli->query($query))){
            return "error al eliminar";
        }else{
            return $resultado;
        }
    }

    function modificarAccion($IdAccion,$NombreAccion,$DescripAccion){
        $mysqli=$this->conexionBD();

        /*if($fotopersonal==""){
            $query="UPDATE `USUARIO` SET `password`='$password',`DNI`='$dni', `nombre`='$nombre', `apellidos`='$apellidos', `telefono`='$telefono', `email`='$email', `FechaNacimiento`='$FechaNacimiento', `sexo`='$sexo' WHERE `login`='$login'";
            $resultado=$mysqli->query($query);
            $mysqli->close();
            return $resultado;
        }else{*/
        $query="UPDATE `ACCION` SET `NombreAccion`='$NombreAccion',`DescripAccion`='$DescripAccion' WHERE `IdAccion`='$IdAccion'";
        $resultado=$mysqli->query($query);
        $mysqli->close();
        return $resultado;

    }

    //}


    function buscarAccion($IdAccion,$NombreAccion,$DescripAccion)
    {

        $mysqli = $this->conexionBD();

        $query = "SELECT * FROM `ACCION` ";
        $primero = false;

        if ($NombreAccion != null) {
            $query .= "WHERE `NombreAccion` like '%$NombreAccion%'";
            $primero = true;
        }
        if ($IdAccion != null && $primero == false) {
            $query .= "WHERE `IdAccion` like '%$IdAccion%'";
            $primero = true;
        } else if ($IdAccion != null && $primero == true) {
            $query .= "or `IdAccion` like '%$IdAccion%'";
            $primero = true;
        }
        if ($DescripAccion != null && $primero == false) {
            $query .= "WHERE DescripAccion` like '%$DescripAccion%'";
            $primero = true;
        } else if ($DescripAccion != null && $primero == true) {
            $query .= "or `DescripAccion` like '%$DescripAccion%'";
            $primero = true;
        }

        if (!($resultado = $mysqli->query($query))) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado
            $filas = null;
            while ($fila = $resultado->fetch_array()) {
                $filas[] = $fila;
            }

            return $filas;
        }
    }
}