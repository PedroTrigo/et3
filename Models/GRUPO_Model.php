<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es el modelo de grupo, por lo tanto
 * se encarga de conectar con la base de datos y realizar las
 * sentencias necesarias en SQL para llevar a cabo todas las operaciones posibles.
 */

class Grupo_Model	{	
	private $IdGrupo;
	private $NombreGrupo;
	private $DescripGrupo;
	//constructor
	function constructor($IdGrupo,$NombreGrupo,$DescripGrupo)
	{		
		$this->IdGrupo=$IdGrupo;
		$this->NombreGrupo=$NombreGrupo;
		$this->DescripGrupo=$DescripGrupo;
	
	}
	
	//funcion para conectar a la base de datos
	function conexionBD(){
		$mysqli=mysqli_connect("127.0.0.1","userET3","passET3","IUET32017");
		if(!$mysqli){
			echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
			echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
			echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
			exit;
		}
		return $mysqli;
	}
		
	function comprobarGrupo($IdGrupo)
	{
		$mysqli=$this->conexionBD();
		$query="SELECT * FROM `GRUPO` WHERE `IdGrupo`='$IdGrupo'";
		$resultado=$mysqli->query($query);
		if(mysqli_num_rows($resultado)){
			return true;
		}else{
			return false;
		}
	}

	function listarGrupos(){
		$mysqli=$this->conexionBD();
		$query="SELECT `IdGrupo`, `NombreGrupo`, `DescripGrupo` FROM `GRUPO` ";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}	    	
			return $filas;
		}
	}

	//Busca las historias por trabajo al que pertenecen
	function buscarPorIdGrupo($IdGrupo){
		$mysqli=$this->conexionBD();
		$query="SELECT `IdGrupo`, `NombreGrupo`, `DescripGrupo` FROM `GRUPO` WHERE `IdGrupo`='$IdGrupo'";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}    	
			return $filas;
		}
	}
	
	function crearGrupo($IdGrupo,$NombreGrupo,$DescripGrupo)
	{
		$mysqli=$this->conexionBD();
		$query="INSERT INTO `GRUPO`(`IdGrupo`, `NombreGrupo`, `DescripGrupo`) VALUES ('$IdGrupo','$NombreGrupo','$DescripGrupo')";
		$resultado=$mysqli->query($query);
		$mysqli->close();
		return $resultado;
	}

	function crearGrupoUSU_GRUPO($login,$IdGrupo)
	{
		$mysqli=$this->conexionBD();
		$query="INSERT INTO `USU_GRUPO`(`login`,`IdGrupo`) VALUES ('$login','$IdGrupo')";
		$resultado=$mysqli->query($query);
		$mysqli->close();
		return $resultado;
	}
	
	function eliminarGrupo($IdGrupo){
		$mysqli=$this->conexionBD();
		$query="DELETE FROM `GRUPO` WHERE `IdGrupo`='$IdGrupo'";
		// si se produce un error en la baja mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){
	    	return "error al eliminar";		
		}else{
			return $resultado;
				}
	}

	function modificarGrupo($IdGrupo,$NombreGrupo,$DescripGrupo){
		$mysqli=$this->conexionBD();		
		$query="UPDATE `GRUPO` SET `NombreGrupo`='$NombreGrupo', `DescripGrupo`='$DescripGrupo' WHERE `IdGrupo`='$IdGrupo'";
		 if (!($resultado = $mysqli->query($query))){
	    	return "error al modificar";		
		}else{
			return $resultado;
				}
	}

	function buscarGrupo($IdGrupo,$NombreGrupo,$DescripGrupo){

 		$mysqli=$this->conexionBD();

 		$query="SELECT * FROM `GRUPO` ";
 		$primero=false;

 		if($IdGrupo!=null ){
 		 $query .="WHERE `IdGrupo` like '%$IdGrupo%'";
 		 $primero=true;
 		}
 		if ($NombreGrupo!=null && $primero==false){
         $query .="WHERE `NombreGrupo` like '%$NombreGrupo%'";
         $primero=true;
        }else if ($DescripGrupo!=null && $primero==true){
         $query .="or `DescripGrupo` like '%$DescripGrupo%'";
         $primero=true;
        }

		if (!($resultado = $mysqli->query($query))){

		return 'Error en la consulta sobre la base de datos';
		}
		else{ // si la busqueda es correcta devolvemos el recordset resultado
    	$filas=null;
    		while($fila = $resultado->fetch_array())
				{
					$filas[] = $fila;
				}
    	
		return $filas;
		}

 	}





}
?>


