<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es el modelo de funcionalidad, por lo tanto
 * se encarga de conectar con la base de datos y realizar las
 * sentencias necesarias en SQL para llevar a cabo todas las operaciones posibles.
 */

class FUNCIONALIDAD_Model{
    var $IdFuncionalidad;
    var $NombreFuncionalidad;
    var $DescripFuncionalidad;
    
    function __construct($IdFuncionalidad,$NombreFuncionalidad,$DescripFuncionalidad){
        $this->IdFuncionalidad=$IdFuncionalidad;
        $this->NombreFuncionalidad=$NombreFuncionalidad;
        $this->DescripFuncionalidad=$DescripFuncionalidad;
    }

    function conexionBD(){
	$mysqli=mysqli_connect("localhost","userET3","passET3","IUET32017");
	if(!$mysqli){
            echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
            exit;
	}
	return $mysqli;
    }
    
    function comprobarFuncionalidad(){
	$mysqli=$this->conexionBD();
	$query="SELECT * FROM FUNCIONALIDAD WHERE  NombreFuncionalidad='$this->NombreFuncionalidad' OR IdFuncionalidad='$this->IdFuncionalidad'";
	$resultado=$mysqli->query($query);
	if(mysqli_num_rows($resultado)){
            return true;
	}
        else{
            return false;
	}
    }
    
    function insert(){
        $mysqli=$this->conexionBD();
        
        $query="INSERT INTO FUNCIONALIDAD(IdFuncionalidad,NombreFuncionalidad,DescripFuncionalidad) VALUES ('$this->IdFuncionalidad','$this->NombreFuncionalidad','$this->DescripFuncionalidad')";
        $toret=$mysqli->query($query);
        $mysqli->close();
        return $toret;
    }
    
    function delete(){
        $mysqli=$this->conexionBD();
        
        $query="DELETE FROM FUNCIONALIDAD WHERE IdFuncionalidad='$this->IdFuncionalidad'";
        if ($toret=$mysqli->query($query)){
	    return $toret;		
	}
        else{
            return "Error al borrar";
	}
    }
    
    function update(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM FUNCIONALIDAD WHERE IdFuncionalidad='$this->IdFuncionalidad'";
        $resultado=$mysqli->query($query);
        $tupla=$resultado->fetch_row();
        if($this->NombreFuncionalidad==""){
            $NombreFuncionalidad=$tupla[1];
        }
        else{
            $NombreFuncionalidad=$this->NombreFuncionalidad;
        }
        if($this->DescripFuncionalidad==""){
            $DescripFuncionalidad=$tupla[2];
        }
        else{
            $DescripFuncionalidad=$this->DescripFuncionalidad;
        }
        
        $query="UPDATE FUNCIONALIDAD SET NombreFuncionalidad='$NombreFuncionalidad',DescripFuncionalidad='$DescripFuncionalidad' WHERE IdFuncionalidad='$this->IdFuncionalidad'";
	$resultado=$mysqli->query($query);
	$mysqli->close();
	return $resultado;
    }
    
    function select(){
        $mysqli=$this->conexionBD();
        
        if($this->NombreFuncionalidad=="" && $this->IdFuncionalidad==""){
            $query="SELECT * FROM FUNCIONALIDAD";
        }
        else{
            $query="SELECT * FROM FUNCIONALIDAD WHERE ";
            $segundo=FALSE;
            if($this->NombreFuncionalidad<>""){
                $query.="NombreFuncionalidad LIKE '%$this->NombreFuncionalidad%'";
                $segundo=TRUE;
            }
            if($this->IdFuncionalidad<>""){
                if($segundo){
                    $query.=" AND ";
                }
                $query.="IdFuncionalidad LIKE '%$this->IdFuncionalidad%'";
                $segundo=TRUE;
            }
        }
        if (!($resultado = $mysqli->query($query))){
                return 'Error en la consulta sobre la base de datos';
            }
        else{
            return $resultado;
        }
    }
    
    function showAll(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM FUNCIONALIDAD";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function showCurrent(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM FUNCIONALIDAD WHERE IdFuncionalidad='$this->IdFuncionalidad'";
        if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
            $funcionalidad=$resultado->fetch_row();
	    return $funcionalidad;
	}
    }
    
    function accionesPorAsignar(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT IdAccion,NombreAccion FROM ACCION WHERE NOT IdAccion IN (SELECT IdAccion FROM FUNC_ACCION WHERE IdFuncionalidad='$this->IdFuncionalidad')";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function asignarAccion($IdAccion){
        $mysqli=$this->conexionBD();
        
        $query="INSERT INTO FUNC_ACCION(IdFuncionalidad,IdAccion) VALUES ('$this->IdFuncionalidad','$IdAccion')";
        $toret=$mysqli->query($query);
        $mysqli->close();
        return $toret;
    }
    
    function desasignarAccion($IdAccion){
        $mysqli=$this->conexionBD();
        
        $query="DELETE FROM FUNC_ACCION WHERE IdFuncionalidad='$this->IdFuncionalidad' AND IdAccion='$IdAccion'";
        if ($toret=$mysqli->query($query)){
	    return $toret;		
	}
        else{
            return "Error al borrar";
	}
    }
    
    function showAllAccion(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM FUNC_ACCION";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function showCurrentAccion($IdAccion){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM ACCION WHERE IdAccion='$IdAccion'";
        if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
            $funcionalidad=$resultado->fetch_row();
	    return $funcionalidad;
	}
    }
}
