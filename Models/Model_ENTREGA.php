<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es el modelo de entrega, por lo tanto
 * se encarga de conectar con la base de datos y realizar las
 * sentencias necesarias en SQL para llevar a cabo todas las operaciones posibles.
 */

class Model_ENTREGA {
    var $login;
    var $IdTrabajo;
    var $Alias;
    var $Horas;
    var $Ruta;
    
    function __construct($login,$IdTrabajo,$Alias,$Horas,$Ruta) {
        $this->login=$login;
        $this->IdTrabajo=$IdTrabajo;
        $this->Alias=$Alias;
        $this->Horas=$Horas;
        $this->Ruta=$Ruta;
    }

    function conexionBD(){
	$mysqli=mysqli_connect("localhost","userET3","passET3","IUET32017");
	if(!$mysqli){
            echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
            exit;
	}
	return $mysqli;
    }
    
    function trabajosPorEntregar(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT IdTrabajo,NombreTrabajo FROM TRABAJO WHERE NOT IdTrabajo IN (SELECT IdTrabajo FROM ENTREGA WHERE login='$this->login')";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function comprobarEntrega(){
	$mysqli=$this->conexionBD();
        
	$query="SELECT * FROM ENTREGA WHERE login='$this->login' AND IdTrabajo='$this->IdTrabajo'";
	$resultado=$mysqli->query($query);
	if(mysqli_num_rows($resultado)){
            return true;
	}
        else{
            return false;
	}
    }
    
    function insert(){
        $mysqli=$this->conexionBD();
        
        $query="INSERT INTO ENTREGA(login,IdTrabajo,Alias,Horas,Ruta) VALUES ('$this->login','$this->IdTrabajo','$this->Alias','$this->Horas','$this->Ruta')";
        $toret=$mysqli->query($query);
        $mysqli->close();
        return $toret;
    }
    
    function delete(){
        $mysqli=$this->conexionBD();
        
        $query="DELETE FROM ENTREGA WHERE login='$this->login' AND IdTrabajo='$this->IdTrabajo'";
        if ($toret=$mysqli->query($query)){
	    return $toret;		
	}
        else{
            return "Error al borrar";
	}
    }
    
    function update(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM ENTREGA WHERE login='$this->login' AND IdTrabajo='$this->IdTrabajo'";
        $resultado=$mysqli->query($query);
        $tupla=$resultado->fetch_row();
        if($this->Alias==""){
            $Alias=$tupla[2];
        }
        else{
            $Alias=$this->Alias;
        }
        if($this->Horas==""){
            $Horas=$tupla[3];
        }
        else{
            $Horas=$this->Horas;
        }
        if($this->Ruta==""){
            $Ruta=$tupla[4];
        }
        else{
            $Ruta=$this->Ruta;
        }
        
        $query="UPDATE ENTREGA SET Alias='$Alias',Horas='$Horas',Ruta='$Ruta' WHERE login='$this->login' AND IdTrabajo='$this->IdTrabajo'";
	$resultado=$mysqli->query($query);
	$mysqli->close();
	return $resultado;
    }
    
    function select(){
        $mysqli=$this->conexionBD();
        
        if($this->login=="" && $this->IdTrabajo=="" && $this->Alias==""){
            $query="SELECT * FROM ENTREGA";
        }
        else{
            $query="SELECT * FROM ENTREGA WHERE ";
            $unico=TRUE;
            if($this->login<>""){
                $query.="login LIKE '%$this->login%'";
                $unico=FALSE;
            }
            if($this->IdTrabajo<>""){
                if(!$unico){
                    $query.=" AND ";
                }
                $query.="IdTrabajo LIKE '%$this->IdTrabajo%'";
                $segundo=FALSE;
            }
            if($this->Alias<>""){
                if(!$unico){
                    $query.=" AND ";
                }
                $query.="Alias LIKE '%$this->Alias%'";
            }
        }
        if (!($resultado = $mysqli->query($query))){
                return 'Error en la consulta sobre la base de datos';
            }
        else{
            return $resultado;
        }
    }
    
    function showAll(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM ENTREGA";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function showCurrent(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM ENTREGA WHERE login='$this->login' AND IdTrabajo='$this->IdTrabajo'";
        if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
            $entrega=$resultado->fetch_row();
	    return $entrega;
	}
    }
    
    function trabajos(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT IdTrabajo,NombreTrabajo FROM TRABAJO";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function usuarios(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT login,Nombre,Apellidos FROM USUARIO";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
}
