<!-- 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es el modelo de nota, por lo tanto
 * se encarga de conectar con la base de datos y realizar las
 * sentencias necesarias en SQL para llevar a cabo todas las operaciones posibles.
 */ -->


<?php 

class Nota_Model	{	
	private $login;
	private $IdTrabajo;
	private $NotaTrabajo;


	//constructor
	function constructor($login,$IdTrabajo,$NotaTrabajo)
	{		
		$this->login=$login;
		$this->IdTrabajo=$IdTrabajo;
		$this->NotaTrabajo=$NotaTrabajo;
	
	}
	
	//funcion para conectar a la base de datos
	function conexionBD(){
		$mysqli=mysqli_connect("127.0.0.1","userET3","passET3","IUET32017");
		if(!$mysqli){
			echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
			echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
			echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
			exit;
		}
		return $mysqli;
	}
		
	function comprobarNota($login, $IdTrabajo)
	{
		$mysqli=$this->conexionBD();
		$query="SELECT * FROM `NOTA_TRABAJO` WHERE  `login`='$login' AND  `IdTrabajo`='$IdTrabajo'";
		$resultado=$mysqli->query($query);
		if(mysqli_num_rows($resultado)){
			return true;
		}else{
			return false;
		}
	}
	
	/*function comprobarLoginAcceso($user,$pass)
	{
		$mysqli=$this->conexionBD();
		$query="SELECT * FROM `USUARIO` WHERE  login='$user' AND password='$pass'";
		$resultado=$mysqli->query($query);
		if(mysqli_num_rows($resultado)){
			return true;	
		}else{
			return false;
		}
	}*/
	//Agrega un susario para usar la App
	
	function listarNotas(){
		$mysqli=$this->conexionBD();
		$query="SELECT `login`, `IdTrabajo`, `NotaTrabajo` FROM `NOTA_TRABAJO` ";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}	    	
			return $filas;
		}
	}

	/* SHOW CURRENT (?)
	
	function showCurrent($login){
		$mysqli=$this->conexionBD();
		$query="SELECT `login`, `DNI`, `Nombre`, `Apellidos`, `Correo`, `Direccion`, `Telefono`  FROM `USUARIO` WHERE `login`='$login' ";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){
			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}	    	
			return $filas;
		}
	}*/

	//Busca las notas por login
	function buscarNotaporLogin($login){
		$mysqli=$this->conexionBD();
		$query="SELECT `login`, `IdTrabajo`, `NotaTrabajo` FROM `NOTA_TRABAJO` WHERE `login`='$login'";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}    	
			return $filas;
		}
	}
	
	//Busca las notas por trabajo al que pertenecen
	function buscarPorIdTrabajo($IdTrabajo){
		$mysqli=$this->conexionBD();
		$query="SELECT `login`, `IdTrabajo`, `NotaTrabajo` FROM `NOTA_TRABAJO` WHERE `IdTrabajo`='$IdTrabajo'";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}    	
			return $filas;
		}
	}
	
	function buscarPorIdsNota($login, $IdTrabajo){
		$mysqli=$this->conexionBD();
		$query="SELECT `login`, `IdTrabajo`, `NotaTrabajo` FROM `NOTA_TRABAJO` WHERE `login`='$login' AND `IdTrabajo`='$IdTrabajo'";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
		if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}    	
			return $filas;
		}
	}

	function crearNota($login,$IdTrabajo,$NotaTrabajo)
	{
		$mysqli=$this->conexionBD();
		$query="INSERT INTO `NOTA_TRABAJO`(`login`, `IdTrabajo`, `NotaTrabajo`) VALUES ('$login','$IdTrabajo','$NotaTrabajo')";
		$resultado=$mysqli->query($query);
		$mysqli->close();
		return $resultado;
	}
	
	function eliminarNota($IdTrabajo){
		$mysqli=$this->conexionBD();
		$query="DELETE FROM `NOTA_TRABAJO` WHERE `IdTrabajo`='$IdTrabajo'";
		// si se produce un error en la baja mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){
	    	return "error al eliminar";		
		}else{
			return $resultado;
				}
	}

	function modificarNota($login,$IdTrabajo,$NotaTrabajo){
		$mysqli=$this->conexionBD();		
		$query="UPDATE `NOTA_TRABAJO` SET `NotaTrabajo`='$NotaTrabajo' WHERE `login`='$login' AND `IdTrabajo`='$IdTrabajo'";
		 if (!($resultado = $mysqli->query($query))){
	    	return "error al modificar";		
		}else{
			return $resultado;
				}
	}

	function buscarNota($login,$IdTrabajo,$NotaTrabajo){

 		$mysqli=$this->conexionBD();

 		$query="SELECT * FROM `NOTA_TRABAJO` ";
 		$primero=false;

 		if($login!=null ){
 		 $query .="WHERE `login` like '%$login%'";
 		 $primero=true;
 		}
 		if ($IdTrabajo!=null && $primero==false){
         $query .="WHERE `IdTrabajo` like '%$IdTrabajo%'";
         $primero=true;
        }else if ($NotaTrabajo!=null && $primero==true){
         $query .="or `NotaTrabajo` like '%$NotaTrabajo%'";
         $primero=true;
        }

		if (!($resultado = $mysqli->query($query))){

		return 'Error en la consulta sobre la base de datos';
		}
		else{ // si la busqueda es correcta devolvemos el recordset resultado
    	$filas=null;
    		while($fila = $resultado->fetch_array())
				{
					$filas[] = $fila;
				}
    	
		return $filas;
		}

 	}





}
?>


