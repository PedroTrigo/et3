<?php

/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es el modelo de trabajo, por lo tanto
 * se encarga de conectar con la base de datos y realizar las
 * sentencias necesarias en SQL para llevar a cabo todas las operaciones posibles.
 */
class Trabajo_Model
{
    private $IdTrabajo;
    private $NombreTrabajo;
    private $FechaIniTrabajo;
    private $FechaFinTrabajo;
    private $PorcentajeNota;

    //constructor
    function constructor($IdTrabajo,$NombreTrabajo,$FechaIniTrabajo,$FechaFinTrabajo,$PorcentajeNota)
    {
        $this->IdTrabajo=$IdTrabajo;
        $this->NombreTrabajo=$NombreTrabajo;
        $this->FechaIniTrabajo=$FechaIniTrabajo;
        $this->FechaFinTrabajo=$FechaFinTrabajo;
        $this->PorcentajeNota=$PorcentajeNota;
    }
    //funcion para conectar a la base de datos
    function conexionBD(){
        $mysqli=mysqli_connect("localhost","userET3","passET3","IUET32017");
        if(!$mysqli){
            echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
        return $mysqli;
    }

    function comprobarTrabajo($IdTrabajo,$NombreTrabajo)
    {
        $mysqli=$this->conexionBD();
        $query="SELECT * FROM `TRABAJO` WHERE  `IdTrabajo`='$IdTrabajo' or  `NombreTrabajo`='$NombreTrabajo'";
        $resultado=$mysqli->query($query);
        if(mysqli_num_rows($resultado)){
            return true;
        }else{
            return false;
        }
    }

    /*function comprobarLoginAcceso($user,$pass)
    {
        $mysqli=$this->conexionBD();
        $query="SELECT * FROM `USUARIO` WHERE  login='$user' AND password='$pass'";
        $resultado=$mysqli->query($query);
        if(mysqli_num_rows($resultado)){
            return true;
        }else{
            return false;
        }
    }*/
    //Agrega un susario para usar la App
    /*function crearUsuarioRegistro($user,$pass,$email)
    {
        $mysqli=$this->conexionBD();
        $query="INSERT INTO `USUARIO`(`login`, `password`, `email`) VALUES ('$user','$pass','$email')";
        $resultado=$mysqli->query($query);
        $mysqli->close();
        return $resultado;
    }*/

    function listarTrabajos(){
        $mysqli=$this->conexionBD();
        $query="SELECT `IdTrabajo`, `NombreTrabajo`, `FechaIniTrabajo`, `FechaFinTrabajo`, `PorcentajeNota` FROM `TRABAJO` ";
        // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if (!($resultado = $mysqli->query($query))){

            return 'Error en la consulta sobre la base de datos';
        }
        else{ // si la busqueda es correcta devolvemos el recordset resultado
            $filas=null;
            while($fila = $resultado->fetch_array())
            {
                $filas[] = $fila;
            }
            return $filas;
        }
    }

    function showCurrent($IdTrabajo){
        $mysqli=$this->conexionBD();
        $query="SELECT `IdTrabajo`, `NombreTrabajo`, `FechaIniTrabajo`, `FechaFinTrabajo`, `PorcentajeNota`  FROM `TRABAJO` WHERE `Idtrabajo`='$IdTrabajo' ";
        // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
        }
        else{ // si la busqueda es correcta devolvemos el recordset resultado
            $filas=null;
            while($fila = $resultado->fetch_array())
            {
                $filas[] = $fila;
            }
            return $filas;
        }
    }

    //Busca el usuario por su login y devuelve los datos
    function buscarPorId($IdTrabajo){
        $mysqli=$this->conexionBD();
        $query="SELECT `IdTrabajo`, `NombreTrabajo`, `FechaIniTrabajo`, `FechaFinTrabajo`, `PorcentajeNota`  FROM `TRABAJO` WHERE `IdTrabajo`='$IdTrabajo' ";
        // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
        if (!($resultado = $mysqli->query($query))){

            return 'Error en la consulta sobre la base de datos';
        }
        else{ // si la busqueda es correcta devolvemos el recordset resultado
            $filas=null;
            while($fila = $resultado->fetch_array())
            {
                $filas[] = $fila;
            }
            return $filas;
        }
    }


    function crearTrabajo($IdTrabajo,$NombreTrabajo,$FechaIniTrabajo,$FechaFinTrabajo,$PorcentajeNota){
        $mysqli=$this->conexionBD();
        $query="INSERT INTO `TRABAJO`( `IdTrabajo`, `NombreTrabajo`, `FechaIniTrabajo`, `FechaFinTrabajo`, `PorcentajeNota`) VALUES ('$IdTrabajo','$NombreTrabajo', '$FechaIniTrabajo', '$FechaFinTrabajo', '$PorcentajeNota')";
        $resultado=$mysqli->query($query);
        $mysqli->close();
        return $resultado;
    }


    function eliminarTrabajo($IdTrabajo){
        $mysqli=$this->conexionBD();
        $query="DELETE FROM `TRABAJO` WHERE `IdTrabajo`='$IdTrabajo'";
        // si se produce un error en la baja mandamos el mensaje de error en la consulta
        if (!($resultado = $mysqli->query($query))){
            return "error al eliminar";
        }else{
            return $resultado;
        }
    }

    function modificarTrabajo($IdTrabajo,$NombreTrabajo,$FechaIniTrabajo,$FechaFinTrabajo,$PorcentajeNota){
        $mysqli=$this->conexionBD();

        /*if($fotopersonal==""){
            $query="UPDATE `USUARIO` SET `password`='$password',`DNI`='$dni', `nombre`='$nombre', `apellidos`='$apellidos', `telefono`='$telefono', `email`='$email', `FechaNacimiento`='$FechaNacimiento', `sexo`='$sexo' WHERE `login`='$login'";
            $resultado=$mysqli->query($query);
            $mysqli->close();
            return $resultado;
        }else{*/
        $query="UPDATE `TRABAJO` SET `NombreTrabajo`='$NombreTrabajo',`FechaIniTrabajo`='$FechaIniTrabajo', `FechaFinTrabajo`='$FechaFinTrabajo', `PorcentajeNota`='$PorcentajeNota' WHERE `IdTrabajo`='$IdTrabajo'";
        $resultado=$mysqli->query($query);
        $mysqli->close();
        return $resultado;

    }

    //}


    function buscarTrabajo($IdTrabajo,$NombreTrabajo,$PorcentajeNota,$dia,$mes,$año,$dia2,$mes2,$año2)
    {

        $mysqli = $this->conexionBD();

        $query = "SELECT * FROM `TRABAJO` ";
        $primero = false;

        if ($NombreTrabajo != null) {
            $query .= "WHERE `NombreTrabajo` like '%$NombreTrabajo%'";
            $primero = true;
        }
        if ($IdTrabajo != null && $primero == false) {
            $query .= "WHERE `IdTrabajo` like '%$IdTrabajo%'";
            $primero = true;
        } else if ($IdTrabajo != null && $primero == true) {
            $query .= "or `IdTrabajo` like '%$IdTrabajo%'";
            $primero = true;
        }
        if ($dia != null && $primero == false) {
            $query .= "WHERE `FechaIniTrabajo` like '%$dia%'";
            $primero = true;
        } else if ($dia != null && $primero == true) {
            $query .= "or `FechaIniTrabajo` like '%$dia%'";
            $primero = true;
        }
        if ($mes != null && $primero == false) {
            $query .= "WHERE `FechaIniTrabajo` like '%$mes%'";
            $primero = true;
        } else if ($mes != null && $primero == true) {
            $query .= "or `FechaIniTrabajo` like '%$mes%'";
            $primero = true;
        }
        if ($año != null && $primero == false) {
            $query .= "WHERE FechaIniTrabajo` like '%$año%'";
            $primero = true;
        } else if ($año != null && $primero == true) {
            $query .= "or `FechaIniTrabajo` like '%$año%'";
            $primero = true;
        }
        if ($dia2 != null && $primero == false) {
            $query .= "WHERE `FechaFinTrabajo` like '%$dia2%'";
            $primero = true;
        } else if ($dia2 != null && $primero == true) {
            $query .= "or `FechaFinTrabajo` like '%$dia2%'";
            $primero = true;
        }
        if ($mes2 != null && $primero == false) {
            $query .= "WHERE `FechaFinTrabajo` like '%$mes2%'";
            $primero = true;
        } else if ($mes2 != null && $primero == true) {
            $query .= "or `FechaFinTrabajo` like '%$mes2%'";
            $primero = true;
        }
        if ($año2 != null && $primero == false) {
            $query .= "WHERE FechaFinTrabajo` like '%$año2%'";
            $primero = true;
        } else if ($año2 != null && $primero == true) {
            $query .= "or `FechaFinTrabajo` like '%$año2%'";
            $primero = true;
        }
        if ($PorcentajeNota != null && $primero == false) {
            $query .= "WHERE PorcentajeNota` like '%$PorcentajeNota%'";
            $primero = true;
        } else if ($PorcentajeNota != null && $primero == true) {
            $query .= "or `PorcentajeNota` like '%$PorcentajeNota%'";
            $primero = true;
        }

        if (!($resultado = $mysqli->query($query))) {
            return 'Error en la consulta sobre la base de datos';
        } else { // si la busqueda es correcta devolvemos el recordset resultado
            $filas = null;
            while ($fila = $resultado->fetch_array()) {
                $filas[] = $fila;
            }

            return $filas;
        }
    }
}