<!-- 
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es el modelo de historia, por lo tanto
 * se encarga de conectar con la base de datos y realizar las
 * sentencias necesarias en SQL para llevar a cabo todas las operaciones posibles.
 */ -->


<?php 

class Historia_Model	{	
	private $IdTrabajo;
	private $IdHistoria;
	private $TextoHistoria;


	//constructor
	function constructor($IdTrabajo,$IdHistoria,$TextoHistoria)
	{		
		$this->IdTrabajo=$IdTrabajo;
		$this->IdHistoria=$IdHistoria;
		$this->TextoHistoria=$TextoHistoria;
	
	}
	
	//funcion para conectar a la base de datos
	function conexionBD(){
		$mysqli=mysqli_connect("127.0.0.1","userET3","passET3","IUET32017");
		if(!$mysqli){
			echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
			echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
			echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
			exit;
		}
		return $mysqli;
	}
		
	function comprobarHistoria($IdHistoria)
	{
		$mysqli=$this->conexionBD();
		$query="SELECT * FROM `HISTORIA` WHERE `IdHistoria`='$IdHistoria'";
		$resultado=$mysqli->query($query);
		if(mysqli_num_rows($resultado)){
			return true;
		}else{
			return false;
		}
	}
	
	/*function comprobarLoginAcceso($user,$pass)
	{
		$mysqli=$this->conexionBD();
		$query="SELECT * FROM `USUARIO` WHERE  login='$user' AND password='$pass'";
		$resultado=$mysqli->query($query);
		if(mysqli_num_rows($resultado)){
			return true;	
		}else{
			return false;
		}
	}*/
	//Agrega un susario para usar la App
	
	function listarHistorias(){
		$mysqli=$this->conexionBD();
		$query="SELECT `IdTrabajo`, `IdHistoria`, `TextoHistoria` FROM `HISTORIA` ";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}	    	
			return $filas;
		}
	}

	/* SHOW CURRENT (?)
	
	function showCurrent($login){
		$mysqli=$this->conexionBD();
		$query="SELECT `login`, `DNI`, `Nombre`, `Apellidos`, `Correo`, `Direccion`, `Telefono`  FROM `USUARIO` WHERE `login`='$login' ";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){
			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}	    	
			return $filas;
		}
	}*/

	//Busca las historias por trabajo al que pertenecen
	function buscarPorIdTrabajo($IdTrabajo){
		$mysqli=$this->conexionBD();
		$query="SELECT `IdTrabajo`, `IdHistoria`, `TextoHistoria` FROM `HISTORIA` WHERE `IdTrabajo`='$IdTrabajo'";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}    	
			return $filas;
		}
	}
	
	function buscarPorIdHistoria($IdHistoria){
		$mysqli=$this->conexionBD();
		$query="SELECT `IdTrabajo`, `IdHistoria`, `TextoHistoria` FROM `HISTORIA` WHERE `IdHistoria`='$IdHistoria'";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}    	
			return $filas;
		}
	}
	
	function buscarPorIds($IdTrabajo, $IdHistoria){
		$mysqli=$this->conexionBD();
		$query="SELECT `IdTrabajo`, `IdHistoria`, `TextoHistoria` FROM `HISTORIA` WHERE `IdTrabajo`='$IdTrabajo' AND `IdHistoria`='$IdHistoria'";
		 // si se produce un error en la busqueda mandamos el mensaje de error en la consulta
		if (!($resultado = $mysqli->query($query))){

			return 'Error en la consulta sobre la base de datos';
		}
	    else{ // si la busqueda es correcta devolvemos el recordset resultado
	    	$filas=null;
	    		while($fila = $resultado->fetch_array())
					{
						$filas[] = $fila;
					}    	
			return $filas;
		}
	}

	function crearHistoria($IdTrabajo,$IdHistoria,$TextoHistoria)
	{
		$mysqli=$this->conexionBD();
		$query="INSERT INTO `HISTORIA`(`IdTrabajo`, `IdHistoria`, `TextoHistoria`) VALUES ('$IdTrabajo','$IdHistoria','$TextoHistoria')";
		$resultado=$mysqli->query($query);
		$mysqli->close();
		return $resultado;
	}
	
	function eliminarHistoria($IdHistoria){
		$mysqli=$this->conexionBD();
		$query="DELETE FROM `HISTORIA` WHERE `IdHistoria`='$IdHistoria'";
		// si se produce un error en la baja mandamos el mensaje de error en la consulta
	    if (!($resultado = $mysqli->query($query))){
	    	return "error al eliminar";		
		}else{
			return $resultado;
				}
	}

	function modificarHistoria($IdTrabajo,$IdHistoria,$TextoHistoria){
		$mysqli=$this->conexionBD();		
		$query="UPDATE `HISTORIA` SET `TextoHistoria`='$TextoHistoria' WHERE `IdHistoria`='$IdHistoria'";
		 if (!($resultado = $mysqli->query($query))){
	    	return "error al modificar";		
		}else{
			return $resultado;
				}
	}

	function buscarHistoria($IdTrabajo,$IdHistoria,$TextoHistoria){

 		$mysqli=$this->conexionBD();

 		$query="SELECT * FROM `HISTORIA` ";
 		$primero=false;

 		if($IdTrabajo!=null ){
 		 $query .="WHERE `IdTrabajo` like '%$IdTrabajo%'";
 		 $primero=true;
 		}
 		if ($IdHistoria!=null && $primero==false){
         $query .="WHERE `IdHistoria` like '%$IdHistoria%'";
         $primero=true;
        }else if ($TextoHistoria!=null && $primero==true){
         $query .="or `TextoHistoria` like '%$TextoHistoria%'";
         $primero=true;
        }

		if (!($resultado = $mysqli->query($query))){

		return 'Error en la consulta sobre la base de datos';
		}
		else{ // si la busqueda es correcta devolvemos el recordset resultado
    	$filas=null;
    		while($fila = $resultado->fetch_array())
				{
					$filas[] = $fila;
				}
    	
		return $filas;
		}

 	}





}
?>


