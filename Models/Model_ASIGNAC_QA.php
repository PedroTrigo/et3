<?php
/**
 * Description of Model_ASIGNAC_QA
 *
 * @author iago
 */

class Model_ASIGNAC_QA {
    var $IdTrabajo;
    var $LoginEvaluador;
    var $LoginEvaluado;
    var $AliasEvaluado;
    
    function __construct($IdTrabajo, $LoginEvaluador, $LoginEvaluado, $AliasEvaluado) {
        $this->IdTrabajo = $IdTrabajo;
        $this->LoginEvaluador = $LoginEvaluador;
        $this->LoginEvaluado = $LoginEvaluado;
        $this->AliasEvaluado = $AliasEvaluado;
    }

    function conexionBD(){
	$mysqli=mysqli_connect("localhost","userET3","passET3","IUET32017");
	if(!$mysqli){
            echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
            exit;
	}
	return $mysqli;
    }
    
    function comprobarAsignacion(){
        $mysqli=$this->conexionBD();
	$query="SELECT * FROM ASIGNAC_QA WHERE IdTrabajo='$this->IdTrabajo' AND LoginEvaluador='$this->LoginEvaluador' AND AliasEvaluado='$this->AliasEvaluado'";
	$resultado=$mysqli->query($query);
	if(mysqli_num_rows($resultado)){
            return true;
	}
        else{
            return false;
	}
    }
    
    function insert(){
        $mysqli=$this->conexionBD();
        
        $query="INSERT INTO ASIGNAC_QA(IdTrabajo,LoginEvaluador,LoginEvaluado,AliasEvaluado) VALUES ('$this->IdTrabajo','$this->LoginEvaluador','$this->LoginEvaluado','$this->AliasEvaluado')";
        $toret=$mysqli->query($query);
        $mysqli->close();
        return $toret;
    }
    
    function delete(){
        $mysqli=$this->conexionBD();
        
        $query="DELETE FROM ASIGNAC_QA WHERE IdTrabajo='$this->IdTrabajo' AND LoginEvaluador='$this->LoginEvaluador' AND AliasEvaluado='$this->AliasEvaluado'";
        if ($toret=$mysqli->query($query)){
	    return $toret;		
	}
        else{
            return "Error al borrar";
	}
    }
    
    function update($AliasNuevo){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM ASIGNAC_QA WHERE IdTrabajo='$this->IdTrabajo' AND LoginEvaluador='$this->LoginEvaluador' AND AliasEvaluado='$this->AliasEvaluado'";
        $resultado=$mysqli->query($query);
        $tupla=$resultado->fetch_row();
        if($this->LoginEvaluado==""){
            $LoginEvaluado=$tupla[2];
        }
        else{
            $LoginEvaluado=$this->LoginEvaluado;
        }
        if($AliasNuevo==""){
            $AliasEvaluado=$tupla[3];
        }
        else{
            $AliasEvaluado=$AliasNuevo;
        }
        
        $query="UPDATE ASIGNAC_QA SET LoginEvaluado='$LoginEvaluado',AliasEvaluado='$AliasEvaluado' WHERE IdTrabajo='$this->IdTrabajo' AND LoginEvaluador='$this->LoginEvaluador' AND AliasEvaluado='$this->AliasEvaluado'";
	$resultado=$mysqli->query($query);
	$mysqli->close();
	return $resultado;
    }
    
    function select(){
        $mysqli=$this->conexionBD();
        
        if($this->LoginEvaluador=="" && $this->IdTrabajo=="" && $this->LoginEvaluado=""){
            $query="SELECT * FROM ASIGNAC_QA";
        }
        else{
            $query="SELECT * FROM ASIGNAC_QA WHERE ";
            $segundo=FALSE;
            if($this->LoginEvaluador<>""){
                $query.="LoginEvaluador LIKE '%$this->LoginEvaluador%'";
                $segundo=TRUE;
            }
            if($this->IdTrabajo<>""){
                if($segundo){
                    $query.=" AND ";
                }
                $query.="IdTrabajo LIKE '%$this->IdTrabajo%'";
                $segundo=TRUE;
            }
            if($this->LoginEvaluado<>""){
                if($segundo){
                    $query.=" AND ";
                }
                $query.="LoginEvaluado LIKE '%$this->LoginEvaluado%'";
                $segundo=TRUE;
            }
        }
        if (!($resultado = $mysqli->query($query))){
                return 'Error en la consulta sobre la base de datos';
            }
        else{
            return $resultado;
        }
    }
    
    function showAll(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM ASIGNAC_QA";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function showCurrent(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM ASIGNAC_QA WHERE IdTrabajo='$this->IdTrabajo' AND LoginEvaluador='$this->LoginEvaluador' AND AliasEvaluado='$this->AliasEvaluado'";
        if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
            $asignacion=$resultado->fetch_row();
	    return $asignacion;
	}
    }
    
    function buscarAlias(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT Alias FROM ENTREGA WHERE IdTrabajo='$this->IdTrabajo' AND login='$this->LoginEvaluado'";
        if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
            $entrega=$resultado->fetch_row();
	    return $entrega[0];
	}
    }
    
    function showAllUsers(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT login FROM USUARIO";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function showAllEntregas(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT IdTrabajo FROM ENTREGA";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
}
