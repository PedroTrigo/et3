<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es el modelo de nota trabajo, por lo tanto
 * se encarga de conectar con la base de datos y realizar las
 * sentencias necesarias en SQL para llevar a cabo todas las operaciones posibles.
 */

class Model_NOTA_TRABAJO {
    var $login;
    var $IdTrabajo;
    var $NotaTrabajo;
    
    function __construct($login,$IdTrabajo,$NotaTrabajo) {
        $this->login=$login;
        $this->IdTrabajo=$IdTrabajo;
        $this->NotaTrabajo=$NotaTrabajo;
    }

    function conexionBD(){
	$mysqli=mysqli_connect("localhost","userET3","passET3","IUET32017");
	if(!$mysqli){
            echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_errno() . PHP_EOL;
            echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
            exit;
	}
	return $mysqli;
    }
    
    function trabajosPorEvaluar(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT IdTrabajo,NombreTrabajo FROM TRABAJO WHERE NOT IdTrabajo IN (SELECT IdTrabajo FROM NOTA_TRABAJO WHERE login='$this->login')";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function comprobarNota(){
	$mysqli=$this->conexionBD();
	$query="SELECT * FROM NOTA_TRABAJO WHERE  IdTrabajo='$this->IdTrabajo' AND login='$this->login'";
	$resultado=$mysqli->query($query);
	if(mysqli_num_rows($resultado)){
            return true;
	}
        else{
            return false;
	}
    }
            
    function insert(){
        $mysqli=$this->conexionBD();
        
        $query="INSERT INTO NOTA_TRABAJO(login,IdTrabajo,NotaTrabajo) VALUES ('$this->login','$this->IdTrabajo','$this->NotaTrabajo')";
        $toret=$mysqli->query($query);
        $mysqli->close();
        return $toret;
    }
    
    function delete(){
        $mysqli=$this->conexionBD();
        
        $query="DELETE FROM NOTA_TRABAJO WHERE IdTrabajo='$this->IdTrabajo' AND login='$this->login'";
        if ($toret=$mysqli->query($query)){
	    return $toret;		
	}
        else{
            return "Error al borrar";
	}
    }
    
    function update(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM NOTA_TRABAJO WHERE IdTrabajo='$this->IdTrabajo' AND login='$this->login'";
        $resultado=$mysqli->query($query);
        $tupla=$resultado->fetch_row();
        if($this->NotaTrabajo==""){
            $NotaTrabajo=$tupla[2];
        }
        else{
            $NotaTrabajo=$this->NotaTrabajo;
        }
        
        $query="UPDATE NOTA_TRABAJO SET NotaTrabajo='$NotaTrabajo' WHERE IdTrabajo='$this->IdTrabajo' AND login='$this->login'";
	$resultado=$mysqli->query($query);
	$mysqli->close();
	return $resultado;
    }
    
    function select(){
        $mysqli=$this->conexionBD();
        
        if($this->login=="" && $this->IdTrabajo==""){
            $query="SELECT * FROM NOTA_TRABAJO";
        }
        else{
            $query="SELECT * FROM NOTA_TRABAJO WHERE ";
            $segundo=FALSE;
            if($this->login<>""){
                $query.="login LIKE '%$this->login%'";
                $segundo=TRUE;
            }
            if($this->IdTrabajo<>""){
                if($segundo){
                    $query.=" AND ";
                }
                $query.="IdTrabajo LIKE '%$this->IdTrabajo%'";
                $segundo=TRUE;
            }
        }
        if (!($resultado = $mysqli->query($query))){
                return 'Error en la consulta sobre la base de datos';
            }
        else{
            return $resultado;
        }
    }
    
    function showAll(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM NOTA_TRABAJO";
	if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
	    return $resultado;
	}
    }
    
    function showCurrent(){
        $mysqli=$this->conexionBD();
        
        $query="SELECT * FROM NOTA_TRABAJO WHERE IdTrabajo='$this->IdTrabajo' AND login='$this->login'";
        if (!($resultado = $mysqli->query($query))){
            return 'Error en la consulta sobre la base de datos';
	}
	else{
            $funcionalidad=$resultado->fetch_row();
	    return $funcionalidad;
	}
    }
}
