<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de usuario, por lo tanto controla las distintas
 * operaciones posibles sobre ellos.
 */

include("../Models/USUARIO_Model.php");
include("../Views/Usuario_SHOWALL.php");
include("../Views/Usuario_ADD.php");
include("../Views/Usuario_EDIT.php");
include("../Views/Usuario_SEARCH.php");
include("../Views/MESSAGE.php"); 
include("../Views/Usuario_DELETE.php");
include("../Views/Usuario_SHOWCURRENT.php");
include("../Views/Usuario_AsignarAGrupo.php");
include("../Models/HISTORIA_Model.php");
include("../Views/Historia_SHOWALL.php");
include("../Views/Historia_ADD.php");
include("../Views/Historia_EDIT.php");
include("../Views/Historia_SEARCH.php");
include("../Views/Historia_DELETE.php");
include("../Views/Historia_SHOWCURRENT.php");
include("../Models/Model_NOTA.php");
include("../Views/Nota_SHOWALL.php");
include("../Views/Nota_ADD.php");
include("../Views/Nota_EDIT.php");
include("../Views/Nota_SEARCH.php");
include("../Views/Nota_DELETE.php");
include("../Views/Nota_SHOWCURRENT.php");
include("../Models/GRUPO_Model.php");
include("../Views/Grupo_SHOWALL.php");
include("../Views/Grupo_ADD.php");
include("../Views/Grupo_EDIT.php");
include("../Views/Grupo_SEARCH.php");
include("../Views/Grupo_DELETE.php");
include("../Views/Grupo_SHOWCURRENT.php");




include("../Views/Inicio.php");


//Carga la vista Usuario_ADD
      function cargarCrearUser()
      { 
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $claseCrearUser=new Usuario_ADD();
        $claseCrearUser->cargar("",$idiom,$comprobarUsuarioGrupo);
      }
	 
//Carga la vista Historia_ADD
      function cargarCrearHistoria()
      { 
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $claseCrearHistoria=new Historia_ADD();
        $claseCrearHistoria->cargar("",$idiom,$comprobarUsuarioGrupo);
      }
	  
//Carga la vista Nota_ADD
      function cargarCrearNota()
      { 
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $claseCrearNota=new Nota_ADD();
        $claseCrearNota->cargar("",$idiom,$comprobarUsuarioGrupo);
      }
	  
	  //Carga la vista Grupo_ADD
      function cargarCrearGrupo()
      { 
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $claseCrearGrupo=new Grupo_ADD();
        $claseCrearGrupo->cargar("",$idiom,$comprobarUsuarioGrupo);
      }
	  
//Envia los datos de la vista Usuario_ADD
      function cargarAltaUser()
      {
            $login=$_POST['loginAdd'];
            $password=$_POST['passAdd'];
            $dni=$_POST['dniAdd'];
            $nombre=$_POST['nombreUserAdd'];
            $apellidos=$_POST['apellidosUserAdd'];
            $email=$_POST['emailUserAdd'];
            $direccion=$_POST['direccionUserAdd'];
            $telefono=$_POST['telefonoUserAdd'];            
           
            $password=md5($password);            

            $modeloUser=new Usuario_Model();
            $usuarioExistente=$modeloUser->comprobarUsuario($login,$dni,$email);

            if($usuarioExistente==false){            
                  $resultado=$modeloUser->crearUser($login,$password,$dni,$nombre,$apellidos,$email,$direccion,$telefono);
                  if($resultado==true){ 
                        header("location: ActionController.php?action=exitoCrearUser");
                  }else{
                         //cargo idiomas
                        $idioma=new idiomas();
                        $idiom=comprobaridioma($idioma);
                        //Comprueba la sesión de ususario para mostrar el menú
                        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                        //cargo la vista
                        $clasecrearuser=new Usuario_ADD();
                        $clasecrearuser->cargar("UsuarioRepe",$idiom,$comprobarUsuarioGrupo);               
                  }
            }else{
                  //cargo el idioma
                  $idioma=new idiomas();
                  $idiom=comprobaridioma($idioma);
                  //Comprueba la sesión de ususario para mostrar el menú
                  $comprobarUsuarioGrupo=ComprobarUsuarioMenu();                        
                  $clasecrear=new MESSAGE();
                  $clasecrear->cargar("DatosDuplicados",$idiom,$comprobarUsuarioGrupo);
           
            }
      }   
	  
	  //Envia los datos de la vista Historia_ADD
      function cargarAltaHistoria()
      {
            $IdTrabajo=$_POST['IdTrabajo'];
            $IdHistoria=$_POST['IdHistoria'];
            $TextoHistoria=$_POST['TextoHistoria'];
                     
           
            $modeloHistoria=new Historia_Model();
            $historiaExistente=$modeloHistoria->comprobarHistoria($IdTrabajo, $IdHistoria);

            if($historiaExistente==false){            
                  $resultado=$modeloHistoria->crearHistoria($IdTrabajo,$IdHistoria,$TextoHistoria);
                  if($resultado==true){ 
                        header("location: ActionController.php?action=exitoCrearHistoria");
                  }else{
                         //cargo idiomas
                        $idioma=new idiomas();
                        $idiom=comprobaridioma($idioma);
                        //Comprueba la sesión de ususario para mostrar el menú
                        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                        //cargo la vista
                        $clasecrearhistoria=new Historia_ADD();
                        $clasecrearhistoria->cargar("HistoriaRepe",$idiom,$comprobarUsuarioGrupo);               
                  }
            }else{
                  //cargo el idioma
                  $idioma=new idiomas();
                  $idiom=comprobaridioma($idioma);
                  //Comprueba la sesión de ususario para mostrar el menú
                  $comprobarUsuarioGrupo=ComprobarUsuarioMenu();                        
                  $clasecrear=new MESSAGE();
                  $clasecrear->cargar("DatosDuplicados",$idiom,$comprobarUsuarioGrupo);
           
            }
      }           
	  
	  //Envia los datos de la vista Nota_ADD
      function cargarAltaNota()
      {
            $login=$_POST['login'];
            $IdTrabajo=$_POST['IdTrabajo'];
            $NotaTrabajo=$_POST['NotaTrabajo'];
                     
           
            $modeloNota=new Nota_Model();
            $notaExistente=$modeloNota->comprobarNota($login, $IdTrabajo);

            if($notaExistente==false){            
                  $resultado=$modeloNota->crearNota($login,$IdTrabajo,$NotaTrabajo);
                  if($resultado==true){ 
                        header("location: ActionController.php?action=exitoCrearNota");
                  }else{
                         //cargo idiomas
                        $idioma=new idiomas();
                        $idiom=comprobaridioma($idioma);
                        //Comprueba la sesión de ususario para mostrar el menú
                        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                        //cargo la vista
                        $clasecrearnota=new Nota_ADD();
                        $clasecrearnota->cargar("NotaRepe",$idiom,$comprobarUsuarioGrupo);               
                  }
            }else{
                  //cargo el idioma
                  $idioma=new idiomas();
                  $idiom=comprobaridioma($idioma);
                  //Comprueba la sesión de ususario para mostrar el menú
                  $comprobarUsuarioGrupo=ComprobarUsuarioMenu();                        
                  $clasecrear=new MESSAGE();
                  $clasecrear->cargar("DatosDuplicados",$idiom,$comprobarUsuarioGrupo);
           
            }
      }   
	  
	function cargarAltaGrupo()
      {
            $IdGrupo=$_POST['IdGrupo'];
            $NombreGrupo=$_POST['NombreGrupo'];
            $DescripGrupo=$_POST['DescripGrupo'];
                     
           
            $modeloGrupo=new Grupo_Model();
            $grupoExistente=$modeloGrupo->comprobarGrupo($IdGrupo);

            if($grupoExistente==false){            
                  $resultado=$modeloGrupo->crearGrupo($IdGrupo,$NombreGrupo,$DescripGrupo);
                  if($resultado==true){ 
                        header("location: ActionController.php?action=exitoCrearGrupo");
                  }else{
                         //cargo idiomas
                        $idioma=new idiomas();
                        $idiom=comprobaridioma($idioma);
                        //Comprueba la sesión de ususario para mostrar el menú
                        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                        //cargo la vista
                        $clasecreargrupo=new Grupo_ADD();
                        $clasecreargrupo->cargar("GrupoRepe",$idiom,$comprobarUsuarioGrupo);               
                  }
            }else{
                  //cargo el idioma
                  $idioma=new idiomas();
                  $idiom=comprobaridioma($idioma);
                  //Comprueba la sesión de ususario para mostrar el menú
                  $comprobarUsuarioGrupo=ComprobarUsuarioMenu();                        
                  $clasecrear=new MESSAGE();
                  $clasecrear->cargar("DatosDuplicados",$idiom,$comprobarUsuarioGrupo);
           
            }
      }
      //Muestra la vista Usuario_AsignarAGrupo.php
      function vistaAsignarAGrupo($login){
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //$IdGrupo=$_POST['IdGrupo'];
        //cargo el modelo de usuarios
        $modeloUser=new Usuario_Model();
        //Cargo el array de idGrupos de la tabla USU-GRUPOS
        $gruposDiferentes=$modeloUser->GruposDiferentes(); 
        var_dump($gruposDiferentes);
        //$datos=$modeloUser->AsignarUserAGrupo($login,$IdGrupo);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista            
        $claseAsignarUser=new Usuario_AsignarGrupo();
        $claseAsignarUser->cargar("",$idiom,$gruposDiferentes,$login,$comprobarUsuarioGrupo);
      }

      function cargarVistaAsignarAGrupo(){
        $IdGrupo=$_POST['IdGrupo'];
        $login=$_POST['login'];
        //cargo el modelo de usuarios
        $modeloUser=new Usuario_Model();
        $resultado=$modeloUser->AsignarUserAGrupo($login,$IdGrupo);
        if($resultado==true){ 
            header("location: ActionController.php?action=exitoAsignarGrupo");
        }else{
         //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //$datos=$modeloUser->buscarPorLogin($login);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorAsignar",$idiom,$comprobarUsuarioGrupo);           
            }
      }  

      function cargarEliminarUser($login){ 
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el modelo de usuarios
            $modeloUser=new Usuario_Model();
            $datos=$modeloUser->buscarPorLogin($login);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista            
            $claseEliminarUser=new Usuario_DELETE();
            $claseEliminarUser->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
      }
	  
	  function cargarEliminarHistoria($IdHistoria){ 
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el array de usuarios
            $modeloHistoria=new Historia_Model();
            $datos=$modeloHistoria->buscarPorIdHistoria($IdHistoria);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista            
            $claseEliminarHistoria=new Historia_DELETE();
            $claseEliminarHistoria->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
      }
	  
	  function cargarEliminarNota($IdTrabajo){ 
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el array de usuarios
            $modeloNota=new Nota_Model();
            $datos=$modeloNota->buscarPorIdTrabajo($IdTrabajo);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista            
            $claseEliminarNota=new Nota_DELETE();
            $claseEliminarNota->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
      }
	  
	  	  function cargarEliminarGrupo($IdGrupo){ 
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el array de usuarios
            $modeloGrupo=new Grupo_Model();
            $datos=$modeloGrupo->buscarPorIdGrupo($IdGrupo);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista            
            $claseEliminarGrupo=new Grupo_DELETE();
            $claseEliminarGrupo->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
      }

      function cargarBajaUser($login){
            //cargo el modelo
            $modeloUser=new Usuario_Model();
            //ejecuto la funcion de eliminar
            $resultado=$modeloUser->eliminarUser($login);
            //si elimina bien vuelve a la vista usuario_SHOWALL mostrando la lista de usuarios actualizada y con mensaje de exito
            if($resultado==true){ 
            header("location: ActionController.php?action=exitoEliminarUser");
            }else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //$datos=$modeloUser->buscarPorLogin($login);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);           
            }
      }  
	  
	    function cargarBajaHistoria($IdHistoria){
            //cargo el modelo
            $modeloHistoria=new Historia_Model();
            //ejecuto la funcion de eliminar
            $resultado=$modeloHistoria->eliminarHistoria($IdHistoria);
            //si elimina bien vuelve a la vista historia_SHOWALL mostrando la lista de historias actualizada y con mensaje de exito
            if($resultado==true){ 
            header("location: ActionController.php?action=exitoEliminarHistoria");
            }else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //$datos=$modeloHistoria->buscarPorLogin($login);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);           
            }
      } 
	  
	  function cargarBajaNota($IdTrabajo){
            //cargo el modelo
            $modeloNota=new Nota_Model();
            //ejecuto la funcion de eliminar
            $resultado=$modeloNota->eliminarNota($IdTrabajo);
            //si elimina bien vuelve a la vista nota_SHOWALL mostrando la lista de notas actualizada y con mensaje de exito
            if($resultado==true){ 
            header("location: ActionController.php?action=exitoEliminarNota");
            }else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //$datos=$modeloNota->buscarPorIdsNota($login, $IdTrabajo);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);           
            }
      } 
	  
	  function cargarBajaGrupo($IdGrupo){
            //cargo el modelo
            $modeloGrupo=new Grupo_Model();
            //ejecuto la funcion de eliminar
            $resultado=$modeloGrupo->eliminarGrupo($IdGrupo);
            //si elimina bien vuelve a la vista gripo_SHOWALL mostrando la lista de notas actualizada y con mensaje de exito
            if($resultado==true){ 
            header("location: ActionController.php?action=exitoEliminarGrupo");
            }else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);           
            }
      } 
	  
      //Recoge los datos del usuario de la vista usuario_EDIT.php
      function modificarUser(){
            $login=$_POST['loginEdit'];
            $password=$_POST['passEdit'];
            $dni=$_POST['dniEdit'];
            $nombre=$_POST['nombreUserEdit'];
            $apellidos=$_POST['apellidosUserEdit'];
            $email=$_POST['emailUserEdit'];
            $direccion=$_POST['direccioncUserEdit'];
            $telefono=$_POST['telefonoUserEdit'];           
           
            $password=md5($password); 

            //Cargo el modelo y ejecuto la función para hacer el update
            $modeloUser=new Usuario_Model();
            $resultado=$modeloUser-> modificarUser($login,$password,$dni,$nombre,$apellidos,$email,$direccion,$telefono);

            //si modifica correctamente vuelve a la vista usuario_SHOWALL.php mostrando mensaje de exito
            if($resultado==true)
            { 
                  header("location: ActionController.php?action=exitoModificarUser");
            //Si no muestra mensaje de error y vuelve a la vista usuario_EDIT.php
            }else{ 
             //cargo idiomas
                  $idioma=new idiomas();
                  $idiom=comprobaridioma($idioma);
                  //Comprueba la sesión de ususario para mostrar el menú
                  $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                  //cargo la vista MESSAGE                  
                  $claseModificarUser=new MESSAGE();
                  $claseModificarUser->cargar("errorModificar",$idiom,$comprobarUsuarioGrupo);
          }
      }
	  
	function modificarHistoria(){
            $IdTrabajo=$_POST['idTEdit'];
            $IdHistoria=$_POST['idHEdit'];
            $TextoHistoria=$_POST['TextoHEdit'];
			
            //Cargo el modelo y ejecuto la función para hacer el update
            $modeloHistoria=new Historia_Model();
            $resultado=$modeloHistoria-> modificarHistoria($IdTrabajo,$IdHistoria,$TextoHistoria);

            //si modifica correctamente vuelve a la vista historia_SHOWALL.php mostrando mensaje de exito
            if($resultado==true)
            { 
                 header("location: ActionController.php?action=exitoModificarHistoria");
            //Si no muestra mensaje de error y vuelve a la vista historia_EDIT.php
            }else{ 
             //cargo idiomas
                  $idioma=new idiomas();
                  $idiom=comprobaridioma($idioma);
                  //Comprueba la sesión de ususario para mostrar el menú
                  $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                  //cargo la vista MESSAGE                  
                  $claseModificarHistoria=new MESSAGE();
                  $claseModificarHistoria->cargar("errorModificar",$idiom,$comprobarUsuarioGrupo);
          }
      }
	  
	function modificarNota(){
            $login2=$_POST['loginEdit'];
            $IdTrabajo=$_POST['idTEdit'];
            $NotaTrabajo=$_POST['NotaTEdit'];

            //Cargo el modelo y ejecuto la función para hacer el update
            $modeloNota=new Nota_Model();
            $resultado=$modeloNota-> modificarNota($login2,$IdTrabajo,$NotaTrabajo);

            //si modifica correctamente vuelve a la vista nota_SHOWALL.php mostrando mensaje de exito
            if($resultado==true)
            { 
                  header("location: ActionController.php?action=exitoModificarNota");
            //Si no muestra mensaje de error y vuelve a la vista hnota_EDIT.php
            }else{ 
             //cargo idiomas
                  $idioma=new idiomas();
                  $idiom=comprobaridioma($idioma);
                  //Comprueba la sesión de ususario para mostrar el menú
                  $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                  //cargo la vista MESSAGE                  
                  $claseModificarNota=new MESSAGE();
                  $claseModificarNota->cargar("errorModificar",$idiom,$comprobarUsuarioGrupo);
          }
      }
	  
	  function modificarGrupo(){
            $IdGrupo=$_POST['IdGrupoEdit'];
            $NombreGrupo=$_POST['NombreGrupoEdit'];
            $DescripGrupo=$_POST['DescripGrupoEdit'];

            //Cargo el modelo y ejecuto la función para hacer el update
            $modeloGrupo=new Grupo_Model();
            $resultado=$modeloGrupo-> modificarGrupo($IdGrupo,$NombreGrupo,$DescripGrupo);

            //si modifica correctamente vuelve a la vista grupo_SHOWALL.php mostrando mensaje de exito
            if($resultado==true)
            { 
                 header("location: ActionController.php?action=exitoModificarGrupo");
            //Si no muestra mensaje de error y vuelve a la vista hnota_EDIT.php
            }else{ 
             //cargo idiomas
                  $idioma=new idiomas();
                  $idiom=comprobaridioma($idioma);
                  //Comprueba la sesión de ususario para mostrar el menú
                  $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                  //cargo la vista MESSAGE                  
                  $claseModificarGrupo=new MESSAGE();
                  $claseModificarGrupo->cargar("errorModificar",$idiom,$comprobarUsuarioGrupo);
          }
      }
	  
      //Carga la vista usuario_EDIT
      function cargarModificarUser($login){
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el modelo
            $modeloUser=new Usuario_Model();
            //busco al usuario a editar
            $datos=$modeloUser->buscarPorLogin($login);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $claseModificarUser=new usuario_EDIT();
            $claseModificarUser->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);       
      }    
	  
      function cargarModificarHistoria($IdHistoria){
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el modelo
            $modeloHistoria=new Historia_Model();
            //busco la historia a editar
            $datos=$modeloHistoria->buscarPorIdHistoria($IdHistoria);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $claseModificarHistoria=new Historia_EDIT();
            $claseModificarHistoria->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);       
      }   
	  
      function cargarModificarNota($IdTrabajo){
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el modelo
            $modeloNota=new Nota_Model();
            //busco la historia a editar
            $datos=$modeloNota->buscarPorIdTrabajo($IdTrabajo);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $claseModificarNota=new Nota_EDIT();
            $claseModificarNota->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);       
      }   
	  
      function cargarModificarGrupo($IdGrupo){
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el modelo
            $modeloGrupo=new Grupo_Model();
            //busco la historia a editar
            $datos=$modeloGrupo->buscarPorIdGrupo($IdGrupo);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $claseModificarGrupo=new Grupo_EDIT();
            $claseModificarGrupo->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);       
      }  

      //carga vista SHOWCURRENT
      function cargarShowCurrentUser($login){
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el modelo
            $modeloUser=new Usuario_Model();
            //busco al usuario a editar
            $datos=$modeloUser->buscarPorLogin($login);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $claseModificarUser=new usuario_SHOWCURRENT();
            $claseModificarUser->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);     

      }
	  
	  //carga vista SHOWCURRENT
      function cargarShowCurrentHistoria($IdHistoria){
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el modelo
            $modeloHistoria=new Historia_Model();
            //busco la historia a editar
            $datos=$modeloHistoria->buscarPorIdHistoria($IdHistoria);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $claseModificarHistoria=new Historia_SHOWCURRENT();
            $claseModificarHistoria->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);     

      }
	  
	  function cargarShowCurrentNota($IdTrabajo){
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el modelo
            $modeloNota=new Nota_Model();
            //busco la historia a editar
            $datos=$modeloNota->buscarPorIdTrabajo($IdTrabajo);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $claseModificarNota=new Nota_SHOWCURRENT();
            $claseModificarNota->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);     

      }
	  
	  function cargarShowCurrentGrupo($IdGrupo){
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el modelo
            $modeloGrupo=new Grupo_Model();
            //busco la historia a editar
            $datos=$modeloGrupo->buscarPorIdGrupo($IdGrupo);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $claseModificarGrupo=new Grupo_SHOWCURRENT();
            $claseModificarGrupo->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);     

      }
	  
      //Carga la vista SEARCH
      function cargarBuscarUser(){       
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            $claseCrearUser=new Usuario_SEARCH();
            $claseCrearUser->cargar("",$idiom,$comprobarUsuarioGrupo);
      }
	  
	   //Carga la vista SEARCH
      function cargarBuscarHistoria(){       
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            $claseCrearHistoria=new Historia_SEARCH();
            $claseCrearHistoria->cargar("",$idiom,$comprobarUsuarioGrupo);
      }
	  
	  function cargarBuscarNota(){       
              //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            $claseCrearNota=new Nota_SEARCH();
            $claseCrearNota->cargar("",$idiom,$comprobarUsuarioGrupo);
      }
	  
	  function cargarBuscarGrupo(){       
              //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            $claseCrearGrupo=new Grupo_SEARCH();
            $claseCrearGrupo->cargar("",$idiom,$comprobarUsuarioGrupo);
      }

      function cargarSearch(){

            $login=$_POST['loginSearch'];
            $dni=$_POST['dniSearch'];
            $nombre=$_POST['nombreUserSearch'];
            $apellidos=$_POST['apellidosUserSearch'];
            $email=$_POST['emailUserSearch'];
            $direccion=$_POST['direccionUserSearch'];
            $telefono=$_POST['telefonoUserSearch'];                    
                  
              if ($nombre==""){
                $nombre=null;
              }else if ($apellidos==""){
                $apellidos=null;
              }else if ($login==""){
                $login=null;
              }else if ($dni==""){
                $dni=null;
              }else if ($email==""){
                $email=null;
              }else if ($direccion==""){
                $direccion=null;
              }else if ($telefono==""){
                $telefono=null;
              }       
            $model=new Usuario_Model();
            $datos=$model->buscarUsuario($login,$dni,$nombre,$apellidos,$email,$direccion,$telefono);    
            return $datos;
      }
	  
	  function cargarSearchH(){

            $IdTrabajo=$_POST['idTSearch'];
            $IdHistoria=$_POST['idHSearch'];
            $TextoHistoria=$_POST['TextoHSearch'];
                  
                  
              if ($IdTrabajo==""){
                $IdTrabajo=null;
              }else if ($IdHistoria==""){
                $IdHistoria=null;
              }else if ($TextoHistoria==""){
                $TextoHistoria=null;
              }       
            $model=new Historia_Model();
            $datos=$model->buscarHistoria($IdTrabajo,$IdHistoria,$TextoHistoria);    
            return $datos;
      }
	  
	  function cargarSearchN(){

            $login=$_POST['loginSearch'];
            $IdTrabajo=$_POST['idTSearch'];
            $NotaTrabajo=$_POST['NotaTSearch'];
                  
                  
              if ($login==""){
                $login=null;
              }else if ($IdTrabajo==""){
                $IdTrabajo=null;
              }else if ($NotaTrabajo==""){
                $NotaTrabajo=null;
              }       
            $model=new Nota_Model();
            $datos=$model->buscarNota($login,$IdTrabajo,$NotaTrabajo);    
            return $datos;
      }
	  
	  function cargarSearchG(){

            $IdGrupo=$_POST['IdGrupoSearch'];
            $NombreGrupo=$_POST['NombreGrupoSearch'];
            $DescripGrupo=$_POST['DescripGrupoSearch'];
                  
                  
              if ($IdGrupo==""){
                $IdGrupo=null;
              }else if ($NombreGrupo==""){
                $NombreGrupo=null;
              }else if ($DescripGrupo==""){
                $DescripGrupo=null;
              }       
            $model=new Grupo_Model();
            $datos=$model->buscarGrupo($IdGrupo,$NombreGrupo,$DescripGrupo);    
            return $datos;
      }

	  	  /*function cargarBuscarGrupoUser($login){
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //cargo el modelo
            $modeloUser=new USUARIO_Model();
            //busco la historia a editar
            $datos=$modeloUser->buscarGrupoUser($login);
            //cargo la vista
            $claseModificarUser=new Usuario_SHOWCURRENT();
            $claseModificarUser->cargar($datos,"",$idiom);     

      }*/
		  
	  
      

      



?>