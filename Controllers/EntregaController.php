<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de entregas, por lo tanto controla las distintas
 * operaciones posibles sobre ellas.
 */
session_start();
$login=$_SESSION['usuario'];

include("../Models/Model_ENTREGA.php");
include("../Models/USUARIO_Model.php");
include("../Views/Entrega_SHOWALL.php");
include("../Views/Entrega_ADD.php");
include("../Views/Entrega_EDIT.php");
include("../Views/Entrega_SEARCH.php");
include("../Views/MESSAGE.php"); 
include("../Views/Entrega_DELETE.php");
include("../Views/Entrega_SHOWCURRENT.php");
include("../Views/EntregaUser_EDIT.php");
include("../Views/EntregaUser_SHOWALL.php");
include("../Functions/idiomas.php");
include("../Functions/comprobaridioma.php");
include("../Views/EntregaUser_SHOWCURRENT.php");

function ComprobarUsuarioMenu(){
    //Cargo el modelo
    $modeloUser=new Usuario_Model();
    //Usuario de la sesion
    $UserSesionActual=$_SESSION['usuario'];
    //Comprueba si es admin 
    $esAdmin=$modeloUser->comprobarUsuarioADMINEnUSU_GRUPO($UserSesionActual);       

            if( ($esAdmin) == "true" ){
                $comprobarUsuarioGrupo="ADMIN";
            }else{
                $comprobarUsuarioGrupo="USER";
            }
            return $comprobarUsuarioGrupo;
    }

switch ($_REQUEST['action']){
    //carga formulario de alta de entrega
    case 'cargarAlta':
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_ENTREGA("","","","","");
        $trabajos=$modelo->trabajos();
        $usuarios=$modelo->usuarios();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaCrearEntrega=new Entrega_ADD();
        $vistaCrearEntrega->cargar("",$idiom,$trabajos,$usuarios,$comprobarUsuarioGrupo);
        break;
    
    //da de alta entrega
    case 'alta':
        $login=$_POST['login'];
        $IdTrabajo=$_POST['IdTrabajo'];
        $Alias=crearAlias();
        $Horas=$_POST['Horas'];
        
        
        $modelo=new Model_ENTREGA($login,$IdTrabajo,$Alias,$Horas,"");
        $comprobar=$modelo->comprobarEntrega();

        if($comprobar==false){
            $resultado=$modelo->insert();
            if($resultado==true){
                header("location: EntregaController.php?action=mensajeResultado&resultado=exito");
            }
            else{
                $modelo=new Model_ENTREGA("","","","","");
                $trabajos=$modelo->trabajos();
                $usuarios=$modelo->usuarios();
                //cargo idiomas
                $idioma=new idiomas();
                $idiom=comprobaridioma($idioma);
                //Comprueba la sesión de ususario para mostrar el menú
                $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                //cargo la vista
                $vistaCrearEntrega=new Entrega_ADD();
                $vistaCrearEntrega->cargar("EntregaRepe",$idiom,$trabajos,$usuarios,$comprobarUsuarioGrupo);
            }
        }
        else{
            $modelo=new Model_ENTREGA("","","","","");
            $trabajos=$modelo->trabajos();
            $usuarios=$modelo->usuarios();
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $vistaCrearEntrega=new Entrega_ADD();
            $vistaCrearEntrega->cargar("EntregaRepe",$idiom,$trabajos,$usuarios,$comprobarUsuarioGrupo);
        }
        break;
        
    //carga la vista para eliminar una entrega
    case 'cargarBaja':
        $login=$_GET['login'];
        $IdTrabajo=$_GET['IdTrabajo'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_ENTREGA($login,$IdTrabajo,"","","");
        $datos=$modelo->showCurrent();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista            
        $vistaEliminarEntrega=new Entrega_DELETE();
        $vistaEliminarEntrega->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    //da de baja entrega
    case 'baja':
        $login=$_GET['login'];
        $IdTrabajo=$_GET['IdTrabajo'];
        //cargo el modelo
        $modelo=new Model_ENTREGA($login,$IdTrabajo,"","","");
        //ejecuto la funcion de eliminar
        $resultado=$modelo->delete();
        //si elimina bien vuelve a la vista SHOWALL mostrando la lista actualizada y con mensaje de exito
        if($resultado==true){ 
            header("location: EntregaController.php?action=mensajeResultado&resultado=exitoborrar");
        }
        else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);

            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);
        }
        break;
        
    case 'cargarModificar':
        $login=$_GET['login'];
        $IdTrabajo=$_GET['IdTrabajo'];
        
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        //cargo el modelo
        $modelo=new Model_ENTREGA($login,$IdTrabajo,"","","");
        //busco al usuario a editar
        $datos=$modelo->showCurrent();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();

        //cargo la vista
        $vistaModificarEntrega=new Entrega_EDIT();
        $vistaModificarEntrega->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'modificar':
        $login=$_POST['login'];
        $IdTrabajo=$_POST['IdTrabajo'];
        $Alias=$_POST['Alias'];
        $Horas=$_POST['Horas'];

        //Cargo el modelo y ejecuto la función para hacer el update
        $modelo=new Model_ENTREGA($login,$IdTrabajo,$Alias,$Horas,"");
        $resultado=$modelo->update();

        //si modifica correctamente vuelve a la vista SHOWALL.php mostrando mensaje de exito
        if($resultado==true)
        { 
            if(isset($_GET['user'])){
                header("location: EntregaController.php?action=mensajeResultadoUser&resultado=exitoModificar");
            }
            header("location: EntregaController.php?action=mensajeResultado&resultado=exitoModificar");
        }
        else{ 
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();

            //cargo la vista MESSAGE                  
            $claseModificarEntrega=new MESSAGE();
            $claseModificarEntrega->cargar("errorModificarEntrega",$idiom,$comprobarUsuarioGrupo);
        }
        break;
    case 'modificar':
        $login=$_POST['login'];
        $IdTrabajo=$_POST['IdTrabajo'];
        $Alias=$_POST['Alias'];
        $Horas=$_POST['Horas'];

        //Cargo el modelo y ejecuto la función para hacer el update
        $modelo=new Model_ENTREGA($login,$IdTrabajo,$Alias,$Horas,"");
        $resultado=$modelo->update();

        //si modifica correctamente vuelve a la vista SHOWALL.php mostrando mensaje de exito
        if($resultado==true)
        {
            if(isset($_GET['user'])){
                header("location: EntregaController.php?action=mensajeResultadoUser&resultado=exitoModificar");
            }
            header("location: EntregaController.php?action=mensajeResultado&resultado=exitoModificar");
        }
        else{
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();

            //cargo la vista MESSAGE
            $claseModificarEntrega=new MESSAGE();
            $claseModificarEntrega->cargar("errorModificarEntrega",$idiom,$comprobarUsuarioGrupo);
        }
        break;

    case 'modificarEntregaUser':
        $login=$_POST['login'];
        $IdTrabajo=$_POST['IdTrabajo'];
        $Alias=$_POST['Alias'];
        $Horas=$_POST['Horas'];

        //Cargo el modelo y ejecuto la función para hacer el update
        $modelo=new Model_ENTREGA($login,$IdTrabajo,$Alias,$Horas,"");
        $resultado=$modelo->update();

        //si modifica correctamente vuelve a la vista SHOWALL.php mostrando mensaje de exito

         header("location: ActionController.php?action=ListarEntregasUser");

        break;
        
    case 'cargarSearch':     
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $vistaBuscarEntrega=new Entrega_SEARCH();
        $vistaBuscarEntrega->cargar("",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'search': 
        $login=$_POST['login'];
        $IdTrabajo=$_POST['IdTrabajo'];
        $Alias=$_POST['Alias'];
        
        $modelo=new Model_ENTREGA($login,$IdTrabajo,$Alias,"","");
        $datos=$modelo->select();
        
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoEntrega=new Entrega_SHOWALL();
        $vistaListadoEntrega->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    //carga vista SHOWCURRENT
    case 'cargarShowCurrent':
        $login=$_GET['login'];
        $IdTrabajo=$_GET['IdTrabajo'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //cargo el modelo
        $modelo=new Model_ENTREGA($login,$IdTrabajo,"","","");
        //busco al usuario a editar
        $datos=$modelo->showCurrent();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista
        $vistaDetalleEntrega=new Entrega_SHOWCURRENT();
        $vistaDetalleEntrega->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);     
        break;
    case 'cargarShowCurrentUser':
        $login=$_GET['login'];
        $IdTrabajo=$_GET['IdTrabajo'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //cargo el modelo
        $modelo=new Model_ENTREGA($login,$IdTrabajo,"","","");
        //busco al usuario a editar
        $datos=$modelo->showCurrent();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista
        $vistaDetalleEntrega=new EntregaUser_SHOWCURRENT();
        $vistaDetalleEntrega->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'showAll':
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_ENTREGA("","","","","");
        $datos=$modelo->showAll();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoEntrega=new Entrega_SHOWALL();
        $vistaListadoEntrega->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'mensajeResultado':
        $texto=$_GET['resultado'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_ENTREGA("","","","","");
        $datos=$modelo->showAll();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoEntrega=new Entrega_SHOWALL();
        $vistaListadoEntrega->cargar($datos,$texto,$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'cargarUserModificar':
        $IdTrabajo=$_GET['IdTrabajo'];
        $login=$_GET['login'];
        
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        if($login==$_SESSION['usuario']){
            //cargo el modelo
            $modelo=new Model_ENTREGA($login,$IdTrabajo,"","","");
            //busco al usuario a editar
            $datos=$modelo->showCurrent();
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $vistaModificarEntrega=new EntregaUser_EDIT();
            $vistaModificarEntrega->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        }
        else{
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista MESSAGE                  
            $claseModificarEntrega=new MESSAGE();
            $claseModificarEntrega->cargar("errorModificarEntrega",$idiom,$comprobarUsuarioGrupo);
        }
        break;
    
    //muestra todos los trabajos del usuario logeado en una vista especial
    case 'showAllUser':
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_ENTREGA($login,"","","","");
        $datos=$modelo->select();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoEntrega=new EntregaUser_SHOWALL();
        $vistaListadoEntrega->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'mensajeResultadoUser':
        $texto=$_GET['resultado'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_ENTREGA("","","","","");
        $datos=$modelo->showAll();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoEntrega=new EntregaUser_SHOWALL();
        $vistaListadoEntrega->cargar($datos,$texto,$idiom,$comprobarUsuarioGrupo);
        break;
}

function crearAlias(){
    $letra=array("a","b","c","d","e","f","g","h","i","j","k","m","n","ñ","o","p","q","r","s","t","u","v","w","x","y","z");
    $Alias="";
    for($i=0;$i<6;$i++){
        $caracter=rand(1,2);
        if($caracter==1){
            $siguiente= rand(0,9);
        }
        else{
            $toret=rand(0,26);
            $siguiente=$letra[$toret];
        }
        $Alias.=$siguiente;
    }
    return $Alias;
}

function enFecha($fin,$actual){
    if($actual["año"]<substr($fin,0,4)){
        return true;
    }
    elseif($actual["año"]>substr($fin,0,4)){
        return false;
    }
    else{
        if($actual["mes"]<substr($fin,5,2)){
            return true;
        }
        elseif($actual["mes"]>substr($fin,5,2)){
            return false;
        }
        else{
            if($actual["dia"]<=substr($fin,8,2)){
                return true;
            }
            else{
                return false;
            }
        }
    }
}
