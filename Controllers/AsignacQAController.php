<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de AsignacionQA, por lo tanto controla las distintas
 * situaciones tras realizar una operación en la asignación de una qa.
 */

session_start();
$login=$_SESSION['usuario'];
include("../Models/Model_ASIGNAC_QA.php");
include("../Views/AsignacQA_SHOWALL.php");
include("../Views/AsignacQA_ADD.php");
include("../Views/AsignacQA_EDIT.php");
include("../Views/AsignacQA_SEARCH.php");
include("../Views/MESSAGE.php"); 
include("../Views/AsignacQA_DELETE.php");
include("../Views/AsignacQA_SHOWCURRENT.php");
include("../Functions/idiomas.php");
include("../Functions/comprobaridioma.php");
include("../Models/USUARIO_Model.php");

function ComprobarUsuarioMenu(){
    //Cargo el modelo
    $modeloUser=new Usuario_Model();
    //Usuario de la sesion
    $UserSesionActual=$_SESSION['usuario'];
    //Comprueba si es admin 
    $esAdmin=$modeloUser->comprobarUsuarioADMINEnUSU_GRUPO($UserSesionActual);       

            if( ($esAdmin) == "true" ){
                $comprobarUsuarioGrupo="ADMIN";
            }else{
                $comprobarUsuarioGrupo="USER";
            }
            return $comprobarUsuarioGrupo;
}

switch ($_REQUEST['action']){
    case 'cargarAlta':
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_ASIGNAC_QA("","","","");
        $trabajos=$modelo->showAllEntregas();
        $users=$modelo->showAllUsers();
        $users2=$modelo->showAllUsers();
        
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaCrearAsig=new AsignacQA_ADD();
        $vistaCrearAsig->cargar("",$idiom,$trabajos,$users,$users2,$comprobarUsuarioGrupo);
        break;
    
    case 'alta':
        $IdTrabajo=$_POST['IdTrabajo'];
        $LoginEvaluador=$_POST['LoginEvaluador'];
        $LoginEvaluado=$_POST['LoginEvaluado'];

        $modelo=new Model_ASIGNAC_QA($IdTrabajo,$LoginEvaluador,$LoginEvaluado,"");
        $AliasEvaluado=$modelo->buscarAlias();
        
        $modelo=new Model_ASIGNAC_QA($IdTrabajo,$LoginEvaluador,$LoginEvaluado,$AliasEvaluado);
        $comprobar=$modelo->comprobarAsignacion();

        if($comprobar==false){
            $resultado=$modelo->insert();
            if($resultado==true){
                header("location: AsignacQAController.php?action=mensajeResultado&resultado=exito");
            }
            else{
                //cargo idiomas
                $idioma=new idiomas();
                $idiom=comprobaridioma($idioma);
                
                $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                
                $modelo=new Model_ASIGNAC_QA("","","","");
                $trabajos=$modelo->showAllEntregas();
                $users=$modelo->showAllUsers();
                
                //cargo la vista
                $vistaCrearAsig=new AsignacQA_ADD();
                $vistaCrearAsig->cargar("AsigRepe",$idiom,$trabajos,$users,$comprobarUsuarioGrupo);
            }
        }
        else{
            //cargo el idioma
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            $clasecrear=new MESSAGE();
            $clasecrear->cargar("DatosDuplicadosAsig",$idiom,$comprobarUsuarioGrupo);
           
        }
        break;
        
    case 'cargarBaja':
        $IdTrabajo=$_GET['IdTrabajo'];
        $LoginEvaluador=$_GET['LoginEvaluador'];
        $AliasEvaluado=$_GET['AliasEvaluado'];
        
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_ASIGNAC_QA($IdTrabajo,$LoginEvaluador,"",$AliasEvaluado);
        $datos=$modelo->showCurrent();
        
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        //cargo la vista            
        $vistaEliminarAsig=new AsignacQA_DELETE();
        $vistaEliminarAsig->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'baja':
        $IdTrabajo=$_GET['IdTrabajo'];
        $LoginEvaluador=$_GET['LoginEvaluador'];
        $AliasEvaluado=$_GET['AliasEvaluado'];
        //cargo el modelo
        $modelo=new Model_ASIGNAC_QA($IdTrabajo,$LoginEvaluador,"",$AliasEvaluado);
        //ejecuto la funcion de eliminar
        $resultado=$modelo->delete();
        //si elimina bien vuelve a la vista SHOWALL mostrando la lista actualizada y con mensaje de exito
        if($resultado==true){ 
            header("location: AsignacQAController.php?action=mensajeResultado&resultado=exitoborrar");
        }
        else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);
        }
        break;
        
    case 'cargarModificar':
        $IdTrabajo=$_GET['IdTrabajo'];
        $LoginEvaluador=$_GET['LoginEvaluador'];
        $AliasEvaluado=$_GET['AliasEvaluado'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //cargo el modelo
        $modelo=new Model_ASIGNAC_QA($IdTrabajo,$LoginEvaluador,"",$AliasEvaluado);
        $users=$modelo->showAllUsers();
        //busco al usuario a editar
        $datos=$modelo->showCurrent();
        
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        //cargo la vista
        $vistaModificarAsig=new AsignacQA_EDIT();
        $vistaModificarAsig->cargar($datos,"",$idiom,$users,$comprobarUsuarioGrupo);
        break;
    
    case 'modificar':
        $IdTrabajo=$_POST['IdTrabajo'];
        $LoginEvaluador=$_POST['LoginEvaluador'];
        $LoginEvaluado=$_POST['LoginEvaluado'];
        $AliasEvaluado=$_POST['AliasEvaluado'];
        
        if($LoginEvaluado<>""){
            $modelo=new Model_ASIGNAC_QA($IdTrabajo,$LoginEvaluador,$LoginEvaluado,"");
            $AliasNuevo=$modelo->buscarAlias();
        }
        else{
            $AliasNuevo="";
        }

        //Cargo el modelo y ejecuto la función para hacer el update
        $modelo=new Model_ASIGNAC_QA($IdTrabajo,$LoginEvaluador,$LoginEvaluado,$AliasEvaluado);
        $resultado=$modelo->update($AliasNuevo);

        //si modifica correctamente vuelve a la vista SHOWALL.php mostrando mensaje de exito
        if($resultado==true)
        { 
            header("location: AsignacQAController.php?action=mensajeResultado&resultado=exitoModificar");
        }
        else{ 
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            
            //cargo la vista MESSAGE
            $claseModificarAsig=new MESSAGE();
            $claseModificarAsig->cargar("errorModificarAsig",$idiom,$comprobarUsuarioGrupo);
        }
        break;
        
    case 'cargarSearch':     
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaBuscarAsig=new AsignacQA_SEARCH();
        $vistaBuscarAsig->cargar("",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'search': 
        $IdTrabajo=$_POST['IdTrabajo'];
        $LoginEvaluador=$_POST['LoginEvaluador'];
        $LoginEvaluado=$_POST['LoginEvaluado'];
        
        $modelo=new Model_ASIGNAC_QA($IdTrabajo,$LoginEvaluador,$LoginEvaluado,"");
        $datos=$modelo->select();
        
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();

        $vistaListadoAsig=new AsignacQA_SHOWALL();
        $vistaListadoAsig->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'cargarShowCurrent':
        $IdTrabajo=$_GET['IdTrabajo'];
        $LoginEvaluador=$_GET['LoginEvaluador'];
        $AliasEvaluado=$_GET['AliasEvaluado'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        //cargo el modelo
        $modelo=new Model_ASIGNAC_QA($IdTrabajo,$LoginEvaluador,"",$AliasEvaluado);
        //busco al usuario a editar
        $datos=$modelo->showCurrent();
        //cargo la vista
        $vistaDetalleAsig=new AsignacQA_SHOWCURRENT();
        $vistaDetalleAsig->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);     
        break;
    
    case 'showAll':
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $modelo=new Model_ASIGNAC_QA("","","","");
        $datos=$modelo->showAll();
        
        $vistaListadoAsig=new AsignacQA_SHOWALL();
        $vistaListadoAsig->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'mensajeResultado':
        $texto=$_GET['resultado'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $modelo=new Model_ASIGNAC_QA("","","","");
        $datos=$modelo->showAll();
        
        $vistaListadoAsig=new AsignacQA_SHOWALL();
        $vistaListadoAsig->cargar($datos,$texto,$idiom,$comprobarUsuarioGrupo);
        break;

    //muestra todos las QAS del usuario logeado 
    case 'showAllUser':
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_ASIGNAC_QA($login,"","","");
        $datos=$modelo->select();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoEntrega=new AsignacQA_SHOWALL();
        $vistaListadoEntrega->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
}