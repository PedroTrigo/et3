<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de notas, por lo tanto controla las distintas
 * operaciones posibles sobre ellas.
 */

include("../Models/Model_NOTA.php");
include("../Views/Nota_SHOWALL.php");
include ("../Views/Nota_ADD.php");
include ("../Views/Nota_SEARCH.php");
include ("../Views/Nota_SHOWCURRENT.php");
include ("../Views/Nota_DELETE.php");
include ("../Views/Nota_EDIT.php");
include("../Views/MESSAGE.php");

      function cargarCrearNota()
      {
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          //Comprueba la sesión de ususario para mostrar el menú
          $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
          $claseCrearNota=new Nota_ADD();
          $claseCrearNota->cargar("",$idiom,$comprobarUsuarioGrupo);
      }

      function cargarAltaNota()
      {
          $login=$_POST['login'];
          $IdTrabajo=$_POST['IdTrabajo'];
          $NotaTrabajo=$_POST['NotaTrabajo'];

          // Ruta donde se guardarán las imágenes que subamos
          //$directorio = $_SERVER['DOCUMENT_ROOT'].'/56fbqn/Files/';
          // Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
          //move_uploaded_file($_FILES['fotoUserAdd']['tmp_name'],$directorio.$fotopersonal);

          $modeloNota=new Nota_Model();
          $NotaExistente=$modeloNota->comprobarNota($login,$IdTrabajo);

          if($NotaExistente==false){
              $resultado=$modeloNota->crearNota($login,$IdTrabajo,$NotaTrabajo);
              if($resultado==true){
                  header("location: ActionController.php?action=exitoCrearNota");
              }else{
                  //cargo idiomas
                  $idioma=new idiomas();
                  $idiom=comprobaridioma($idioma);
                  //Comprueba la sesión de ususario para mostrar el menú
                  $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                  //cargo la vista
                  $clasecrearNota=new Nota_ADD();
                  $clasecrearNota->cargar("NotaRepe",$idiom,$comprobarUsuarioGrupo);
              }
          }else{
              //cargo el idioma
              $idioma=new idiomas();
              $idiom=comprobaridioma($idioma);
              //Comprueba la sesión de ususario para mostrar el menú
              $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
              $clasecrear=new MESSAGE();
              $clasecrear->cargar("DatosDuplicados",$idiom,$comprobarUsuarioGrupo);

          }
      }

      function cargarEliminarNota($IdTrabajo){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          //cargo el array de historias
          $modeloNota=new Nota_Model();
          $datos=$modeloHistoria->buscarPorIdTrabajo($IdTrabajo);
          //Comprueba la sesión de ususario para mostrar el menú
          $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
          //cargo la vista
          $claseEliminarNota=new Nota_DELETE();
          $claseEliminarNota->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
      }

      function cargarBajaNota($login, $IdTrabajo){
          //cargo el modelo
          $modeloNota=new Nota_Model();
          //ejecuto la funcion de eliminar
          $resultado=$modeloNota->eliminarNota($IdTrabajo);
          //si elimina bien vuelve a la vista nota_SHOWALL mostrando la lista de usuarios actualizada y con mensaje de exito
          if($resultado==true){
              header("location: ActionController.php?action=exitoEliminarNota");
          }else{
              //sino muestra el mesaje de error en la vista MESSAGE
              $idioma=new idiomas();
              $idiom=comprobaridioma($idioma);
              //$datos=$modeloUser->buscarPorLogin($login);
              //Comprueba la sesión de ususario para mostrar el menú
              $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
              $claseVistaMESSAGE=new MESSAGE();
              $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);
          }
      }
      //Recoge los datos de la historia de la vista historia_EDIT.php
      function modificarNota(){
          $login=$_POST['login'];
          $IdTrabajo=$_POST['IdTrabajo'];
          $NotaTrabajo=$_POST['NotaTrabajo'];

          // Ruta donde se guardarán las imágenes que subamos
          //$directorio = $_SERVER['DOCUMENT_ROOT'].'/56fbqn/Files/';
          // Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
          //move_uploaded_file($_FILES['fotoUserEdit']['tmp_name'],$directorio.$fotopersonal);


          //Cargo el modelo y ejecuto la función para hacer el update
          $modeloNota=new Nota_Model();
          $resultado=$modeloNota-> modificarNota($login,$IdTrabajo,$NotaTrabajo);

          //si modifica correctamente vuelve a la vista nota_SHOWALL.php mostrando mensaje de exito
          if($resultado==true)
          {
              header("location: ActionController.php?action=exitoModificarNota");
              //Si no muestra mensaje de error y vuelve a la vista nota_EDIT.php
          }else{
              //cargo idiomas
              $idioma=new idiomas();
              $idiom=comprobaridioma($idioma);
              //Comprueba la sesión de ususario para mostrar el menú
              $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
              //cargo la vista MESSAGE
              $claseModificarNota=new MESSAGE();
              $claseModificarNota->cargar("errorModificar",$idiom,$comprobarUsuarioGrupo);
          }
      }
      //Carga la vista nota_EDIT
      function cargarModificarNota($IdTrabajo){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          //cargo el modelo
          $modeloNota=new Nota_Model();
          //busco la nota a editar
          $datos=$modeloNota->buscarPorIdTrabajo($IdTrabajo);
          //Comprueba la sesión de ususario para mostrar el menú
          $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
          //cargo la vista
          $claseModificarNota=new Nota_EDIT();
          $claseModificarNota->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
      }

      //carga vista SHOWCURRENT
      function cargarShowCurrentNota($IdTrabajo){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          //cargo el modelo
          $modeloNota=new Nota_Model();
          //busco la nota a editar
          $datos=$modeloNota->buscarPorIdTrabajo($IdTrabajo);
          //Comprueba la sesión de ususario para mostrar el menú
          $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
          //cargo la vista
          $claseModificarNota=new Nota_SHOWCURRENT();
          $claseModificarNota->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);

      }
      //Carga la vista SEARCH
      function cargarBuscarNota(){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          //Comprueba la sesión de ususario para mostrar el menú
          $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
          $claseCrearNota=new Nota_SEARCH();
          $claseCrearNota->cargar("",$idiom,$comprobarUsuarioGrupo);
      }

      function cargarSearchN(){

          $login=$_POST['login'];
          $IdTrabajo=$_POST['IdTrabajo'];
          $NotaTrabajo=$_POST['NotaTrabajo'];

          if($login==""){
              $login=null;
          }else if ($IdTrabajo==""){
              $IdTrabajo=null;
          }else if ($NotaTrabajo==""){
              $NotaTrabajo=null;
          }
          $model=new Nota_Model();
          $datos=$model->buscarNota($login,$IdTrabajo,$NotaTrabajo);
          return $datos;
      }






?>