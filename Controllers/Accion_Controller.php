<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de acciones, por lo tanto controla las distintas
 * operaciones posibles sobre ellas.
 */
include("../Models/Accion_Model.php");
include("../Views/Accion_SHOWALL.php");
include ("../Views/Accion_ADD.php");
include ("../Views/Accion_SEARCH.php");
include ("../Views/Accion_SHOWCURRENT.php");
include ("../Views/Accion_DELETE.php");
include ("../Views/Accion_EDIT.php");
include_once("../Views/MESSAGE.php");


function cargarCrearAccion()
{
    //cargo idiomas
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //Comprueba la sesión de ususario para mostrar el menú
    $comprobarUsuarioGrupo=ComprobarUsuarioMenu();    
    //Carga la vista
    $claseCrearAccion=new Accion_ADD();
    $claseCrearAccion->cargar("",$idiom,$comprobarUsuarioGrupo);
}

function cargarAltaAccion()
{
    $IdAccion=$_POST['IdAccion'];
    $NombreAccion=$_POST['NombreAccion'];
    $DescripAccion=$_POST['DescripAccion'];

    // Ruta donde se guardarán las imágenes que subamos
    //$directorio = $_SERVER['DOCUMENT_ROOT'].'/56fbqn/Files/';
    // Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
    //move_uploaded_file($_FILES['fotoUserAdd']['tmp_name'],$directorio.$fotopersonal);

    $modeloAccion=new Accion_Model();
    $AccionExistente=$modeloAccion->comprobarAccion($IdAccion,$NombreAccion);

    if($AccionExistente==false){
        $resultado=$modeloAccion->crearAccion($IdAccion,$NombreAccion,$DescripAccion);
        if($resultado==true){
            header("location: ActionController.php?action=exitoCrearAccion");
        }else{
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $clasecrearAccion=new Accion_ADD();
            $clasecrearAccion->cargar("UsuarioRepe",$idiom,$comprobarUsuarioGrupo);
        }
    }else{
        //cargo el idioma
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista    
        $clasecrear=new MESSAGE();
        $clasecrear->cargar("DatosDuplicados Acción",$idiom,$comprobarUsuarioGrupo);

    }
}

function cargarEliminarAccion($IdAccion){
    //cargo idiomas
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //cargo el array de usuarios
    $modeloAccion=new Accion_Model();
    $datos=$modeloAccion->buscarPorId($IdAccion);
    //Comprueba la sesión de ususario para mostrar el menú
    $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
    //cargo la vista
    $claseEliminarAccion=new Accion_DELETE();
    $claseEliminarAccion->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
}

function cargarBajaAccion($IdAccion){
    //cargo el modelo
    $modeloAccion=new Accion_Model();
    //ejecuto la funcion de eliminar
    $resultado=$modeloAccion->eliminarAccion($IdAccion);
    //si elimina bien vuelve a la vista usuario_SHOWALL mostrando la lista de usuarios actualizada y con mensaje de exito
    if($resultado==true){
        header("location: ActionController.php?action=exitoEliminarAccion");
    }else{
        //sino muestra el mesaje de error en la vista MESSAGE
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $claseVistaMESSAGE=new MESSAGE();
        $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);
    }
}
//Recoge los datos del usuario de la vista usuario_EDIT.php
function modificarAccion(){
    $IdAccion=$_POST['IdAccion'];
    $NombreAccion=$_POST['NombreAccion'];
    $DescripAccion=$_POST['DescripAccion'];


    // Ruta donde se guardarán las imágenes que subamos
    //$directorio = $_SERVER['DOCUMENT_ROOT'].'/56fbqn/Files/';
    // Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
    //move_uploaded_file($_FILES['fotoUserEdit']['tmp_name'],$directorio.$fotopersonal);


    //Cargo el modelo y ejecuto la función para hacer el update
    $modeloAccion=new Accion_Model();
    $resultado=$modeloAccion-> modificarAccion($IdAccion,$NombreAccion,$DescripAccion);

    //si modifica correctamente vuelve a la vista usuario_SHOWALL.php mostrando mensaje de exito
    if($resultado==true)
    {
        header("location: ActionController.php?action=exitoModificarAccion");
        //Si no muestra mensaje de error y vuelve a la vista usuario_EDIT.php
    }else{
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista MESSAGE
        $claseModificarAccion=new MESSAGE();
        $claseModificarAccion->cargar("errorModificar",$idiom,$comprobarUsuarioGrupo);
    }
}
//Carga la vista usuario_EDIT
function cargarModificarAccion($IdAccion){
    //cargo idiomas
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //cargo el modelo
    $modeloAccion=new Accion_Model();
    //busco al usuario a editar
    $datos=$modeloAccion->buscarPorId($IdAccion);
    //Comprueba la sesión de ususario para mostrar el menú
    $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
    //cargo la vista
    $claseModificarAccion=new Accion_EDIT();
    $claseModificarAccion->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
}

//carga vista SHOWCURRENT
function cargarShowCurrentAccion($IdAccion){
    //cargo idiomas
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //cargo el modelo
    $modeloAccion=new Accion_Model();
    //busco al usuario a editar
    $datos=$modeloAccion->buscarPorId($IdAccion);
    //Comprueba la sesión de ususario para mostrar el menú
    $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
    //cargo la vista
    $claseModificarAccion=new Accion_SHOWCURRENT();
    $claseModificarAccion->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);

}
//Carga la vista SEARCH
function cargarBuscarAccion(){
    //cargo idiomas
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //Comprueba la sesión de ususario para mostrar el menú
    $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
    $claseCrearAccion=new Accion_SEARCH();
    $claseCrearAccion->cargar("",$idiom,$comprobarUsuarioGrupo);
}

function cargarSearchA(){

    $IdAccion=$_POST['IdAccion'];
    $NombreAccion=$_POST['NombreAccion'];
    $DescripAccion=$_POST['DescripAccion'];

    if($NombreAccion==""){
        $NombreAccion=null;
    }else if ($IdAccion==""){
        $IdAccion=null;
    }else if ($DescripAccion=="") {
        $DescripAccion = null;
    }
    $model=new Accion_Model();
    $datos=$model->buscarAccion($IdAccion,$NombreAccion,$DescripAccion);
    return $datos;
}






?>