<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de permisos,
 * por lo tanto controla los permisos que se pueden dar.
 */
session_start();
$login=$_SESSION['usuario'];

include("../Models/PERMISO_Model.php");
include("../Models/USUARIO_Model.php");
include("../Views/Permiso_SHOWALL.php");
include("../Views/Permiso_ADD.php");
include("../Views/Permiso_EDIT.php");
include("../Views/Permiso_SEARCH.php");
include("../Views/MESSAGE.php"); 
include("../Views/Permiso_DELETE.php");
include("../Views/Permiso_SHOWCURRENT.php");
include("../Functions/idiomas.php");
include("../Functions/comprobaridioma.php");

function ComprobarUsuarioMenu(){
    //Cargo el modelo
    $modeloUser=new Usuario_Model();
    //Usuario de la sesion
    $UserSesionActual=$_SESSION['usuario'];
    //Comprueba si es admin 
    $esAdmin=$modeloUser->comprobarUsuarioADMINEnUSU_GRUPO($UserSesionActual);       

            if( ($esAdmin) == "true" ){
                $comprobarUsuarioGrupo="ADMIN";
            }else{
                $comprobarUsuarioGrupo="USER";
            }
            return $comprobarUsuarioGrupo;
    }
	
switch ($_REQUEST['action']){
    //carga formulario de alta de entrega
    case 'cargarAlta':
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_PERMISO("","","");
        $grupos=$modelo->grupos();
        $funcionalidades=$modelo->funcionalidades();
		$acciones=$modelo->acciones();
        
		$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
		
        $vistaCrearPermiso=new Permiso_ADD();
        $vistaCrearPermiso->cargar("",$idiom,$grupos,$funcionalidades,$acciones,$comprobarUsuarioGrupo);
        break;
    
    //da de alta entrega
    case 'alta':
        $IdGrupo=$_POST['IdGrupo'];
        $IdFuncionalidad=$_POST['IdFuncionalidad'];
        $IdAccion=$_POST['IdAccion'];

        $modelo=new Model_PERMISO($IdGrupo,$IdFuncionalidad,$IdAccion);
        $comprobar=$modelo->comprobarPermiso();

        if($comprobar==false){
            $resultado=$modelo->insert();
            if($resultado==true){
                header("location: PermisoController.php?action=mensajeResultado&resultado=exito");
            }
            else{
                $modelo=new Model_PERMISO("","","");
                $grupos=$modelo->grupos();
				$funcionalidades=$modelo->funcionalidades();
				$acciones=$modelo->acciones();
                //cargo idiomas
                $idioma=new idiomas();
                $idiom=comprobaridioma($idioma);
				
				$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                //cargo la vista
                $vistaCrearPermiso=new Permiso_ADD();
                $vistaCrearPermiso->cargar("PermisoRepe",$idiom,$grupos,$funcionalidades,$acciones,$comprobarUsuarioGrupo);
            }
        }
        else{
            $modelo=new Model_PERMISO("","","","","");
            $grupos=$modelo->grupos();
			$funcionalidades=$modelo->funcionalidades();
			$acciones=$modelo->acciones();
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
			
			$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $vistaCrearPermiso=new Permiso_ADD();
            $vistaCrearPermiso->cargar("PermisoRepe",$idiom,$grupos,$funcionalidades,$acciones,$comprobarUsuarioGrupo);
        }
        break;
    
    //carga la vista para eliminar una entrega
    case 'cargarBaja':
        $IdGrupo=$_GET['IdGrupo'];
        $IdFuncionalidad=$_GET['IdFuncionalidad'];
		$IdAccion=$_GET['IdAccion'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_PERMISO($IdGrupo,$IdFuncionalidad,$IdAccion);
        $datos=$modelo->showCurrent();
		
		$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista            
        $vistaEliminarPermiso=new Permiso_DELETE();
        $vistaEliminarPermiso->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    //da de baja entrega
    case 'baja':
		$IdGrupo=$_GET['IdGrupo'];
        $IdFuncionalidad=$_GET['IdFuncionalidad'];
		$IdAccion=$_GET['IdAccion'];
        //cargo el modelo
        $modelo=new Model_PERMISO($IdGrupo,$IdFuncionalidad,$IdAccion);
        //ejecuto la funcion de eliminar
        $resultado=$modelo->delete();
        //si elimina bien vuelve a la vista SHOWALL mostrando la lista actualizada y con mensaje de exito
        if($resultado==true){ 
            header("location: PermisoController.php?action=mensajeResultado&resultado=exitoborrar");
        }
        else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
			
			$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);
        }
        break;
        
    case 'cargarModificar':
		$IdGrupo=$_GET['IdGrupo'];
        $IdFuncionalidad=$_GET['IdFuncionalidad'];
		$IdAccion=$_GET['IdAccion'];
        
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        //cargo el modelo
        $modelo=new Model_PERMISO($IdGrupo,$IdFuncionalidad,$IdAccion);
        //busco al usuario a editar
        $datos=$modelo->showCurrent();
		
		$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista
        $vistaModificarPermiso=new Permiso_EDIT();
        $vistaModificarPermiso->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'modificar':
        $IdGrupo=$_POST['IdGrupo'];
        $IdFuncionalidad=$_POST['IdFuncionalidad'];
		$IdAccion=$_POST['IdAccion'];

        //Cargo el modelo y ejecuto la función para hacer el update
        $modelo=new Model_PERMISO($IdGrupo,$IdFuncionalidad,$IdAccion);
        $resultado=$modelo->update();

        //si modifica correctamente vuelve a la vista SHOWALL.php mostrando mensaje de exito
        if($resultado==true)
        { 
            if(isset($_GET['user'])){
                header("location: PermisoController.php?action=mensajeResultadoUser&resultado=exitoModificar");
            }
            header("location: PermisoController.php?action=mensajeResultado&resultado=exitoModificar");
        }
        else{ 
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
			
			$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista MESSAGE                  
            $claseModificarPermiso=new MESSAGE();
            $claseModificarPermiso->cargar("errorModificarPermiso",$idiom,$comprobarUsuarioGrupo);
        }
        break;
        
    case 'cargarSearch':     
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        $vistaBuscarPermiso=new Permiso_SEARCH();
		
		$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
		
        $vistaBuscarPermiso->cargar("",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'search': 
        $IdGrupo=$_POST['IdGrupo'];
        $IdFuncionalidad=$_POST['IdFuncionalidad'];
		$IdAccion=$_POST['IdAccion'];
        
        $modelo=new Model_PERMISO($IdGrupo,$IdFuncionalidad,$IdAccion);
        $datos=$modelo->select();
        
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
		
		$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoPermiso=new Permiso_SHOWALL();
        $vistaListadoPermiso->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    //carga vista SHOWCURRENT
    case 'cargarShowCurrent':
		$IdGrupo=$_GET['IdGrupo'];
        $IdFuncionalidad=$_GET['IdFuncionalidad'];
		$IdAccion=$_GET['IdAccion'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //cargo el modelo
        $modelo=new Model_PERMISO($IdGrupo,$IdFuncionalidad,$IdAccion);
        //busco el permiso a editar
        $datos=$modelo->showCurrent();
        //cargo la vista
		$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
		
        $vistaDetallePermiso=new Permiso_SHOWCURRENT();
        $vistaDetallePermiso->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);     
        break;
    
    case 'showAll':
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_PERMISO("","","");
        $datos=$modelo->showAll();
        
		$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
		
        $vistaListadoPermiso=new Permiso_SHOWALL();
        $vistaListadoPermiso->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'mensajeResultado':
        $texto=$_GET['resultado'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_PERMISO("","","");
        $datos=$modelo->showAll();
        
		$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
		
        $vistaListadoPermiso=new Permiso_SHOWALL();
        $vistaListadoPermiso->cargar($datos,$texto,$idiom,$comprobarUsuarioGrupo);
        break;
    
    
    }

