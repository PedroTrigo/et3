<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de grupos, por lo tanto controla las distintas
 * operaciones posibles sobre ellos.
 */

include("../Models/GRUPO_Model.php");
include("../Views/Grupo_SHOWALL.php");
include ("../Views/Grupo_ADD.php");
include ("../Views/Grupo_SEARCH.php");
include ("../Views/Grupo_SHOWCURRENT.php");
include ("../Views/Grupo_DELETE.php");
include ("../Views/Grupo_EDIT.php");
include("../Views/MESSAGE.php");

      function cargarCrearGrupo()
      {
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          $claseCrearGrupo=new Grupo_ADD();
          $claseCrearGrupo->cargar("",$idiom);
      }

      function cargarAltaGrupo()
      {
          $IdGrupo=$_POST['IdGrupo'];
          $NombreGrupo=$_POST['NombreGrupo'];
          $DescripGrupo=$_POST['DescripGrupo'];

          // Ruta donde se guardarán las imágenes que subamos
          //$directorio = $_SERVER['DOCUMENT_ROOT'].'/56fbqn/Files/';
          // Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
          //move_uploaded_file($_FILES['fotoUserAdd']['tmp_name'],$directorio.$fotopersonal);

          $modeloGrupo=new Grupo_Model();
          $GrupoExistente=$modeloGrupo->comprobarGrupo($IdGrupo);

          if($GrupoExistente==false){
              $resultado=$modeloGrupo->crearGrupo($IdGrupo,$NombreGrupo,$DescripGrupo);              
              if($resultado==true){
                  header("location: ActionController.php?action=exitoCrearGrupo");
              }else{
                  //cargo idiomas
                  $idioma=new idiomas();
                  $idiom=comprobaridioma($idioma);
                  //cargo la vista
                  $clasecrearGrupo=new Grupo_ADD();
                  $clasecrearGrupo->cargar("GrupoRepe",$idiom);
              }
          }else{
              //cargo el idioma
              $idioma=new idiomas();
              $idiom=comprobaridioma($idioma);
              $clasecrear=new MESSAGE();
              $clasecrear->cargar("DatosDuplicados",$idiom);

          }
      }

      function cargarEliminarGrupo($IdGrupo){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          //cargo el array de historias
          $modeloGrupo=new Grupo_Model();
          $datos=$modeloGrupo->buscarPorIdGrupo($IdGrupo);
          //cargo la vista
          $claseEliminarGrupo=new Grupo_DELETE();
          $claseEliminarGrupo->cargar($datos,"",$idiom);
      }

      function cargarBajaGrupo($IdGrupo){
          //cargo el modelo
          $modeloGrupo=new Grupo_Model();
          //ejecuto la funcion de eliminar
          $resultado=$modeloGrupo->eliminarGrupo($IdGrupo);
          //si elimina bien vuelve a la vista grupo_SHOWALL mostrando la lista de usuarios actualizada y con mensaje de exito
          if($resultado==true){
              header("location: ActionController.php?action=exitoEliminarGrupo");
          }else{
              //sino muestra el mesaje de error en la vista MESSAGE
              $idioma=new idiomas();
              $idiom=comprobaridioma($idioma);
              //$datos=$modeloUser->buscarPorLogin($login);
              $claseVistaMESSAGE=new MESSAGE();
              $claseVistaMESSAGE->cargar("errorDELETE",$idiom);
          }
      }
      //Recoge los datos de la historia de la vista grupo_EDIT.php
      function modificarGrupo(){
          $IdGrupo=$_POST['IdGrupo'];
          $NombreGrupo=$_POST['NombreGrupo'];
          $DescripGrupo=$_POST['DescripGrupo'];

          // Ruta donde se guardarán las imágenes que subamos
          //$directorio = $_SERVER['DOCUMENT_ROOT'].'/56fbqn/Files/';
          // Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
          //move_uploaded_file($_FILES['fotoUserEdit']['tmp_name'],$directorio.$fotopersonal);


          //Cargo el modelo y ejecuto la función para hacer el update
          $modeloGrupo=new Grupo_Model();
          $resultado=$modeloGrupo-> modificarGrupo($IdGrupo,$NombreGrupo,$DescripGrupo);

          //si modifica correctamente vuelve a la vista grupo_SHOWALL.php mostrando mensaje de exito
          if($resultado==true)
          {
              header("location: ActionController.php?action=exitoModificarGrupo");
              //Si no muestra mensaje de error y vuelve a la vista grupo_EDIT.php
          }else{
              //cargo idiomas
              $idioma=new idiomas();
              $idiom=comprobaridioma($idioma);
              //cargo la vista MESSAGE
              $claseModificarGrupo=new MESSAGE();
              $claseModificarGrupo->cargar("errorModificar",$idiom);
          }
      }
      //Carga la vista grupo_EDIT
      function cargarModificarGrupo($IdGrupo){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          //cargo el modelo
          $modeloGrupo=new Grupo_Model();
          //busco el grupo a editar
          $datos=$modeloGrupo->buscarPorIdGrupo($IdGrupo);
          //cargo la vista
          $claseModificarGrupo=new Grupo_EDIT();
          $claseModificarGrupo->cargar($datos,"",$idiom);
      }

      //carga vista SHOWCURRENT
      function cargarShowCurrentGrupo($IdGrupo){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          //cargo el modelo
          $modeloGrupo=new Grupo_Model();
          //busco la historia a editar
          $datos=$modeloGrupo->buscarPorIdGrupo($IdGrupo);
          //cargo la vista
          $claseModificarGrupo=new Grupo_SHOWCURRENT();
          $claseModificarGrupo->cargar($datos,"",$idiom);

      }
      //Carga la vista SEARCH
      function cargarBuscarGrupo(){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          $claseCrearGrupo=new Grupoa_SEARCH();
          $claseCrearGrupo->cargar("",$idiom);
      }

      function cargarSearchG(){

          $IdGrupo=$_POST['IdGrupo'];
          $NombreGrupo=$_POST['NombreGrupo'];
          $DescripGrupo=$_POST['DescripGrupo'];

          if($IdGrupo==""){
              $IdGrupo=null;
          }else if ($NombreGrupo==""){
              $NombreGrupo=null;
          }else if ($DescripGrupo==""){
              $DescripGrupo=null;
          }
          $model=new Grupo_Model();
          $datos=$model->buscarGrupo($IdGrupo,$NombreGrupo,$DescripGrupo);
          return $datos;
      }






?>