<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de funcionalidades, por lo tanto controla las distintas
 * operaciones posibles sobre ellas.
 */
session_start();
include("../Models/FUNCIONALIDAD_Model.php");
include("../Models/USUARIO_Model.php");
include("../Views/Funcionalidad_SHOWALL.php");
include("../Views/Funcionalidad_ADD.php");
include("../Views/Funcionalidad_EDIT.php");
include("../Views/Funcionalidad_SEARCH.php");
include("../Views/MESSAGE.php"); 
include("../Views/Funcionalidad_DELETE.php");
include("../Views/Funcionalidad_SHOWCURRENT.php");
include("../Views/AsignarAccion.php");
include("../Views/DesasignarAccion.php");
include("../Functions/idiomas.php");
include("../Functions/comprobaridioma.php");

function ComprobarUsuarioMenu(){
    //Cargo el modelo
    $modeloUser=new Usuario_Model();
    //Usuario de la sesion
    $UserSesionActual=$_SESSION['usuario'];
    //Comprueba si es admin 
    $esAdmin=$modeloUser->comprobarUsuarioADMINEnUSU_GRUPO($UserSesionActual);       

            if( ($esAdmin) == "true" ){
                $comprobarUsuarioGrupo="ADMIN";
            }else{
                $comprobarUsuarioGrupo="USER";
            }
            return $comprobarUsuarioGrupo;
    }

switch ($_REQUEST['action']){
    //carga formulario de alta de funcionalidad
    case 'cargarAlta':
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $vistaCrearFun=new Funcionalidad_ADD();
        $vistaCrearFun->cargar("",$idiom,$comprobarUsuarioGrupo);
        break;
    
    //da de alta funcionalidad
    case 'alta':
        $IdFuncionalidad=$_POST['IdFuncionalidad'];
        $NombreFuncionalidad=$_POST['NombreFuncionalidad'];
        $DescripFuncionalidad=$_POST['DescripFuncionalidad'];

        $modelo=new FUNCIONALIDAD_Model($IdFuncionalidad,$NombreFuncionalidad,$DescripFuncionalidad);
        $comprobar=$modelo->comprobarFuncionalidad();

        if($comprobar==false){
            $resultado=$modelo->insert();
            if($resultado==true){
                header("location: FuncionalidadController.php?action=mensajeResultado&resultado=exito");
            }
            else{
                //cargo idiomas
                $idioma=new idiomas();
                $idiom=comprobaridioma($idioma);
                //Comprueba la sesión de ususario para mostrar el menú
                $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                //cargo la vista
                $vistaCrearFun=new Funcionalidad_ADD();
                $vistaCrearFun->cargar("FuncionalidadRepe",$idiom,$comprobarUsuarioGrupo);
            }
        }
        else{
            //cargo el idioma
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();

            $clasecrear=new MESSAGE();
            $clasecrear->cargar("DatosDuplicadosFun",$idiom,$comprobarUsuarioGrupo);
           
        }
        break;
    
    //carga la vista para eliminar una funcionalidad
    case 'cargarBaja':
        $IdFuncionalidad=$_GET['IdFuncionalidad'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new FUNCIONALIDAD_Model($IdFuncionalidad,"","");
        $datos=$modelo->showCurrent();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();

        //cargo la vista            
        $vistaEliminarFun=new Funcionalidad_DELETE();
        $vistaEliminarFun->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    //da de baja funcionalidad
    case 'baja':
        $IdFuncionalidad=$_GET['IdFuncionalidad'];
        //cargo el modelo
        $modelo=new FUNCIONALIDAD_Model($IdFuncionalidad,"","");
        //ejecuto la funcion de eliminar
        $resultado=$modelo->delete();
        //si elimina bien vuelve a la vista SHOWALL mostrando la lista actualizada y con mensaje de exito
        if($resultado==true){ 
            header("location: FuncionalidadController.php?action=mensajeResultado&resultado=exitoborrar");
        }
        else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);
        }
        break;
    
    case 'cargarModificar':
        $IdFuncionalidad=$_GET['IdFuncionalidad'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //cargo el modelo
        $modelo=new FUNCIONALIDAD_Model($IdFuncionalidad,"","");
        //busco al usuario a editar
        $datos=$modelo->showCurrent();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista
        $vistaModificarFun=new Funcionalidad_EDIT();
        $vistaModificarFun->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'modificar':
        $IdFuncionalidad=$_POST['IdFuncionalidad'];
        $NombreFuncionalidad=$_POST['NombreFuncionalidad'];
        $DescripFuncionalidad=$_POST['DescripFuncionalidad'];

        //Cargo el modelo y ejecuto la función para hacer el update
        $modelo=new FUNCIONALIDAD_Model($IdFuncionalidad,$NombreFuncionalidad,$DescripFuncionalidad);
        $resultado=$modelo->update();

        //si modifica correctamente vuelve a la vista SHOWALL.php mostrando mensaje de exito
        if($resultado==true)
        { 
            header("location: FuncionalidadController.php?action=mensajeResultado&resultado=exitoModificar");
        }
        else{ 
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista MESSAGE                  
            $claseModificarFun=new MESSAGE();
            $claseModificarFun->cargar("errorModificarFun",$idiom,$ComprobarUsuarioMenu);
        }
        break;
    
    case 'cargarSearch':     
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $vistaBuscarFun=new Funcionalidad_SEARCH();
        $vistaBuscarFun->cargar("",$idiom,$comprobarUsuarioGrupo);
        break;
      
    case 'search': 
        $IdFuncionalidad=$_POST['IdFuncionalidad'];
        $NombreFuncionalidad=$_POST['NombreFuncionalidad'];
        
        $modelo=new FUNCIONALIDAD_Model($IdFuncionalidad, $NombreFuncionalidad,"");
        $acciones=$modelo->showAllAccion();
        $datos=$modelo->select();
        
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();

        $vistaListadoFun=new Funcionalidad_SHOWALL();
        $vistaListadoFun->cargar($datos,"",$idiom,$acciones,$comprobarUsuarioGrupo);
        break;
    
    //carga vista SHOWCURRENT
    case 'cargarShowCurrent':
        $IdFuncionalidad=$_GET['IdFuncionalidad'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //cargo el modelo
        $modelo=new FUNCIONALIDAD_Model($IdFuncionalidad,"","");
        //busco al usuario a editar
        $datos=$modelo->showCurrent();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista
        $vistaDetalleFun=new Funcionalidad_SHOWCURRENT();
        $vistaDetalleFun->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);     
        break;
    
    case 'showAll':
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new FUNCIONALIDAD_Model("","","");
        $datos=$modelo->showAll();
        $acciones=$modelo->showAllAccion();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoFun=new Funcionalidad_SHOWALL();
        $vistaListadoFun->cargar($datos,"",$idiom,$acciones,$comprobarUsuarioGrupo); //CAMBIAR
        break;
    
    case 'mensajeResultado':
        $texto=$_GET['resultado'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new FUNCIONALIDAD_Model("","","");
        $datos=$modelo->showAll();
        $acciones=$modelo->showAllAccion();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoFun=new Funcionalidad_SHOWALL();
        $vistaListadoFun->cargar($datos,$texto,$idiom,$acciones,$comprobarUsuarioGrupo);
        break;
    
    case 'cargarAsignarAccion':
        $IdFuncionalidad=$_GET['IdFuncionalidad'];
        
        $modelo=new FUNCIONALIDAD_Model($IdFuncionalidad,"","");
        $acciones=$modelo->accionesPorAsignar();
        
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaAsignarAccion=new AsignarAccion();
        $vistaAsignarAccion->cargar("",$idiom,$IdFuncionalidad,$acciones,$comprobarUsuarioGrupo); //HACER ESTE
        break;
    
    case 'asignarAccion':
        $IdFuncionalidad=$_POST['IdFuncionalidad'];
        $IdAccion=$_POST['IdAccion'];
        
        $modelo=new FUNCIONALIDAD_Model($IdFuncionalidad,"","");
        $resultado=$modelo->asignarAccion($IdAccion);
        if($resultado==true){ 
            header("location: FuncionalidadController.php?action=mensajeResultado&resultado=exitoAsignar");
        }
        else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);

            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorAsignarAccion",$idiom,$comprobarUsuarioGrupo);
        }
        break;
        
    case 'cargarDesasignarAccion':
        $IdFuncionalidad=$_GET['IdFuncionalidad'];
        $IdAccion=$_GET['IdAccion'];
        
        $modelo=new FUNCIONALIDAD_Model($IdFuncionalidad,"","");
        $funcionalidad=$modelo->showCurrent();
        $accion=$modelo->showCurrentAccion($IdAccion);
        
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaAsignarAccion=new DesasignarAccion();
        $vistaAsignarAccion->cargar($datos,"",$idiom,$funcionalidad,$accion,$comprobarUsuarioGrupo); //HACER ESTE
        break;
    
    case 'desasignarAccion':
        $IdFuncionalidad=$_GET['IdFuncionalidad'];
        $IdAccion=$_GET['IdAccion'];
        
        $modelo=new FUNCIONALIDAD_Model($IdFuncionalidad,"","");
        $resultado=$modelo->desasignarAccion($IdAccion);
        if($resultado==true){ 
            header("location: FuncionalidadController.php?action=mensajeResultado&resultado=exitoDesasignar");
        }
        else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);

            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorDesasignarAccion",$idiom,$comprobarUsuarioGrupo);
        }
        break;
}