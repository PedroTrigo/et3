<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador del index, por lo tanto
 * accede a la aplicación si el usuario está logeado.
 */
session_start();
	if (!isset($_SESSION['usuario'])){
		header("location: ActionController.php?action=acceder");
	}else{
		header("location: ActionController.php?action=logeado");
	}
?>