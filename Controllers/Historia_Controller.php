<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de historias, por lo tanto controla las distintas
 * operaciones posibles sobre ellas.
 */

include("../Models/Model_HISTORIA.php");
include("../Views/Historia_SHOWALL.php");
include ("../Views/Historia_ADD.php");
include ("../Views/Historia_SEARCH.php");
include ("../Views/Historia_SHOWCURRENT.php");
include ("../Views/Historia_DELETE.php");
include ("../Views/Historia_EDIT.php");
include("../Views/MESSAGE.php");

      function cargarCrearHistoria()
      {
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          $claseCrearHistoria=new Historia_ADD();
          $claseCrearHistoria->cargar("",$idiom);
      }

      function cargarAltaHistoria()
      {
          $IdTrabajo=$_POST['IdTrabajo'];
          $IdHistoria=$_POST['IdHistoria'];
          $TextoHistoria=$_POST['TextoHistoria'];

          // Ruta donde se guardarán las imágenes que subamos
          //$directorio = $_SERVER['DOCUMENT_ROOT'].'/56fbqn/Files/';
          // Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
          //move_uploaded_file($_FILES['fotoUserAdd']['tmp_name'],$directorio.$fotopersonal);

          $modeloHistoria=new Historia_Model();
          $HistoriaExistente=$modeloHistoria->comprobarHistoria($IdTrabajo,$IdHistoria);

          if($HistoriaExistente==false){
              $resultado=$modeloHistoria->crearHistoria($IdTrabajo,$IdHistoria,$TextoHistoria);
              if($resultado==true){
                  header("location: ActionController.php?action=exitoCrearHistoria");
              }else{
                  //cargo idiomas
                  $idioma=new idiomas();
                  $idiom=comprobaridioma($idioma);
                  //cargo la vista
                  $clasecrearHistoria=new Historia_ADD();
                  $clasecrearHistoria->cargar("HistoriaRepe",$idiom);
              }
          }else{
              //cargo el idioma
              $idioma=new idiomas();
              $idiom=comprobaridioma($idioma);
              $clasecrear=new MESSAGE();
              $clasecrear->cargar("DatosDuplicados",$idiom);

          }
      }

      function cargarEliminarHistoria($IdHistoria){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          //cargo el array de historias
          $modeloHistoria=new Historia_Model();
          $datos=$modeloHistoria->buscarPorIdHistoria($IdHistoria);
          //cargo la vista
          $claseEliminarHistoria=new Historia_DELETE();
          $claseEliminarHistoria->cargar($datos,"",$idiom);
      }

      function cargarBajaHistoria($IdHistoria){
          //cargo el modelo
          $modeloHistoria=new Historia_Model();
          //ejecuto la funcion de eliminar
          $resultado=$modeloHistoria->eliminarHistoria($IdHistoria);
          //si elimina bien vuelve a la vista historia_SHOWALL mostrando la lista de usuarios actualizada y con mensaje de exito
          if($resultado==true){
              header("location: ActionController.php?action=exitoEliminarHistoria");
          }else{
              //sino muestra el mesaje de error en la vista MESSAGE
              $idioma=new idiomas();
              $idiom=comprobaridioma($idioma);
              //$datos=$modeloUser->buscarPorLogin($login);
              $claseVistaMESSAGE=new MESSAGE();
              $claseVistaMESSAGE->cargar("errorDELETE",$idiom);
          }
      }
      //Recoge los datos de la historia de la vista historia_EDIT.php
      function modificarHistoria(){
          $IdTrabajo=$_POST['IdTrabajo'];
          $IdHistoria=$_POST['IdHistoria'];
          $TextoHistoria=$_POST['TextoHistoria'];

          // Ruta donde se guardarán las imágenes que subamos
          //$directorio = $_SERVER['DOCUMENT_ROOT'].'/56fbqn/Files/';
          // Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
          //move_uploaded_file($_FILES['fotoUserEdit']['tmp_name'],$directorio.$fotopersonal);


          //Cargo el modelo y ejecuto la función para hacer el update
          $modeloHistoria=new Historia_Model();
          $resultado=$modeloHistoria-> modificarHistoria($IdTrabajo,$IdHistoria,$TextoHistoria);

          //si modifica correctamente vuelve a la vista historia_SHOWALL.php mostrando mensaje de exito
          if($resultado==true)
          {
              header("location: ActionController.php?action=exitoModificarHistoria");
              //Si no muestra mensaje de error y vuelve a la vista historia_EDIT.php
          }else{
              //cargo idiomas
              $idioma=new idiomas();
              $idiom=comprobaridioma($idioma);
              //cargo la vista MESSAGE
              $claseModificarHistoria=new MESSAGE();
              $claseModificarHistoria->cargar("errorModificar",$idiom);
          }
      }
      //Carga la vista historia_EDIT
      function cargarModificarHistoria($IdHistoria){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          //cargo el modelo
          $modeloTrabajo=new Historia_Model();
          //busco al usuario a editar
          $datos=$modeloTrabajo->buscarPorIdHistoria($IdHistoria);
          //cargo la vista
          $claseModificarHistoria=new Historia_EDIT();
          $claseModificarHistoria->cargar($datos,"",$idiom);
      }

      //carga vista SHOWCURRENT
      function cargarShowCurrentHistoria($IdHistoria){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          //cargo el modelo
          $modeloTrabajo=new Historia_Model();
          //busco la historia a editar
          $datos=$modeloTrabajo->buscarPorIdHistoria($IdHistoria);
          //cargo la vista
          $claseModificarHistoria=new Historia_SHOWCURRENT();
          $claseModificarHistoria->cargar($datos,"",$idiom);

      }
      //Carga la vista SEARCH
      function cargarBuscarHistoria(){
          //cargo idiomas
          $idioma=new idiomas();
          $idiom=comprobaridioma($idioma);
          $claseCrearHistoria=new Historia_SEARCH();
          $claseCrearHistoria->cargar("",$idiom);
      }

      function cargarSearchH(){

          $IdTrabajo=$_POST['IdTrabajo'];
          $IdHistoria=$_POST['IdHistoria'];
          $TextoHistoria=$_POST['TextoHistoria'];

          if($IdTrabajo==""){
              $IdTrabajo=null;
          }else if ($IdHistoria==""){
              $IdHistoria=null;
          }else if ($TextoHistoria==""){
              $TextoHistoria=null;
          }
          $model=new Historia_Model();
          $datos=$model->buscarHistoria($IdTrabajo,$IdHistoria,$TextoHistoria);
          return $datos;
      }






?>