<?php 
session_start();
//include("../Models/Model_USUARIO.php");
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador del login, por lo tanto controla
 * que el usuario se pueda logear.
 */

include("../Views/login.php");



//Carga la vista del login
function cargarLogin($texto){
      $idioma=new idiomas();
      $idiom=comprobaridioma($idioma);
      $clase=new login();
      $clase->cargar($texto,$idiom);
}


//Verifica los datos introducidos en la vista login-php
function cargoAcceso()
{
  //cargo idiomas
  $idioma=new idiomas();
  $idiom=comprobaridioma($idioma);

  $user=$_POST['usuario'];
  $pass=$_POST['password'];
  $model=new Usuario_Model();
  $resultado=$model->comprobarLoginAcceso($user,$pass);
  //$grupo=$model->buscarGrupo($user);
    if($resultado==true){
          $_SESSION['usuario']=$user;
          header("location: ActionController.php?action=dentro");
        
    }else{
          $clase=new login();//creo la clase login
          $clase->cargar("UsuarioIncorrecto",$idiom); //carga la vista con mesaje de error
    }  
}

?>