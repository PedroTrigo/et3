<?php
include("../Views/registrarse.php");
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador del registro, por lo tanto controla
 * que el usuario pueda registrarse.
 */

//Carga la vista registrarse.php
 function cargarRegistro()
{
     //cargo idiomas
      $idioma=new idiomas();
      $idiom=comprobaridioma($idioma);

      $clase=new registrar();//creo la clase login
      $clase->cargar("",$idiom);//lanzo la funcion de la clase cargar();
}

//Captura los datos introducidos 
function cargarUsuarioRegistrarse()
{
            $login=$_POST['loginAdd'];
            $password=$_POST['passAdd'];
            $dni=$_POST['dniAdd'];
            $nombre=$_POST['nombreUserAdd'];
            $apellidos=$_POST['apellidosUserAdd'];
            $email=$_POST['emailUserAdd'];
            $direccion=$_POST['direccionUserAdd'];
            $telefono=$_POST['telefonoUserAdd'];          
           
            $password=md5($password);
          
            $modeloUser=new Usuario_Model();
            $usuarioExistente=$modeloUser->comprobarUsuario($login,$dni,$email);

         
            if($usuarioExistente==false){            
                  $resultado=$modeloUser->crearUser($login,$password,$dni,$nombre,$apellidos,$email,$direccion,$telefono);
                  $grupoDefecto="ALUMNO";
                  $asignarGrupo=$modeloUser->asignarGrupoDefecto($login,$grupoDefecto);
                  if($resultado==true and $asignarGrupo==true){ 
                        header("location: ActionController.php?action=registrado");
                  }else{
                         //cargo idiomas
                        $idioma=new idiomas();
                        $idiom=comprobaridioma($idioma);
                        //cargo la vista registrar en caso de que no se haga correctamente
                        $clasecrearuser=new registrar();
                        $clasecrearuser->cargar("errorRegistrar",$idiom);               
                  }
            }else{
                        //cargo la vista registrar en caso de que haya datos duplicados
                        $idioma=new idiomas();
                        $idiom=comprobaridioma($idioma);                        
                        $clasecrearUsuarios=new registrar();
                        $clasecrearUsuarios->cargar("DatosDuplicados",$idiom);
           
            }
}  




    ?>