<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de Action, por lo tanto controla las distintas
 * situaciones tras realizar una operación en la aplicación.
 */
	include("Controller.php");
	include("LoginController.php");
	include("RegistroController.php");
	include("../Functions/idiomas.php");
	include("../Functions/comprobaridioma.php");
	include("Trabajo_Controller.php");
	include ("Accion_Controller.php");
	
	

	if(isset($_REQUEST['action'])){

			Switch ($_REQUEST['action']){
		  
			   	case 'logeado':	//viene de indexController.php si estás logueado te envia a la vista principal SHOWALL
					header("Location: ../index.php");
					break;
		   		case 'acceder':	//viene de indexController.php si no estas logeado te lleva a login.php.
		   			cargarLogin("");
		   			break;
		   		case 'comprobarUsuario': // boton enviar del formulario de login.php 
		   			cargoAcceso();
		   			break;
		   		case 'dentro':   //carga la vista principal 
					cargarInicio("");
					break;
                case 'dentroTrabajo':   //carga la vista principal
                    cargarListaTrabajos("");
                    break;
                case 'dentroAccion':   //carga la vista principal
                    cargarListaAcciones("");
                    break;
				case 'registro':	//boton registrarse de login.php
					cargarRegistro();
					break;
				case 'registrarse':	//boton de registrarse de registrarse.php
					cargarUsuarioRegistrarse();
					break;
				case 'registrado':	//cargo la vista login.php con exito.
					cargarLogin("UsuarioCreadoconExito");
					break;
				case 'ingles':		//seleccionamos ingles sin logearse
					$_SESSION['idioma']='ingles';
		   			cargarLogin("");
		   			break;
		   		case 'español':		//seleccionamos boton español sin logearse
		   			$_SESSION['idioma']='español';
		   			cargarLogin("");
		   			break;
		   		case 'españollogeado':	//seleccionamos boton español cuando estas dentro
		   			$_SESSION['idioma']='español';
		   			cargarInicio("");
		   			break;
		   		case 'ingleslogeado':	//seleccionamos boton inglés cuando estas dentro
		   			$_SESSION['idioma']='ingles';
		   			cargarInicio("");
		   			break;
		   		case 'gallegologeado':	//seleccionamos boton inglés cuando estas dentro
		   			$_SESSION['idioma']='gallego';
		   			cargarInicio("");
		   			break;
		   		case 'volver':	//seleccionamos boton inglés cuando estas dentro		   			
		   			cargarInicio("");
		   			break;
                case 'volverTrabajos':	//seleccionamos boton inglés cuando estas dentro
                    cargarListaTrabajos("");
                    break;
                case 'volverAcciones':	//seleccionamos boton inglés cuando estas dentro
                    cargarListaAcciones("");
                    break;
					//MENU LATERAL HREFS
				case 'ListarUsers':	//boton menu usuarios_SHOWALL.php
		   			cargarListaUsers("");
		   			break;
				case 'ListarFuncionalidades':	//boton menu Funcionalidades_SHOWALL.php
		   			header("Location: ../Controllers/FuncionalidadController.php?action=showAll");
		   			break;
				case 'ListaHistorias':	//boton menu Historias_SHOWALL.php
		   			cargarListaHistorias("");
		   			break;		
				case 'ListarNotas':	//boton menu Historias_SHOWALL.php
		   			CargarListaNotas("");
		   			break;		
				case 'ListarGrupos':	//boton menu Historias_SHOWALL.php
		   			CargarListaGrupos("");
		   			break;	
				case 'ListarEntregas':	//boton menu Entregas_SHOWALL.php
				header("Location: ../Controllers/EntregaController.php?action=showAll");
		   			break;
				case 'ListarEntregasUser':	//boton menu Entregas_SHOWALLUSER.php
				header("Location: ../Controllers/EntregaController.php?action=showAllUser");
		   			break;
		   		case 'ListarQAsUser':	//boton menu Entregas_SHOWALLUSER.php
				header("Location: ../Controllers/AsignacQAController.php?action=showAllUser");
		   			break;
				case 'ListarPermisos':	//boton menu Permisos_SHOWALLUSER.php
				header("Location: ../Controllers/PermisoController.php?action=showAll");
		   			break;
				case 'ListarEvaluaciones':	//boton menu Evaluaciones_SHOWALLUSER.php
				header("Location: ../Controllers/EvaluacionController.php?action=showAll");
		   			break;
                case 'CrearTrabajoSHOWALL':	//boton añadir usuario de usuario_SHOWALL.php
                    cargarCrearTrabajo();
                    break;
                case 'CrearAccionSHOWALL':	//boton añadir usuario de usuario_SHOWALL.php
                    cargarCrearAccion();
                    break;
                case 'AltaTrabajo':	//boton submit de usuario_ADD.php
                    cargarAltaTrabajo();
                    break;
                case 'AltaAccion':	//boton submit de usuario_ADD.php
                    cargarAltaAccion();
                    break;
                case 'exitoCrearTrabajo':	//lanzamos la vista Usuario_SHOWALL despues insertar un usuario
                    cargarListaTrabajos("exitoCrearTrabajo");
                    break;
                case 'exitoCrearAccion':	//lanzamos la vista Usuario_SHOWALL despues insertar un usuario
                    cargarListaAcciones("exitoCrearAccion");
                    break;
                case 'exitoEliminarTrabajo':	//lanzamos la vista Usuario_SHOWALL despues insertar un usuario
                    cargarListaTrabajos("exitoborrarTrabajo");
                    break;
                case 'exitoEliminarAccion':	//lanzamos la vista Usuario_SHOWALL despues insertar un usuario
                    cargarListaAcciones("exitoborrarAccion");
                    break;
                case 'modificarTrabajo':	//Boton submit de usuario_EDIT.php
                    modificarTrabajo();
                    break;
                case 'exitoModificarTrabajo':	//carga la vista SHOWALL con mensaje de exito
                    cargarListaTrabajos("exitoModificarTrabajo");
                    break;
                case 'modificarAccion':	//Boton submit de usuario_EDIT.php
                    modificarAccion();
                    break;
                case 'exitoModificarAccion':	//carga la vista SHOWALL con mensaje de exito
                    cargarListaAcciones("exitoModificarAccion");
                    break;
                case 'SearchTrabajoSHOWALL':	//carga la vista SEARCH al pulsar la lupa de SHOWALL
                    cargarBuscarTrabajo();
                    break;
                case 'SearchTrabajo': //carga los datos introducidos al hacer una busqueda y muestra la vista SHOWALL con los resultados
                    $datos=cargarSearchT();
                    cargarListaTrabajoSearch($datos);
                    break;
                case 'SearchAccionSHOWALL':	//carga la vista SEARCH al pulsar la lupa de SHOWALL
                    cargarBuscarAccion();
                    break;
                case 'SearchAccion': //carga los datos introducidos al hacer una busqueda y muestra la vista SHOWALL con los resultados
                    $datos=cargarSearchA();
                    cargarListaAccionSearch($datos);
                    break;
                case 'modificarSHOWALLTrabajo':	//Boton submit de usuario_EDIT.php
                    modificarTrabajo();
                    break;
		   		case 'CrearUserSHOWALL':	//boton añadir user de usuarios_SHOWALL.php
		   			cargarCrearUser();
		   			break;	
				case 'CrearHistoriaSHOWALL':	//boton añadir historia de historia_SHOWALL.php
		   			cargarCrearHistoria();
		   			break;	
				case 'CrearNotaSHOWALL':	//boton añadir historia de historia_SHOWALL.php
		   			cargarCrearNota();
		   			break;	
				case 'CrearGrupoSHOWALL':	//boton añadir historia de historia_SHOWALL.php
		   			cargarCrearGrupo();
		   			break;
		   		case 'AltaUser':	//boton submit de usuario_ADD.php
		   			cargarAltaUser();
		   			break;
				case 'AltaHistoria':	//boton submit de historia_ADD.php
		   			cargarAltaHistoria();
		   			break;
				case 'AltaNota':	//boton submit de historia_ADD.php
		   			cargarAltaNota();
		   			break;
				case 'AltaGrupo':	//boton submit de historia_ADD.php
		   			cargarAltaGrupo();
		   			break;
		   		case 'exitoCrearUser':	//lanzamos la vista Usuario_SHOWALL despues insertar un usuario
		   			cargarInicio("exito");
		   			break;	
		   		case 'exitoCrearHistoria':	//lanzamos la vista historias_SHOWALL despues insertar una historia
		   			cargarListaHistorias("exito");
		   			break;
				case 'exitoCrearNota':	//lanzamos la vista historias_SHOWALL despues insertar una historia
		   			cargarListaNotas("exito");
		   			break;	
				case 'exitoCrearGrupo':	//lanzamos la vista historias_SHOWALL despues insertar una historia
		   			cargarListaGrupos("exito");
		   			break;					
		   		case 'exitoEliminarUser':	//lanzamos la vista Usuario_SHOWALL despues insertar un usuario
		   			cargarInicio("exitoborrar");
		   			break;
		   		case 'exitoEliminarHistoria':	//lanzamos la vista Usuario_SHOWALL despues insertar un usuario
		   			cargarListaHistorias("exitoborrar");
		   			break;
				case 'exitoEliminarNota':	//lanzamos la vista Usuario_SHOWALL despues insertar un usuario
		   			cargarListaNotas("exitoborrar");
		   			break;
				case 'exitoEliminarGrupo':	//lanzamos la vista Usuario_SHOWALL despues insertar un usuario
		   			cargarListaGrupos("exitoborrar");
		   			break;
		   		case 'modificarUser':	//Boton submit de usuario_EDIT.php
		   			modificarUser();
		   			break;
				case 'modificarHistoria':	//Boton submit de historia_EDIT.php
		   			modificarHistoria();
		   			break;
				case 'modificarNota':	//Boton submit de nota_EDIT.php
		   			modificarNota();
		   			break;
				case 'modificarGrupo':	//Boton submit de grupo_EDIT.php
		   			modificarGrupo();
		   			break;
		   		case 'asignarGrupo':	//Boton submit de Usuario_AsignarAGrupo.php
		   			cargarVistaAsignarAGrupo();
		   			break;		   		
		   		case 'exitoModificarUser':	//carga la vista SHOWALL con mensaje de exito
		   			cargarInicio("exitoModificar");
		   			break;
				case 'exitoModificarHistoria':	//carga la vista SHOWALL con mensaje de exito
		   			cargarListaHistorias("exitoModificar");
		   			break;
				case 'exitoModificarNota':	//carga la vista SHOWALL con mensaje de exito
		   			cargarListaNotas("exitoModificar");
		   			break;
				case 'exitoModificarGrupo':	//carga la vista SHOWALL con mensaje de exito
		   			cargarListaGrupos("exitoModificar");
		   			break;
				case 'SearchUserSHOWALL':	//carga la vista SEARCH al pulsar la lupa de SHOWALL
		   			cargarBuscarUser();
		   			break;
				case 'SearchHistoriaSHOWALL':	//carga la vista SEARCH al pulsar la lupa de SHOWALL
		   			cargarBuscarHistoria();
		   			break;
				case 'SearchNotaSHOWALL':	//carga la vista SEARCH al pulsar la lupa de SHOWALL
		   			cargarBuscarNota();
		   			break;
				case 'SearchGrupoSHOWALL':	//carga la vista SEARCH al pulsar la lupa de SHOWALL
		   			cargarBuscarGrupo();
		   			break;
		   		case 'SearchUser': //carga los datos introducidos al hacer una busqueda y muestra la vista SHOWALL con los resultados
		   			$datos=cargarSearch();
		   			cargarListaUsuarioSearch($datos);
					break;
				case 'SearchHistoria': //carga los datos introducidos al hacer una busqueda y muestra la vista SHOWALL con los resultados
		   			$datos=cargarSearchH();
		   			cargarListaHistoriaSearch($datos);
					break;
				case 'SearchNota': //carga los datos introducidos al hacer una busqueda y muestra la vista SHOWALL con los resultados
		   			$datos=cargarSearchN();
		   			cargarListaNotaSearch($datos);
		   		break;
				case 'SearchGrupo': //carga los datos introducidos al hacer una busqueda y muestra la vista SHOWALL con los resultados
		   			$datos=cargarSearchG();
		   			cargarListaGrupoSearch($datos);
		   		break;
		   		case 'Salir':	//boton Salir
		   			session_destroy();
					header("Location: ../index.php");
		   			break;
		   		case 'exitoAsignarGrupo':	
		   			cargarListaUsers("exitoAsignarGrupo");
		   			break;
		   		default:
		   			
		   		break;
		   	}
	}
	//carga la vista de nota_DELETE al darle al incono de borrar en usuario_SHOWALL
   	if(isset($_REQUEST['eliminarSHOWALL'])){
   		cargarEliminarUser($_REQUEST['eliminarSHOWALL']);
   	}
   	//captura el boton SHOWCCURRENT  y ejecuta la funcion de Controller.php que carga la vista Usuario_AsignarAGrupo.php.php
   	if(isset($_REQUEST['vistaAsignarAGrupo'])){
   		vistaAsignarAGrupo($_REQUEST['vistaAsignarAGrupo']);
   	}
   	//captura el boton AñadirGrupo de la vista Usuario_AsignarAGrupo.php
    //if(isset($_REQUEST['cargarAsignarAGrupo'])){
   	//	cargarVistaAsignarAGrupo($_REQUEST['cargarAsignarAGrupo']);
   	//}   
   	//captura el boton DELETE de la vista nota_ADD 
    if(isset($_REQUEST['baja'])){
   		cargarBajaUser($_REQUEST['baja']);
   	}
   	//captura el boton EDIT de la vista SHOWALL y ejecuta la funcion de Controller.php que carga la vista nota_EDIT.php
   	if(isset($_REQUEST['modificarSHOWALL'])){
   		cargarModificarUser($_REQUEST['modificarSHOWALL']);
   	}
   	//captura el boton SHOWCCURRENT  y ejecuta la funcion de Controller.php que carga la vista nota_SHOWCURRENT.php
   	if(isset($_REQUEST['detalleSHOWALL'])){
   		cargarShowCurrentUser($_REQUEST['detalleSHOWALL']);
   	}   
	//carga la vista de nota_DELETE al darle al incono de borrar en usuario_SHOWALL
   	if(isset($_REQUEST['eliminarSHOWALLH'])){
   		cargarEliminarHistoria($_REQUEST['eliminarSHOWALLH']);
   	}
   	//captura el boton DELETE de la vista nota_ADD 
    if(isset($_REQUEST['bajaH'])){
   		cargarBajaHistoria($_REQUEST['bajaH']);
   	}
   	//captura el boton EDIT de la vista SHOWALL y ejecuta la funcion de Controller.php que carga la vista nota_EDIT.php
   	if(isset($_REQUEST['modificarSHOWALLH'])){
   		cargarModificarHistoria($_REQUEST['modificarSHOWALLH']);
   	}
   	//captura el boton SHOWCCURRENT  y ejecuta la funcion de Controller.php que carga la vista nota_SHOWCURRENT.php
   	if(isset($_REQUEST['detalleSHOWALLH'])){
   		cargarShowCurrentHistoria($_REQUEST['detalleSHOWALLH']);
   	}   	
		//carga la vista de nota_DELETE al darle al incono de borrar en usuario_SHOWALL
   	if(isset($_REQUEST['eliminarSHOWALLN'])){
   		cargarEliminarNota($_REQUEST['eliminarSHOWALLN']);
   	}
   	//captura el boton DELETE de la vista nota_ADD 
    if(isset($_REQUEST['bajaN'])){
   		cargarBajaNota($_REQUEST['bajaN']);
   	}
   	//captura el boton EDIT de la vista SHOWALL y ejecuta la funcion de Controller.php que carga la vista nota_EDIT.php
   	if(isset($_REQUEST['modificarSHOWALLN'])){
   		cargarModificarNota($_REQUEST['modificarSHOWALLN']);
   	}
   	//captura el boton SHOWCCURRENT  y ejecuta la funcion de Controller.php que carga la vista nota_SHOWCURRENT.php
   	if(isset($_REQUEST['detalleSHOWALLN'])){
   		cargarShowCurrentNota($_REQUEST['detalleSHOWALLN']);
   	}
	//carga la vista de usuario_DELETE al darle al incono de borrar en usuario_SHOWALL
	if(isset($_REQUEST['eliminarSHOWALLTrabajo'])){
	    cargarEliminarTrabajo($_REQUEST['eliminarSHOWALLTrabajo']);
	}
	if(isset($_REQUEST['eliminarSHOWALLAccion'])){
	    cargarEliminarAccion($_REQUEST['eliminarSHOWALLAccion']);
	}
	//captura el boton DELETE de la vista usuario_ADD
	if(isset($_REQUEST['bajaTrabajo'])){
	    cargarBajaTrabajo($_REQUEST['bajaTrabajo']);
	}
	if(isset($_REQUEST['bajaAccion'])){
	    cargarBajaAccion($_REQUEST['bajaAccion']);
	}
	//captura el boton EDIT de la vista SHOWALL y ejecuta la funcion de Controller.php que carga la vista usuario_EDIT.php
	if(isset($_REQUEST['modificarSHOWALLTrabajo'])){
	    cargarModificarTrabajo($_REQUEST['modificarSHOWALLTrabajo']);
	}
	if(isset($_REQUEST['modificarSHOWALLAccion'])){
	    cargarModificarAccion($_REQUEST['modificarSHOWALLAccion']);
	}
	//captura el boton SHOWCCURRENT  y ejecuta la funcion de Controller.php que carga la vista usuario_SHOWCURRENT.php
	if(isset($_REQUEST['detalleSHOWALLTrabajo'])){
	    cargarShowCurrentTrabajo($_REQUEST['detalleSHOWALLTrabajo']);
	}
	if(isset($_REQUEST['detalleSHOWALLAccion'])){
	    cargarShowCurrentAccion($_REQUEST['detalleSHOWALLAccion']);
	}
function cargarListaTrabajos($texto){
    //cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //cargo el array de usuarios
    $modeloTrabajo=new Trabajo_Model();
    $datos=$modeloTrabajo->listarTrabajos();
    //Comprueba la sesión de ususario para mostrar el menú
 	$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
    //cargo la vista
    $claselistadoTrabajos=new Trabajo_SHOWALL();
    $claselistadoTrabajos->cargar($datos,$texto,$idiom,$comprobarUsuarioGrupo);
}
function cargarListaAcciones($texto){
    //cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //cargo el array de usuarios
    $modeloAccion=new Accion_Model();
    $datos=$modeloAccion->listarAcciones();
    //Comprueba la sesión de ususario para mostrar el menú
 	$comprobarUsuarioGrupo=ComprobarUsuarioMenu();

    //cargo la vista
    $claselistadoAcciones=new Accion_SHOWALL();
    $claselistadoAcciones->cargar($datos,$texto,$idiom,$comprobarUsuarioGrupo);
}
function cargarListaTrabajoSearch($datos){
    //cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //Comprueba la sesión de ususario para mostrar el menú
 	$comprobarUsuarioGrupo=ComprobarUsuarioMenu();

    $claselistadoTrabajos=new Trabajo_SHOWALL();
    $claselistadoTrabajos->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);

}
function cargarListaAccionSearch($datos){
    //cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //Comprueba la sesión de ususario para mostrar el menú
 	$comprobarUsuarioGrupo=ComprobarUsuarioMenu();

    $claselistadoAcciones=new Accion_SHOWALL();
    $claselistadoAcciones->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);

}
	
	//carga la vista de grupo_DELETE al darle al incono de borrar en usuario_SHOWALL
   	if(isset($_REQUEST['eliminarSHOWALLG'])){
   		cargarEliminarGrupo($_REQUEST['eliminarSHOWALLG']);
   	}
   	//captura el boton DELETE de la vista grupo_ADD 
    if(isset($_REQUEST['bajaG'])){
   		cargarBajaGrupo($_REQUEST['bajaG']);
   	}
   	//captura el boton EDIT de la vista SHOWALL y ejecuta la funcion de Controller.php que carga la vista grupo_EDIT.php
   	if(isset($_REQUEST['modificarSHOWALLG'])){
   		cargarModificarGrupo($_REQUEST['modificarSHOWALLG']);
   	}
   	//captura el boton SHOWCCURRENT  y ejecuta la funcion de Controller.php que carga la vista grupo_SHOWCURRENT.php
   	if(isset($_REQUEST['detalleSHOWALLG'])){
   		cargarShowCurrentGrupo($_REQUEST['detalleSHOWALLG']);
   	}  	

   	//Comprueba que el usuario de la sesion es ADMIN
   	function ComprobarUsuarioMenu(){
   	//Cargo el modelo
   	$modeloUser=new Usuario_Model();
    //Usuario de la sesion
    $UserSesionActual=$_SESSION['usuario'];
    //Comprueba si es admin 
    $esAdmin=$modeloUser->comprobarUsuarioADMINEnUSU_GRUPO($UserSesionActual);       

			if( ($esAdmin) == "true" ){
				$comprobarUsuarioGrupo="ADMIN";
			}else{
				$comprobarUsuarioGrupo="USER";
			}
			return $comprobarUsuarioGrupo;
   	}

	function cargarInicio($texto){
   	//cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //Comprueba la sesión de ususario para mostrar el menú
 	$comprobarUsuarioGrupo=ComprobarUsuarioMenu();
   
    //cargo la vista
    $claselistadoUsers=new Inicio();    
    $claselistadoUsers->cargar($texto,$idiom,$comprobarUsuarioGrupo);
   }
   
   function cargarListaHistorias($texto){
   	//cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
 	//cargo el array de usuarios
    $modeloHistorias=new Historia_Model();
    $datos=$modeloHistorias->listarHistorias();
    //Comprueba la sesión de ususario para mostrar el menú
    $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
    //cargo la vista
    $claselistadoHistorias=new Historia_SHOWALL();
    $claselistadoHistorias->cargar($datos,$texto,$idiom,$comprobarUsuarioGrupo);
   }
   
   function cargarListaUsers($texto){
   	//cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
 	//cargo el array de la tabla USUARIO
    $modeloUsers=new USUARIO_Model();
    $modeloUsers->crearArrayUsuarios();
    //Cargo el array de grupo de la tabla USU-GRUPOS
    $grupos=$modeloUsers->crearArrayGrupos(); 
      
    $modeloUsers->RellenarArrayUsuGrupos($grupos);
    //Junto los 2 arrays
    include("../Locales/Archivos/consultUsuGrupos.php");
    $datos=new consult();
    $formfinal=$datos->array_consultarUsuGrupos();
    //Comprueba la sesión de usuario para mostrar el menú
    $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
    //cargo la vista
    $vistaListadoUsers=new Usuario_SHOWALL();
    $vistaListadoUsers->cargar($formfinal,$texto,$idiom,$comprobarUsuarioGrupo);
   }   
   
   function cargarListaNotas($texto){
   	//cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
 	//cargo el array de usuarios
    $modeloNotas=new Nota_Model();
    $datos=$modeloNotas->listarNotas();
    //Comprueba la sesión de ususario para mostrar el menú
    $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
    //cargo la vista
    $claselistadoNotas=new Nota_SHOWALL();
    $claselistadoNotas->cargar($datos,$texto,$idiom,$comprobarUsuarioGrupo);
   }
   
     function cargarListaGrupos($texto){
   	//cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
 	//cargo el array de usuarios
    $modeloGrupos=new Grupo_Model();
    $datos=$modeloGrupos->listarGrupos();
    //Comprueba la sesión de ususario para mostrar el menú
    $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
    //cargo la vista
    $claselistadoGrupos=new Grupo_SHOWALL();
    $claselistadoGrupos->cargar($datos,$texto,$idiom,$comprobarUsuarioGrupo);
   }
   
    function cargarListaUsuarioSearch($datos){
	 //cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //Comprueba la sesión de ususario para mostrar el menú
 	$comprobarUsuarioGrupo=ComprobarUsuarioMenu();

    $claselistadoUsers=new Usuario_SHOWALL();
    $claselistadoUsers->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);

   }
   
   function cargarListaHistoriaSearch($datos){
	 //cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //Comprueba la sesión de ususario para mostrar el menú
 	$comprobarUsuarioGrupo=ComprobarUsuarioMenu();

    $claselistadoHistorias=new Historia_SHOWALL();
    $claselistadoHistorias->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);

   }
   
   function cargarListaNotaSearch($datos){
	 //cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //Comprueba la sesión de ususario para mostrar el menú
 	$comprobarUsuarioGrupo=ComprobarUsuarioMenu();

    $claselistadoNotas=new Nota_SHOWALL();
    $claselistadoNotas->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);

   }
      
   function cargarListaGrupoSearch($datos){
	 //cargo el idioma
    $idioma=new idiomas();
    $idiom=comprobaridioma($idioma);
    //Comprueba la sesión de ususario para mostrar el menú
 	$comprobarUsuarioGrupo=ComprobarUsuarioMenu();

    $claselistadoGrupos=new Grupo_SHOWALL();
    $claselistadoGrupos->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);

   }

?>