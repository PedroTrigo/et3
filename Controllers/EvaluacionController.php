<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de evaluación, por lo tanto controla las distintas
 * situaciones tras realizar una operación en la evaluación.
 */
session_start();
$login=$_SESSION['usuario'];

include("../Models/Model_EVALUACION.php");
include("../Models/USUARIO_Model.php");
include("../Views/Evaluacion_SHOWALL.php");
include("../Views/Evaluacion_ADD.php");
include("../Views/Evaluacion_EDIT.php");
include("../Views/Evaluacion_SEARCH.php");
include("../Views/MESSAGE.php"); 
include("../Views/Evaluacion_DELETE.php");
include("../Views/Evaluacion_SHOWCURRENT.php");
include("../Functions/idiomas.php");
include("../Functions/comprobaridioma.php");

function ComprobarUsuarioMenu(){
    //Cargo el modelo
    $modeloUser=new Usuario_Model();
    //Usuario de la sesion
    $UserSesionActual=$_SESSION['usuario'];
    //Comprueba si es admin 
    $esAdmin=$modeloUser->comprobarUsuarioADMINEnUSU_GRUPO($UserSesionActual);       

            if( ($esAdmin) == "true" ){
                $comprobarUsuarioGrupo="ADMIN";
            }else{
                $comprobarUsuarioGrupo="USER";
            }
            return $comprobarUsuarioGrupo;
    }

switch ($_REQUEST['action']){
    //carga formulario de alta de entrega
    case 'cargarAlta':
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_EVALUACION("","","","","","","","","");
        $trabajos=$modelo->trabajos();
        $historias=$modelo->historias();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaCrearEvaluacion=new Evaluacion_ADD();
        $vistaCrearEvaluacion->cargar("",$idiom,$trabajos,$historias,$comprobarUsuarioGrupo);
        break;
    
    //da de alta entrega
    case 'alta':
        $IdTrabajo=$_POST['IdTrabajo'];
        $LoginEvaluador=$_POST['LoginEvaluador'];
        $AliasEvaluado=$_POST['AliasEvaluado'];
        $IdHistoria=$_POST['IdHistoria'];
        $CorrectoA=$_POST['CorrectoA'];
        $ComenIncorrectoA=$_POST['ComenIncorrectoA'];
        $CorrectoP=$_POST['CorrectoP'];
        $ComenIncorrectoP=$_POST['ComenIncorrectoP'];
        $OK=$_POST['OK'];
		 
        $modelo=new Model_EVALUACION($IdTrabajo,$LoginEvaluador,$AliasEvaluado,$IdHistoria,
		$CorrectoA,$ComenIncorrectoA,$CorrectoP,$ComenIncorrectoP,$OK);
		
        $comprobar=$modelo->comprobarEvaluacion();

        if($comprobar==false){
            $resultado=$modelo->insert();
            if($resultado==true){
                header("location: EvaluacionController.php?action=mensajeResultado&resultado=exito");
            }
            else{
                $modelo=new Model_EVALUACION("","","","","","","","","");
                $trabajos=$modelo->trabajos();
                $historias=$modelo->historias();
                //cargo idiomas
                $idioma=new idiomas();
                $idiom=comprobaridioma($idioma);
                //Comprueba la sesión de ususario para mostrar el menú
                $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                //cargo la vista
                $vistaCrearEvaluacion=new Evaluacion_ADD();
                $vistaCrearEvaluacion->cargar("EvaluacionRepe",$idiom,$trabajos,$historias,$comprobarUsuarioGrupo);
            }
        }
        else{
            $modelo=new Model_EVALUACION("","","","","","","","","");
            $trabajos=$modelo->trabajos();
            $historias=$modelo->historias();
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista
            $vistaCrearEvaluacion=new Evaluacion_ADD();
            $vistaCrearEvaluacion->cargar("EvaluacionRepe",$idiom,$trabajos,$historias,$comprobarUsuarioGrupo);
        }
        break;
        
    //carga la vista para eliminar una evaluacion
    case 'cargarBaja':
        $IdTrabajo=$_POST['IdTrabajo'];
        $LoginEvaluador=$_POST['LoginEvaluador'];
        $AliasEvaluado=$_POST['AliasEvaluado'];
        $IdHistoria=$_POST['IdHistoria'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_EVALUACION($IdTrabajo,$LoginEvaluador,$AliasEvaluado,$IdHistoria,"","","","","");
        $datos=$modelo->showCurrent();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista            
        $vistaEliminarEvaluacion=new Evaluacion_DELETE();
        $vistaEliminarEvaluacion->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    //da de baja entrega
    case 'baja':
        $IdTrabajo=$_POST['IdTrabajo'];
        $LoginEvaluador=$_POST['LoginEvaluador'];
        $AliasEvaluado=$_POST['AliasEvaluado'];
        $IdHistoria=$_POST['IdHistoria'];
        //cargo el modelo
        $modelo=new Model_EVALUACION($IdTrabajo,$LoginEvaluador,$AliasEvaluado,$IdHistoria,"","","","","");
        //ejecuto la funcion de eliminar
        $resultado=$modelo->delete();
        //si elimina bien vuelve a la vista SHOWALL mostrando la lista actualizada y con mensaje de exito
        if($resultado==true){ 
            header("location: EvaluacionController.php?action=mensajeResultado&resultado=exitoborrar");
        }
        else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);

            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);
        }
        break;
        
    case 'cargarModificar':
        $IdTrabajo=$_POST['IdTrabajo'];
        $LoginEvaluador=$_POST['LoginEvaluador'];
        $AliasEvaluado=$_POST['AliasEvaluado'];
        $IdHistoria=$_POST['IdHistoria'];
        
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        //cargo el modelo
        $modelo=new Model_EVALUACION($IdTrabajo,$LoginEvaluador,$AliasEvaluado,$IdHistoria,"","","","","");
        //busco al usuario a editar
        $datos=$modelo->showCurrent();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();

        //cargo la vista
        $vistaModificarEvaluacion=new Evaluacion_EDIT();
        $vistaModificarEvaluacion->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'modificar':
        $IdTrabajo=$_POST['IdTrabajo'];
        $LoginEvaluador=$_POST['LoginEvaluador'];
        $AliasEvaluado=$_POST['AliasEvaluado'];
        $IdHistoria=$_POST['IdHistoria'];
        $CorrectoA=$_POST['CorrectoA'];
        $ComenIncorrectoA=$_POST['ComenIncorrectoA'];
        $CorrectoP=$_POST['CorrectoP'];
        $ComenIncorrectoP=$_POST['ComenIncorrectoP'];
        $OK=$_POST['OK'];
        
        //Cargo el modelo y ejecuto la función para hacer el update
        $modelo=new Model_EVALUACION($IdTrabajo,$LoginEvaluador,$AliasEvaluado,$IdHistoria,$CorrectoA,$ComenIncorrectoA,$CorrectoP,$ComenIncorrectoP,$OK);
        $resultado=$modelo->update();

        //si modifica correctamente vuelve a la vista SHOWALL.php mostrando mensaje de exito
        if($resultado==true)
        { 
            if(isset($_GET['user'])){
                header("location: EvaluacionController.php?action=mensajeResultadoUser&resultado=exitoModificar");
            }
            header("location: EvaluacionController.php?action=mensajeResultado&resultado=exitoModificar");
        }
        else{ 
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();

            //cargo la vista MESSAGE                  
            $claseModificarEvaluacion=new MESSAGE();
            $claseModificarEvaluacion->cargar("errorModificarEntrega",$idiom,$comprobarUsuarioGrupo);
        }
        break;
        
    case 'cargarSearch':     
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $vistaBuscarEvaluacion=new Evaluacion_SEARCH();
        $vistaBuscarEvaluacion->cargar("",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'search': 
        $IdTrabajo=$_POST['IdTrabajo'];
        $LoginEvaluador=$_POST['LoginEvaluador'];
        $AliasEvaluado=$_POST['AliasEvaluado'];
        $IdHistoria=$_POST['IdHistoria'];
        $CorrectoA=$_POST['CorrectoA'];
        $ComenIncorrectoA=$_POST['ComenIncorrectoA'];
        $CorrectoP=$_POST['CorrectoP'];
        $ComenIncorrectoP=$_POST['ComenIncorrectoP'];
        $OK=$_POST['OK'];
		
        $modelo=new Model_EVALUACION($IdTrabajo,$LoginEvaluador,$AliasEvaluado,$IdHistoria,$CorrectoA,$ComenIncorrectoA,$CorrectoP,$ComenIncorrectoP,$OK);
        $datos=$modelo->select();
        
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoEvaluacion=new Evaluacion_SHOWALL();
        $vistaListadoEvaluacion->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    //carga vista SHOWCURRENT
    case 'cargarShowCurrent':
        $IdTrabajo=$_POST['IdTrabajo'];
        $LoginEvaluador=$_POST['LoginEvaluador'];
        $AliasEvaluado=$_POST['AliasEvaluado'];
        $IdHistoria=$_POST['IdHistoria'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //cargo el modelo
        $modelo=new Model_EVALUACION($IdTrabajo,$LoginEvaluador,$AliasEvaluado,$IdHistoria,"","","","","");
        //busco al usuario a editar
        $datos=$modelo->showCurrent();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista
        $vistaDetalleEvaluacion=new Evaluacion_SHOWCURRENT();
        $vistaDetalleEvaluacion->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);     
        break;
    
    case 'showAll':
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_EVALUACION("","","","","","","","","");
        $datos=$modelo->showAll();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoEvaluacion=new Evaluacion_SHOWALL();
        $vistaListadoEvaluacion->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'mensajeResultado':
        $texto=$_GET['resultado'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_EVALUACION("","","","","","","","","");
        $datos=$modelo->showAll();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoEvaluacion=new Evaluacion_SHOWALL();
        $vistaListadoEvaluacion->cargar($datos,$texto,$idiom,$comprobarUsuarioGrupo);
        break;
    
   
}
