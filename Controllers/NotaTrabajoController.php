<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este es el controlador de notas de trabajos, por lo tanto controla las distintas
 * operaciones posibles sobre ellas.
 */

session_start();
$login=$_SESSION['usuario'];

include("../Models/Model_NOTA_TRABAJO.php");
include("../Views/NotaTrabajo_SHOWALL.php");
include("../Views/NotaTrabajo_ADD.php");
include("../Views/NotaTrabajo_EDIT.php");
include("../Views/NotaTrabajo_SEARCH.php");
include("../Views/MESSAGE.php"); 
include("../Views/NotaTrabajo_DELETE.php");
include("../Views/NotaTrabajo_SHOWCURRENT.php");
include("../Views/PonerNotaTrabajo.php");
include("../Functions/idiomas.php");
include("../Functions/comprobaridioma.php");

switch ($_REQUEST['action']){
    //carga formulario de alta de nota
    case 'cargarAlta':
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        $modelo=new Model_NOTA_TRABAJO($login,"","");
        $trabajos=$modelo->trabajosPorEvaluar();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $vistaCrearNota=new NotaTrabajo_ADD();
        $vistaCrearNota->cargar("",$idiom,$trabajos,$comprobarUsuarioGrupo);
        break;
    
    //carga formulario de alta de nota llamado desde Trabajo_SHOWALL
    case 'ponerNota':
        $IdTrabajo=$_GET['IdTrabajo'];
        
        $modelo=new Model_NOTA_TRABAJO($login,$IdTrabajo,"");
        $comprobar=$modelo->comprobarNota();
        if($comprobar==TRUE){
            header("location: NotaTrabajoController.php?action=cargarModificar&IdTrabajo=$IdTrabajo");
        }
        
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $vistaPonerNota=new PonerNotaTrabajo();
        $vistaPonerNota->cargar("",$idiom,$IdTrabajo,$comprobarUsuarioGrupo);
        break;
    
    //da de alta nota
    case 'alta':
        $IdTrabajo=$_POST['IdTrabajo'];
        $NotaTrabajo=$_POST['NotaTrabajo'];

        $modelo=new Model_NOTA_TRABAJO($login,$IdTrabajo,$NotaTrabajo);
        $comprobar=$modelo->comprobarNota();

        if($comprobar==false){
            $resultado=$modelo->insert();
            if($resultado==true){
                header("location: NotaTrabajoController.php?action=mensajeResultado&resultado=exito");
            }
            else{
                //cargo idiomas
                $idioma=new idiomas();
                $idiom=comprobaridioma($idioma);
                 //Comprueba la sesión de ususario para mostrar el menú
                $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
                //cargo la vista
                $vistaCrearFun=new Funcionalidad_ADD();
                $vistaCrearFun->cargar("NotaRepe",$idiom,$comprobarUsuarioGrupo);
            }
        }
        else{
            //cargo el idioma
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();                        
            $clasecrear=new MESSAGE();
            $clasecrear->cargar("DatosDuplicadosNota",$idiom,$comprobarUsuarioGrupo);
           
        }
        break;
        
    //carga la vista para eliminar una nota
    case 'cargarBaja':
        $IdTrabajo=$_GET['IdTrabajo'];
        $login=$_GET['login'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_NOTA_TRABAJO($login,$IdTrabajo,"");
        $datos=$modelo->showCurrent();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista            
        $vistaEliminarNota=new NotaTrabajo_DELETE();
        $vistaEliminarNota->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    //da de baja nota
    case 'baja':
        echo "pasa por aqui";
        $IdTrabajo=$_GET['IdTrabajo'];
        $login=$_GET['login'];
        //cargo el modelo
        $modelo=new Model_NOTA_TRABAJO($login,$IdTrabajo,"");
        //ejecuto la funcion de eliminar
        $resultado=$modelo->delete();
        //si elimina bien vuelve a la vista SHOWALL mostrando la lista actualizada y con mensaje de exito
        if($resultado==true){ 
            header("location: NotaTrabajoController.php?action=mensajeResultado&resultado=exitoborrar");
        }
        else{
            //sino muestra el mesaje de error en la vista MESSAGE         
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();            
            $claseVistaMESSAGE=new MESSAGE();
            $claseVistaMESSAGE->cargar("errorDELETE",$idiom,$comprobarUsuarioGrupo);
        }
        break;
        
    case 'cargarModificar':
        $IdTrabajo=$_GET['IdTrabajo'];
        $login=$_GET['login'];
        
        if($login<>$_SESSION['usuario']){
            header("location: NotaTrabajoController.php?action=mensajeResultado&resultado=errorEditLoginIncorr");
        }
        
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //cargo el modelo
        $modelo=new Model_NOTA_TRABAJO($login,$IdTrabajo,"");
        //busco al usuario a editar
        $datos=$modelo->showCurrent();
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista
        $vistaModificarNota=new NotaTrabajo_EDIT();
        $vistaModificarNota->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'modificar':
        $IdTrabajo=$_POST['IdTrabajo'];
        $NotaTrabajo=$_POST['NotaTrabajo'];

        //Cargo el modelo y ejecuto la función para hacer el update
        $modelo=new Model_NOTA_TRABAJO($login,$IdTrabajo,$NotaTrabajo);
        $resultado=$modelo->update();

        //si modifica correctamente vuelve a la vista SHOWALL.php mostrando mensaje de exito
        if($resultado==true)
        { 
            header("location: NotaTrabajoController.php?action=mensajeResultado&resultado=exitoModificar");
        }
        else{ 
            //cargo idiomas
            $idioma=new idiomas();
            $idiom=comprobaridioma($idioma);
            //Comprueba la sesión de ususario para mostrar el menú
            $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
            //cargo la vista MESSAGE                  
            $claseModificarUser=new MESSAGE();
            $claseModificarUser->cargar("errorModificarNota",$idiom,$comprobarUsuarioGrupo);
        }
        break;
        
    case 'cargarSearch':     
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        $vistaBuscarFun=new NotaTrabajo_SEARCH();
        $vistaBuscarFun->cargar("",$idiom);
        break;
    
    case 'search': 
        $IdTrabajo=$_POST['IdTrabajo'];
        $login=$_POST['login'];
        
        $modelo=new Model_NOTA_TRABAJO($login,$IdTrabajo,"");
        $datos=$modelo->select();
        
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();

        $vistaListadoNota=new NotaTrabajo_SHOWALL();
        $vistaListadoNota->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    //carga vista SHOWCURRENT
    case 'cargarShowCurrent':
        $IdTrabajo=$_GET['IdTrabajo'];
        $login=$_GET['login'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        //cargo el modelo
        $modelo=new Model_NOTA_TRABAJO($login,$IdTrabajo,"");
        //busco al usuario a editar
        $datos=$modelo->showCurrent();
         //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        //cargo la vista
        $vistaDetalleNota=new NotaTrabajo_SHOWCURRENT();
        $vistaDetalleNota->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);     
        break;
    
    case 'showAll':
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_NOTA_TRABAJO("","","");
        $datos=$modelo->showAll();

         //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoNota=new NotaTrabajo_SHOWALL();
        $vistaListadoNota->cargar($datos,"",$idiom,$comprobarUsuarioGrupo);
        break;
    
    case 'mensajeResultado':
        $texto=$_GET['resultado'];
        //cargo idiomas
        $idioma=new idiomas();
        $idiom=comprobaridioma($idioma);
        
        $modelo=new Model_NOTA_TRABAJO("","","");
        $datos=$modelo->showAll();

        //Comprueba la sesión de ususario para mostrar el menú
        $comprobarUsuarioGrupo=ComprobarUsuarioMenu();
        
        $vistaListadoFun=new NotaTrabajo_SHOWALL();
        $vistaListadoFun->cargar($datos,$texto,$idiom,$comprobarUsuarioGrupo);
        break;
}