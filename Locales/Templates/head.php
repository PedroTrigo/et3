<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es la cabecera de la página.
 */
class head{
	function cargar($idi,$title,$comprobarUsuarioGrupo){
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="description" content="Práctica de IU">
    <meta name="keywords" content="HTML,CSS,JavaScript">
    <meta name="author" content="Pedro Trigo Collazo">

	<meta name="viewport" content="width=device-width, 
	user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimum-scale=1.0, shrink-to-fit=no">
	
	<!--Tigra calendar 
		Cambiado el formato fecha por defecto a d/m/Y-->
	<link rel="stylesheet" type="text/css" href="../Locales/simple-calendar/tcal.css" />
	<script type="text/javascript" src="../Locales/simple-calendar/tcal.js"></script> 

	<!--Iconos-->
	<link rel="stylesheet" href="../Locales/css/font-awesome-4.7.0/css/font-awesome.min.css">

	<!--Bootstrap-->
	<link rel="stylesheet" href="../Locales/bootstrap/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../Locales/bootstrap/bootstrap.min.css">

    <!--css-->
	<link rel="stylesheet" type="text/css" href="../Locales/css/styles.css">      

	<!--Menu lateral-->
	<script language="JavaScript" type="text/javascript" src="../Locales/js/jquery-3.2.1.js" ></script>
	<script language="JavaScript" type="text/javascript" src="../Locales/js/menu_lateral.js"></script>

	<!--Validaciones-->
	<!-- <script  type="text/javascript" src="../Locales/js/validaciones.js"></script> -->
	<?php include '../Locales/js/validaciones.js'; ?>
	<!--MD5-->
	<script language="JavaScript" type="text/javascript" src="../Locales/js/md5-min.js"></script> 

	<title><?php if($title=="creaUser")echo $idi["crearUser"]; elseif($title=="modificarUser")echo $idi["modificarUser"]; elseif($title=="listarUsers")echo $idi["listarUsers"];elseif($title=="eliminarUser")echo $idi["eliminarUser"];elseif($title=="searchUser")echo $idi["searchUser"];elseif($title=="MESSAGE")echo $idi["MESSAGE"];
	elseif($title=="detalleUser")echo $idi["detalleUser"];elseif($title=="MESSAGE")echo $idi["message"];?></title>
</head>
<body>

<!--CABECERA-->			
<div class="container-fluid">
	<div class="row" id="cabecera">		
		<div class="col-md-12">
			
				<label class="text-left">
					<i class="fa fa-user" aria-hidden="true"></i>
					<?php echo $idi['bienvenido'];?> <?php echo $_SESSION['usuario']; ?>
				</label>				
					</h3></a>		
				<div class="row">
					<!--Botón idioma-->
					<form name="idiomaForm" action="#" method="POST">
						<div class="col-md-4">	  					
		  					<a href="../Controllers/ActionController.php?action=españollogeado"><img src="../Locales/css/img/espana.png" name="espana" id="espana" width="50" height="50" > </a>
		  					
				    		<a href="../Controllers/ActionController.php?action=ingleslogeado"><img src="../Locales/css/img/ingles.jpg" name="ingles" id="ingles" width="50" height="50" > </a>

				    		<a href="../Controllers/ActionController.php?action=gallegologeado"><img src="../Locales/css/img/galicia3.svg" name="gal" id="gal" width="50" height="50" > </a>
						</div>
					</form>
					<div class="col-md-4">
					</div>
					<!--Botón Logout-->
					<div class="col-md-4">					 
						<a href="../Controllers/ActionController.php?action=Salir" type="button" class="btn btn-danger pull-right" title="Salir">
							<i class="fa fa-sign-out" aria-hidden="true"></i>						
						</a>
					</div>
				</div>
			
		</div>
	</div>
<!--CABECERA-->	

<!--Menu lateral-->
	<div class="row">		
		<div class="col-sm-3">
				<div class="contenedor-menu">
					<a href="#" class="btn-menu">Menu<i class="icono fa fa-bars" aria-hidden="true"></i></a>
			
					<ul class="menu">
					<!-- admin-->
					<?php if($comprobarUsuarioGrupo=="ADMIN"){?>     
                    <li class="active">
                        <li>

                        </li>
                    </li>
					<li><a href="#"><i class="icono izquierda fa fa-wrench" aria-hidden="true"></i>ADMIN<i class="icono derecha fa fa-chevron-down" aria-hidden="true"></i></a>		
						<ul>

							<li><a href="../Controllers/ActionController.php?action=ListarUsers"><?=$idi["Users"]?></a></li>
							<li><a href="../Controllers/ActionController.php?action=ListarGrupos"><?=$idi["Grupos"]?></a></li>
							<li><a href="../Controllers/ActionController.php?action=ListarFuncionalidades"><?=$idi["Funcionalidades"]?></a></li>
							<li><a href="../Controllers/ActionController.php?action=dentroAccion"><?=$idi["Acciones"]?></a></li>
							<li><a href="../Controllers/PermisoController.php?action=showAll"><?=$idi["Permisos"]?></a></li>							
							<li><a href="../Controllers/ActionController.php?action=dentroTrabajo"><?=$idi["Trabajos"]?></a></li>
							<li><a href="../Controllers/EvaluacionController.php?action=showAll"><?=$idi["Evaluaciones"]?></a></li>
							<li><a href="../Controllers/ActionController.php?action=ListaHistorias"><?=$idi["Historias"]?></a></li>
							<li><a href="../Controllers/AsignacQAController.php?action=showAll"><?=$idi["AsignacionQAS"]?></a></li>
							<li><a href="../Controllers/ActionController.php?action=ListarNotas"><?=$idi["Notas"]?></a>
							<li><a href="../Controllers/ActionController.php?action=ListarEntregas""></i><?=$idi["Entregas"]?></a>
						</li>	
						</ul>
					</li>
						
					<?php  } ?>
					<!--user-->
					<?php if($comprobarUsuarioGrupo=="USER"){?> 
						<li><a href="../Controllers/ActionController.php?action=ListarEntregasUser"><i class="icono izquierda fa fa-user-o" aria-hidden="true"></i><?=$idi["Entregas"]?><i class="icono derecha fa fa-chevron-down" aria-hidden="true"></i></a>
						</li>
						<li><a href="../Controllers/ActionController.php?action=ListarQAsUser"><i class="icono izquierda fa fa-star" aria-hidden="true"></i>QAS<i class="icono derecha fa fa-chevron-down" aria-hidden="true"></i></a>
						</li>
						<li><a href="#"><i class="icono izquierda fa fa-wrench" aria-hidden="true"></i><?=$idi["Resultados"]?><i class="icono derecha fa fa-chevron-down" aria-hidden="true"></i></a>		
							<ul>
								<li><a href="#">ET1</a></li>
								<li><a href="#">ET2</a></li>
								<li><a href="#">ET3</a></li>
							</ul>
						</li>
						<?php  } ?>
					</ul>

				</div>

		</div>
<!--Menu lateral-->
	
<?php		
	}
}
?>