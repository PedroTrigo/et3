/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Este archivo es el javascript del menú lateral de la web.
 */
$(document).ready(function(){ //Cuando la página está cargada, ejecuta la función
    $('.menu li:has(ul)').click(function(e){ // Previene el comportamiento de los elementos li del menu que tienen lu (submenus)
      // e.preventDefault(); //al presionar botón NO redirige a ninguna página
		
		if($(this).hasClass('activado')){ //Elementos con la clase activado (submenus desplegados)
            $(this).removeClass('activado'); //Elimina la clase activado
            $(this).children('ul').slideUp(); //Oculta el submenu (al volver a hacer click)
        }else{ //si no tiene la clase activado 
            $('.menu li ul').slideUp();  //los submenus se ocultan
            $('.menu li').removeClass('activado'); //elimina la clase activado de los elemntos li
            $(this).addClass('activado'); // al elemento clickeado se le añade la clase activado
            $(this).children('ul').slideDown(); //muestra los hijos
        }
    });
}); 


