
<script type="text/javascript">
    /**
     * Autor: Shopify
     * Fecha de creación: 06/12/2017
     * Función: Este archivo es el javascript de todos los campos
     * de los formularios de las operaciones sobre las tablas de la base de datos.
     */




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////VALIDACIONES PEDIDAS EN LA DEFINICIÓN//////(((((((//////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
1)function comprobarVacio(campo)
2)function comprobarText(campo,size)
3)function comprobarAlfabetico(campo,size)
4)function comprobarInt(campo,valormenor,valormayor)
5)function comprobarReal(campo, numerodecimales, valormenor, valormayor)
6)function comprobarDni(value)
7)function comprobarTelefono(campo)
8)function comprobarEmail(campo)

NOTA: ESTAS FUNCIONES MUESTRAN LOS MENSAJES DE ERROR CON ALERTS. Ignora los mensajes de error en rojo debajo del campo.
*/
//Comprobar que el campo no está vacio
//parametro campo: atributos del input
//Para probarlo pon esto en el campo loginSearch (linea 144): onblur="comprobarVacio(this);"

function comprobarVacio(campo) {
	var texto=''; //mensaje de error
	var resul = true;//variable booleana de retorno de la función
  	if ((campo.value == null) || (campo.value.length == 0)){ //comprueba que el campo no está vacio
  		texto='<?php echo $idi['noVacio']; ?>';   				
  		resul=false;
  	}else{
  		resul = true;
  	}
  	mensaje(campo.name,texto); //función que muestra el mensaje de error
  	return resul;	//retorno de la función  		  
 } 
////////////////////////////////////////////////////////
 /*VALIDACION IdTrabajo

*/
 function comprobarIDTAdd(campo){
	var texto=""; //almacena el texto a mostrar en el mensaje de error.
	var resul=true; //variable booleana de salida.
	if ((campo.value != null) && (campo.value.length != 0)){  //no vacio		
    	if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑ\s])+$/.test(campo.value) == false){ //letras y/o numeros y espacios
			texto = '<?php echo $idi['NombreError']; ?>';
			resul = false;
		}else if(campo.value.length>size) { //comprueba tamaño
			texto= '<?php echo $idi['EmailError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';		
			resul = false;
		}else{		
			resul=true;	//todo ok	
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	}
  	mensaje(campo.name,texto); //formatea el mensaje de error
  	return resul;	
}

/*VALIDACION IdHistoria

 */
 function comprobarIDHAdd(campo){
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
  	var telefInternacional=/^(0034){0,1}[0-9]{9}$/; //Prefijo internacional(opcional) y 9 cifras consecutivas 	
 	if((campo.value == null) || (campo.value.length == 0)){ //Comprueba que no está vacio    	 
    	 texto = '<?php echo $idi['noVacio']; ?>';  
    	 resul=false;
  	}else if(!telefInternacional.test(campo.value)){ //Comprueba que se cumple la expresión regular
       	texto = '<?php echo $idi['TelError']; ?>'; 
		resul=false;
		}else{
			resul=true; //Todo correcto
		}
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
}

/*VALIDACION TextoHistoria
en add
*/
 function comprobarTHAdd(campo){
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
  	var telefInternacional=/^(0034){0,1}[0-9]{9}$/; //Prefijo internacional(opcional) y 9 cifras consecutivas 	
 	if((campo.value == null) || (campo.value.length == 0)){ //Comprueba que no está vacio    	 
    	 texto = '<?php echo $idi['noVacio']; ?>';  
    	 resul=false;
  	}else if(!telefInternacional.test(campo.value)){ //Comprueba que se cumple la expresión regular
       	texto = '<?php echo $idi['TelError']; ?>'; 
		resul=false;
		}else{
			resul=true; //Todo correcto
		}
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
}
/*en edit*/
function comprobarTHEdit(campo){
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
  	var telefInternacional=/^(0034){0,1}[0-9]{9}$/; //Prefijo internacional(opcional) y 9 cifras consecutivas 	
 	if((campo.value == null) || (campo.value.length == 0)){ //Comprueba que no está vacio    	 
    	 texto = '<?php echo $idi['noVacio']; ?>';  
    	 resul=false;
  	}else if(!telefInternacional.test(campo.value)){ //Comprueba que se cumple la expresión regular
       	texto = '<?php echo $idi['TelError']; ?>'; 
		resul=false;
		}else{
			resul=true; //Todo correcto
		}
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
}
/*VALIDACION login
en add
*/
 function comprobarloginAdd(campo){
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
  	var telefInternacional=/^(0034){0,1}[0-9]{9}$/; //Prefijo internacional(opcional) y 9 cifras consecutivas 	
 	if((campo.value == null) || (campo.value.length == 0)){ //Comprueba que no está vacio    	 
    	 texto = '<?php echo $idi['noVacio']; ?>';  
    	 resul=false;
  	}else if(!telefInternacional.test(campo.value)){ //Comprueba que se cumple la expresión regular
       	texto = '<?php echo $idi['TelError']; ?>'; 
		resul=false;
		}else{
			resul=true; //Todo correcto
		}
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
}

/*VALIDACION NotaTrabajo
en add
*/
 function comprobarNTAdd(campo){
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
  	var telefInternacional=/^(0034){0,1}[0-9]{9}$/; //Prefijo internacional(opcional) y 9 cifras consecutivas 	
 	if((campo.value == null) || (campo.value.length == 0)){ //Comprueba que no está vacio    	 
    	 texto = '<?php echo $idi['noVacio']; ?>';  
    	 resul=false;
  	}else if(!telefInternacional.test(campo.value)){ //Comprueba que se cumple la expresión regular
       	texto = '<?php echo $idi['TelError']; ?>'; 
		resul=false;
		}else{
			resul=true; //Todo correcto
		}
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
}
/*en edit*/
function comprobarTEdit(campo){
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
  	var telefInternacional=/[0-9]{1,2}$/; //Prefijo internacional(opcional) y 9 cifras consecutivas 	
 	if((campo.value == null) || (campo.value.length == 0)){ //Comprueba que no está vacio    	 
    	 texto = '<?php echo $idi['noVacio']; ?>';  
    	 resul=false;
  	}else if(!telefInternacional.test(campo.value)){ //Comprueba que se cumple la expresión regular
       	texto = '<?php echo $idi['TelError']; ?>'; 
		resul=false;
		}else{
			resul=true; //Todo correcto
		}
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
}
//////////////////////////////////////////
/*VALIDACION DNI
Empieza por número 8 números seguidos de letra mayúscula o minúscula

*/
function comprobarDniAdd(campo) {
  	var texto=""; //Almacena el texto de error
	var resul= true;//variable booleana de retorno de la función
  	var numero; //indice de la cadena
  	var letraDNI; //letra introducida por teclado
  	var letra; //letras válidas de un NIF  	
  	var expresion_regular_nif;//almacena la expresion regular del nif
 	//var expresion_regular_dni//almacena la expresion regular del dni
  	expresion_regular_nif = /^\d{8}[a-zA-Z]$/; //Expresión regular NIF
  	
 	if ((campo.value == null) || (campo.value.length == 0)){ //Comprobación vacio
 		texto = '<?php echo $idi['noVacio']; ?>';  				
  		resul = false;
  	}else if(expresion_regular_nif.test (campo.value) == true){
    	numero = campo.value.substr(0,campo.value.length-1); //obtiene la parte numeríca de la cadena introducida en el campo del formulario
   	 	letraDNI = campo.value.substr(campo.value.length-1,1); //obtiene la letra del NIF introducido en el campo del formulario
    	numero = numero % 23; //calcula el modulo 23 del DNI
    	letra='TRWAGMYFPDXBNJZSQVHLCKET'; //Letras válidas de un NIF
    	letra=letra.substring(numero,numero+1); //Substring de un solo caracter de la cadena de letras válidas que empieza en la posición marcada por la variable numero después de hacer el módulo 23
    		if (letra!=letraDNI.toUpperCase()) { //comprueba la letra introducida por teclado (pasada a mayusculas) y la compara con la calculada en el paso anterior
       			texto = '<?php echo $idi['NIFletra']; ?>';			
				resul = false;
    		}else{       			
       			 //Todo ok			
				resul = true;
    		}
  	}else{   				
   				texto = '<?php echo $idi['NIFerror']; ?>';	//NO cumple el patrón		
				resul = false;
	}
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
	}

/*VALIDACION DNI de EDIT
Empieza por número 8 números seguidos de letra mayúscula o minúscula
Se diferencia del formulario ADD en que puede estar vacio
*/
function comprobarDniEdit(campo) {
  	var texto=""; //Almacena el texto de error
	var resul= true;//variable booleana de retorno de la función
  	var numero; //indice de la cadena
  	var letraDNI; //letra introducida por teclado
  	var letra; //letras válidas de un NIF  	
  	var expresion_regular_nif;//almacena la expresion regular del nif
 	//var expresion_regular_dni//almacena la expresion regular del dni
  	expresion_regular_nif = /^\d{8}[a-zA-Z]$/; //Expresión regular NIF  	

  	if ( (campo.value != null) && (campo.value.length != 0) ){  //no vacio	
 	
	  	if(expresion_regular_nif.test (campo.value) == true){
	    	numero = campo.value.substr(0,campo.value.length-1); //obtiene la parte numeríca de la cadena introducida en el campo del formulario
	   	 	letraDNI = campo.value.substr(campo.value.length-1,1); //obtiene la letra del NIF introducido en el campo del formulario
	    	numero = numero % 23; //calcula el modulo 23 del DNI
	    	letra='TRWAGMYFPDXBNJZSQVHLCKET'; //Letras válidas de un NIF
	    	letra=letra.substring(numero,numero+1); //Substring de un solo caracter de la cadena de letras válidas que empieza en la posición marcada por la variable numero después de hacer el módulo 23
	    		if (letra!=letraDNI.toUpperCase()) { //comprueba la letra introducida por teclado (pasada a mayusculas) y la compara con la calculada en el paso anterior
	       			texto = '<?php echo $idi['NIFletra']; ?>';			
					resul = false;
	    		}else{       			
	       			 //Todo ok			
					resul = true;
	    		}
	  	}else{   				
	   			texto = '<?php echo $idi['NIFerror']; ?>';	//NO cumple el patrón		
				resul = false;
		}
	}else{       			
	       	//Todo ok aunque esté vacío			
			resul = true;
	    }
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
	
}


/*COMPROBAR TELEFONO ADD*/
/*Para probarlo pon lo siguiente en el campo loginSearch (linea 144): onblur="comprobarTelefono(this);"*/
function comprobarTelefonoAdd(campo){
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
  	var telefInternacional=/^(0034){0,1}[0-9]{9}$/; //Prefijo internacional(opcional) y 9 cifras consecutivas 	
 	if((campo.value == null) || (campo.value.length == 0)){ //Comprueba que no está vacio    	 
    	 texto = '<?php echo $idi['noVacio']; ?>';  
    	 resul=false;
  	}else if(!telefInternacional.test(campo.value)){ //Comprueba que se cumple la expresión regular
       	texto = '<?php echo $idi['TelError']; ?>'; 
		resul=false;
		}else{
			resul=true; //Todo correcto
		}
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
}

/*COMPROBAR TELEFONO EDIT*/
/*Puede estar vacio*/
function comprobarTelefonoEdit(campo){
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
  	var telefInternacional=/^(0034){0,1}[0-9]{9}$/; //Prefijo internacional(opcional) y 9 cifras consecutivas 	
 	if((campo.value != null) && (campo.value.length != 0)){ //Comprueba que no está vacio    	 
    	 
  		if(!telefInternacional.test(campo.value)){ //Comprueba que se cumple la expresión regular
       	texto = '<?php echo $idi['TelError']; ?>'; 
		resul=false;
		}else{
			resul=true; //todo ok
		}
	}else{
			resul=true; //Todo correcto si no se introduce nada en el input
	}
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
}

/*VALIDACION EMAIL:

Mayúsculas y minúsculas del alfabeto ingles.
Números de 0 al 9
puede contener punto pero no al inicio o repetirse.
puede usar los caracteres: !#$%&'*+-/=?^_`{|}~

/* Comportamiento expresión regular Email 
^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$

^ Comienzo de posicion al principio de la cadena
Primer grupo de NO captura: (?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")
	Primera alternativa: [^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*
		[^<>()[\].,;:\s@"]+
			Coincide con un caracter simple NO presente en la lista: <>()[\].,;:\s@"
			+ Cuantificador — 1 o más veces

		Primer grupo de captura: (\.[^<>()[\].,;:\s@"]+)*
			* Cuantificador — Entre 0 o varias veces
			\. Caracter . 
			Caracteres distintos de: [^<>()[\].,;:\s@"]+
			+ Cuantificador — Entre 1 o varias veces
			Caracteres de la lista: <>()[\].,;:\s@"
	Segunda alternativa: "[^\n"]+"
		Caracter " seguido de caracteres distintos de \n"
		+ Cuantificador — Entre 1 o varias veces

	Caracter @
	Grupo de NO captura (?:[^<>()[\].,;:\s@"]+\.)+
	+ Cuantificador — Entre 1 o varias veces
	Caracteres distintos de: [^<>()[\].,;:\s@"]+
	+ Cuantificador — Entre 1 o varias veces
	Lista de caracteres: <>()[\].,;:\s@"
	
	Cracter . \.

	Caracteres distintos de: [^<>()[\]\.,;:\s@"]{2,63}
	{2,63} Cuantificador — entre 2 y 63
	Lista de caracteres: <>()[\]\.,;:\s@"

	$ al final de la cadena

 */
// function comprobarEmailAdd(campo) { 
// 	//Si no cumple la regex muestra mensaje de error
// 	var texto=""; //Almacena el texto de error
// 	var resul= true;//variable booleana de retorno de la función
//     if ( (/^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/.test(campo.value)==false) ){  		
//   		texto = 'La dirección de email es incorrecta!.';  				
//   		resul = false;
//   	}else{
//   		resul = true;
// }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////VALIDACION DE LOS CAMPOS DE LOS FORMULARIOS ADD EDIT SEARCH///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*Validación campo loginAdd*/
//NO puede estar vacio
//Admite letras numeros y caracteres especiales . - _ 
//No admite espacios
//Comprueba el tamaño
function comprobarLoginAdd(campo,size) {
	var texto=""; //Almacena el texto de error
	var resul= true;//variable booleana de retorno de la función
	if ((campo.value == null) || (campo.value.length == 0)){ //input vacio
  		texto = '<?php echo $idi['noVacio']; ?>';  				
  		resul = false;
  	}else if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑ0-9\-\._])+$/.test(campo.value) == false){ //no se cumple la expresin regular
		texto = '<?php echo $idi['LoginError']; ?>';
		resul = false;
	}else if((campo.value.length>size) || (campo.value.length<4)) { //comprobació tamaño

		texto= '<?php echo $idi['LoginError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
		resul = false;
	}else{		
		resul=true;	//todo ok	
	}
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}

/*Validación campo passAdd*/
//NO puede estar vacio
//Admite letras numeros y caracteres especiales . - _
//NO admite espacios
//Comprueba el tamaño
function comprobarPassAdd(campo,size){
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
	if ((campo.value == null) || (campo.value.length == 0)){ //vacio
  		texto = '<?php echo $idi['noVacio']; ?>';  				
  		resul = false;
  	}else if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑ0-9\-\._])+$/.test(campo.value) == false){ //letras, numeros y .-_
		texto = '<?php echo $idi['LoginError']; ?>';			
		resul = false;
	}else if((campo.value.length>size) || (campo.value.length<4)) { //no puede ser menor de 6 y mayor de 25
		texto= '<?php echo $idi['LoginError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
		resul = false;	
	}else{		
		resul=true;	//todo ok	
	}
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}

/*Validación campo passEdit*/
//Admite letras numeros y caracteres especiales . - _
//Comprueba el tamaño
function comprobarPassEdit(campo,size){
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
	if ( (campo.value != null) && (campo.value.length != 0) ){  //no vacio			
		if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑ0-9\-\._])+$/.test(campo.value) == false){ //letras, numeros y .-_
			texto = '<?php echo $idi['LoginError']; ?>';			
			resul = false;
		}else if((campo.value.length<4) || (campo.value.length>size) ) { //no puede ser menor de 6 y mayor de 25
			texto= '<?php echo $idi['LoginError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';				
			resul = false;	
		}else{
			resul=true; //todo ok
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	}
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}

/*Validación campo emailUserAdd*/
//NO puede estar vacio
/*
    Mayúsculas y minúsculas del alfabeto.
    Números de 0 al 9
    puede contener punto pero no al inicio o repetirse.
    puede usar los caracteres: !#$%&'*+-/=?^_`{|}~
*/
//Comprueba el tamaño maximo
function comprobarEmailAdd(campo,size) {
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
	if ((campo.value == null) || (campo.value.length == 0)){ //vacio
  		texto = '<?php echo $idi['noVacio']; ?>';  				
  		resul = false;	
  	}else if ( (/^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/.test(campo.value)==false) ){ //no cumple condiciones regex (explicada linea 180)
  		texto='<?php echo $idi['EmailError']; ?>';
  		resul=false;
  	}else if(campo.value.length>size){ //No cumple el tamaño
  		texto= '<?php echo $idi['EmailError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
		resul = false;
  	}else{
   		resul = true; //todo ok
  	}
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}

/*Validación campo emailUserEdit*/
function comprobarEmailEdit(campo,size) {
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función	
	if ((campo.value != null) && (campo.value.length != 0)){  	//No vacio	
  		if ( (/^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/.test(campo.value)==false) ){ //no cumple condiciones regex (explicada linea 180)
  			texto = "<?php echo $idi['EmailError']; ?>";
  			resul = false;
  		}else if(campo.value.length>size){ //Comprueba el tamaño maximo
  			texto = '<?php echo $idi['EmailError1']; ?> ' + campo.size + ' caracteres';			
			resul = false;
  		}else{
			resul=true; //todo ok
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	} 
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}

/*Validación campo nombreUserAdd*/
//NO puede estar vacio
//Admite nombre compuestos
//Comprueba el tamaño
function comprobarNombreUserAdd(campo,size) {
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
	if ((campo.value == null) || (campo.value.length == 0)){ //vacio
  		texto = '<?php echo $idi['noVacio']; ?>';  				
  		resul = false;
  	}else if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑ\s])+$/.test(campo.value) == false){ //letras y espacios
		texto = '<?php echo $idi['NombreError']; ?>';
		resul = false;
	}else if((campo.value.length>size) || (campo.value.length<4)) { //comprueba tamaño
		texto= '<?php echo $idi['LoginError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
		resul = false;
	}else{		
		resul=true;	//todo ok	
	}
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}

/*Validación campo nombreUserEdit*/
//Admite nombre compuestos
//Puede estar vacio
//Comprueba el tamaño
function comprobarNombreUserEdit(campo,size) {
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
	if ((campo.value != null) && (campo.value.length != 0)){  //no vacio		
    	if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑ\s])+$/.test(campo.value) == false){ //letras y espacios
			texto = '<?php echo $idi['NombreError']; ?>';
			resul = false;
		}else if( (campo.value.length>size) || (campo.value.length<4) ) { //comprueba el tamaño
			texto= '<?php echo $idi['LoginError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
			resul = false;
		}else{
			resul=true; //todo ok
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	}
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}
/*Validación campo apellidosUserAdd*/
//NO puede estar vacio
//Admite letras y espacios 
//Comprueba el tamaño
function comprobarApellUserAdd(campo,size) {
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
	if ((campo.value == null) || (campo.value.length == 0)){ //vacio
  		texto = '<?php echo $idi['noVacio']; ?>';  				
  		resul = false;
  	}else if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑ\s])+$/.test(campo.value) == false){ //letras y espacios
		texto = '<?php echo $idi['NombreError']; ?>';
		resul = false; //falso
	}else if((campo.value.length>size) || (campo.value.length<4)) { //comprueba el tamaño
		texto= '<?php echo $idi['LoginError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
		resul = false; //falso
	}else{		
		resul=true;	//todo ok	
	}	
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}

/*Validación campo apellidosUserAdd*/
//Admite letras y espacios
//Comprueba el tamaño
//Puede estar vacio
function comprobarApellUserEdit(campo,size) {
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
	if ((campo.value != null) && (campo.value.length != 0)){  //se introducen datos en el campo		
  		if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑ\s])+$/.test(campo.value) == false){  //letras y espacios
			texto = '<?php echo $idi['NombreError']; ?>';
			resul = false; //falso
		}else if((campo.value.length>size) || (campo.value.length<4)) { //comprueba el tamaño max y min
			texto= '<?php echo $idi['LoginError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
			resul = false; //falso
		}else{
			resul=true; //todo ok
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	}
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}
/*Validar edit nota*/
function comprobarNotaEdit(campo,size) {
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
	if ((campo.value != null) && (campo.value.length != 0)){  //se introducen datos en el campo		
  		if (/^([0-9\s])+$/.test(campo.value) == false){  //letras y espacios
			texto = '<?php echo $idi['NombreError']; ?>';
			resul = false; //falso
		}else if((campo.value.length>size) || (campo.value.length>4)) { //comprueba el tamaño max y min
			texto= '<?php echo $idi['LoginError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
			resul = false; //falso
		}else{
			resul=true; //todo ok
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	}
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}

/*Validación campo direccionUserAdd*/
//NO puede estar vacio
//Admite letras, números y espacios
//Comprueba el tamaño
function comprobarDireccionAdd(campo,size) {
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
	if ((campo.value == null) || (campo.value.length == 0)){ //vacio
  		texto = '<?php echo $idi['noVacio']; ?>';  				
  		resul = false; //falso
  	}else if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑº\-0-9\s])+$/.test(campo.value) == false){ //letras, espacios, numeros y caracteres especiales º-
		texto = '<?php echo $idi['NombreError']; ?>';
		resul = false; //falso
	}else if((campo.value.length>size) || (campo.value.length<4)) { //comprueba tamaño
		texto= '<?php echo $idi['LoginError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
		resul = false; //falso
	}else{		
		resul=true;	//todo ok	
	}
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}

/*Validación campo direccionUserAdd*/
//Admite letras, números, espacios y caracteres especiales º-
//Comprueba el tamaño
//Puede estar vacio
function comprobarDireccionEdit(campo,size) {
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
	if ((campo.value != null) && (campo.value.length != 0)){  //se introducen datos en el campo		
  		if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑº\-0-9\s])+$/.test(campo.value) == false){  //letras, espacios, numeros y caracteres especiales º-
			texto = '<?php echo $idi['NombreError']; ?>';
			resul = false; //falso
		}else if((campo.value.length>size) || (campo.value.length<4)) { //comprueba el tamaño max y min
			texto= '<?php echo $idi['LoginError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
			resul = false; //falso
		}else{
			resul=true; //todo ok
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	}
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}

/////////////////////////////////////////////////////VALIDACION FORMULARIO SEARCH/////////////////////////////////////////

/*Validación campo LoginSearch*/
//Admite letras numeros y caracteres especiales . - _
//Comprueba el tamaño
function comprobarLoginSearch(campo,size) {
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
	if ((campo.value != null) && (campo.value.length != 0)){  	//no vacio		
  		if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑ0-9\-\._])+$/.test(campo.value) == false) { //letras (min/mayus) y/o numeros t .-_
			texto = '<?php echo $idi['LoginError']; ?>';
			resul = false;
		}else if(campo.value.length>size){ //comprueba tamaño
			texto= '<?php echo $idi['EmailError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
			resul = false;	
		}else{
			resul=true; //todo ok
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	}
  	mensaje(campo.name,texto); //función que muestra el mensaje (explicada al final del archivo)
  	return resul;	
}

/*VALIDACION DNI de SEARCH
Empieza por número 8 números seguidos de letra mayúscula o minúscula
Se diferencia del formulario ADD en que puede estar vacio
*/
function comprobarDniSearch(campo) {
  	var texto=""; //Almacena el texto de error
	var resul= true;//variable booleana de retorno de la función
  	var numero; //indice de la cadena
  	var letraDNI; //letra introducida por teclado
  	var letra; //letras válidas de un NIF  	
  	var expresion_regular_nif;//almacena la expresion regular del nif
 	//var expresion_regular_dni//almacena la expresion regular del dni
  	expresion_regular_nif = /^\d{8}[a-zA-Z]$/; //Expresión regular NIF  	

  	if ( (campo.value != null) && (campo.value.length != 0) ){  //no vacio	
 	
	  	if(expresion_regular_nif.test (campo.value) == true){
	    	numero = campo.value.substr(0,campo.value.length-1); //obtiene la parte numeríca de la cadena introducida en el campo del formulario
	   	 	letraDNI = campo.value.substr(campo.value.length-1,1); //obtiene la letra del NIF introducido en el campo del formulario
	    	numero = numero % 23; //calcula el modulo 23 del DNI
	    	letra='TRWAGMYFPDXBNJZSQVHLCKET'; //Letras válidas de un NIF
	    	letra=letra.substring(numero,numero+1); //Substring de un solo caracter de la cadena de letras válidas que empieza en la posición marcada por la variable numero después de hacer el módulo 23
	    		if (letra!=letraDNI.toUpperCase()) { //comprueba la letra introducida por teclado (pasada a mayusculas) y la compara con la calculada en el paso anterior
	       			texto = '<?php echo $idi['NIFletra']; ?>';			
					resul = false;
	    		}else{       			
	       			 //Todo ok			
					resul = true;
	    		}
	  	}else{   				
	   			texto = '<?php echo $idi['NIFerror']; ?>';	//NO cumple el patrón		
				resul = false;
		}
	}else{       			
	       	//Todo ok aunque esté vacío			
			resul = true;
	    }
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
	
}

/*Validación campo emailUserSearch*/
//Comprueba algunos de los caracteres válidos de email para buscar parcialmente
//Comprueba el tamaño maximo
function comprobarEmailSearch(campo,size) {
	var texto=""; //almacena el texto a mostrar en el mensaje de error.
	var resul=true; //variable booleana de salida.
	if ((campo.value != null)  && (campo.value.length != 0)){  //si no vacio 	
  		if ((/^([a-zA-ZñÑ0-9\-\._@])+$/.test(campo.value))==false) { //letras y/o numeros y _-.@
  			texto='<?php echo $idi['EmailError']; ?>';
  			resul=false;
  		}else if(campo.value.length>size){ //comprueba el tamaño másximo
  			texto= '<?php echo $idi['EmailError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
			resul = false;
  		}else{		
			resul=true; //todo ok		
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	}
  	mensaje(campo.name,texto); //formatea el mensaje de error
  	return resul;	
}

/*Validación campo nombreUserSearch*/
//Admite nombre compuestos
//Puede estar vacio
//Comprueba el tamaño maximo
function comprobarNombreUserSearch(campo,size) {
	var texto=""; //almacena el texto a mostrar en el mensaje de error.
	var resul=true; //variable booleana de salida.
	if ((campo.value != null) && (campo.value.length != 0)){  //no vacio		
    	if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑ\s])+$/.test(campo.value) == false){ //letras y/o numeros y espacios
			texto = '<?php echo $idi['NombreError']; ?>';
			resul = false;
		}else if(campo.value.length>size) { //comprueba tamaño
			texto= '<?php echo $idi['EmailError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';		
			resul = false;
		}else{		
			resul=true;	//todo ok	
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	}
  	mensaje(campo.name,texto); //formatea el mensaje de error
  	return resul;	
}

/*Validación campo apellidosUserSearch*/
//Admite letras y espacios
//Comprueba el tamaño maximo
//Puede estar vacio
function comprobarApellUserSearch(campo,size) {
	var texto=""; //almacena el texto a mostrar en el mensaje de error.
	var resul=true; //variable booleana de salida.
	if ((campo.value != null) && (campo.value.length != 0)) {  	//no vacio		
  		if (/^([a-zA-ZáÁéÉíÍóÓúÚñÑ\s])+$/.test(campo.value) == false){ //letras y/o numeros y espacios
			texto = '<?php echo $idi['NombreError']; ?>';
			resul = false;
		}else if(campo.value.length>size) { //comprueba tamaño
			texto= '<?php echo $idi['EmailError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';			
			resul = false;
		}else{		
			resul=true;	//todo ok		
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	}
  	mensaje(campo.name,texto); //formatea el mensaje de error
  	return resul;	
}

/*COMPROBAR TELEFONO SEARCH*/
/*Puede estar vacio*/
function comprobarTelefonoSearch(campo){
	var texto=""; //Almacena el texto de error
	var resul=true; //variable booleana de retorno de la función
  	var telefInternacional=/^(0034){0,1}[0-9]{9}$/; //Prefijo internacional(opcional) y 9 cifras consecutivas 	
 	if((campo.value != null) && (campo.value.length != 0)){ //Comprueba que no está vacio    	 
    	 
  		if(!telefInternacional.test(campo.value)){ //Comprueba que se cumple la expresión regular
       	texto = '<?php echo $idi['TelError']; ?>'; 
		resul=false;
		}else{
			resul=true; //todo ok
		}
	}else{
			resul=true; //Todo correcto si no se introduce nada en el input
	}
	mensaje(campo.name,texto); //función que muestra el mensaje 
  	return resul;	//retorno de la función
}

/*Validación campo nombreUserSearch*/
//Admite nombre compuestos
//Puede estar vacio
//Comprueba el tamaño maximo
function comprobarDireccionUserSearch(campo,size) {
	var texto=""; //almacena el texto a mostrar en el mensaje de error.
	var resul=true; //variable booleana de salida.
	if ((campo.value != null) && (campo.value.length != 0)){  //no vacio		
    	if (/^([a-zA-ZáÁéÉíÍóÓúÚñº\-0-9\s])+$/.test(campo.value) == false){ //letras, espacios, numeros y caracteres especiales º-
			texto = '<?php echo $idi['NombreError']; ?>';
			resul = false;
		}else if(campo.value.length>size) { //comprueba tamaño
			texto= '<?php echo $idi['EmailError1']; ?> ' + campo.size + ' <?php echo $idi['LoginError11']; ?>';		
			resul = false;
		}else{		
			resul=true;	//todo ok	
		}
	}else{
		resul=true; //todo ok si no se introduce nada en el input
	}
  	mensaje(campo.name,texto); //formatea el mensaje de error
  	return resul;	
}


/////////////////////////////////////////////////////////////////////////VALIDACIONES EN EL SUBMIT///////////////////////////////////////////////////
/*
1)validarSEARCH()
2)validarADD()
3)validarEDIT()
En estas 3 funciones se comprueba que las validaciones anterioes devuelven true en cada uno de los formularios(SEARCH, ADD, EDIT).Si es así se envía el formulario.
En caso contrario muestra un mesaje de error.
*/
function validarSEARCH(){	
	//Almaceno en toret las variables booleanas de cada funcion
	toret1=comprobarLoginSearch(document.FormSearch.loginSearch,9);
	toret2=comprobarDniSearch(document.FormAdd.dniAdd);
	toret3=comprobarNombreUserSearch(document.FormSearch.nombreUserSearch,30);
	toret4=comprobarApellUserSearch(document.FormSearch.apellidosUserSearch,50);	
	toret5=comprobarEmailSearch(document.FormSearch.emailUserSearch,40);
	toret6=comprobarDireccionUserSearch(document.FormSearch.direccionUserSeach,60);		
	toret7=comprobarTelefonoSearch(document.FormSearch.telefonoUserSearch);	

	if( toret1==true && toret2==true && toret3==true && toret4==true && toret5==true && toret6==true && toret7==true ){
		
		alert('<?php echo $idi['exitoCampos']; ?>'); //si todos son true muestra el mesaje de error
		document.FormSearch.submit(); //hace el dubmit del formulario
	
	}else{
		alert('<?php echo $idi['errorCampos']; ?>'); //todos son false, muestra el mensaje de error
		return false;	//devuelve false	
	}	
}

function validarADD(){	
	//Almeceno en toret las variables booleanas de cada funcion
	toret1=comprobarLoginAdd(document.FormAdd.loginAdd,9);
	toret2=comprobarPassAdd(document.FormAdd.passAdd,20);	
	toret3=comprobarDniAdd(document.FormAdd.dniAdd);
	toret4=comprobarNombreUserAdd(document.FormAdd.nombreUserAdd,30);
	toret5=comprobarApellUserAdd(document.FormAdd.apellidosUserAdd,50);	
	toret6=comprobarEmailAdd(document.FormAdd.emailUserAdd,40);
	toret7=comprobarDireccionUserAdd(document.FormAdd.direccionUserAdd,60);
	toret8=comprobarTelefonoAdd(document.FormAdd.telefonoUserAdd);
	

	if( toret1==true && toret2==true && toret3==true && toret4==true && toret5==true && toret6==true && toret7==true && toret8==true ){		
		alert('<?php echo $idi['exitoCampos']; ?>'); //si todos son true muestra el mesaje de error
		document.FormAdd.submit(); //hace el dubmit del formulario
	
	}else{
		alert('<?php echo $idi['errorCampos']; ?>');  //todos son false, muestra el mensaje de error
		return false;	//devuelve false		
	}
}

function validarEdit(){	
	//Almeceno en toret las variables booleanas de cada funcion
	toret1=comprobarPassEdit(document.FormEdit.passEdit,9);	
	toret2=comprobarDniEdit(document.FormEdit.dniEdit);
	toret3=comprobarNombreUserEdit(document.FormEdit.nombreUserEdit,30);
	toret4=comprobarApellUserEdit(document.FormEdit.apellidosUserEdit,50);
	toret5=comprobarEmailEdit(document.FormEdit.emailUserEdit,40);
	toret6=comprobarDireccionEdit(document.FormEdit.direccionUserEdit,60);
	toret7=comprobarTelefonoEdit(document.FormEdit.telefonoUserEdit);
	

	if(toret1=true && toret2==true && toret3==true && toret4==true && toret5==true && toret6==true && toret7==true){		
		alert('<?php echo $idi['exitoCampos']; ?>'); //si todos son true muestra el mesaje de error
		document.FormEdit.submit(); //hace el dubmit del formulario
	
	}else{
		alert('<?php echo $idi['errorCampos']; ?>');  //todos son false, muestra el mensaje de error
		return false;	//devuelve false			
	}
}

function validarEditNota(){	
	//Almeceno en toret las variables booleanas de cada funcion
	/*toret1=comprobarTEdit(document.FormEdit.NotaTEdit,9);	
	

	if(toret1==true){		
		alert('<?php echo $idi['exitoCampos']; ?>'); //si todos son true muestra el mesaje de error
		document.FormEdit.submit(); //hace el dubmit del formulario
	
	}else{
		alert('<?php echo $idi['errorCampos']; ?>');  //todos son false, muestra el mensaje de error
		return false;	//devuelve false			
	}*/
}

//////////////////////////////////////////////////////////Función auxiliar////////////////////////////////////////////////////
/////////////////////////////////////////////////////////mensaje(campo,texto)/////////////////////////////////////////////////
//Colorea de rojo los mensajes de error en un párrafo debajo de cada campo 
//name1: Es el atributo name del input
//texto1: Mensaje de texto a mostrar recogido de la función que valida ese campo
function mensaje(name1,texto1)
	{		
		//Para SEARCH
		if(name1 == "loginSearch"){  //si el nombre del campo es loginSearch		
    	var estilo=document.getElementById("loginTextoSearch"); //selecciona el parrafo a colorear
        estilo.style.color="#c92323"; //lo pone en rojo
        document.getElementById("loginTextoSearch").innerHTML = texto1; //introduce el HTML en ese parrafo

        //y así sucesicamente con todos los campos
   	 	}else if(name1 == "passSearch"){  		
    	var estilo=document.getElementById("passTextoSearch");
        estilo.style.color="#c92323";
        document.getElementById("passTextoSearch").innerHTML = texto1;

    	}else if(name1 == "dniSearch"){  		
    	var estilo=document.getElementById("dniTextoSearch");
        estilo.style.color="#c92323";
        document.getElementById("dniTextoSearch").innerHTML = texto1;

		}else if(name1 == "fechnacUserSearch"){  		
    	var estilo=document.getElementById("fechnacUserTextoSearch");
        estilo.style.color="#c92323";
        document.getElementById("fechnacUserTextoSearch").innerHTML = texto1;

        }else if(name1 == "sexoSearch"){  		
    	var estilo=document.getElementById("sexoTextoSearch");
        estilo.style.color="#c92323";
        document.getElementById("sexoTextoSearch").innerHTML = texto1;

   	 	}else if(name1 == "emailUserSearch"){  		
    	var estilo=document.getElementById("emailUserTextoSearch");
        estilo.style.color="#c92323";
        document.getElementById("emailUserTextoSearch").innerHTML = texto1; 

   	 	}else if(name1 == "nombreUserSearch"){  		
    	var estilo=document.getElementById("nombreUserTextoSearch");
        estilo.style.color="#c92323";
        document.getElementById("nombreUserTextoSearch").innerHTML = texto1;

   	 	}else if(name1 == "apellidosUserSearch"){  	
    	var estilo=document.getElementById("apellidosUserTextoSearch");
        estilo.style.color="#c92323";
        document.getElementById("apellidosUserTextoSearch").innerHTML = texto1; 

   	 	}else if(name1 == "telefonoUserSearch"){  		
    	var estilo=document.getElementById("telefonoUserTextoSearch");
        estilo.style.color="#c92323";
        document.getElementById("telefonoUserTextoSearch").innerHTML = texto1;

        }else if(name1 == "fotoUserSearch"){  		
    	var estilo=document.getElementById("fotoUserTextoSearch");
        estilo.style.color="#c92323";
        document.getElementById("fotoUserTextoSearch").innerHTML = texto1;

        }else if(name1 == "fechaNacUserSearchDia"){  		
    	var estilo=document.getElementById("fechaNacUserTextoDia");
        estilo.style.color="#c92323";
        document.getElementById("fechaNacUserTextoDia").innerHTML = texto1;

        }else if(name1 == "fechaNacUserSearchMes"){  		
    	var estilo=document.getElementById("fechaNacUserTextoMes");
        estilo.style.color="#c92323";
        document.getElementById("fechaNacUserTextoMes").innerHTML = texto1;

        }else if(name1 == "fechaNacUserSearchAño"){  		
    	var estilo=document.getElementById("fechaNacUserTextoAño");
        estilo.style.color="#c92323";
        document.getElementById("fechaNacUserTextoAño").innerHTML = texto1;

        }else if(name1 == "direccionUserSearch"){  		
    	var estilo=document.getElementById("direccionUserTextoSearch");
        estilo.style.color="#c92323";
        document.getElementById("direccionUserTextoSearch").innerHTML = texto1;



        //Para ADD   	 
   	 	}else if(name1 == "loginAdd"){  		
    	var estilo=document.getElementById("loginTextoAdd");
        estilo.style.color="#c92323";
        document.getElementById("loginTextoAdd").innerHTML = texto1; 

   	 	}else if(name1 == "passAdd"){  		
    	var estilo=document.getElementById("passTextoAdd");
        estilo.style.color="#c92323";
        document.getElementById("passTextoAdd").innerHTML = texto1;

    	}else if(name1 == "dniAdd"){  		
    	var estilo=document.getElementById("dniTextoAdd");
        estilo.style.color="#c92323";
        document.getElementById("dniTextoAdd").innerHTML = texto1;

		}else if(name1 == "fechnacUserAdd"){  		
    	var estilo=document.getElementById("fechnacUserTextoAdd");
        estilo.style.color="#c92323";
        document.getElementById("fechnacUserTextoAdd").innerHTML = texto1;

        }else if(name1 == "sexoAdd"){  		
    	var estilo=document.getElementById("sexoTextoAdd");
        estilo.style.color="#c92323";
        document.getElementById("sexoTextoAdd").innerHTML = texto1;

   	 	}else if(name1 == "emailUserAdd"){  		
    	var estilo=document.getElementById("emailUserTextoAdd");
        estilo.style.color="#c92323";
        document.getElementById("emailUserTextoAdd").innerHTML = texto1; 

   	 	}else if(name1 == "nombreUserAdd"){  		
    	var estilo=document.getElementById("nombreUserTextoAdd");
        estilo.style.color="#c92323";
        document.getElementById("nombreUserTextoAdd").innerHTML = texto1;

   	 	}else if(name1 == "apellidosUserAdd"){  	
    	var estilo=document.getElementById("apellidosUserTextoAdd");
        estilo.style.color="#c92323";
        document.getElementById("apellidosUserTextoAdd").innerHTML = texto1; 

   	 	}else if(name1 == "telefonoUserAdd"){  		
    	var estilo=document.getElementById("telefonoUserTextoAdd");
        estilo.style.color="#c92323";
        document.getElementById("telefonoUserTextoAdd").innerHTML = texto1;

        }else if(name1 == "fotoUserAdd"){  		
    	var estilo=document.getElementById("fotoUserTextoAdd");
        estilo.style.color="#c92323";
        document.getElementById("fotoUserTextoAdd").innerHTML = texto1;

    	}else if(name1 == "direccionUserAdd"){  		
    	var estilo=document.getElementById("direccionUserTextoAdd");
        estilo.style.color="#c92323";
        document.getElementById("direccionUserTextoAdd").innerHTML = texto1;

  	 	//Para EDIT 	 
   	 	}else if(name1 == "loginEdit"){  		
    	var estilo=document.getElementById("loginTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("loginTextoEdit").innerHTML = texto1; 

   	 	}else if(name1 == "passEdit"){  		
    	var estilo=document.getElementById("passTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("passTextoEdit").innerHTML = texto1;

    	}else if(name1 == "dniEdit"){  		
    	var estilo=document.getElementById("dniTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("dniTextoEdit").innerHTML = texto1;

		}else if(name1 == "fechnacUserEdit"){  		
    	var estilo=document.getElementById("fechnacUserTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("fechnacUserTextoEdit").innerHTML = texto1;

        }else if(name1 == "sexoEdit"){  		
    	var estilo=document.getElementById("sexoTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("sexoTextoEdit").innerHTML = texto1;

   	 	}else if(name1 == "emailUserEdit"){  		
    	var estilo=document.getElementById("emailUserTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("emailUserTextoEdit").innerHTML = texto1; 

   	 	}else if(name1 == "nombreUserEdit"){  		
    	var estilo=document.getElementById("nombreUserTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("nombreUserTextoEdit").innerHTML = texto1;

   	 	}else if(name1 == "apellidosUserEdit"){  	
    	var estilo=document.getElementById("apellidosUserTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("apellidosUserTextoEdit").innerHTML = texto1; 

   	 	}else if(name1 == "telefonoUserEdit"){  		
    	var estilo=document.getElementById("telefonoUserTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("telefonoUserTextoEdit").innerHTML = texto1;

        }else if(name1 == "fotoUserEdit"){  		
    	var estilo=document.getElementById("fotoUserTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("fotoUserTextoEdit").innerHTML = texto1;  	 	

  	 	}else if(name1 == "fotoNombreUserEdit"){  		
    	var estilo=document.getElementById("fotoNombreUserTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("fotoNombreUserTextoEdit").innerHTML = texto1;

        }else if(name1 == "direccionUserEdit"){  		
    	var estilo=document.getElementById("direccionUserTextoEdit");
        estilo.style.color="#c92323";
        document.getElementById("direccionUserTextoEdit").innerHTML = texto1;
  	 	}



  	}

	
//encripta la password
        function encriptar(campo){
          //comprueba todos los campos del formulario devuelven true y son correctos.
        var resultado=false;//variable boolean que guarda el resultado de la función.
        //alert("dentro");
        if(campo.name=="registrarseform"){  //comprueba si viene del formulario registro
			var password=document.registrarseform.password;//input del campo password del formulario
			//alert(password.value);
			var passwordencriptada=hex_md5(password.value); //password encriptada
			 document.getElementById("password").value=passwordencriptada;  //introducimos la pass encriptada en el html password
			//alert(passwordencriptada);
			resultado=true;
        }else if (campo.name=="Loginform"){    //comprueba si viene del formulario login          
			var password=document.Loginform.password;//input del campo password del formulario
			var passwordencriptada=hex_md5(password.value); //password encriptada
			document.getElementById("password").value=passwordencriptada;   //introducimos la pass encriptada en el html password
			resultado=true;
			
		}else if (campo.name=="FormAdd"){    //comprueba si viene del formulario ADD         
			var password=document.Loginform.password;//input del campo password del formulario
			var passwordencriptada=hex_md5(password.value); //password encriptada
			document.getElementById("passAdd").value=passwordencriptada;   //introducimos la pass encriptada en el html password
			resultado=true;
			
		}else if (campo.name=="FormEdit"){    //comprueba si viene del formulario EDIT        
			var password=document.Loginform.password;//input del campo password del formulario
			var passwordencriptada=hex_md5(password.value); //password encriptada
			document.getElementById("passEdit").value=passwordencriptada;   //introducimos la pass encriptada en el html password
			resultado=true;
			}
         return resultado;
        }
/*La función comprobarText verifica que un campo texto*/
function comprobarText(campo,size) {
    if (campo.value.length>size) {//se comprueba si el campo supera la longitd permitida
        alert('Longitud incorrecta. El atributo ' + campo.name + ' debe ser maximo ' + size + ' y es ' + campo.value.length);
        campo.focus();
        return false;
    }
    return true;
}
/*La función comprobarInt verifica que un campo entero esté entre el mínimo y el máximo permitido*/
function comprobarInt(campo,size) {
    if (campo.value.length>size) {//se comprueba si el entero supera la longitud permitida
        valormaximo = (10 * size) -1;//variable máximo
        alert('Longitud incorrecta. El atributo ' + campo.name + 'debe ser maximo ' + valormaximo + ' y es ' + campo.value);
        campo.focus();
        return false;
    }
    return true;
}
/*La función esVacio verifica si un campo está o no vacío*/
function esVacio(campo){
    if ((campo.value == null) || (campo.value.length == 0)){//se comprueba si es nulo o su longitud es cero
        alert('El atributo ' + campo.name + ' no puede ser vacio');
        campo.focus();
        return false;
    }
    else{
        return true;
    }
}
/*La función comprobarAlfabetico verifica si un campo está o no vacío*/
function comprobarAlfabetico(campo) {
    var filter6=/^[A-Za-záéíóúÁÉÍÓÚ\_\-\.\s\xF1\xD1]+$/;//expresión regular con sólo letras
    if (!filter6.test(campo.value)){//se comprueba si tiene sólo letras
        alert("El campo "+campo.name+ " tiene que tener sólo letras");
        campo.focus();
        return false;
    }
    else{
        return true;
    }
}
/*La función comprobarIdTrabajo verifica que el IdTrabajo sea adecuado*/
function comprobarIdTrabajo(campo) {
    var ET=/^(ET|QA)[0-9]{1,4}/;//expresión regular para que sea de la forma ETn o QAn, siendo n un número
    if (!ET.test(campo.value)){//se si es de la forma adecuada
        alert("El campo "+campo.name+ " tiene que ser del tipo: ETn o QAn, siendo n un número entero positivo," +
            " además tiene que ser de longitud 6, no más.");
        campo.focus();
        return false;
    }
    else{
        return true;
    }

}
/*La función comprobarIdTrabajo verifica que el IdAccion sea adecuado*/
function comprobarIdAccion(campo) {
    var ET=/^(AC|ac)[0-9]{1,4}/;//expresión regular para que sea de la forma ACn o acn, siendo n un número
    if (!ET.test(campo.value)){//se comprueba si es de la forma adecuada
        alert("El campo "+campo.name+ " tiene que ser del tipo: ACn o acn, siendo n un número entero positivo," +
            " además tiene que ser de longitud 6, no más.");
        campo.focus();
        return false;
    }
    else{
        return true;
    }

}
//Función auxiliar de comprobarReal(campo, numerodecimales, valormenor, valormayor)
//Cuenta los decimales de un numero real
function contarDecimales(f) {
    var real = f.split("."); // Array formado por la parte entera y decimal
    var decimal = real[1]; // Array de la parte decimal
    if (decimal != null){ //Si tiene parte decimal
        return decimal.toString().length; //Convirtiendo a string se puede contar su longitud
    }
    else{
        return decimal=0; //No tiene decimales, asigna el valor 0 a la variable decimal
    }
}

//COMPROBAR REAL
//El número decimal puede empezar por punto. --> Ejemplo: .50
//O por un número seguido de un punto seguido de la parte decimal. --> Ejemplo: 23.56
//Comprueba que el número de decimales es correcto (numerodecimales) y por último comprueba que está dentro del rango (valormenor-valormayor)
//Acepta negativos
//Para probarlo pon lo siguiente en el campo loginSearch (linea 144): onblur="comprobarReal(this,2,-10,1000);"
function comprobarReal(campo, numerodecimales, valormenor, valormayor) {
    var resul = true;//variable booleana de retorno de la función
    var decimal=contarDecimales(campo.value);//cuenta decimales
    //Comprueba que es un número real
    if (((/^-?[0-9]*[.][0-9]+$/.test(campo.value))==false) ){
        alert('El valor ' + campo.name + ' no es un número decimal, debe introducir un número, ' +
            'luego un punto, y finalmente más números');
        resul = false;
        //Comprueba el número de decimales
    }else if (decimal>numerodecimales){
        alert('Número de decimales incorrecto, maximo: ' +numerodecimales);
        resul = false;
        //Comprueba que el nº introducido está dentro del rango
    }else if ((campo.value < valormenor) || (campo.value > valormayor)){
        alert('El atributo ' + campo.name + ' tiene que estar entre ' + valormenor + ' y ' + valormayor);
        resul = false;
    }
    else{
        resul=true;
    }
    return resul;
}
/*La función validate verifica que la fecha de fin de un trabajo sea mayor que la del inicio de este*/
function validate() {
    inicio=document.getElementById('FechaIniTrabajo').value;//captura la fecha de inicio
    fin=document.getElementById('FechaFinTrabajo').value;//captura la fecha de fin
    if(fin<=inicio) {//si la fecha de fin es menor o igual que la de inicio
        alert("Fecha de fin del trabajo incorrecta, debe introducir una fecha de fin superior a la fecha de inicio");
        return false;
    }else{
    	return true;
	}
}
/*function compararFechas($primera, $segunda)
{
    $valoresPrimera = explode ("/", $primera);
    $valoresSegunda = explode ("/", $segunda);
    $diaPrimera    = $valoresPrimera[0];
    $mesPrimera  = $valoresPrimera[1];
    $anyoPrimera   = $valoresPrimera[2];
    $diaSegunda   = $valoresSegunda[0];
    $mesSegunda = $valoresSegunda[1];
    $anyoSegunda  = $valoresSegunda[2];
    $diasPrimeraJuliano = gregoriantojd($mesPrimera, $diaPrimera, $anyoPrimera);
    $diasSegundaJuliano = gregoriantojd($mesSegunda, $diaSegunda, $anyoSegunda);
    if(!checkdate($mesPrimera, $diaPrimera, $anyoPrimera)){
        // "La fecha ".$primera." no es válida";
        return 0;
    }else
    if(!checkdate($mesSegunda, $diaSegunda, $anyoSegunda)){
        // "La fecha ".$segunda." no es válida";
        return 0;
    }else{
        return  $diasPrimeraJuliano - $diasSegundaJuliano;
    }
}*/
/*La función comprobar_ACCIONES verifica que todos los campos de una acción sean correctos
en el submit del formulario add de acción*/
function comprobar_ACCIONES(){
    return (esVacio(FormAdd.IdAccion)
        && comprobarText(FormAdd.IdAccion, 6)
        && esVacio(FormAdd.NombreAccion)
        && comprobarAlfabetico(FormAdd.NombreAccion)
        && comprobarText(FormAdd.NombreAccion, 60)
        && esVacio(FormAdd.DescripAccion)
        && comprobarText(FormAdd.DescripAccion, 100)
        && comprobarAlfabetico(FormAdd.DescripAccion)
        && comprobarIdAccion(FormAdd.IdAccion)
    )
}
/*La función comprobar_ACCIONES2 verifica que todos los campos de una acción sean correctos
 en el submit del formulario edit de acción*/
function comprobar_ACCIONES2(){
    return (
         esVacio(FormEdit.NombreAccion)
        && comprobarAlfabetico(FormEdit.NombreAccion)
        && comprobarText(FormEdit.NombreAccion, 60)
        && esVacio(FormEdit.DescripAccion)
        && comprobarText(FormEdit.DescripAccion, 100)
        && comprobarAlfabetico(FormEdit.DescripAccion)
    )
}
/*La función comprobar_TRABAJOS verifica que todos los campos de un trabajo sean correctos
 en el submit del formulario add de trabajo*/
function comprobar_TRABAJOS(){
    return (esVacio(FormAdd.IdTrabajo)
        && comprobarIdTrabajo(FormAdd.IdTrabajo)
        && esVacio(FormAdd.NombreTrabajo)
        && comprobarText(FormAdd.NombreTrabajo, 60)
        && comprobarAlfabetico(FormAdd.NombreTrabajo)
        && esVacio(FormAdd.FechaIniTrabajo)
        && esVacio(FormAdd.FechaFinTrabajo)
        && esVacio(FormAdd.PorcentajeNota)
        && validate()
        && comprobarReal(FormAdd.PorcentajeNota,2,0,100)
    )
}
/*La función comprobar_TRABAJOS2 verifica que todos los campos de un trabajo sean correctos
 en el submit del formulario edit de trabajo*/
function comprobar_TRABAJOS2(){
    return (
         esVacio(FormEdit.NombreTrabajo)
        && comprobarText(FormEdit.NombreTrabajo, 60)
        && comprobarAlfabetico(FormEdit.NombreTrabajo)
        && esVacio(FormEdit.FechaIniTrabajo)
        && esVacio(FormEdit.FechaFinTrabajo)
        && esVacio(FormEdit.PorcentajeNota)
        && validate()
        && comprobarReal(FormEdit.PorcentajeNota,2,0,100)
    )
}



</script>