<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Esta es la función comprobarIdioma, por lo tanto
 * controla el idioma de la aplicación.
 */
function comprobaridioma($idioma){

$idiom=$idioma->español();

if (isset($_SESSION['idioma'])){
    if($_SESSION['idioma']=="español"){
        $idiom=$idioma->español();
    }elseif($_SESSION['idioma']=="ingles"){
       $idiom=$idioma->ingles();    
    }elseif($_SESSION['idioma']=="gallego"){
       $idiom=$idioma->gallego();
    }
}
return $idiom;
}
?>