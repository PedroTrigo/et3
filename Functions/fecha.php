<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Esta función dá la fecha exacta del sistema.
 */

function fecha(){
    $toret= getdate();
    
    $fecha=array(
        "dia"=>$toret["mday"],
        "mes"=>$toret["mon"],
        "año"=>$toret["year"],
        "hora"=>$toret["hours"],
        "minuto"=>$toret["minutes"],
        "segundo"=>$toret["seconds"]
        );
    return $fecha;
}

