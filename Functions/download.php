<?php
/**
 * Autor: Shopify
 * Fecha de creación: 06/12/2017
 * Función: Esta es la función que recibe la ruta de un archivo y los descarga.
 */
$Ruta=$_GET['Ruta'];

header("Content-disposition: attachment; filename=$Ruta");
header("Content-type: application/octet-stream");
readfile("$Ruta");
?>